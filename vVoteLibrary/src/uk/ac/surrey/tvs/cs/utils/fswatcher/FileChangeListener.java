/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.fswatcher;

import java.io.File;

/**
 * Interface for receiver FileChangeEvents from a FileSystemWatcher
 * 
 * @author Chris Culnane
 *
 */
public interface FileChangeListener {

  /**
   * Fired when a watched folder changes, the exact file that has changed is in filePath
   * @param filePath File that points to the changed file
   */
  public void fileChanged(File filePath);

}
