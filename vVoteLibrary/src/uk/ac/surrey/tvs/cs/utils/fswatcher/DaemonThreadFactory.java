/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.fswatcher;

import java.util.concurrent.ThreadFactory;

/**
 * A simple thread factory that creates daemon threads. This is required to allow the creation of daemon threads in an executor
 * service.
 * 
 * @author Chris Culnane
 * 
 */
public class DaemonThreadFactory implements ThreadFactory {

  /**
   * String variable to store the thread name for all threads created by this factory - if null no name is explicitly given
   */
  private String threadName = null;

  /**
   * Default constructor for a DaemonThreadFactory - creates threads without an explicity name and sets them to be daemon threads.
   */
  public DaemonThreadFactory() {
  }

  /**
   * Constructor to create a new DaemonThreadFactory that will apply the specified name to all created threads and set thread to be
   * daemon threads
   * 
   * @param threadName
   *          String of the thread name to use
   */
  public DaemonThreadFactory(String threadName) {
    this.threadName = threadName;
  }

  /**
   * Creates new threads for the specified runnable object
   * 
   * @param r
   *          Runnable to create a thread for
   * @return Thread containing the runnable
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.util.concurrent.ThreadFactory#newThread(java.lang.Runnable)
   */
  @Override
  public Thread newThread(Runnable r) {
    Thread newThread;
    if (this.threadName != null) {
      newThread = new Thread(r, this.threadName);
    }
    else {
      newThread = new Thread(r);
    }
    newThread.setDaemon(true);
    return newThread;

  }

}
