/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.fswatcher;

/**
 * 
 * Exception that occurs when something goes wrong during a FileSystemWatcher run or preparation
 * 
 * @author Chris Culnane
 * 
 */
public class FileWatcherException extends Exception {

  /**
   * Required for inherited serialisation.
   */
  private static final long serialVersionUID = -4198575479719013376L;

  /**
   * Constructs a new exception with {@code null} as its detail message.
   */
  public FileWatcherException() {
    
  }

  /**
   * Constructs a new exception with the specified detail message.
   * 
   * @param message
   *          the detail message.
   */
  public FileWatcherException(String message) {
    super(message);
    
  }

  /**
   * Constructs a new exception with the specified detail message and cause.
   * <p>
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically incorporated in this exception's detail
   * message.
   * 
   * @param message
   *          the detail message.
   * @param cause
   *          the cause. A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.
   */
  public FileWatcherException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructs a new exception with the specified detail message, cause, suppression enabled or disabled, and writable stack trace
   * enabled or disabled.
   * 
   * @param message
   *          the detail message.
   * @param cause
   *          the cause. (A {@code null} value is permitted, and indicates that the cause is nonexistent or unknown.)
   * @param enableSuppression
   *          whether or not suppression is enabled or disabled
   * @param writableStackTrace
   *          whether or not the stack trace should be writable
   */
  public FileWatcherException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  /**
   * Constructs a new exception with the specified cause and a detail message.
   * 
   * @param cause
   *          the cause. A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.
   */
  public FileWatcherException(Throwable cause) {
    super(cause);
  }

}
