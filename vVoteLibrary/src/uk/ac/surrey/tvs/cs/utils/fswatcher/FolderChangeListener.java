/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.fswatcher;

import java.io.File;

/**
 * Interface for registering for Folder change events with a FileSystemWatcher
 * 
 * @author Chris Culnane
 * 
 */
public interface FolderChangeListener {

  /**
   * Fired when a change takes place within a watched folder. Will specify the individual file object that has changed and the type
   * of change it was
   * 
   * @param filePath
   *          File that points to the changed file
   * @param type
   *          FileChangeType that specifies the type of change that was detected
   */
  public void folderChanged(File filePath, FileChangeType type);
}
