/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.fswatcher;

/**
 * Enum to reflect the different type of file change events
 * 
 * @author Chris Culnane
 * 
 */
public enum FileChangeType {
  CREATED, MODIFIED, DELETED, UNKNOWN
}
