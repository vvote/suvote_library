/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.crls;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Simple filename filter that filters to only files that have the extension .crl. This check is not case sensitive.
 * 
 * @author Chris Culnane
 * 
 */
public class CRLFileNameFilter implements FilenameFilter {

  /**
   * Default constructor for the CRLFileNameFilter
   */
  public CRLFileNameFilter() {

  }

  /*
   * (non-Javadoc)
   * 
   * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
   */
  @Override
  public boolean accept(File dir, String name) {
    if (name.toLowerCase().endsWith(".crl")) {
      return true;
    }
    else {
      return false;
    }

  }

}
