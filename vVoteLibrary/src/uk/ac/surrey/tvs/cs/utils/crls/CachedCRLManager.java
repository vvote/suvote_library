/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.tvs.cs.utils.crls;

import java.io.File;
import java.io.IOException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.tvs.cs.utils.fswatcher.FileSystemWatcher;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileWatcherException;

/**
 * Provides a wrapper around a FileSystemWatcher to both initially load and monitor a series of CRLs. If the CRL file is altered,
 * for example, if it is replaced with an updated copy, the FileSystemWatcher will fire an event and the cached CRL will be updated
 * so that future connections will use that CRL instead of the older one.
 * 
 * @author Chris Culnane
 * 
 */
public class CachedCRLManager {

  /**
   * Reference to the FileSystemWatcher to use for watching the CRL folder
   */
  private FileSystemWatcher   folderWatcher;
  /**
   * CertificateFactory to use for loading certificates
   */
  private CertificateFactory  certFact;

  /**
   * Folder where the CRLs are
   */
  private File                baseCRLs;

  /**
   * ArrayList of CRLS
   */
  private ArrayList<CRL>      crls          = new ArrayList<CRL>();

  /**
   * Boolean of whether this cache has been initialised
   */
  private boolean             isInitialised = false;

  /**
   * Logger
   */
  private static final Logger logger        = LoggerFactory.getLogger(CachedCRLManager.class);

  /**
   * Constructs a new CachedCRLManager where the crls are located in the crlFolder
   * 
   * Having constructed a CachedCRLManager startWatching must be called to commence file monitoring, otherwise no changes will be
   * detected. On completion it is the callers responsibility to call shutdown to release the file system resources
   * 
   * @param crlFolder
   *          String path to the CRL folder to watch and load CRLs from
   * @throws IOException
   * @throws CertificateException
   * @throws CRLException
   * @throws FileWatcherException
   */
  public CachedCRLManager(String crlFolder) throws IOException, CertificateException, CRLException, FileWatcherException {
    logger.info("Creating new CRLManager for CRLFolder: {}", crlFolder);
    this.baseCRLs = new File(crlFolder);
    this.certFact = CertificateFactory.getInstance("X.509");
    this.folderWatcher = new FileSystemWatcher(crlFolder);
  }

  /**
   * Gets the Collection of CRL objects that will be used by the SSLServerSocket for connection checking
   * 
   * @return Collection<CRL> of CRL objects
   */
  public Collection<CRL> getCRLCollection() {
    if (!this.isInitialised) {
      this.init();
    }
    return this.crls;
  }

  /**
   * Initialises the CachedCRLManager by loading the initial CRL files and setting FileSystemWatchers on them
   */
  private void init() {
    logger.info("Initialising CRLManager");
    File[] initCRLs = this.baseCRLs.listFiles(new CRLFileNameFilter());
    for (File crl : initCRLs) {
      try {
        logger.info("Adding base CRL: {}", crl.getAbsolutePath());
        this.crls.add(new CachedX509CRL(crl, this.certFact, this.folderWatcher));
      }
      catch (CachedX509CRLException e) {
        logger.warn("Exception loading CRL {}, will try loading remaining CRLs", crl);
      }
    }
    this.isInitialised = true;
  }

  /**
   * Shuts down the CachedCRLManager - this is important since the native FileSystemWatcher needs to be stopped otherwise resources
   * could be leaked.
   * 
   * @throws FileWatcherException
   */
  public void shutdown() throws FileWatcherException {
    logger.info("Stopping Folder Watcher");
    this.folderWatcher.stopWatching();
  }

  /**
   * Starts the FileSystemWatcher to begin monitoring the folder. This is outside of the constructor, and as such requires an
   * explicit call. If the CachedCRLManager has not already been initialised the initialisation will be performed before commening
   * watching files.
   * 
   * @throws FileWatcherException
   */
  public void startWatching() throws FileWatcherException {
    if (!this.isInitialised) {
      this.init();
    }
    logger.info("Starting Folder Watcher");
    this.folderWatcher.startWatching();
  }

}
