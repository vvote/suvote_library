/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import org.json.JSONArray;

/**
 * Wrapper class for responding to a request to send a message to the Private WBB (MBB). This is used to hold the JSONArray and
 * whether it is has been pre-checked for validity.
 * 
 * @author Chris Culnane
 * 
 */
public class MBBResponse {

  /**
   * boolean if true indicates the response array has already been checked - early response
   */
  private boolean   isThresholdChecked = false;
  
  /**
   * JSONArray of responses from Peers, may be all responses or in the case of an early response only a threshold of valid responses
   */
  private JSONArray responseArray;

  /**
   * Holds a threshold valid data items
   */
  private String dataItem=null;

  /**
   * Constructs a new MBBResponse with a JSONArray of responses and explicitly stating whether the response array has already been checked.
   * @param responseArray JSONArray of peer responses
   * @param isThresholdChecked boolean true if responses checked, false if not
   */
  public MBBResponse(JSONArray responseArray, boolean isThresholdChecked) {
    this.responseArray = responseArray;
    this.isThresholdChecked = isThresholdChecked;
  }

  public MBBResponse(JSONArray responseArray, boolean isThresholdChecked, String dataItem) {
    this.responseArray = responseArray;
    this.isThresholdChecked = isThresholdChecked;
    this.dataItem =dataItem;
  }
  /**
   * Construct a new MBBResponse with a JSONArray of responses, defaults the isThresholdChecked to false
   * @param responseArray JSONArray of responses from peers
   */
  public MBBResponse(JSONArray responseArray) {
    this.responseArray = responseArray;
    this.isThresholdChecked = false;
  }

  /**
   * Checks whether the responses in the JSONArray have already been checked or not
   * @return true if they have been checked, false if not
   */
  public boolean isThresholdChecked() {
    return this.isThresholdChecked;
  }

  /**
   * Gets the actual JSONArray of responses from the peers
   * @return JSONArray of peer responses
   */
  public JSONArray getResponseArray() {
    return this.responseArray;
  }
  
  /**
   * Gets the associated data item that would have been set if a threshold of valid responses was received
   * @return String containing the dataitem
   */
  public String getDataItem(){
    return this.dataItem;
  }

}
