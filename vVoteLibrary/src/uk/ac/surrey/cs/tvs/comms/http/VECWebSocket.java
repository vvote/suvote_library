/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.http;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Wrapper for WebSocketServer that we use to send progress updates to an HTML based front-end.
 * 
 * @author Chris Culnane
 * 
 */
public class VECWebSocket extends WebSocketServer {

  /**
   * Logger
   */
  private static final Logger             logger              = LoggerFactory.getLogger(VECWebSocket.class);

  /**
   * Constant of the maximum time we will wait for a web socket to initialise when waiting to get the assigned port number
   */
  private static int                      MAX_WAIT_FOR_PORT   = 5000;

  /**
   * Timeout between checks of whether the port has been set, should be very small we expect it to happen almost instantly
   */
  private static int                      WAIT_FOR_PORT_SLEEP = 10;

  /**
   * JSON field name for port value
   */
  private static String                   PORT_FIELD          = "port";

  /**
   * Web socket listeners.
   */
  private ArrayList<VECWebSocketListener> listeners           = new ArrayList<VECWebSocketListener>();

  /**
   * Creates new VECWebSocket that listens on the specified address
   * 
   * @param address
   *          to listen on
   */
  public VECWebSocket(InetSocketAddress address) {
    super(address);

    logger.info("WebSocket created on {}", address.toString());
  }

  /**
   * Adds a web socket listener.
   * 
   * @param listener
   *          The listener to add.
   */
  public void addWebSocketListener(VECWebSocketListener listener) {
    this.listeners.add(listener);
  }

  /**
   * Called when the WebSocket is closed.
   * 
   * @param ws
   *          WebSocket we are using
   * @param code
   *          Close code
   * @param msg
   *          Reason for closing
   * @param remote
   *          Closed by the remote side?
   * 
   * @see org.java_websocket.server.WebSocketServer#onClose(org.java_websocket.WebSocket, int, java.lang.String, boolean)
   */
  @Override
  public void onClose(WebSocket ws, int code, String msg, boolean remote) {
    logger.info("WebSocket closed Code:{}, Message:{}, Remote:{}", code, msg, remote);
  }

  /**
   * Called when a WebSocket error occurss.
   * 
   * @param ws
   *          WebSocket we are using
   * @param webSocketException
   *          The exception being raised.
   * 
   * @see org.java_websocket.server.WebSocketServer#onError(org.java_websocket.WebSocket, java.lang.Exception)
   */
  @Override
  public void onError(WebSocket ws, Exception webSocketException) {
    logger.info("WebSocket error", webSocketException);
  }

  /**
   * Called when a message is received on the underlying WebSocket.
   * 
   * @param ws
   *          WebSocket we are using
   * @param msg
   *          The received message
   * 
   * @see org.java_websocket.server.WebSocketServer#onMessage(org.java_websocket.WebSocket, java.lang.String)
   */
  @Override
  public void onMessage(WebSocket ws, String msg) {
    // See if we can construct a JSONObject from it and if we can process it
    try {
      JSONObject jmsg = new JSONObject(msg);
      this.processMessage(jmsg, ws);
    }
    catch (JSONException e) {
      logger.warn("JSONException whilst parsing and processing message - will ignore", e);
    }
  }

  /**
   * Called when the WebSocket is opened.
   * 
   * @param ws
   *          WebSocket we are using
   * @param clientHS
   *          The client handshake
   * 
   * @see org.java_websocket.server.WebSocketServer#onOpen(org.java_websocket.WebSocket,
   *      org.java_websocket.handshake.ClientHandshake)
   */
  @Override
  public void onOpen(WebSocket ws, ClientHandshake clientHS) {
    logger.info("WebSocket open from {}", ws.getRemoteSocketAddress());
  }

  /**
   * Processes an incoming message and acts upon it
   * 
   * @param msg
   *          JSONObject containing the message
   * @param ws
   *          WebSocket we are using
   */
  private void processMessage(JSONObject msg, WebSocket ws) {
    for (VECWebSocketListener listener : this.listeners) {
      listener.processMessage(msg, ws);
    }
  }

  /**
   * Removes a web socket listener.
   * 
   * @param listener
   *          The listener to remove.
   */
  public void removeWebSocketListener(VECWebSocketListener listener) {
    this.listeners.remove(listener);
  }

  /**
   * Method to get the automatically assigned port number and save it into the specified JSON port configuration file. This is
   * required because when using ephemeral ports they are not assigned until the socket has started. As such, we have to wait for
   * the socket to finish initialising. There is no built in method to achieve this, since the port is initialising on a separate
   * thread anyway. When calling getPort it will return 0 until a port has been set. This calls getPort until it receives a non zero
   * value and then save that as a JSONObject into the specified file.
   * 
   * @param portConfPath
   *          File to save JSONObject containing port number
   * @throws VECWebSocketException
   * @throws JSONException
   * @throws JSONIOException
   */
  public void waitForStartAndUpdatePort(File portConfPath) throws VECWebSocketException, JSONException, JSONIOException {
    int totalWait = 0;

    while (this.getPort() <= 0 ) {
      try {
        Thread.sleep(VECWebSocket.WAIT_FOR_PORT_SLEEP);

        totalWait = totalWait + VECWebSocket.WAIT_FOR_PORT_SLEEP;
        if (totalWait >= VECWebSocket.MAX_WAIT_FOR_PORT) {
          throw new VECWebSocketException("Timed out waiting for VECWebSocket to start");
        }
      }
      catch (InterruptedException e) {
        throw new VECWebSocketException("Interrupted whilst waiting for WebSocket to start", e);
      }
    }

    logger.info("WebSocket listening on {}", this.getPort());
    JSONObject port = new JSONObject();
    port.put(VECWebSocket.PORT_FIELD, this.getPort());
    IOUtils.writeJSONToFile(port, portConfPath.getAbsolutePath());
  }

  /**
   * Method to get the automatically assigned port number and save it into the specified JSON port configuration file. This is
   * required because when using ephemeral ports they are not assigned until the socket has started. As such, we have to wait for
   * the socket to finish initialising. There is no built in method to achieve this, since the port is initialising on a separate
   * thread anyway. When calling getPort it will return 0 until a port has been set. This calls getPort until it receives a non zero
   * value and then save that as a JSONObject into the specified file.
   * 
   * @param portConfPath
   *          String path of file to save JSONObject containing port number
   * @throws VECWebSocketException
   * @throws JSONException
   * @throws JSONIOException
   */
  public void waitForStartAndUpdatePort(String portConfPath) throws VECWebSocketException, JSONException, JSONIOException {
    this.waitForStartAndUpdatePort(new File(portConfPath));
  }
}
