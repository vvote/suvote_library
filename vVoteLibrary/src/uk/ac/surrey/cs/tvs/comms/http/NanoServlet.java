/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.http;

import java.io.File;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Interface that defines a NanoServlet that can be used with a modified NanoHTTPD and SimpleWebServer to provide simple Java
 * servlet functionality
 * 
 * @author Chris Culnane
 * 
 */
public interface NanoServlet {

  /**
   * Runs the servlet with the following parameters
   * 
   * @param uri
   *          String of the requested uri
   * @param method
   *          Method that the request was sent using
   * @param header
   *          Map of header values
   * @param params
   *          Map of parameter values
   * @param files
   *          Map of files
   * @param homeDir
   *          File of homeDir
   * @return Response object
   */
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir);
}
