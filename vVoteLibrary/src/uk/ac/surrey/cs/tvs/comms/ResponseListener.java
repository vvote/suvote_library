/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

/**
 * Interface for a ResponseListener that is notified when a response is received from an MBB Peer
 * 
 * @author Chris Culnane
 * 
 */
public interface ResponseListener {

  /**
   * Called when a response is received from the waiting peer
   * 
   * @param response
   *          String of JSONObject that contains the response
   */
  public void responseFromWait(String response);
}
