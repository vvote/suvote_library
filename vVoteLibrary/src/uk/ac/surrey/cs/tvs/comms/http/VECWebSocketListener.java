/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.http;

import org.java_websocket.WebSocket;
import org.json.JSONObject;

/**
 * Listener for VECWebSocket
 * 
 * @author Chris Culnane
 * 
 */
public interface VECWebSocketListener {

  /**
   * Fired whenever a message is received on the WebSocket - the message must conform to being a JSONObject
   * 
   * @param message
   *          JSONObject of message
   * @param ws
   *          WebSocket it was received on
   */
  public void processMessage(JSONObject message, WebSocket ws);
}
