/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;

/**
 * FieldResponseChecker provides a more advanced ResponseChecker implementation. It is for situations where additional data is
 * returned by the MBB and must be added into the signatures being checked. A good example of such a situation is when the
 * commitTime is returned on a voting message.
 * 
 * The certificates associated with the responses should all be contained within a single TVSKeyStore and be accessible with the
 * same suffix. This fits with the standard PrivateWBB peer implementation.
 * 
 * @author Chris Culnane
 * 
 */
public class FieldResponseChecker implements ResponseChecker {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(FieldResponseChecker.class);
  /**
   * TVSKeyStore that holds the public keys for peers sending the responses
   */
  private TVSKeyStore         keyStore;

  /**
   * String holding the data to place in the signature before the message field data
   */
  private String              predata;
  /**
   * String holding the data to place in the signature after the message field data
   */
  private String              postdata;

  /**
   * Suffix for the peerID in the TVSKeyStore
   */
  private String              suffix;

  /**
   * String fieldname to get data from the message from
   */
  private String              fieldName;

  /**
   * Constructs a FieldResponseChecker with the specified TVSKeyStore, String Suffix and signature data. The predata and postdata
   * parameters can be used to specify the data to add into the signature before and after the value to be obtained from the
   * response. If the field data is at the start or end of the data going into the signature set the pre or post data parameters to
   * an empty, as appropriate.
   * 
   * @param keyStore
   *          TVSKeyStore containing certificates for all peer responses
   * @param suffix
   *          String suffix for PeerID certificates in the TVSKeyStore
   * @param predata
   *          String of the data to place in the signature before the field data
   * @param postdata
   *          String of the data to place in the signature after the field data
   * @param fieldName
   *          String of the fieldName to check (normally commitTime)
   */
  public FieldResponseChecker(TVSKeyStore keyStore, String suffix, String predata, String postdata, String fieldName) {
    this.keyStore = keyStore;
    this.predata = predata;
    this.postdata = postdata;
    this.suffix = suffix;
    this.fieldName = fieldName;
  }

  /**
   * Performs the actual processing associated with checking a response, taking the JSONObject received as the response. The actual
   * processing is dependent on the type of expected response. In the case of the FieldResponseChecker it is a signature check with
   * the relevant field value added into the signature object.
   * 
   * @param response
   *          JSONObject of the response received
   * @return ResponseCheckerResult response checker result determining if the response is valid or not
   */
  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.comms.ResponseChecker#checkEarlyResponse(org.json.JSONObject)
   */
  @Override
  public ResponseCheckerResult checkEarlyResponse(JSONObject response) {
    try {
      // Gets the peerID from the response
      String peerID = response.getString(JSONConstants.Signature.SIGNER_ID);
      // Create a signature and update
      TVSSignature sig = new TVSSignature(SignatureType.BLS, keyStore.getBLSCertificate(peerID + this.suffix));
      sig.setPartial(true);

      // add predata
      sig.update(this.predata);

      // Add field data
      String data = null;
      if (response.has(this.fieldName)) {
        data = response.getString(this.fieldName);
        sig.update(data);
      }

      // add post data
      sig.update(this.postdata);
      // If it is valid create a suitable ResponseCheckerResult
      if (sig.verify(response.getString(JSONConstants.Signature.SIGNATURE), EncodingType.BASE64)) {
        return new ResponseCheckerResult(data, true);
      }
      else {
        return new ResponseCheckerResult(data, false);
      }
    }
    catch (JSONException e) {
      logger.warn("JSONException when reading response, will return invalid", e);
      return new ResponseCheckerResult(false);
    }
    catch (TVSKeyStoreException e) {
      logger.warn("CryptoException whilst checking response, will return invalid", e);
      return new ResponseCheckerResult(false);
    }
    catch (TVSSignatureException e) {
      logger.warn("CryptoException whilst checking response, will return invalid", e);
      return new ResponseCheckerResult(false);
    }
  }
}
