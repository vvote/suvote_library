/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Response worker runs in the background listening for a response on a socket. This needs to be run on a separate thread, otherwise
 * if the receiver sends an error message and closes the connection, before the sender finished sending, the response will be lost.
 * 
 * @author Chris Culnane
 * 
 */
public class ResponseWorker implements Runnable {

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(ResponseWorker.class);

  /**
   * Underlying input stream from the socket
   */
  private InputStream         is;

  /**
   * String to store the response in
   */
  private String              response    = null;

  /**
   * Boolean to record whether we have received a response
   */
  private boolean             responseSet = false;

  /**
   * Constructs a new ResponseWorker
   * 
   * @param is
   *          InputStream to listen to
   */
  public ResponseWorker(InputStream is) {
    super();

    this.is = is;
  }

  /**
   * Gets the response that was received or null
   * 
   * @return String of the response or null if no response was received
   */
  public String getResponse() {
    return this.response;
  }

  /**
   * Checks if we received a response
   * 
   * @return boolean, true if we received a response, false it not
   */
  public boolean gotResponse() {
    return this.responseSet;
  }

  /**
   * Waits for a response to be received on the input stream and then ends
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    BufferedReader br = null;

    try {
      // We will wait for a response - all responses end with a newline, so create a reader and call readLine
      br = new BufferedReader(new InputStreamReader(this.is));
      this.response = br.readLine();
      this.responseSet = true;
      logger.info("Received response:{} at {}", this.response, System.currentTimeMillis());
    }
    catch (IOException e) {
      logger.warn("Exception waiting for response", e);
    }
    finally {
      if (br != null) {
        try {
          br.close();
        }
        catch (IOException e) {
          logger.warn("Exception whilst closing reader");
        }
      }
    }
  }
}
