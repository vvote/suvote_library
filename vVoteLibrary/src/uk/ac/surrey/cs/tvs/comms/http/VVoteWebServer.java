/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.http;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import fi.iki.elonen.SimpleWebServer;

/**
 * VVoteWebServer extends the SimpleWebServer to provide the functionality to handle servlets. Provides an extension of the serve
 * method to detect when servlets are called and then calls the servlet.
 * 
 * @author Chris Culnane
 * 
 */
public class VVoteWebServer extends SimpleWebServer {

  /**
   * Logger
   */
  private static final Logger      logger            = LoggerFactory.getLogger(VVoteWebServer.class);

  /**
   * Common mime type for JSON content
   */
  public static final String       MIME_JSON         = "application/json";

  /**
   * String constant to look for in the URI to signify the call is to a servlet
   */
  private static final String      SERVLET_EXT       = "/servlet/";

  /**
   * Char that appears in the URI to indicate the start of the query string
   */
  private static final char        QUERY_STRING_CHAR = '?';

  /**
   * Hashmap of servlets
   */
  private Map<String, NanoServlet> servlets;

  /**
   * File pointing to root directory to serve
   */
  private File                     wwwroot;

  /**
   * Update constructor to create a simple web server with Servlets.
   * 
   * @param host
   *          String host
   * @param port
   *          int port
   * @param wwwroot
   *          root directory to server
   * @param servlets
   *          Map of servlets - can be empty
   */
  public VVoteWebServer(String host, int port, File wwwroot, Map<String, NanoServlet> servlets) {
    super(host, port, wwwroot, true);
    logger.info("Listenting on {}:{}, with root {}",host,port,wwwroot);
    this.servlets = servlets;
    this.wwwroot = wwwroot;
  }

  /**
   * Overrides the serve method to handle serving of the servlets, otherwise calls the serve method from the super class.
   * 
   * @param session
   *          IHTTPSession representing the current session
   * 
   * @return Response response to send to the client
   * 
   * @see fi.iki.elonen.SimpleWebServer#serve(fi.iki.elonen.NanoHTTPD.IHTTPSession)
   */
  @Override
  public Response serve(IHTTPSession session) {
    Map<String, String> files = new HashMap<String, String>();
    Method method = session.getMethod();

    if (Method.PUT.equals(method) || Method.POST.equals(method)) {
      try {
        session.parseBody(files);
      }
      catch (IOException e) {
        return new Response(Response.Status.INTERNAL_ERROR, MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + e.getMessage());
      }
      catch (ResponseException e) {
        return new Response(e.getStatus(), MIME_PLAINTEXT, e.getMessage());
      }
    }

    Map<String, String> header = session.getHeaders();
    Map<String, String> parms = session.getParms();
    String uri = session.getUri();

    if (uri.startsWith(SERVLET_EXT)) {
//      logger.info("Received request {}", uri);
      String servletName = uri.trim().replace(File.separatorChar, '/');
      servletName = servletName.substring(SERVLET_EXT.length());

      if (servletName.indexOf(QUERY_STRING_CHAR) >= 0) {
        servletName = servletName.substring(0, servletName.indexOf(QUERY_STRING_CHAR));
      }

      if (this.servlets != null && this.servlets.containsKey(servletName)) {
 //       logger.info("Running servlet {}", servletName);
        NanoServlet ns = this.servlets.get(servletName);

        return ns.runServlet(uri, session.getMethod(), header, parms, files, this.wwwroot);
      }
      else {
        logger.warn("Servlet {} was not found", servletName);
        return new Response(Response.Status.NOT_FOUND, VVoteNanoHTTPD.MIME_PLAINTEXT, "Error 404, Servlet not found.");
      }
    }

    return super.serve(session);
  }
}
