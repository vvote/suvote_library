/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;

/**
 * SimpleResponseChecker provides the most basic of ResponseChecker implementations. It is for situations where the contents of the
 * response signature is known in advance and can therefore be preset. The certificates associated with the responses should all be
 * contained within a single TVSKeyStore and be accessible with the same suffix. This fits with the standard PrivateWBB peer
 * implementation.
 * 
 * @author Chris Culnane
 * 
 */
public class SimpleResponseChecker implements ResponseChecker {
  /**
   * Logger
   */
  private static final Logger logger             = LoggerFactory.getLogger(SimpleResponseChecker.class);
  /**
   * TVSKeyStore that holds the public keys for peers sending the responses
   */
  private TVSKeyStore keyStore;

  /**
   * String holding the data in the signature
   */
  private String      data;

  /**
   * Suffix for the peerID in the TVSKeyStore
   */
  private String      suffix;

  /**
   * Constructs a SimpleResponseChecker with the specified TVSKeyStore, String Suffix and String data.
   * 
   * @param keyStore
   *          TVSKeyStore containing certificates for all peer responses
   * @param suffix
   *          String suffix for PeerID certificates in the TVSKeyStore
   * @param data
   *          String of the signature data
   */
  public SimpleResponseChecker(TVSKeyStore keyStore, String suffix, String data) {
    this.keyStore = keyStore;
    this.data = data;
    this.suffix = suffix;
  }

  /**
   * Performs the actual processing associated with checking a response, taking the JSONObject received as the response. The actual
   * processing is dependent on the type of expected response. In the case of the SimpleResponseChecker it is just a signature check
   * on the preset data from the constructor.
   * @param response JSONObject of the response received
   * @return ResponseCheckerResult response checker result determining if the response is valid or not
   */
  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.surrey.cs.tvs.comms.ResponseChecker#checkEarlyResponse(org.json.JSONObject)
   */
  @Override
  public ResponseCheckerResult checkEarlyResponse(JSONObject response) {
    try {
      //Gets the peerID from the response
      String peerID = response.getString(JSONConstants.Signature.SIGNER_ID);
      //Create a signature and update
      TVSSignature sig = new TVSSignature(SignatureType.BLS, keyStore.getBLSCertificate(peerID + this.suffix));
      sig.setPartial(true);
      sig.update(this.data);
      //If it is valid create a suitable ResponseCheckerResult
      if (sig.verify(response.getString(JSONConstants.Signature.SIGNATURE), EncodingType.BASE64)) {
        return new ResponseCheckerResult(true);
      }
      else {
        return new ResponseCheckerResult(false);
      }
    }
    catch (JSONException e) {
      logger.warn("JSONException when reading response, will return invalid",e);
      return new ResponseCheckerResult(false);
    }
    catch (TVSKeyStoreException e) {
      logger.warn("CryptoException whilst checking response, will return invalid",e);
      return new ResponseCheckerResult(false);
    }
    catch (TVSSignatureException e) {
      logger.warn("CryptoException whilst checking response, will return invalid",e);
      return new ResponseCheckerResult(false);
    }
  }
}
