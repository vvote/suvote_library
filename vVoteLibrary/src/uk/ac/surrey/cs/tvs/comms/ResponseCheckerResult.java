/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

/**
 * ResponseCheckerResult combines the validity of the response with variable to hold any additional data items. The data item is
 * just a String, but could contain a JSONArray or JSONObject if multiple values need returning.
 * 
 * @author Chris Culnane
 * 
 */
public class ResponseCheckerResult {

  /**
   * boolean variable that determines if the response was valid or not
   */
  private boolean isValid     = false;

  /**
   * boolean variable that determines if this response had any additional data items
   */
  private boolean hasDataItem = false;

  /**
   * String variable for holding any additional data items
   */
  private String  dataItem    = null;

  /**
   * Constructs a new ResponseCheckerResult setting any data items and the validity of the response. This constructs set the
   * hasDataItem to true, if no dataItem exists the alternative constructor should be used
   * 
   * @param dataItem String dataItem in the response
   * @param isValid boolean true if valid false if invalid
   */
  public ResponseCheckerResult(String dataItem, boolean isValid) {
    this.dataItem = dataItem;
    this.isValid = isValid;
    this.hasDataItem = true;
  }

  /**
   * Constructs a new ResponseCheckerResult setting hasDataItem to false and the validity to the passed in value.
   * @param isValid boolean true if valid false if invalid
   */
  public ResponseCheckerResult(boolean isValid) {

    this.isValid = isValid;
    this.hasDataItem = false;
  }

  /**
   * Returns whether the response had any additional data items
   * @return boolean true if there are additional items, false it not
   */
  public boolean hasDataItem() {
    return this.hasDataItem;
  }

  /**
   * Checks whether the response was valid or not
   * @return boolean true if valid, false if invalid
   */
  public boolean isValid() {
    return this.isValid;
  }

  /**
   * Gets any associated additional data item
   * @return String containing the additional data item
   */
  public String getDataItem() {
    return this.dataItem;
  }
}
