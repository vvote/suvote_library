/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import org.json.JSONObject;

/**
 * Interface for a ResponseChecker. The ResponseChecker is used to customise how a response should be processed. A flexible approach
 * is needed because not all responses can be checked in the same way. For example, a simple message submission just requires the
 * signature checking, since the data the signature covers is known in advanced. However, in the case of more complicated
 * submission, for example a Vote message, there is an additional data field included in the response (commitTIme) and is covered by
 * the signature. This requires the signature checker to be more advanced since it has to correctly construct the message digest by
 * combining previously known data with the responded commitTime. By using this interface it allows a single method to handle
 * different responses.
 * 
 * @author Chris Culnane
 * 
 */
public interface ResponseChecker {

  /**
   * Called as soon as a response is received to check whether it is valid or not.
   * @param response JSONObject of the received response
   * @return ResponseCheckerResult that combines the validity of the response and any data items in the response
   */
  public ResponseCheckerResult checkEarlyResponse(JSONObject response);
}
