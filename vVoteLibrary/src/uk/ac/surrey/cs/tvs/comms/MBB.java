/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.net.ssl.SSLSocketFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.ErrorMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;

/**
 * Utility class with static methods for communicating with a set of MBB Peers.
 * 
 * @author Chris Culnane
 * 
 */
public class MBB {

  /**
   * Logger
   */
  private static final Logger logger             = LoggerFactory.getLogger(MBB.class);

  /**
   * Message separator constant, used for submitting multiple messages at once (primarily file messages)
   */
  public static final String  MESSAGE_SEPARATOR  = "\n";

  /**
   * File token to indicate the following message is a file message
   */
  public static final String  FILE_SUB_TOKEN     = "FILE:";

  /**
   * File token combined with message separator
   */
  public static final String  FILE_SUB_SEPARATOR = MESSAGE_SEPARATOR + FILE_SUB_TOKEN;

  /**
   * Constructs an object that we utilise to communicate with an MBB
   */
  private MBB() {
    super();
  }

  /**
   * Checks if we have a threshold of non-error response from the MBB
   * 
   * @param responses
   *          JSONArray of responses received from the MBB
   * @param threshold
   *          int threshold to check against
   * @return boolean true if we have a threshold of non-error messages, false otherwise
   * @throws ConsensusException
   */
  public static boolean checkThresholdNonErrors(JSONArray responses, int threshold) throws ConsensusException {
    try {
      int nonErrorCount = 0;
      for (int i = 0; i < responses.length(); i++) {

        JSONObject response = responses.getJSONObject(i);
        if (!response.getString(MessageFields.TYPE).equals(ErrorMessage.TYPE_ERROR)) {
          nonErrorCount++;
        }
      }

      if (nonErrorCount >= threshold) {
        return true;
      }
      else {
        return false;
      }
    }
    catch (JSONException e) {
      throw new ConsensusException("Exception checking for errors in response - JSON error");
    }
  }

  /**
   * Checks the threshold of the responses received to see if there is a set of non-error responses. This does not check the
   * responses are consistent, just that there is a threshold of signatures to be checked.
   * 
   * @param responses
   *          JSONArray of responses to check for valid (non-error) responses
   * @param threshold
   *          integer of threshold
   * @return boolean true if there is a threshold of responses, false if not
   */
  public static boolean checkThresholdOfResponsesReceived(JSONArray responses, int threshold) {
    int validReceived = 0;

    for (int i = 0; i < responses.length(); i++) {
      try {
        JSONObject response = responses.getJSONObject(i);

        if (!response.getString(MessageFields.TYPE).equals(ErrorMessage.TYPE_ERROR)
            && response.has(MessageFields.PeerResponse.PEER_SIG)) {
          validReceived++;
        }
      }
      catch (JSONException e) {
        logger.warn("JSONException thrown when checking response", e);
      }
    }
    if (validReceived >= threshold) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Extracts just the valid signature fields from the checked responses and returns them in a new JSONArray
   * 
   * @param responses
   *          JSONArray of the responses that have been checked for validity
   * @return JSONArray of only valid MBB signatures
   * @throws JSONException
   */
  public static JSONArray getJustSignatures(JSONArray responses) throws JSONException {
    // Record all of the returned MBB signatures.
    JSONArray sigArray = new JSONArray();

    for (int i = 0; i < responses.length(); i++) {
      JSONObject response = responses.getJSONObject(i);

      if (response.getBoolean(TVSSignature.IS_VALID)) {
        JSONObject sigObj = new JSONObject();
        sigObj.put(MessageFields.PeerResponse.PEER_ID, response.getString(MessageFields.PeerResponse.PEER_ID));
        sigObj.put(MessageFields.PeerResponse.PEER_SIG, response.getString(MessageFields.PeerResponse.PEER_SIG));
        sigArray.put(sigObj);
      }
      else {
        logger.warn("Error extracting signatures from {}", responses);
      }
    }

    return sigArray;
  }

  /**
   * Checks the responses to find the consensus value for a field. For example, if the CommitTime is returned by each peer this
   * method will check the responses and find the returned CommitTime value that a threshold of peers have all returned. If a
   * consensus is not met it will throw and exception.
   * 
   * @param responses
   *          JSONArray of responses received
   * @param field
   *          String field name to find value for
   * @param threshold
   *          integer of the threshold required of agreeing peers
   * @return String containing consensus value of field
   * @throws ConsensusException
   */
  public static String getThresholdFieldValue(JSONArray responses, String field, int threshold) throws ConsensusException {
    try {
      HashMap<String, AtomicInteger> valueCounts = new HashMap<String, AtomicInteger>();

      for (int i = 0; i < responses.length(); i++) {
        int currentCount = 0;
        JSONObject response = responses.getJSONObject(i);

        if (!response.getString(MessageFields.TYPE).equals(ErrorMessage.TYPE_ERROR)
            && response.has(MessageFields.PeerResponse.PEER_SIG) && response.has(field)) {
          String value = response.getString(field);

          if (valueCounts.containsKey(value)) {
            currentCount = valueCounts.get(value).incrementAndGet();
          }
          else {
            currentCount = 1;
            valueCounts.put(value, new AtomicInteger(currentCount));
          }
          if (currentCount >= threshold) {
            return value;
          }
        }
      }

      throw new ConsensusException("Could not find a consensus on the field value");
    }
    catch (JSONException e) {
      throw new ConsensusException("Exception finding consensus - JSON error", e);
    }
  }

  /**
   * Sends the message to all the peers defined in MBBConfig
   * 
   * @param mbbConf
   *          MBBConfig containing details of each peer to send to
   * @param message
   *          String representation of JSON message
   * @return JSONArray of responses
   * @throws MBBCommunicationException
   */
  public static JSONArray sendMessage(MBBConfig mbbConf, String message) throws MBBCommunicationException {
    return MBB.sendMessage(mbbConf, message, null, null);
  }

  /**
   * Sends a message to the MBB Peers using SSL if factory is not null and using the specified cipher suites if specified. This
   * blocks until the response has been received or the timeout specified in the config file has expired.
   * 
   * @param mbbConf
   *          MBBConfig that contains the address of the MBB Peers
   * @param message
   *          String message to send
   * @param factory
   *          SSLFactory to use or null if none SSL
   * @param cipherSuites
   *          String array of cipher suites to use if using SSL, if null and factory is provided it will use the default set
   * @return JSONArray of responses from the peers
   * @throws MBBCommunicationException
   */
  public static JSONArray sendMessage(MBBConfig mbbConf, String message, SSLSocketFactory factory, String[] cipherSuites)
      throws MBBCommunicationException {
    try {
      logger.info("About to send message to MBB Peers {}", message);

      // Get the peers we are sending to
      Collection<JSONObject> peers = mbbConf.getPeers().values();

      // Prepare a list to hold callable objects
      ArrayList<Callable<String>> messageToPeers = new ArrayList<Callable<String>>();

      // Create message senders for each peer
      for (JSONObject peer : peers) {
        SendAndWaitForResponse sendAndWait = new SendAndWaitForResponse(peer.getString(ConfigFiles.MBBConfig.PEER_ADDRESS),
            peer.getInt(ConfigFiles.MBBConfig.PEER_PORT), message);
        if (factory != null) {
          sendAndWait.setSSLSocketFactory(factory);
        }
        if (cipherSuites != null) {
          sendAndWait.setCipherSuites(cipherSuites);
        }
        messageToPeers.add(sendAndWait);
      }
      // Create the thread pool and execute
      ExecutorService clientExe = Executors.newFixedThreadPool(peers.size());

      try {
        int timeout = mbbConf.getIntParameter(ConfigFiles.MBBConfig.MBB_TIMEOUT);
        //If this is a file upload check if the FILE_TIMEOUT has been set
        if (message.indexOf(FILE_SUB_SEPARATOR) >= 0) {
          int fileTimeout = mbbConf.getIntParameter(ConfigFiles.MBBConfig.MBB_FILE_TIMEOUT);
          if (fileTimeout > 0) {
            //MBBFileUploadTimeout has been set so use that instead
            timeout = fileTimeout;
          }
        }

        if (timeout < 0) {
          logger.warn("Timeout for sending to MBB Peers is not defined");
        }

        List<Future<String>> rets = clientExe.invokeAll(messageToPeers, timeout, TimeUnit.SECONDS);

        // Get the returned signatures
        JSONArray responseArray = new JSONArray();
        for (Future<String> future : rets) {
          if (!future.isCancelled()) {
            try {
              responseArray.put(new JSONObject(future.get()));
            }
            catch (JSONException e1) {
              logger.warn("JSONException received on response {}", future.get());
            }
          }
        }

        // Shutdown the thread pool and return signature array
        return responseArray;
      }
      catch (InterruptedException e) {
        throw new MBBCommunicationException("Communication Thread Pool was interrupted", e);
      }
      finally {
        clientExe.shutdown();
      }
    }
    catch (JSONException e) {
      throw new MBBCommunicationException("Exception whilst trying to send a message to the MBB", e);
    }
    catch (ExecutionException e) {
      throw new MBBCommunicationException("Exception whilst trying to send a message to the MBB", e);
    }
  }

  /**
   * Sends a message to the MBB Peers using SSL if factory is not null and using the specified cipher suites if specified. This
   * blocks until there have been a threshold of valid response or the timeout specified in the config file has expired.
   * 
   * @param mbbConf
   *          MBBConfig that contains the address of the MBB Peers
   * @param message
   *          String message to send
   * @param factory
   *          SSLFactory to use or null if none SSL
   * @param cipherSuites
   *          String array of cipher suites to use if using SSL, if null and factory is provided it will use the default set
   * @param responseChecker
   *          ResponseChecker to use when checking the responses as soon as they are received
   * @param threshold
   *          int of the threshold to wait for
   * @return MBBResponse contains either all responses or if a valid threshold has been received just the valid responses. Also
   *         contains a value determining if the responses were valid or not to save re-checking the signatures
   * @throws MBBCommunicationException
   */
  public static MBBResponse sendMessageWaitForThreshold(MBBConfig mbbConf, String message, SSLSocketFactory factory,
      String[] cipherSuites, ResponseChecker responseChecker, int threshold) throws MBBCommunicationException {
    ExecutorService clientExe = null;

    try {
      logger.info("About to send message to MBB Peers {}", message);

      // Get the peers we are sending to
      Collection<JSONObject> peers = mbbConf.getPeers().values();

      // Create the thread pool and execute
      clientExe = Executors.newFixedThreadPool(peers.size());

      // Create an ExecutorCompletionService to wrap the executor with
      ExecutorCompletionService<String> completionService = new ExecutorCompletionService<String>(clientExe);

      // We don't currently use the futures, but store them here
      List<Future<String>> rets = new ArrayList<Future<String>>();

      // Create message senders for each peer and add to the CompletionService
      for (JSONObject peer : peers) {
        SendAndWaitForResponse sendAndWait = new SendAndWaitForResponse(peer.getString(ConfigFiles.MBBConfig.PEER_ADDRESS),
            peer.getInt(ConfigFiles.MBBConfig.PEER_PORT), message);
        if (factory != null) {
          sendAndWait.setSSLSocketFactory(factory);
        }
        if (cipherSuites != null) {
          sendAndWait.setCipherSuites(cipherSuites);
        }
        rets.add(completionService.submit(sendAndWait));
      }

      // Get the timeout
      int timeout = mbbConf.getIntParameter(ConfigFiles.MBBConfig.MBB_TIMEOUT);
      //If this is a file upload check if the FILE_TIMEOUT has been set
      if (message.indexOf(FILE_SUB_SEPARATOR) >= 0) {
        int fileTimeout = mbbConf.getIntParameter(ConfigFiles.MBBConfig.MBB_FILE_TIMEOUT);
        if (fileTimeout > 0) {
          //MBBFileUploadTimeout has been set so use that instead
          timeout = fileTimeout;
        }
      }
      if (timeout < 0) {
        logger.warn("Timeout for sending to MBB Peers is not defined");
      }
      // Calculate when the timeout will expire
      long timeoutEnd = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout);

      // Prepare two response objects. The earlyResponseArray will be used for responses that do not have any additional data items
      JSONArray earlyResponseArray = new JSONArray();

      // We use a JSONObject for holding responses that contain an additional data item. All the additional data items should be the
      // same, but there is no guarantee they will be. We therefore add them to the JSONObject and only consider a threshold to have
      // been received when a threshold of valid responses with the same dataitem value have been received
      JSONObject dataResponse = new JSONObject();
      Future<String> resp = null;

      // All responses irrespective of whether they are valid or not got into this JSONArray. It will only be returned if we don't
      // find a valid threshold of responses
      JSONArray responseArray = new JSONArray();

      int responseCounter = 0;
      // Loop whilst we haven't reached the timeout, we will process one response for each loop
      while (System.currentTimeMillis() < timeoutEnd) {
        // Calculate how much of the timeout remains
        long currentTimeout = (timeoutEnd - System.currentTimeMillis());
        if (currentTimeout > 0) {
          // Poll for the next response blocking until one is available or the timeout expires
          resp = completionService.poll(currentTimeout, TimeUnit.MILLISECONDS);
          responseCounter++;
          // Check the response isn't null or cancelled
          if (resp != null && resp.isCancelled() == false) {
            try {
              // Build a responses object, add it to the the responseArray
              JSONObject respObj = new JSONObject(resp.get());
              responseArray.put(respObj);
              // Call the responseChecker with the object
              ResponseCheckerResult result = responseChecker.checkEarlyResponse(respObj);
              if (result.hasDataItem() && result.isValid()) {
                logger.info("ResponseChecker has additional dataItem: {}", result.getDataItem());
                // The response has additional data items, therefore index by the additional item
                if (dataResponse.has(result.getDataItem())) {
                  dataResponse.getJSONArray(result.getDataItem()).put(respObj);
                }
                else {
                  JSONArray dataArray = new JSONArray();
                  dataArray.put(respObj);
                  dataResponse.put(result.getDataItem(), dataArray);
                }

                // Check if there are threshold of valid responses with the same data time
                if (dataResponse.getJSONArray(result.getDataItem()).length() >= threshold) {
                  return new MBBResponse(dataResponse.getJSONArray(result.getDataItem()), true, result.getDataItem());
                }
              }
              else if (result.isValid()) {
                // No additional data items so put in the earlyResponse JSONArray
                earlyResponseArray.put(respObj);
                // Check if we have a threshold of valid responses
                if (earlyResponseArray.length() >= threshold) {
                  clientExe.shutdownNow();
                  return new MBBResponse(earlyResponseArray, true);
                }
              }
              else {
                logger.warn("Response was invalid {}", respObj.toString());
              }
            }
            catch (JSONException e) {
              logger.warn("Response caused a JSON Exception, will ignore", e);
            }
          }
        }
        if (responseCounter >= rets.size()) {
          logger.warn("Received all responses but don't appear to have got a valid response");
          break;
        }
      }
      // Timeout has finished, check if there are any remaining responses and process them
      while ((resp = completionService.poll()) != null) {
        try {
          responseArray.put(new JSONObject(resp.get()));
        }
        catch (JSONException e) {
          logger.warn("Response caused a JSON Exception, will ignore", e);
        }
      }
      List<Runnable> unFinishedSubs = clientExe.shutdownNow();
      if (unFinishedSubs.size() > 0) {
        logger.warn("There were {} unfinished SendAndWaitForResponses", unFinishedSubs.size());
      }
      return new MBBResponse(responseArray);
    }
    catch (JSONException e) {
      throw new MBBCommunicationException("Exception whilst trying to send a message to the MBB", e);
    }
    catch (ExecutionException e) {
      throw new MBBCommunicationException("Exception whilst trying to send a message to the MBB", e);
    }
    catch (InterruptedException e) {
      throw new MBBCommunicationException("Exception whilst trying to send a message to the MBB", e);
    }
    finally {
      if (clientExe != null) {
        List<Runnable> unFinishedSubs = clientExe.shutdownNow();
        if (unFinishedSubs.size() > 0) {
          logger.warn("There were {} unfinished SendAndWaitForResponses", unFinishedSubs.size());
        }
      }
    }
  }

  /**
   * Sends a message to a set of MBB peers and waits for a response
   * 
   * 
   * @param mbbConf
   *          MBBConfig contains
   * @param message
   *          the message to send, could be a JSON String or a combination of messages if it is a file messages
   * @return JSONArray of response signatures
   * @throws MBBCommunicationException
   */
  public static JSONArray sendMessageWaitForSigs(MBBConfig mbbConf, String message) throws MBBCommunicationException {
    return MBB.sendMessageWaitForSigs(mbbConf, message, null, null);
  }

  /**
   * Sends a message to the MBB peers, waiting for responses. It checks the responses and only returns non-error messages and only
   * the ID and Signature from those valid responses.
   * 
   * @param mbbConf
   *          MBBConfig containing details of each peer to send to
   * @param message
   *          String representation of JSON message
   * @param factory
   *          SSLFactory to use or null if none SSL
   * @param cipherSuites
   *          String array of cipher suites to use if using SSL, if null and factory is provided it will use the default set
   * @return JSONArray containing only valid responses and then just the ID and signatures
   * @throws MBBCommunicationException
   */
  public static JSONArray sendMessageWaitForSigs(MBBConfig mbbConf, String message, SSLSocketFactory factory, String[] cipherSuites)
      throws MBBCommunicationException {
    try {
      logger.info("About to send message to MBB Peers {}", message);
      JSONArray responses = sendMessage(mbbConf, message, factory, cipherSuites);
      JSONArray sigArray = new JSONArray();

      // Record all of the returned MBB signatures.
      for (int i = 0; i < responses.length(); i++) {
        JSONObject response = responses.getJSONObject(i);
        if (!response.getString(MessageFields.TYPE).equals(ErrorMessage.TYPE_ERROR)
            && response.has(MessageFields.PeerResponse.PEER_SIG)) {
          JSONObject sigObj = new JSONObject();
          sigObj.put(MessageFields.PeerResponse.PEER_ID, response.getString(MessageFields.PeerResponse.PEER_ID));
          sigObj.put(MessageFields.PeerResponse.PEER_SIG, response.getString(MessageFields.PeerResponse.PEER_SIG));
          sigArray.put(sigObj);
        }
        else {
          logger.warn("Error in response from Peer {} for message {}", response, message);
        }
      }

      return sigArray;
    }
    catch (JSONException e) {
      throw new MBBCommunicationException("Exception whilst trying to send a message to the MBB", e);
    }
  }
}
