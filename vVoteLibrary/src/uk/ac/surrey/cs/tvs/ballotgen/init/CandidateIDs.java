/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.init;

import java.io.IOException;
import java.math.BigInteger;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Utility class for creating CandidateIDs from random points on the underlying Elliptic Curve. This is a pre-processing step
 * required before ballots are generated. It should be run once and the output published.
 * 
 * @author Chris Culnane
 * 
 */
public class CandidateIDs {

  /**
   * Creates a series of random candidate IDs from the underlying Elliptic Curve. The curve is obtained from ECGroup and the
   * publicKeyFile. Random points on the curve will be selected and output in a plain text file as well as encrypted with a fixed
   * randomness of 1 and stored in a separate file. Both files should be made public before the start of ballot generation.
   * 
   * @param numberToCreate
   *          number of points to select
   * @param publicKeyFile
   *          string path to JSON Public Key
   * @param plaintextFile
   *          string path to store raw candidate IDs
   * @param baseEncryptionsFile
   *          string path to store encrypted candidate IDs
   * @throws JSONIOException
   * @throws IOException
   * @throws JSONException
   */
  public static void create(int numberToCreate, String publicKeyFile, String plaintextFile, String baseEncryptionsFile)
      throws JSONIOException, JSONException {
    System.out.println("Generating new candidate ID and encrypting");
    // Obtain the Elliptic Curve and public key file.
    ECUtils group = new ECUtils();
    JSONObject pkJson = IOUtils.readJSONObjectFromFile(publicKeyFile);
    ECPoint pk = group.pointFromJSON(pkJson);

    // Create the output file structure and content.
    JSONArray plaintext = new JSONArray();
    JSONArray ciphs = new JSONArray();

    // Get the random candidate id, store it in plain text and store it encrypted.
    for (int i = 0; i < numberToCreate; i++) {
      ECPoint candidateID = group.getRandomValue();
      plaintext.put(ECUtils.ecPointToJSON(candidateID));
      ciphs.put(group.cipherToJSON(group.encrypt(candidateID, pk, BigInteger.ONE)));
    }

    // Write the data to the files.
    IOUtils.writeJSONToFile(plaintext, plaintextFile);
    IOUtils.writeJSONToFile(ciphs, baseEncryptionsFile);
  }

  /**
   * Encrypts an existing series of random candidate IDs from a JSON plaintext file. The curve is obtained from ECGroup and the
   * publicKeyFile. CandidateIDs will be encrypted with a fixed randomness of 1 and stored in a separate file. Both files should be
   * made public before the start of ballot generation.
   * 
   * @param publicKeyFile
   *          string path to JSON Public Key
   * @param plaintextFile
   *          string path to load raw candidate IDs
   * @param baseEncryptionsFile
   *          string path to store encrypted candidate IDs
   * @throws JSONIOException
   * @throws JSONException
   */
  public static void encrypt(String publicKeyFile, String plaintextFile, String baseEncryptionsFile) throws JSONIOException,
      JSONException {
    System.out.println("Doing encryption on existing file:" + plaintextFile);
    // Obtain the Elliptic Curve and public key file.
    ECUtils group = new ECUtils();
    JSONObject pkJson = IOUtils.readJSONObjectFromFile(publicKeyFile);
    ECPoint pk = group.pointFromJSON(pkJson);

    // Create the output file structure and content.
    JSONArray plaintext = IOUtils.readJSONArrayFromFile(plaintextFile);
    JSONArray ciphs = new JSONArray();

    // Get the random candidate id, store it in plain text and store it encrypted.
    for (int i = 0; i < plaintext.length(); i++) {
      ECPoint candidateID = group.pointFromJSON(plaintext.getJSONObject(i));
      ciphs.put(group.cipherToJSON(group.encrypt(candidateID, pk, BigInteger.ONE)));
    }

    // Write the data to the files.
    IOUtils.writeJSONToFile(plaintext, plaintextFile);
    IOUtils.writeJSONToFile(ciphs, baseEncryptionsFile);

  }

  /**
   * Main entry point to create the candidate ids.
   * 
   * @param args
   *          Command line arguments.
   * @throws JSONIOException
   * @throws IOException
   * @throws JSONException
   */
  public static void main(String[] args) throws JSONIOException, JSONException {
    if (args.length == 3) {
      try {
        CandidateIDs.encrypt(args[0], args[1], args[2]);
      }
      catch (NumberFormatException e) {
        CandidateIDs.printUsage();
      }
    }
    else if (args.length == 4) {
      try {
        CandidateIDs.create(Integer.parseInt(args[0]), args[1], args[2], args[3]);
      }
      catch (NumberFormatException e) {
        CandidateIDs.printUsage();
      }
    }
    else {
      CandidateIDs.printUsage();
    }
  }

  /**
   * Displays the usage information for the class when run from the command line.
   */
  private static final void printUsage() {
    System.out.println("Usage of CandidateIDs:");
    System.out.println("CandidateIDs provide a utility function for generating");
    System.out.println("initial candidateID values, along with encryptions of ");
    System.out.println("them using a fixed randomness of 1 (one). The command ");
    System.out.println("is as follows:\n");
    System.out.println("CandidateIDs [numberToCreate] publicKey plaintextCandIDs encryptedOutput\n");
    System.out
        .println("\t[numberToCreate]: Optional, the number of candidateIDs to generate,\n\t\t if not set the plaintexts must be in plaintextCandIDs");
    System.out.println("\tpublicKey: path to public key JSON file");
    System.out.println("\tplaintextCandIDs: path to file to read/save plaintext values");
    System.out.println("\tencryptedOutput: path to file to save encrypted values");
  }
}
