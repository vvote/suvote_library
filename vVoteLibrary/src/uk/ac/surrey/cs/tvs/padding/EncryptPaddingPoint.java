package uk.ac.surrey.cs.tvs.padding;

import java.math.BigInteger;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

public class EncryptPaddingPoint {

  public EncryptPaddingPoint() {
  }

  public static void encryptPaddingPoint(String paddingPoint, String output, String publicKeyPath) throws JSONIOException,
      JSONException {
    ECUtils ecUtils = new ECUtils();
    ECPoint padPoint = ecUtils.pointFromJSON(IOUtils.readJSONObjectFromFile(paddingPoint));
    ECPoint publicKey = ecUtils.pointFromJSON(IOUtils.readJSONObjectFromFile(publicKeyPath));
    ECPoint[] encryptedPadPoint = ecUtils.encrypt(padPoint, publicKey, BigInteger.ONE);
    IOUtils.writeJSONToFile(ecUtils.cipherToJSON(encryptedPadPoint), output);
    // TODO Verify Padding Point does not exist in candidate IDs
  }

  public static void main(String[] args) throws JSONIOException, JSONException {
    if (args.length == 3) {
      System.out.println("Encrypting Padding point " + args[0] + " using " + args[1] + ", saving to " + args[2]);
      EncryptPaddingPoint.encryptPaddingPoint(args[0], args[2], args[1]);
    }
    else {
      System.out.println("Usage: EncryptPaddingPoint paddigPointPath publicKeyPath outputPath");
      System.out.println("\tpaddingPointPath - path to JSON padding point");
      System.out.println("\tpublicKeyPath - path to JSON Public Key");
      System.out.println("\toutputPath - path to save encrypted point to");
    }

  }

}
