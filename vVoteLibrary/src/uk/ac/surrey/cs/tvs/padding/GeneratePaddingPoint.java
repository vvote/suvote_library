package uk.ac.surrey.cs.tvs.padding;

import org.json.JSONException;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;


public class GeneratePaddingPoint {

  public GeneratePaddingPoint() {
  }

  public static void generatePaddingPoint(String filename) throws JSONIOException, JSONException{
    ECUtils ecUtils = new ECUtils();
    IOUtils.writeJSONToFile(ECUtils.ecPointToJSON( ecUtils.getRandomValue()),filename);
    //TODO Verify Padding Point does not exist in candidate IDs
  }

  public static void main(String[] args) throws JSONIOException, JSONException {
    if(args.length==0){
      System.out.println("Generating Padding Point and Saving to paddingpoint.json");
      GeneratePaddingPoint.generatePaddingPoint("paddingpoint.json");      
    }else if(args.length==1){
      System.out.println("Generating Padding Point and Saving to " + args[0]);
      GeneratePaddingPoint.generatePaddingPoint(args[0]);
    }else{
      System.out.println("Usage: GeneratePaddingPoint [path]");
      System.out.println("\tpath is an option file path to save the padding point to");
      System.out.println("\totherwise it saves to paddingpoint.json");
    }

  }

}
