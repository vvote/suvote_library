/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ui;

import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;

/**
 * ProgressMonitor to listen for ProgressListener updates and construct a JSONObject of the data and send it to a client via a
 * WebSocket.
 * 
 * @author Chris Culnane
 * 
 */
public class ProgressMonitor implements ProgressListener {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(ProgressMonitor.class);

  /**
   * Which progress listener this is relevant to - WebSocket could be the channel for multiple different progress updates
   */
  private String              progressID;

  /**
   * WebSocket to write the data out to
   */
  private WebSocket           ws;

  /**
   * Constructs a ProgressMonitor to receive progress updates and pass them onto the WebSocket
   * 
   * @param progressID
   *          the ID of the ProgressListener
   * @param ws
   *          WebSocket to write the update to
   */
  public ProgressMonitor(String progressID, WebSocket ws) {
    super();

    this.progressID = progressID;
    this.ws = ws;
  }

  /**
   * Receives progress updates and writes them to a WebSocket
   * 
   * @param value
   *          updated progress
   * 
   * @see uk.ac.surrey.cs.tvs.ui.ProgressListener#updateProgress(int)
   */
  @Override
  public void updateProgress(int value) {
    JSONObject jobj = new JSONObject();
    try {
      jobj.put(JSONConstants.ProgressMessage.TYPE, JSONConstants.ProgressMessage.TYPE_PROGRESS_UPDATE);
      jobj.put(JSONConstants.ProgressMessage.PROGRESS_ID, this.progressID);
      jobj.put(JSONConstants.ProgressMessage.VALUE, value);
      this.ws.send(jobj.toString());
    }
    catch (JSONException e) {
      logger.warn("JSONException during progress update", e);
    }
  }
}
