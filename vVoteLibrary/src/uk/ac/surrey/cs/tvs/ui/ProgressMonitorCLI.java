/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ui;

/**
 * ProgressMonitorCLI is designed to display progress on the command line for use on headless servers.
 * 
 * @author Chris Culnane
 * 
 */
public class ProgressMonitorCLI implements ProgressListener {

  /**
   * Carriage return
   */
  private static final char  CARRIAGE_RETURN  = '\r';
  /**
   * Border top left
   */
  private static final char  CORNER_TOP_LEFT  = '\u2554';
  /**
   * Border top horizontal
   */
  private static final char  BORDER_TOP       = '\u2550';
  /**
   * T-Bar border top
   */
  private static final char  T_BAR_TOP        = '\u2566';

  /**
   * Border top Right
   */
  private static final char  CORNER_TOP_RIGHT = '\u2557';

  /**
   * Border vertical
   */
  private static final char  BORDER_VERTICAL  = '\u2551';

  /**
   * Progress Symbol - hash
   */
  private static final char  PROGRESS_SYMBOL  = '\u2592';
  /**
   * Percent character
   */
  private static final char  PERCENT          = '%';
  /**
   * Space character
   */
  private static final char  SPACE            = ' ';
  /**
   * Reference to the ProgressMonitorCLI that is a child of this ProgressMonitorCLI
   */
  private ProgressMonitorCLI child            = null;

  /**
   * Reference to a parent ProgressMonitorCLI, only set if this is a child ProgressMonitorCLI
   */
  private ProgressMonitorCLI parent           = null;

  /**
   * Current progress value
   */
  private int                value            = 0;

  /**
   * Records whether the headings for the progress bars have been outputted. If not we need to redraw them
   */
  private boolean            headingPrinted   = false;

  /**
   * Constructs a ProgressMonitorCLI to receive progress updates and display them on the CLI
   * 
   * @param progressID
   *          the ID of the ProgressListener
   * 
   */
  public ProgressMonitorCLI() {
    super();
  }

  /**
   * Constructs a new ProgressMonitorCLI as a child of the specified parent ProgressMonitorCLI
   * 
   * @param parent
   *          ProgressMonitorCLI that is the parent of the newly constructed instance
   */
  public ProgressMonitorCLI(ProgressMonitorCLI parent) {
    super();
    this.parent = parent;
  }

  /**
   * Sets the child ProgressMonitorCLI for this ProgressMonitorCLI
   * 
   * @param child
   *          ProgressMonitorCLI to treat as child
   */
  public void setChild(ProgressMonitorCLI child) {
    this.child = child;
  }

  /**
   * Sets a parent ProgressMonitorCLI for this instance
   * 
   * @param parent
   *          ProgressMonitorCLI that is the parent of this instance
   */
  public void setParent(ProgressMonitorCLI parent) {
    this.parent = parent;
  }

  /**
   * This redraws the progress to the CLI by first asking the parent to repaint its progress and then calling printProgress on this
   * instance, which will also request all child progress bar to redraw as well
   */
  public void repaintProgress() {
    if (this.parent != null) {
      this.parent.repaintProgress();
    }
    else {
      this.printProgress();
    }
  }

  /**
   * Receives progress updates and redraws the CLI progress bars
   * 
   * @param value
   *          updated progress
   * 
   * @see uk.ac.surrey.cs.tvs.ui.ProgressListener#updateProgress(int)
   */
  @Override
  public void updateProgress(int value) {
    this.value = value;
    if (this.parent != null) {
      this.parent.repaintProgress();
    }
    else {
      this.printProgress();
    }
  }

  /**
   * Print the headings for the progress bars, these are the graphical characters which create an outline
   */
  public void printHeading() {
    this.outputChar(CORNER_TOP_LEFT);
    this.outputChar(BORDER_TOP, 10);
    this.outputChar(T_BAR_TOP);
    this.outputChar(BORDER_TOP, 4);
    this.outputChar(CORNER_TOP_RIGHT);
  }

  /**
   * Print the actual progress, if necessary printing the headers first. Then ask all child progress bar to also print their
   * progress
   */
  public void printProgress() {
    // One character per 10%
    int chars = value / 10;

    if (this.parent == null) {
      // There is no parent element, so we are the root ProgressMonitorCLI
      if (!headingPrinted) {
        // Print the headings because they haven't already been printed
        this.printHeading();
        if (child != null) {
          // Output a space and then ask the child to print its heading
          this.outputChar(SPACE);
          child.printHeading();
        }
        this.headingPrinted = true;
        // Create a new line, ready to output the actual progress
        System.out.println();
      }
      // Carriage return to bring us back to the start of the line
      this.outputChar(CARRIAGE_RETURN);
    }
    // Border character
    this.outputChar(BORDER_VERTICAL);

    // Output the actual progress characters
    this.outputChar(PROGRESS_SYMBOL, chars);
    // Padding with spaces
    this.outputChar(SPACE, 10 - chars);

    // Output border and current value in percent
    this.outputChar(BORDER_VERTICAL);
    this.outputString(String.valueOf(value));
    this.outputChar(PERCENT);

    // Pad the value text
    if (value < 100) {
      this.outputChar(SPACE);
    }
    if (value < 10) {
      this.outputChar(SPACE);
    }
    this.outputChar(BORDER_VERTICAL);

    // If a child exists put a space between the progress bars and ask it to redraw
    if (child != null) {
      this.outputChar(SPACE);
      child.printProgress();
    }

  }

  /**
   * Outputs a character to the System.out CLI
   * 
   * @param output
   *          char to output
   */
  private void outputChar(char output) {
    System.out.print(output);
  }

  /**
   * Outputs a character multiple times to System.out CLI
   * 
   * @param output
   *          char to output
   * @param repeat
   *          int number of times to output
   */
  private void outputChar(char output, int repeat) {
    for (int i = 0; i < repeat; i++) {
      outputChar(output);
    }
  }

  /**
   * Outputs a string to the CLI - useful for values and messages
   * 
   * @param output
   *          String value to output
   */
  private void outputString(String output) {
    System.out.print(output);
  }

}
