/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import java.io.File;
import java.io.FileFilter;

/**
 * A FileFilter to return just directories
 * 
 * @author Chris Culnane
 * 
 */
public class DirectoryFilter implements FileFilter {

  /**
   * Tests whether or not the specified abstract pathname should be included in a pathname list.
   * 
   * @param file
   *          File object to be check
   * @return <code>true</code> if and only if <code>file</code> is a directory
   * 
   * @see java.io.FileFilter#accept(java.io.File)
   */
  @Override
  public boolean accept(File file) {
    return file.isDirectory();
  }
}
