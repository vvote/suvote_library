/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

/**
 * Different Signature types. Currently ECDSA, DSA and DEFAULT are implemented. BLS has not yet been implemented.
 * 
 * @author Chris Culnane
 * 
 */
public enum SignatureType {
  ECDSA, DSA, BLS, DEFAULT, RSA
}
