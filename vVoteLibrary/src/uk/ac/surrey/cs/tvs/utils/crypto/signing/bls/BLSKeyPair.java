/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import it.unisa.dia.gas.jpbc.Element;

/**
 * Simple wrapper class to hold a BLSPublicKey and BLSPrivateKeyPair. It also provides a utility function for generating a new BLS
 * KeyPair. Note: this will be standard KeyPair according the system curve parameters and is not a threshold key pair.
 * 
 * @author Chris Culnane
 * 
 */
public class BLSKeyPair {

  /**
   * Variable to hold BLSPrivateKey
   */
  private BLSPrivateKey priv;
  /**
   * Variable to hold BLSPublicKey
   */
  private BLSPublicKey  pub;

  /**
   * Constructs a new BLSKeyPair from the specified BLSPrivateKey and BLSPublicKey
   * @param privateKey BLSPrivateKey
   * @param publicKey corresponding BLSPublicKey
   */
  public BLSKeyPair(BLSPrivateKey privateKey, BLSPublicKey publicKey) {
    this.priv = privateKey;
    this.pub = publicKey;
  }

  /**
   * Generate a new BLSKeyPair using the system wide curve parameters and construct a BLSKeyPair object from the newly generated keys.
   * @return BLSKeyPair containing the newly generated key pair
   */
  public static BLSKeyPair generateKeyPair() {
    //Create a random PrivateKey;
    BLSPrivateKey privKey = new BLSPrivateKey(CurveParams.getInstance().getPairing().getZr().newRandomElement());

    //Create the generator at random
    Element g = CurveParams.getInstance().getPairing().getG2().newRandomElement().getImmutable();

    //Using the generate and the private key to construct the public key
    Element publicKey = g.powZn(privKey.getKey().getImmutable()).getImmutable();
    
    //Construct the public key from the variables
    BLSPublicKey pubKey = new BLSPublicKey(publicKey, g);
    return new BLSKeyPair(privKey, pubKey);
  }

  /**
   * Gets the BLSPrivateKey
   * @return BLSPrivateKey in the key pair
   */
  public BLSPrivateKey getPrivateKey() {
    return this.priv;
  }

  /**
   * Gets the BLSPublicKey
   * @return BLSPublicKey in the key pair
   */
  public BLSPublicKey getPublicKey() {
    return this.pub;
  }

}
