/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * This extends the TimeoutManager, utilising the same underlying timeout code: @see{@link TimeoutManager}
 * 
 * This extension augments the in-memory timeout manager with a file based backup to allow it to recover timeouts following a
 * shutdown of the code, even in the case of a crash. It achieves this by creating a timeout file in a specified directory for each
 * timeout that is created. If the timeout fires normally and is handled the file is deleted. However, if the system crashes before
 * the timeout is run when the FileBackedTimeoutManager is restarted it will iterate through all compatible files in the directory
 * and re-initialise the timeouts. This can lead to timeouts being fired multiple times, for example, if a crash happened between
 * completing a timeout and removing the timeout file, however, we take the view that it is better to fire more than once than
 * possibly never.
 * 
 * It is not recommended to use a FileBackedTimeoutManager unless it is essential, most timeouts are self cancelling in the event of
 * a crash. For example, if a timeout is being held on a socket response the crash will take care of closing that socket anyway.
 * This is of more use for deleting files or taking positive actions after a period of time. Note, because of the nature of
 * recovering from the timeout at some point in the future, the moment in time when the timeout will fire cannot be guaranteed,
 * since the recovery could take place days later. If a timeout can become invalid, and therefore should be run, the implementer
 * should place the appropriate guards in the recovery methods.
 * 
 * @author Chris Culnane
 * 
 */
public class FileBackedTimeoutManager extends TimeoutManager implements Runnable {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(FileBackedTimeoutManager.class);

  /**
   * Underlying backup directory to store timeout files in
   */
  private File                backupDir;

  /**
   * Constructor for TimeoutManager
   * 
   * @param interval
   *          in milliseconds between ticks
   * @param segments
   *          number of segments on the wheel
   * @param defaultTimeout
   *          default timeout
   * @param backupDirPath
   *          String path to the backup directory to store file backups to
   * @param recoverObject
   *          FileBackedTimeout this object will be used for running the performRecovery on the read files
   * @throws MaxTimeoutExceeded
   * @throws IOException 
   */
  public FileBackedTimeoutManager(int interval, int segments, int defaultTimeout, String backupDirPath,
      FileBackedTimeout recoverObject) throws MaxTimeoutExceeded, IOException {
    super(interval, segments, defaultTimeout);
    
    //Create the backup directory if it doesn't exist
    this.backupDir = new File(backupDirPath);
    IOUtils.checkAndMakeDirs(this.backupDir);
    
    //Read the files that need to be recovered
    File[] recoveryFiles = backupDir.listFiles(new DefaultTimeoutFileFilter());
    for (File recoveredFile : recoveryFiles) {
      try {
        //Call perform recovery - NOTE performRecovery should not rely on instance variables it should be considered to be equivalent to static.
        recoverObject.performRecovery(recoveredFile, this);
      }
      catch (IOException e) {
        //If it fails we will log it and continue trying to recover
        logger.warn("IOException when recovering timeout file {}, will ignore", recoveredFile.getName(), e);
      }
    }
  }

  /**
   * Add default timeout Task
   * 
   * Since most of our timeouts are default timeouts we provide a utility method to save constantly checking the configured timeout
   * period
   * 
   * @param task
   *          FileBackedTimeout task
   */
  public void addDefaultTimeout(FileBackedTimeout task) {
    task.setBackUpDir(this.backupDir);
    super.addDefaultTimeout(task);
    try {
      task.createFileBackup(super.defaultTimeout);
    }
    catch (IOException e) {
      logger.warn("IOException when creating file backup of timeout - will rely on in-memory timeout only", e);
    }
  }

  /**
   * Add a new timeout task. The actual task can arbitrary as long as it implements Runnable
   * 
   * @param task
   *          FileBackedTimeout that implements Runnable - will be run when timeout occurs
   * @param timeout
   *          the period for the timeout
   * @throws MaxTimeoutExceeded
   */
  public void addTimeout(FileBackedTimeout task, int timeout) throws MaxTimeoutExceeded {

    task.setBackUpDir(this.backupDir);
    super.addTimeout(task, timeout);

    try {
      task.createFileBackup(timeout);
    }
    catch (IOException e) {
      logger.warn("IOException when creating file backup of timeout - will rely on in-memory timeout only", e);
    }
  }

  /**
   * Shuts down the manager.
   */
  public void shutdown() {
    super.shutdown();
  }

  /**
   * Gets the File that points to the directory for storing timeout backup files
   * @return File pointing to backup directory
   */
  public File getBackupDir() {
    return this.backupDir;
  }
}
