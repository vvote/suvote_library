/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.verify;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;

/**
 * Utility class for verifying preference lists are valid. This should be used by the Servlet and the MBB to check that the incoming
 * preferences are valid and therefore should be accepted/sent.
 * 
 * @author Chris Culnane
 * 
 */
public class VerifyPreferences {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(VerifyPreferences.class);

  /**
   * Checks LA, LC-ATl and LC-BTL to ensure collectively they form a valid ballot. The LA vote is checked independently, whilst the
   * ATL and BTL votes are checked for consistency. For example, you cannot vote in both. The actual checking if performed by other
   * methods, this merely builds the JSONArrays and checkes the results to determine the overall result.
   * 
   * @param races
   *          JSONArray of races and preferences that conforms the schema standard
   * @return PrefsVerification of the result
   * @throws JSONException
   */
  public static PrefsVerification checkAllRacePreferences(JSONArray races) throws JSONException {
    logger.info("Checking all preferences of {}", races.toString());
    JSONArray laPrefs = null;
    JSONArray lcATLPrefs = null;
    JSONArray lcBTLPrefs = null;

    for (int i = 0; i < races.length(); i++) {
      JSONObject race = races.getJSONObject(i);
      if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LC_ATL)) {
        lcATLPrefs = race.getJSONArray(MessageFields.VoteMessage.PREFERENCES);
      }
      else if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LC_BTL)) {
        lcBTLPrefs = race.getJSONArray(MessageFields.VoteMessage.PREFERENCES);
      }
      else if (race.getString(MessageFields.VoteMessage.RACE_ID).equalsIgnoreCase(MessageFields.VoteMessage.LA)) {
        laPrefs = race.getJSONArray(MessageFields.VoteMessage.PREFERENCES);
      }
    }

    PrefsVerification laResult = checkPrefs(laPrefs);
    if (laResult != PrefsVerification.VALID_PREF && laResult != PrefsVerification.EMPTY) {
      logger.info("LA preferences are invalid will stop checking the remainder");
      return PrefsVerification.INVALID;
    }

    // If we get here we know the LA was valid
    PrefsVerification lcATLResult = checkSinglePref(lcATLPrefs);
    PrefsVerification lcBTLResult = checkPrefs(lcBTLPrefs);

    // Checks the consistency between ATL and BTL
    if (lcATLResult == PrefsVerification.EMPTY && lcBTLResult == PrefsVerification.VALID_PREF) {
      logger.info("ATL is empty, BTL is valid - Overall valid");
      return PrefsVerification.ALL_VALID;
    }
    else if (lcATLResult == PrefsVerification.SINGLE_PREF && lcBTLResult == PrefsVerification.EMPTY) {
      logger.info("ATL is a single preference, BTL is empty - Overall valid");
      return PrefsVerification.ALL_VALID;
    }else if(lcATLResult == PrefsVerification.EMPTY && lcBTLResult == PrefsVerification.EMPTY){
      logger.info("ATL and BTL is empty - Overall valid");
      return PrefsVerification.ALL_VALID;
    }
    else {
      logger.info("Either ATL or BTL is invalid - Overall invalid");
      return PrefsVerification.INVALID;
    }
  }

  /**
   * Checks a set of preferences in a JSONArray to ensure they are valid. Valid indicates the set of preferences is a set of numbers
   * that range from 1 to n sequentially, with no repeats.
   * 
   * @param prefs
   *          JSONArray of preferences where preferences are strings, with empty preferences being a single space character
   * @return PrefsVerification result (INVALID, EMPTY or VALID_PREF)
   * @throws JSONException
   */
  public static PrefsVerification checkPrefs(JSONArray prefs) throws JSONException {
    try {
      if (prefs == null) {
        logger.warn("Prefs array was null");
        return PrefsVerification.INVALID;
      }

      int[] prefCheck = new int[prefs.length() + 1];
      int maxPrefFound = 0;

      for (int i = 0; i < prefs.length(); i++) {
        String prefString = prefs.getString(i);

        if (!prefString.equals(" ")) {
          int prefInt = Integer.parseInt(prefString);

          if (prefInt > prefs.length()) {
            logger.warn("Preference is larger than the maximum preference allowed");
            return PrefsVerification.INVALID;
          }

          prefCheck[prefInt]++;
          if (prefInt > maxPrefFound) {
            maxPrefFound = prefInt;
          }
        }
      }

      if (prefCheck[0] != 0) {
        logger.warn("Preference of zero was received");
        return PrefsVerification.INVALID;
      }

      boolean foundZero = false;

      for (int i = 1; i < prefCheck.length; i++) {
        if (prefCheck[i] == 0) {
          foundZero = true;
        }
        else if (prefCheck[i] > 1) {
          logger.warn("The preference {} appeared {} times - this is invalid", i, prefCheck[i]);
          return PrefsVerification.INVALID;
        }
        else if (foundZero == true && prefCheck[i] == 1) {
          logger.warn("Preferences are not a sequential list of numbers");
          return PrefsVerification.INVALID;
        }
      }

      if (maxPrefFound == 0) {
        return PrefsVerification.EMPTY;
      }
      else {
        return PrefsVerification.VALID_PREF;
      }
    }
    catch (Exception e) {
      logger.warn("An exception occured when processing the preference list - possible non-numeric character", e);
      return PrefsVerification.INVALID;
    }
  }

  /**
   * Checks that a JSONArray of preferences contains a single 1 and the rest are string consisting of a single space character
   * 
   * @param prefs
   *          JSONArray of preferences where preferences are strings, with empty preferences being a single space character
   * @return PrefsVerification result (INVALID, EMPTY or SINGLE_PREF)
   * @throws JSONException
   */
  public static PrefsVerification checkSinglePref(JSONArray prefs) throws JSONException {
    if (prefs == null) {
      logger.warn("Prefs array was null");
      return PrefsVerification.INVALID;
    }

    int prefCount = 0;

    for (int i = 0; i < prefs.length(); i++) {
      if (prefs.getString(i).equals(" ")) {
        // do nothing we have this clause for empty arrays to optimise checking
      }
      else if (prefs.getString(i).equals("1")) {
        prefCount++;
      }
      else {
        logger.warn("Preference value was something other than a space of 1: {}", prefs.getString(i));
        return PrefsVerification.INVALID;
      }
    }

    if (prefCount == 0) {
      return PrefsVerification.EMPTY;
    }
    else if (prefCount == 1) {
      return PrefsVerification.SINGLE_PREF;
    }
    else {
      logger.warn("Prefcount is {}, this is invalid", prefCount);
      return PrefsVerification.INVALID;
    }
  }
}
