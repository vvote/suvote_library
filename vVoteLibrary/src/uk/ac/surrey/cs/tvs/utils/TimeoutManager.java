/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;

/**
 * This is an attempt at an optimised timeout manager that only requires a single thread to run and does not involve the checking of
 * actual times. It is analogous to a hashed timing wheel, but not as advanced.
 * 
 * The basic construction is to have an ArrayList of ConcurrentLinkedQueues setup in a wheel. Each segment in the wheel represents a
 * tick on that wheel. After each tick the next segment is read and the timeout runnables removed and executed. When all segments
 * have been read it starts back at the beginning like a wheel.
 * 
 * As such, if your wheel consists of s segments and the current tick is t, if you want to set a timeout for n ticks time, you take
 * t+n mod s and store the Runnable in that segment.
 * 
 * This puts a limit on the maximum timeout of the number of segments you have. In this class, we construct a wheel with 2 *
 * requested number of segments. This is simplify and the handling of rollovers from max to 0.
 * 
 * The tick size can be set at construction, which can alter the max timeout, at the expense of periodicity.
 * 
 * @author Chris Culnane
 * 
 */
public class TimeoutManager implements Runnable {

  private static final Logger                        logger          = LoggerFactory.getLogger(TimeoutManager.class);
  /**
   * The interval between ticks
   */
  protected int                                        interval;

  /**
   * The number of segments on the wheel
   */
  protected int                                        segments;

  /**
   * The current tick of the wheel
   */
  protected int                                        currentTick     = 0;

  /**
   * The default timeout applied to a runnable
   */
  protected int                                        defaultTimeout  = 30;

  /**
   * Set to true to gracefully shutdown
   */
  private boolean                                    shutdown        = false;

  /**
   * An arraylist of ConcurrentLinkedQueues, each element in the ArrayList represents a tick, whilst each queue is the set of the
   * Runnable. The outer list does not need to Concurrent/Synchronized because once it is setup it is never changed, it could
   * equally be constructed for a basic array.
   */
  private ArrayList<ConcurrentLinkedQueue<Runnable>> wheel           = new ArrayList<ConcurrentLinkedQueue<Runnable>>();

  /**
   * Executor service with a fixed thread pool for running the timeout runnable. We use this to avoid processing on the tick thread
   */
  protected ExecutorService                            timeoutExecutor = Executors.newFixedThreadPool(16);

  /**
   * Constructor for TimeoutManager
   * 
   * @param interval
   *          in milliseconds between ticks
   * @param segments
   *          number of segments on the wheel
   * @param defaultTimeout
   *          default timeout
   * @throws MaxTimeoutExceeded
   */
  public TimeoutManager(int interval, int segments, int defaultTimeout) throws MaxTimeoutExceeded {
    super();

    this.interval = interval;
    this.defaultTimeout = defaultTimeout;

    // Check the constructed wheel can handle the default timeout
    if (defaultTimeout > segments) {
      throw new MaxTimeoutExceeded("The maximum default timeout available is " + segments
          + ", try creating a TimeoutManager with more segments");
    }
    logger.info("Created new TimeoutManager with interval:{}, segments:{}, default:{}", interval, segments, defaultTimeout);
    // Use twice as many segments. This helps with role over from max to min and also provides time for processing of the queue.
    // Otherwise there is a danger of processing a queue whilst a new timeout comes into that segment. By increasing the size of the
    // wheel we gives ourselves time to perform the processing without having to clone or lock the wheel. It is worth noting that
    // "processing time" during which a collision could happen is only the time needed to pass the runnable to the executor, not the
    // time to actually process the Runnable
    this.segments = segments * 2;

    // Create wheel
    for (int i = 0; i < this.segments; i++) {
      this.wheel.add(new ConcurrentLinkedQueue<Runnable>());
    }
  }

  /**
   * Add default timeout Task
   * 
   * Since most of our timeouts are default timeouts we provide a utility method to save constantly checking the configured timeout
   * period
   * 
   * @param task
   *          Runnable task
   */
  public void addDefaultTimeout(Runnable task) {
    int target = (this.currentTick + (this.defaultTimeout)) % this.segments;
    this.wheel.get(target).add(task);
  }

  /**
   * Add a new timeout task. The actual task can be arbitrary as long as it implements Runnable
   * 
   * @param task
   *          Object that implements Runnable - will be run when timeout occurs
   * @param timeout
   *          the period for the timeout
   * @throws MaxTimeoutExceeded
   */
  public void addTimeout(Runnable task, int timeout) throws MaxTimeoutExceeded {
    // Check the timeout does not exceed segments
    if (timeout > this.segments / 2) {
      throw new MaxTimeoutExceeded("The maximum timeout available is " + this.segments
          + ", try creating a TimeoutManager with more segments");
    }

    // Calculate target segment, get segment and add task
    int target = (this.currentTick + (timeout)) % this.segments;

    this.wheel.get(target).add(task);
  }

  /**
   * The actual clock that ticks
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    while (!this.shutdown) {
      // Check the next segment
      ConcurrentLinkedQueue<Runnable> queue = this.wheel.get(this.currentTick);
      this.currentTick++;

      // Increment currentTick and reset if we are beyond max segments
      if (this.currentTick >= this.segments) {
        this.currentTick = 0;
      }

      // Run through each task in queue and add to the Executor
      Runnable currentTask;
      while ((currentTask = queue.poll()) != null) {
        this.timeoutExecutor.execute(currentTask);
      }
      try {
        // Sleep for the interval
        Thread.sleep(this.interval);
      }
      catch (InterruptedException e) {
        logger.warn("TimeoutManager was interrupted whilst waiting");
        // Do nothing - we will run again.
      }
    }
  }

  /**
   * Shuts down the manager.
   */
  public void shutdown() {
    logger.info("Shutdown called on TimeoutManager");
    this.shutdown = true;
  }
}
