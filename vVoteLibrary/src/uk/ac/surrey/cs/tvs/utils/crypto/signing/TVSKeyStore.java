/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import it.unisa.dia.gas.crypto.jpbc.signature.bls01.params.BLS01PublicKeyParameters;
import it.unisa.dia.gas.jpbc.Element;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateException;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.io.Streams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPublicKey;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import ximix.util.BLSKeyStore;
import ximix.util.BLSPublicKeyFactory;
import ximix.util.PartialPublicKeyInfo;

/**
 * TVSKeyStore provides a unified keystore for both BLS and non-BLS key pairs. BLS keys cannot be stored in a standard java KeyStore
 * because they cannot be constructed into the standard PublicKey and PrivateKey. Those classes can't be extended to provide the
 * additional functionality and neither can a KeyStore object. Additionally, it is not possible to construct a standards compliant
 * certificate from a BLS key because no CA currently supports the algorithm. As such, we are forced to store the BLS keys outside
 * of the normal stores.
 * 
 * This class wraps a custom BLS keystore and a standard Java KeyStore to provide seamless integration between the two. We do not
 * implement all the methods associated with a Java keyStore since most operations require the Keystore to be passed to the
 * underlying object, as such, we override the appropriate methods (getCertificate) but in all other situations when call something
 * that requires access to standard Java keys the getKeyStore method should be called and then interrogated directly.
 * 
 * Note: the BLS keystore always takes priority when using methods to get generic types (getCertificate). The class checks if the
 * key exists in the BLS keystore and if it does it will return it, otherwise it will look in the underlying Java keystore.
 * 
 * The intended philosophy is that a TVSKeyStore should be used whenever combing BLS key and standard keys. If you know in advance
 * that no BLS keys will be used, for example, during SSL connections, it is better to load a standard KeyStore directly. However,
 * for situations where both types of keys could be encountered, for example, creating and verifying signatures, it is better to
 * create a TVSKeyStore. The TVSKeyStore is just a JSONObject, with entries for each BLS key stored and a field called, with the
 * name specified in JKSPATH, that provides the path to an underlying Java KeyStore. When loading the TVSKeyStore the favoured
 * approach is to load via a String path to the JSONObject, which should have the JKSPATH field set, this will automatically load
 * both the BLS keystore and the underlying Java KeyStore, taking care to shut any streams that are opened during that reading.
 * Methods are provided for direct loading of the Java KeyStore, but they are not the recommended approach.
 * 
 * Note: The underlying Java KeyStore can have a password associated with it, however, currently the BLS KeyStore is stored in
 * plaintext. TODO add PBE based encryption to secure the BLS KeyStore
 * 
 * @author Chris Culnane
 * 
 */
public class TVSKeyStore {

  /**
   * Static string constant that specifies the field in the JSONObject that contains the path to the underlying KeyStore
   */
  public static final String  JKSPATH                   = "jksPath";
  /**
   * Logger
   */
  private static final Logger logger                    = LoggerFactory.getLogger(TVSKeyStore.class);

  /**
   * Constant string for BLS PrivateKey entry in JSONObject
   */
  private static final String PRIVATE_KEY               = "privateKey";

  /**
   * Constant String for BLS PublicKey entry in JSONObject
   */
  private static final String PUBLIC_KEY                = "pubKeyEntry";

  /**
   * Constant for retrieving BLS entries from Ximix BLS PKCS#12 key stores
   */
  private static final String XIMIX_BLS_SIGNATURE_ENTRY = "BLSSIGKEY";

  /**
   * Static utility method for extracting the BLS Key from a Ximix formatted PKCS12 keystore. It will extract the key pairs and
   * store the pair in a private TVSKeyStore file and the public key into a separate TVSKeyStore to allow them to be distributed
   * more widely.
   * 
   * The generated TVSKeyStore will be a pure BLS KeyStore without any Java KeyStore specified. This is to make moving the KeyStore
   * into the appropriate location easier and to make combining the public key stores easier.
   * 
   * If wondering why we aren't using the BLSKeyStore directly, it doesn't currently expose any methods for adding keys
   * 
   * @param ximixStorePath
   *          String path to Ximix PKCS12(.p12) BLS file (for example bls_node1.p12)
   * @param password
   *          String password on the Ximix PKCS12 file
   * @param alias
   *          String alias to store keys under in the new KeyStore (suggest PeerID + appropriate Suffix)
   * @param publicOutput
   *          String path to store Public TVSKeyStore to
   * @param privateOutput
   *          String path to store the Private TVSKeyStore to
   * @throws TVSKeyStoreException
   */
  public static void extractBLSKeysFromPKCS12(String ximixStorePath, String password, String alias, String publicOutput,
      String privateOutput) throws TVSKeyStoreException {
    try {
      logger.info("Starting extraction of BLS Keys from Ximix PKCS12 file at {}", ximixStorePath);

      // Add the provider
      Security.addProvider(new BouncyCastleProvider());

      // Construct the Ximix BLSKeyStore (PKCS12 file)
      BLSKeyStore keyStore = new BLSKeyStore();
      // Load the keystore
      keyStore.load(password.toCharArray(), Streams.readAll(new FileInputStream(ximixStorePath)));

      // Get the PublicKeyInfo
      SubjectPublicKeyInfo pubKeyInfo = keyStore.fetchPublicKey(XIMIX_BLS_SIGNATURE_ENTRY);
      logger.info("Got SubjectPublicKeyInfo");

      // Gets the PublicKeyParameters - BLS01PublicKeyParameters is a jPBC class so there converts from the underlying structure
      BLS01PublicKeyParameters pubKeyParams = BLSPublicKeyFactory.createKey(pubKeyInfo);

      // Gets the partial private key - this is the threshold secret share
      Element privKeyShare = keyStore.getPartialPrivateKey(XIMIX_BLS_SIGNATURE_ENTRY);

      // Wrap the raw Element in our BLSPrivateKey object
      BLSPrivateKey blsPrivateKey = new BLSPrivateKey(privKeyShare);
      logger.info("Got PrivateKey and Wrapped in BLSPrivateKey");

      // Get the generator from the public key params
      Element g = pubKeyParams.getParameters().getG();

      // Gets the joint public key
      Element jointPublicKey = pubKeyParams.getPk();

      // Get the partial public key info and get the partial public key from it
      PartialPublicKeyInfo partialPubKeyInfo = keyStore.fetchPartialPublicKey(XIMIX_BLS_SIGNATURE_ENTRY);
      Element partialPublicKey = BLSPublicKeyFactory.createKey(partialPubKeyInfo.getPartialKeyInfo()).getPk();

      // Construct out BLSPublicKey object, pulling the sequence number from the paritalPublicKeyInfo
      BLSPublicKey blsPublicKey = new BLSPublicKey(jointPublicKey, g, partialPublicKey, partialPubKeyInfo.getSequenceNo());
      logger.info("Got PublicKey and Wrapped in BLSPublicKey");

      File outputFolder = new File(publicOutput);
      if (!outputFolder.getParentFile().exists()) {
        if(outputFolder.getParentFile().mkdirs()){
          logger.info("Created output folder for public files {}", outputFolder.getAbsolutePath());
        }else{
          throw new IOException("Could not create public output directory:" + outputFolder.getAbsolutePath());
        }
      }
      outputFolder = new File(privateOutput);
      if (!outputFolder.getParentFile().exists()) {
        if(outputFolder.getParentFile().mkdirs()){
          logger.info("Created output folder for private files {}", outputFolder.getAbsolutePath());
        }else{
          throw new IOException("Could not create private output directory:" + outputFolder.getAbsolutePath());
        }
      }
      // Create a new TVSKeyStore for private keys - use default KeyStore type, it isn't important because we won't save the Java
      // keyStore
      TVSKeyStore tvsKeyStorePrivate = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeyStorePrivate.addBLSKeyPair(blsPublicKey, blsPrivateKey, alias);
      tvsKeyStorePrivate.store(privateOutput);
      logger.info("Stored private keystore to {}", privateOutput);
      // Create a new TVSKeyStore for public keys - use default KeyStore type, it isn't important because we won't save the Java
      // keyStore
      TVSKeyStore tvsKeyStorePublic = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeyStorePublic.addBLSPublicKey(blsPublicKey, alias);
      tvsKeyStorePublic.store(publicOutput);
      logger.info("Stored public keystore to {}", publicOutput);
    }
    catch (IOException e) {
      throw new TVSKeyStoreException(e);
    }
    catch (GeneralSecurityException e) {
      throw new TVSKeyStoreException(e);

    }
  }

  /**
   * Gets a new instance of a TVSKeyStore with an underlying Java KeyStore of the specified type
   * 
   * @param type
   *          String type for underlying Java KeyStore
   * @return TVSKeyStore wrapping an empty BLS KeyStore and a standard Java KeyStore
   * @throws KeyStoreException
   */
  public static TVSKeyStore getInstance(String type) throws KeyStoreException {
    return new TVSKeyStore(type);
  }

  /**
   * Gets a new instance of a TVSKeyStore with an underlying Java KeyStore of the specified type created using the specified
   * Provider
   * 
   * @param type
   *          String type for underlying Java KeyStore
   * @param provider
   *          Provider object to use when creating the underlying Java Keystore
   * @return TVSKeyStore wrapping an empty BLS KeyStore and a standard Java KeyStore
   * @throws KeyStoreException
   */
  public static TVSKeyStore getInstance(String type, Provider provider) throws KeyStoreException {
    return new TVSKeyStore(type, provider);
  }

  /**
   * Gets a new instance of a TVSKeyStore with an underlying Java KeyStore of the specified type created using the specified
   * Provider
   * 
   * @param type
   *          String type for underlying Java KeyStore
   * @param provider
   *          String name of the provider to use when creating the underlying Java Keystore
   * @return TVSKeyStore wrapping an empty BLS KeyStore and a standard Java KeyStore
   * @throws KeyStoreException
   * @throws NoSuchProviderException
   */
  public static TVSKeyStore getInstance(String type, String provider) throws KeyStoreException, NoSuchProviderException {
    return new TVSKeyStore(type, provider);
  }

  /**
   * Underlying BLSStore, which is JSONObject with entries for each alias
   */
  private JSONObject blsStore = new JSONObject();

  /**
   * Underlying java keystore associated with this TVSKeyStore
   */
  protected KeyStore keyStore;

  /**
   * Constructs a TVSKeyStore from a standard Java KeyStore. An empty BLS KeyStore will exist, but nothing will be loaded into it.
   * 
   * @param keyStore
   *          KeyStore containing standard Java keys
   */
  public TVSKeyStore(KeyStore keyStore) {
    this.keyStore = keyStore;
  }

  /**
   * Private constructor used for creating a new empty TVSKeyStore with an empty BLS key store and a new Java KeyStore of the type
   * specified in the type parameter. Note, this does not initialise the underlying Java KeyStore, it must be loaded as a normal
   * KeyStore would be.
   * 
   * @param type
   *          String type for underlying Java KeyStore
   * @throws KeyStoreException
   */
  private TVSKeyStore(String type) throws KeyStoreException {
    keyStore = KeyStore.getInstance(type);
  }

  /**
   * Private constructor used for creating a new empty TVSKeyStore with an empty BLS key store and a new Java KeyStore of the type
   * specified in the type parameter. Note, this does not initialise the underlying Java KeyStore, it must be loaded as a normal
   * KeyStore would be.
   * 
   * @param type
   *          String type for underlying Java KeyStore
   * @param provider
   *          Provider object to use when creating the underlying Java Keystore
   * @throws KeyStoreException
   */
  private TVSKeyStore(String type, Provider provider) throws KeyStoreException {
    keyStore = KeyStore.getInstance(type, provider);

  }

  /**
   * Private constructor used for creating a new empty TVSKeyStore with an empty BLS key store and a new Java KeyStore of the type
   * specified in the type parameter. Note, this does not initialise the underlying Java KeyStore, it must be loaded as a normal
   * KeyStore would be.
   * 
   * @param type
   *          String type for underlying Java KeyStore
   * @param provider
   *          String name of the provider to use when creating the underlying Java Keystore
   * @throws KeyStoreException
   */
  private TVSKeyStore(String type, String provider) throws KeyStoreException, NoSuchProviderException {
    keyStore = KeyStore.getInstance(type, provider);
  }

  /**
   * Adds a BLSKeyPair, containing a BLSPublicKey and BLSPrivateKey, to the BLS KeyStore under the specified alias
   * 
   * @param pair
   *          BLSKeyPair to add to KeyStore
   * @param alias
   *          String alias to store the entry under
   * @throws TVSKeyStoreException
   */
  public void addBLSKeyPair(BLSKeyPair pair, String alias) throws TVSKeyStoreException {
    addBLSKeyPair(pair.getPublicKey(), pair.getPrivateKey(), alias);
  }

  /**
   * Adds a BLSPublicKey and BLSPrivateKey pair to the BLS KeyStore under the specified alias
   * 
   * @param pubKey
   *          BLSPublicKey to add to KeyStore
   * @param privKey
   *          BLSPrivateKey associated with pubKey to add to KeyStore
   * @param alias
   *          String alias to store the entry under
   * @throws TVSKeyStoreException
   */
  public void addBLSKeyPair(BLSPublicKey pubKey, BLSPrivateKey privKey, String alias) throws TVSKeyStoreException {
    try {
      JSONObject aliasEntry = new JSONObject();
      aliasEntry.put(PUBLIC_KEY, pubKey.toJSON());
      aliasEntry.put(PRIVATE_KEY, privKey.toJSON());
      this.blsStore.put(alias, aliasEntry);
    }
    catch (JSONException e) {
      throw new TVSKeyStoreException(e);
    }
  }

  /**
   * Copies the BLS KeyStore entries from the specified TVSKeyStore into this KeyStore. This can be useful when combining multiple
   * public key KeyStores into a single unified TVSKeyStore. This will not copy any underlying entries from the Java KeyStore
   * 
   * @param keyStoreToAdd
   *          TVSKeyStore to copy BLS KeyStore entries from
   * @throws TVSKeyStoreException
   */
  public void addBLSKeyStoreEntries(TVSKeyStore keyStoreToAdd) throws TVSKeyStoreException {
    try {
      // Gets list of alias names
      JSONArray names = keyStoreToAdd.blsStore.names();
      // Loop through aliases copying them into this keystore
      for (int i = 0; i < names.length(); i++) {
        this.blsStore.put(names.getString(i), keyStoreToAdd.blsStore.getJSONObject(names.getString(i)));
      }
    }
    catch (JSONException e) {
      throw new TVSKeyStoreException("Cannot access underlying JSON Store", e);
    }
  }

  /**
   * Adds a BLSPrivateKey to the BLSKeyStore under the specified alias. If the alias already exists the PrivateKey part will be
   * overwritten, but the PublicKey part will be left as is. This is not something that would normally occur, but handle situations
   * where additional data is added, this will only overwrite the data it is changing, rather than the entire entry.
   * 
   * @param privKey
   *          BLSPrivateKey to add
   * @param alias
   *          String alias to store it under
   * @throws TVSKeyStoreException
   */
  public void addBLSPrivateKey(BLSPrivateKey privKey, String alias) throws TVSKeyStoreException {
    try {
      JSONObject aliasEntry = new JSONObject();
      if (this.blsStore.has(alias)) {
        aliasEntry = this.blsStore.getJSONObject(alias);
      }
      aliasEntry.put(PRIVATE_KEY, privKey.toJSON());
      this.blsStore.put(alias, aliasEntry);
    }
    catch (JSONException e) {
      throw new TVSKeyStoreException(e);
    }
  }

  /**
   * Adds a BLSPublicKey to the BLSKeyStore under the specified alias. If the alias already exists the PublicKey part will be
   * overwritten, but the PrivateKey part will be left as is. This might seem unusual, but could be useful during key generation,
   * where an incomplete public key might be know and then needs updating.
   * 
   * @param pubKey
   *          BLSPublicKey to add
   * @param alias
   *          String alias to store it under
   * @throws TVSKeyStoreException
   */
  public void addBLSPublicKey(BLSPublicKey pubKey, String alias) throws TVSKeyStoreException {
    try {
      JSONObject aliasEntry = new JSONObject();
      if (this.blsStore.has(alias)) {
        aliasEntry = this.blsStore.getJSONObject(alias);
      }
      aliasEntry.put(PUBLIC_KEY, pubKey.toJSON());
      this.blsStore.put(alias, aliasEntry);
    }
    catch (JSONException e) {
      throw new TVSKeyStoreException(e);
    }
  }

  /**
   * Adds a TVSCertificate to the relevant KeyStore with the specified alias. If the TVSCertificate wraps a BLSPublicKey it will be
   * added to the BLS KeyStore, otherwise it will be added to the Java KeyStore. If the KeyStore has not be initialised it will
   * throw a KeyStoreException
   * 
   * @param cert
   *          TVSCertificate of the certificate to add
   * @param alias
   *          String alias to list the certificate under
   * @throws KeyStoreException
   */
  public void addCertificate(TVSCertificate cert, String alias) throws KeyStoreException {
    if (cert.getBLSPublicKey() != null) {
      this.addBLSPublicKey(cert.getBLSPublicKey(), alias);
    }
    else {
      this.keyStore.setCertificateEntry(alias, cert.getCertificate());
    }
  }

  /**
   * Gets a BLSCertificate from the BLS KeyStore. This is just a simple wrapper of the getBLSPublicKey method that just wraps the
   * returned BLSPublicKey in a TVSCertificate object.
   * 
   * @param alias
   *          String alias to looks for in BLS keystore
   * @return TVSCertificate or null if not found in the BLS keystore
   * @throws TVSKeyStoreException
   */
  public TVSCertificate getBLSCertificate(String alias) throws TVSKeyStoreException {
    return new TVSCertificate(getBLSPublicKey(alias));
  }

  /**
   * Gets a String array of the aliases held in the BLS keyStore.
   * 
   * @return String[] containing the aliases that exist in the BLS KeyStore
   * @throws TVSKeyStoreException
   */
  public String[] getBLSEntries() throws TVSKeyStoreException {
    try {
      if (this.blsStore.names() == null) {
        return new String[0];
      }
      else {
        String[] names = new String[this.blsStore.names().length()];
        for (int i = 0; i < names.length; i++) {
          names[i] = this.blsStore.names().getString(i);
        }
        return names;
      }
    }
    catch (JSONException e) {
      throw new TVSKeyStoreException("Cannot access underlying JSON Store", e);
    }
  }

  /**
   * Gets the BLSPrivateKey associated with the specified alias, or null if it doesn't exist in the BLS KeyStore. This will not
   * check the underlying Java KeyStore since we know it cannot contain a BLSPrivateKey
   * 
   * @param alias
   *          String alias of PrivateKey to look for
   * @return BLSPrivateKey for the alias or null if not found
   * @throws TVSKeyStoreException
   */
  public BLSPrivateKey getBLSPrivateKey(String alias) throws TVSKeyStoreException {
    try {
      if (this.blsStore.has(alias)) {
        // We have an entry for that alias
        JSONObject aliasEntry = this.blsStore.getJSONObject(alias);
        // Check if there is a privatekey value in the entry and if there is return it
        if (aliasEntry.has(PRIVATE_KEY)) {
          return new BLSPrivateKey(aliasEntry.getJSONObject(PRIVATE_KEY));
        }
        else {
          return null;
        }
      }
      else {
        return null;
      }
    }
    catch (JSONException e) {
      throw new TVSKeyStoreException(e);
    }
  }

  /**
   * Gets a BLSPublicKey from the BLS KeyStore, this will not check the Java Keystore since it isn't possible to store BLS Keys in a
   * Java KeyStore
   * 
   * @param alias
   *          String alias of BLS Key to look for
   * @return BLSPublicKey or null if not found
   * @throws TVSKeyStoreException
   */
  public BLSPublicKey getBLSPublicKey(String alias) throws TVSKeyStoreException {
    try {
      if (this.blsStore.has(alias)) {
        JSONObject aliasEntry = this.blsStore.getJSONObject(alias);
        if (aliasEntry.has(PUBLIC_KEY)) {
          return new BLSPublicKey(aliasEntry.getJSONObject(PUBLIC_KEY));
        }
        else {
          return null;
        }
      }
      else {
        return null;
      }
    }
    catch (JSONException e) {
      throw new TVSKeyStoreException(e);
    }
  }

  /**
   * Gets a TVSCertificate for the specified alias. This method will first check if there is a BLS entry for the alias and if there
   * is it will return the associated PublicKey. If there is no matching alias in the BLS keystore it will attempt to check the
   * underlying Java KeyStore. If there is a match in the BLS Keystore, but no matching PublicKey it will look in the Java KeyStore
   * 
   * Irrespective of which KeyStore the value is obtained from it will be wrapped in a TVSCertificate and returned. If the alias is
   * not found in either KeyStore null will be returned.
   * 
   * @param alias
   *          String alias to look for in the keystores
   * @return TVSCertificate wrapping the underlying certificate/public key
   * @throws KeyStoreException
   */
  public TVSCertificate getCertificate(String alias) throws KeyStoreException {
    if (this.blsStore.has(alias)) {
      try {
        if (this.blsStore.getJSONObject(alias).has(PUBLIC_KEY)) {
          return new TVSCertificate(getBLSPublicKey(alias));
        }
      }
      catch (JSONException e) {
        // we ignore this exception because the .has guard should protect it from ever happening. It can only happen if the BLS
        // store is invalid, will look in Java KeyStore instead.
      }
    }
    // If we get here it wasn't in the BLS store, check the Java KeyStore
    return new TVSCertificate(this.keyStore.getCertificate(alias));

  }

  /**
   * Gets the underlying Java KeyStore associated with this TVSKeyStore
   * 
   * @return KeyStore containing non-BLS keys and certificates
   */
  public KeyStore getKeyStore() {
    return this.keyStore;
  }

  /**
   * Loads the underlying Java KeyStore using the specified InputStream object and char[] password. This just calls the load method
   * on the underlying Java KeyStore object. Note, the KeyStore does not manage the resource closing for you, you will need to close
   * the passed in InputStream yourself.
   * 
   * @param stream
   *          InputStream to load the underlying KeyStore from
   * @param password
   *          char[] password to access the KeyStore
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws IOException
   */
  public void load(InputStream stream, char[] password) throws NoSuchAlgorithmException, CertificateException, IOException {
    keyStore.load(stream, password);
  }

  /**
   * Initialises the underlying Java Keystore
   * 
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws IOException
   */
  public void initKeyStore() throws NoSuchAlgorithmException, CertificateException, IOException {
    keyStore.load(null);
  }

  /**
   * Loads a BLSKeyStore with the JSONObject in the file specified in blsStorePath. The char[] password currently only applies to
   * the underlying Java KeyStore, not the BLS KeyStore which are stored in plaintext. This method reads the JSONObject from the
   * file and then checks if it has the JKSPATH field set and if it does it will load the underlying Java keyStore with the contents
   * of the file specified in the JKSPATH field. Otherwise a new Java KeyStore is initialised but no JKSPATH will not be set. To
   * create the appropriate field value and store the newly created Java KeyStore the appropriate store must be called: @see
   * TVSKeyStore#store(String, String, char[])
   * 
   * @param blsStorePath
   *          String path the file containing JSONObject of BLS KeyStore
   * @param password
   *          char[] password to use for underlying Java KeyStore, if specified by the BLS KeyStore
   * @throws TVSKeyStoreException
   */
  public void load(String blsStorePath, char[] password) throws TVSKeyStoreException {
    try {
      logger.info("Loading BLSKeyStore from {}", blsStorePath);
      this.blsStore = IOUtils.readJSONObjectFromFile(blsStorePath);
      if (this.blsStore.has(JKSPATH)) {
        logger.info("Has jksPath set will loading keystore at {}", this.blsStore.getString(JKSPATH));
        this.keyStore = CryptoUtils.loadKeyStore(this.blsStore.getString(JKSPATH), password);
      }
      else {
        logger.info("No jksPath set, will create new Java KeyStore");
        this.keyStore.load(null, password);
      }
    }
    catch (JSONException e) {
      throw new TVSKeyStoreException(e);
    }
    catch (JSONIOException e) {
      throw new TVSKeyStoreException(e);
    }
    catch (CryptoIOException e) {
      throw new TVSKeyStoreException(e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new TVSKeyStoreException(e);
    }
    catch (CertificateException e) {
      throw new TVSKeyStoreException(e);
    }
    catch (IOException e) {
      throw new TVSKeyStoreException(e);
    }
  }

  /**
   * Stores a TVSKeyStore to file, saving both the BLS KeyStore and the underlying JavaKeyStore if the BLSKeyStore JSONObject
   * contains a value for JKSPATH. This is the normal way for opening and storing a TVSKeyStore that has already been previously
   * initialised. It requires only the output location of the JSON to be specified. This method defaults the underlying Java
   * KeyStore password am empty string
   * 
   * This method will throw an exception if it is called on a TVSKeyStore that does not have a JKSPATH set and has entries in the
   * underlying Java KeyStore.
   * 
   * 
   * @param blsStorePath
   *          blsStorePath String path to store the BLS KeyStore to
   * @throws IOException
   * @throws TVSKeyStoreException
   */
  public void store(String blsStorePath) throws IOException, TVSKeyStoreException {
    store(blsStorePath, "".toCharArray());

  }

  /**
   * Stores a TVSKeyStore to file, saving both the BLS KeyStore and the underlying JavaKeyStore if the BLSKeyStore JSONObject
   * contains a value for JKSPATH. This is the normal way for opening and storing a TVSKeyStore that has already been previously
   * initialised. It requires only the output location of the JSON to be specified. This method can also be used to store a
   * TVSKeyStore that has no underlying Java KeyStore - in such circumstances the password value is ignored.
   * 
   * This method will throw an exception if it is called on a TVSKeyStore that does not have a JKSPATH set and has entries in the
   * underlying Java KeyStore.
   * 
   * @param blsStorePath
   *          String path to store the BLS KeyStore to
   * @param password
   *          char[] password - only used if there is an underlying Java KeyStore
   * @throws IOException
   * @throws TVSKeyStoreException
   */
  public void store(String blsStorePath, char[] password) throws IOException, TVSKeyStoreException {
    try {
      int keyStoreSize = 0;
      try {
        keyStoreSize = this.keyStore.size();
      }
      catch (KeyStoreException e) {
        // ignore this exception because it will fire if no underlying keystore has been initialised
      }
      if (this.blsStore.has(JKSPATH)) {
        CryptoUtils.storeKeyStore(this.keyStore, this.blsStore.getString(JKSPATH), password);
      }
      else if (keyStoreSize > 0) {
        throw new TVSKeyStoreException(
            "Storing TVSKeyStore without setting path for Java Keystore even though Java KeyStore is not empty");
      }
      IOUtils.writeJSONToFile(this.blsStore, blsStorePath);
    }
    catch (JSONIOException e) {
      throw new IOException(e);
    }
    catch (CryptoIOException e) {
      throw new IOException(e);
    }
    catch (JSONException e) {
      throw new IOException(e);
    }
  }

  /**
   * Stores a TVSKeyStore to file, saving both the BLS KeyStore and the underlying JavaKeyStore to the specified locations, updating
   * the JKSPATH to point to the correct location. The char[] password currently only applies to the Java KeyStore. The BLS KeyStore
   * will be stored in plaintext
   * 
   * @param blsStorePath
   *          String path to save BLS KeyStore to
   * @param keyStorePath
   *          String path to store Java KeyStore to
   * @param password
   *          char[] password to apply to the Java KeyStore
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws IOException
   */
  public void store(String blsStorePath, String keyStorePath, char[] password) throws KeyStoreException, NoSuchAlgorithmException,
      CertificateException, IOException {

    try {
      // Store the Java KeyStore
      CryptoUtils.storeKeyStore(this.keyStore, keyStorePath, password);
      // Set the JKSPATH to the updated Java KeyStore path
      this.blsStore.put(JKSPATH, keyStorePath);
      // Save the JSONObject of the BLSKeyStore to the specified path
      IOUtils.writeJSONToFile(this.blsStore, blsStorePath);
    }
    catch (JSONIOException e) {
      throw new IOException(e);
    }
    catch (JSONException e) {
      throw new IOException(e);
    }
    catch (CryptoIOException e) {
      throw new IOException(e);
    }
  }

  /**
   * Gets the JSONObject that holds the underlying BLS KeyStore
   * 
   * @return JSONObject containing the BLS Keystore
   */
  public JSONObject getBLSKeyStore() {
    return this.blsStore;
  }

  /**
   * Utility method for exporting the publickey component of an entry as a new TVSKeyStore. This is useful when sharing the public
   * keys, since TVSKeyStore provides a way of importing the a keystore into itself, keeping the alias the same, saving the user
   * from having to enter the alias themselves. This is important because the certificate will be looked up based on the ID sent in
   * the message, so the alias needs to match.
   * 
   * @param alias
   *          String alias to export
   * @param path
   *          String path to create exported TVSKeyStore
   * @throws KeyStoreException
   * @throws IOException
   */
  public void exportPublicKeyAsPublicStore(String alias, String path) throws KeyStoreException, IOException {
    BLSPublicKey pubKey = this.getBLSPublicKey(alias);
    if (pubKey != null) {
      TVSKeyStore tempStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tempStore.addBLSPublicKey(pubKey, alias);
      tempStore.store(path);
    }

  }

  /**
   * Gets the String path of the JavaKeyStore associated with this TVSKeyStore, if one exists. Otherwise it returns null.
   * 
   * @return String path of JavaKeyStore or null if one is not set
   */
  public String getJavaKeystorePath() {
    String path = null;
    try {
      path = this.blsStore.getString(TVSKeyStore.JKSPATH);
    }
    catch (JSONException e) {
      logger.warn("Exception whilst trying to get JKSPATH, will return null");
    }
    return path;
  }
}
