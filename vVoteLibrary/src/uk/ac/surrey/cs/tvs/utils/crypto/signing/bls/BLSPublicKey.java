/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Pairing;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Wrapper to hold a BLS Public Key. The wrapper can accommodate both threshold public key and normal public keys. The difference is
 * that a threshold public key contains the joint public key and a "partial" key that is used for verifying the signature share
 * before combining
 * 
 * @author Chris Culnane
 * 
 */
public class BLSPublicKey {

  /**
   * Element to hold the Joint Public Key value
   */
  private Element             publicKey          = null;
  /**
   * Element to hold the partial public key value, used to validate signature shares - will only be present in threshold public key
   */
  private Element             partialPublicKey   = null;

  /**
   * Element containing the generator g for the public key values
   */
  private Element             g                  = null;

  /**
   * If this is a threshold key there will also be a sequence number (used for lagrange interpolation)
   */
  private int                 sequenceNo         = -1;

  /**
   * Constant String of the JSON field name for a partialPublicKey
   */
  private static final String PARTIAL_PUBLIC_KEY = "partialPublicKey";

  /**
   * Constant String of the JSON field name for a full (joint in the threshold case) public key
   */
  private static final String PUBLIC_KEY         = "publicKey";

  /**
   * Constant String of the JSON field name for the public key generator g
   */
  private static final String G                  = "g";

  /**
   * Constant String of the JSON field name for the sequence number associated with a key share
   */
  private static final String SEQUENCE_NO        = "sequenceNo";

  /**
   * Constructs a new BLSPublicKey from a suitably formatted JSONObject. The JSONObject should have the PUBLIC_KEY and G fields at a
   * minimum. If the key represents a threshold public key it should also have the SEQUENCE_NO and PARTIAL_PUBLIC_KEY fields set.
   * 
   * If a field is not set it will be ignored. As such, it would be possible to construct an empty BLSPublicKey object, if that was
   * used at any point it will cause a null pointer exception
   * 
   * @param publicKeyObj
   *          JSONObject with the appropriate fields set
   * @throws JSONException
   */
  public BLSPublicKey(JSONObject publicKeyObj) throws JSONException {
    // Create a pairing object, which we will need for loading the key
    Pairing pairing = CurveParams.getInstance().getPairing();

    // Check if there is a partial public key
    if (publicKeyObj.has(PARTIAL_PUBLIC_KEY)) {
      // Construct a new element
      partialPublicKey = pairing.getG2().newElement();
      // Set the element value from the bytes decoded from the JSON
      partialPublicKey.setFromBytes(IOUtils.decodeData(EncodingType.BASE64, publicKeyObj.getString(PARTIAL_PUBLIC_KEY)));
    }
    if (publicKeyObj.has(PUBLIC_KEY)) {
      // Construct a new element
      publicKey = pairing.getG2().newElement();
      // Set the element value from the bytes decoded from the JSON
      publicKey.setFromBytes(IOUtils.decodeData(EncodingType.BASE64, publicKeyObj.getString(PUBLIC_KEY)));
    }
    if (publicKeyObj.has(SEQUENCE_NO)) {
      // If sequence number is present set the value
      sequenceNo = publicKeyObj.getInt(SEQUENCE_NO);
    }
    if (publicKeyObj.has(G)) {
      // Construct a new element
      g = pairing.getG2().newElement();
      // Set the element value from the bytes decoded from the JSON
      g.setFromBytes(IOUtils.decodeData(EncodingType.BASE64, publicKeyObj.getString(G)));
    }
  }

  /**
   * Construct a BLSPublicKey from just the PublicKey element. Strictly speaking this should not be used, since the generator will
   * be needed at a later point. However, the generator is fixed for all keys generated in a threshold group, so may not be set in
   * the public key file. This constructor is to provide flexibility, but should only be used if the generator G is being stored
   * somewhere else.
   * 
   * @param publicKey
   *          Element containing the public key
   */
  public BLSPublicKey(Element publicKey) {
    this.publicKey = publicKey;
  }

  /**
   * Construct a BLSPublicKey from an element containing the public key and an element containing the generator g. This is likely to
   * be a non-threshold key since the sequence number and partial public key are not set.
   * 
   * @param publicKey
   *          Element containing the public key
   * @param g
   *          Element containing the generator g
   */
  public BLSPublicKey(Element publicKey, Element g) {
    this.publicKey = publicKey;
    this.g = g;
  }

  /**
   * Constructs a threshold BLSPublicKey from a public key Element, Generator Element g and a partial public key Element. This
   * constructor should only be used in circumstances where the sequence number is stored elsewhere or can be implied from some
   * other information.
   * 
   * @param publicKey
   *          Element containing the joint public key
   * @param g
   *          Element containing the generator
   * @param partialPublicKey
   *          Element with the partial public key for the corresponding share of the secret key
   */
  public BLSPublicKey(Element publicKey, Element g, Element partialPublicKey) {
    this.partialPublicKey = partialPublicKey;
    this.publicKey = publicKey;
    this.g = g;
  }

  /**
   * Constructs a complete threshold public key, consisting of a Joint public key element, generator element, partial public key
   * element and sequence number. The information contained within this public key is sufficient to both verify an individual
   * signature share and provide the necessary information to combine the signature share into a joint signature.
   * 
   * @param publicKey
   *          Element containing the joint public key
   * @param g
   *          Element containing the generator
   * @param partialPublicKey
   *          Element with the partial public key for the corresponding share of the secret key
   * @param sequenceNo
   *          integer sequence number
   */
  public BLSPublicKey(Element publicKey, Element g, Element partialPublicKey, int sequenceNo) {
    this.partialPublicKey = partialPublicKey;
    this.publicKey = publicKey;
    this.sequenceNo = sequenceNo;
    this.g = g;
  }

  /**
   * Encodes the BLSPublicKey as a JSONObject converting any existing elements into Base64 encoded strings and setting them in the
   * appropriate fields. If a value is unset or does not exist it is not exported.
   * 
   * @return JSONObject with the appropriate fields set for the values that exist in this instance
   * @throws JSONException
   */
  public JSONObject toJSON() throws JSONException {
    // Create a new JSON object
    JSONObject store = new JSONObject();

    if (this.partialPublicKey != null) {
      store.put(PARTIAL_PUBLIC_KEY, IOUtils.encodeData(EncodingType.BASE64, this.partialPublicKey.toBytes()));
    }
    if (this.publicKey != null) {
      store.put(PUBLIC_KEY, IOUtils.encodeData(EncodingType.BASE64, this.publicKey.toBytes()));
    }
    if (this.g != null) {
      store.put(G, IOUtils.encodeData(EncodingType.BASE64, this.g.toBytes()));
    }
    if (this.sequenceNo >= 0) {
      store.put(SEQUENCE_NO, this.sequenceNo);
    }
    return store;

  }

  /**
   * Gets the generator g for this public key
   * 
   * @return Element g containing the generator
   */
  public Element getG() {
    return this.g;
  }

  /**
   * Gets the Element containing the underlying public key. In a threshold setting this is the joint public key.
   * 
   * @return Element containing the public key
   */
  public Element getPublicKey() {
    return this.publicKey;
  }

  /**
   * Gets the partial public key Element associated with this public key. This will only be set if this is a threshold public key,
   * otherwise it will return null.
   * 
   * @return Element containing the partial public key, or null if not set
   */
  public Element getPartialPublicKey() {
    return this.partialPublicKey;
  }

  /**
   * Gets the sequence number associated with this public key. This is only relevant in the threshold setting. If it has not been
   * set it will return -1.
   * 
   * @return int sequence number or -1 if not set
   */
  public int getSequenceNo() {
    return this.sequenceNo;
  }

}
