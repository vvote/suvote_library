/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import it.unisa.dia.gas.jpbc.Element;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * A wrapper class to represent a BLSPrivateKey. Ideally this would have been included within a JCE implementation, however, that
 * wasn't possible so we have to wrap the underlying PBC Element to provide some utility functions. Due to restrictions imposed by
 * the JVM we cannot extend PrivateKey, hence a standalone BLSPrivateKey
 * 
 * @author Chris Culnane
 * 
 */
public class BLSPrivateKey {

  /**
   * JPBC Element that contains the actual private key
   */
  private Element             privateKey  = null;

  /**
   * String constant holding the field name of the private key in a JSON object
   */
  private static final String PRIVATE_KEY = "privatekey";

  /**
   * Constructs a BLSPrivateKey from a suitable formatted JSONObject. The JSONObject should have a Base64 encoded String of the
   * private key element, in a field with the name specified in PRIVATE_KEY constant
   * 
   * @param privKeyObj JSONObject with PRIVATE_KEY field containing the private key string
   * @throws JSONException
   */
  public BLSPrivateKey(JSONObject privKeyObj) throws JSONException {
    //Create a new element
    privateKey = CurveParams.getInstance().getPairing().getZr().newElement();
    
    //Sets the element from the decoded bytes obtained from the JSONObject
    privateKey.setFromBytes(IOUtils.decodeData(EncodingType.BASE64, privKeyObj.getString(PRIVATE_KEY)));
  }

  /**
   * Constructs a BLSPrivateKey directly from a JPBC Element
   * @param privateKey Element containing the private key
   */
  public BLSPrivateKey(Element privateKey) {
    this.privateKey = privateKey;
  }

  /**
   * Constructs a suitable formatted JSONObject containing the private key encoded as a Base64 String stored in a field with the name specified by PRIVATE_KEY
   * @return JSONObject containing a single field with the Base64 encoded Private Key Element 
   * @throws JSONException
   */
  public JSONObject toJSON() throws JSONException {
    JSONObject store = new JSONObject();
    store.put(PRIVATE_KEY, IOUtils.encodeData(EncodingType.BASE64, this.privateKey.toBytes()));
    return store;
  }

  /**
   * Gets the underlying Element containing the private key
   * @return Element containing the private key
   */
  public Element getKey() {
    return this.privateKey;
  }
}
