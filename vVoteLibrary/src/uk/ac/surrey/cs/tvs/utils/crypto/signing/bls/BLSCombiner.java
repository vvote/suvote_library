/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import it.unisa.dia.gas.jpbc.Element;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import ximix.util.LagrangeWeightCalculator;

/**
 * Utility class for combining BLS signatures shares into a single joint signature. Note: this class does not test the validity of
 * the shares, that should be done using the BLSSignature class. This only combines shares.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class BLSCombiner {

  /**
   * Logger
   */
  private static final Logger logger     = LoggerFactory.getLogger(BLSCombiner.class);
  /**
   * Array of Elements, one for each peer (total number of key shares, even if we have less actual signature shares)
   */
  private Element[]           sigs;

  /**
   * Int threshold value - how many elements we need to successfully combine
   */
  private int                 threshold;

  /**
   * Int counter of shares we have received
   */
  private int                 shareCount = 0;

  /**
   * Constructs a new BLSCombiner, creating an element array of the appropriate size
   * 
   * @param nodes
   *          int number of nodes (key shares) that were created
   * @param threshold
   *          int threshold of shares needed to combined
   * @throws TVSSignatureException
   */
  public BLSCombiner(int nodes, int threshold) throws TVSSignatureException {
    if (threshold > nodes) {
      throw new TVSSignatureException("Cannot have a threshold greater than the number of nodes");
    }
    logger.info("Creating new combiner with {} peers and a threshold of {}", nodes, threshold);
    sigs = new Element[nodes];
    this.threshold = threshold;
  }

  /**
   * Add a valid signature share element to the combiner
   * 
   * @param share
   *          Element containing the signature share
   * @param idx
   *          int sequence/index number of the peer that provided the share (needed for Lagrange Interpolation)
   */
  public void addShare(Element share, int idx) {
    // We only increment the share count if we didn't have anything for this share before
    if (sigs[idx] == null) {
      shareCount++;
    }
    sigs[idx] = share;
  }

  /**
   * Add a valid signature share byte to the combiner, converting the byte array to an Element
   * 
   * @param share
   *          byte array containing the signature share
   * @param idx
   *          int sequence/index number of the peer that provided the share (needed for Lagrange Interpolation)
   */
  public void addShare(byte[] share, int idx) {
    // We only increment the share count if we didn't have anything for this share before
    if (sigs[idx] == null) {
      shareCount++;
    }
    sigs[idx] = BLSSignature.getSignatureElement(share);
  }

  /**
   * Gets the combined signature by calculating the Lagrange Interpolation and then combining the shares. It will throw an exception
   * if an insufficient number of shares have been added to the combiner.
   * 
   * @return Element containing the combined signature
   * @throws TVSSignatureException
   * 
   */
  public Element combined() throws TVSSignatureException {
    // Prepare a Element variable
    Element combined = null;
    if (shareCount >= threshold) {
      logger.info("Combining {} signature shares", shareCount);
      // If we have a threshold of shares calculate the LagrangeWeight. Note we pass sigs.length to the Lagrange calculator because
      // sigs.length = the total number of peers, not the number of shares we have set. If a share is missing it will be null in the
      // array, this is important in the lagrange calculation.
      LagrangeWeightCalculator weightCalculator = new LagrangeWeightCalculator(sigs.length, CurveParams.getInstance().getPairing()
          .getZr().getOrder());

      // Calculate the actual weights
      BigInteger[] weights = weightCalculator.computeWeights(sigs);

      // Step through the sigs array, applying the weight (if an element exists) and combining the values
      for (int i = 0; i < sigs.length; i++) {
        if (combined == null) {
          // This is the first element we are going to add to the combined value so just set it to the signature share multiplied by
          // the weight
          if (sigs[i] != null) {
            combined = sigs[i].mul(weights[i]);
          }
        }
        else {
          if (sigs[i] != null) {
            // If we have a signature, apply the weight and combine with the previous shares
            combined = combined.duplicate().mul(sigs[i].mul(weights[i]));
          }
        }
      }
    }
    else {
      logger.warn("Combined was called with only {} shares when the threshold was {}", shareCount, threshold);
      // We haven't received enough shares so throw an exception
      throw new TVSSignatureException("Not enough shares");
    }
    // Return the combined signature
    return combined;
  }

}
