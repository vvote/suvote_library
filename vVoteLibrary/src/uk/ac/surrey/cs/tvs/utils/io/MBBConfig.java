/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * A helper class for MBB config files - these files contains the information needed to interact with the MBB peers, for example, IP
 * Address, ports, thresholds etc.
 * 
 * @author Chris Culnane
 * 
 */
public class MBBConfig extends ConfigFile {

  /**
   * Stores a JSON object for each Peer listed in the config file. The JSON object provides access to the address and port of the
   * peer. It is indexed by the Peer ID
   */
  private Map<String, JSONObject> peers = new HashMap<String, JSONObject>();

  /**
   * Constructor for MBBConfig file using the default config file named config.conf. The MBBConfig file contains address information
   * for each peer.
   * 
   * @throws JSONIOException
   */
  public MBBConfig() throws JSONIOException {
    super();

    this.loadPeers();
  }

  /**
   * Constructor for MBBConfig file using the passed in config file path. The MBBConfig file contains address information for each
   * peer.
   * 
   * @param configFile
   *          path to config file to load
   * @throws JSONIOException
   */
  public MBBConfig(String configFile) throws JSONIOException {
    super(configFile);

    this.loadPeers();
  }

  /**
   * Gets the JSON object for the requested host
   * 
   * @param id
   *          PeerID of the required host
   * @return JSONObject containing host address and port
   */
  public JSONObject getHostDetails(String id) {
    return this.peers.get(id);
  }

  /**
   * Gets the details of the peers as defined in the config file
   * 
   * @return Map of the peers indexed by peer id
   */
  public Map<String, JSONObject> getPeers() {
    return this.peers;
  }

  /**
   * Loads the peers in the config file into the HashMap to allow easy access
   * 
   * @throws JSONIOException
   */
  private void loadPeers() throws JSONIOException {
    try {
      // Extract the peers from the configuration file
      JSONArray hostArray = this.conf.getJSONArray(uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.MBBConfig.PEERS);
      for (int i = 0; i < hostArray.length(); i++) {
        JSONObject host = hostArray.getJSONObject(i);
        String hostIndex = host.getString(uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.MBBConfig.PEER_ID);
        this.peers.put(hostIndex, host);
      }
    }
    catch (JSONException e) {
      throw new JSONIOException("Exception reading MBBConfig file", e);
    }
  }
}
