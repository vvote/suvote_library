/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Date;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v1CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v1CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.GenerateSignatureKeyAndCSR;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Base class for performing KeyGeneration, CSR construction and importing of Certificates to key stores. This class should be
 * extended by the various components to handle the particular requirements of each.
 * 
 * @author Chris Culnane
 * 
 */
public class KeyGeneration {

  /**
   * Logger
   */
  private static final Logger   logger             = LoggerFactory.getLogger(KeyGeneration.class);

  /**
   * This is the ID for the certificate generation. It will be used as part of the x.509 DN and for entity names and file names
   * where appropriate
   */
  protected String              id;

  /**
   * Organisation - override to change
   */
  protected static final String ORGANISATION       = "VEC";

  /**
   * Organisation Unit - override to change
   */
  protected static final String ORGANISATION_UNIT  = "vVote";

  /**
   * Country - override to change
   */
  protected static final String COUNTRY            = "AU";

  /**
   * Location - override to change
   */
  protected static final String LOCATION           = "Melbourne";

  /**
   * State - override to change
   */
  protected static final String STATE              = "Victoria";

  /**
   * Certificate life in days - override to change
   */
  protected static final int    CERTIFICATE_LIFE   = 720;

  /**
   * Constant pointing to the base keystore. This should be the keystore that has the CA root certificates in
   */
  protected static final String BASE_KEYSTORE_PATH = "./defaults/basekeystore.jks";

  /**
   * Constant for the file extension for certificate signing requests
   */
  protected static final String CSR_EXT            = ".csr";

  /**
   * Constant specifying the type of certificate build
   */
  protected static final String CERTIFICATE_TYPE   = "X.509";

  /**
   * Constructor for key generation, single parameter is the id to use throughout
   * 
   * @param id
   *          String id for the keys being generated
   */
  public KeyGeneration(String id) {
    super();

    this.id = id;
    logger.info("Created new KeyGeneration class with id: {}", id);
  }

  /**
   * Add a field to the distinguished name StringBuffer
   * 
   * @param dn
   *          StringBuffer used to build the distinguished name
   * @param dnKey
   *          String distinguished name field
   * @param value
   *          String value of field
   */
  protected void addToDistinguishedName(StringBuffer dn, String dnKey, String value) {
    dn.append(", ");
    dn.append(dnKey);
    dn.append("=");
    dn.append(value);
  }

  /**
   * Generates a CSR for a corresponding KeyPair. It builds the DistinguisedName using the constants declared in this field.
   * 
   * @param kp
   *          KeyPair to generate CSR for
   * @param suffix
   *          String suffix to use in distinguished name
   * @param csrFile
   *          File to store CSR in
   * @param type
   *          SignatureType of the corresponding KeyPair
   * @return String containing the distinguished name
   * @throws CertificateRequestGenException
   * @throws IOException
   */
  protected String generateCSR(KeyPair kp, String suffix, File csrFile, SignatureType type) throws CertificateRequestGenException,
      IOException {
    // Prepare the distinguished name for the certificate from any available parameters
    StringBuffer sb = new StringBuffer();
    sb.append(ClientConstants.X509.COMMON_NAME);
    sb.append("=");
    sb.append(this.id);
    sb.append(suffix);

    // this.addToDistinguishedName(sb, ClientConstants.X509.ORGANISATION, KeyGeneration.ORGANISATION);
    this.addToDistinguishedName(sb, ClientConstants.X509.ORGANISATION, KeyGeneration.ORGANISATION);
    this.addToDistinguishedName(sb, ClientConstants.X509.ORGANISATION_UNIT, KeyGeneration.ORGANISATION_UNIT);
    this.addToDistinguishedName(sb, ClientConstants.X509.COUNTRY, KeyGeneration.COUNTRY);
    this.addToDistinguishedName(sb, ClientConstants.X509.LOCATION, KeyGeneration.LOCATION);
    this.addToDistinguishedName(sb, ClientConstants.X509.STATE, KeyGeneration.STATE);

    String csr = GenerateSignatureKeyAndCSR.generateCertificateSigningRequest(sb.toString(), kp, KeyGeneration.CERTIFICATE_LIFE,
        type, "");

    IOUtils.writeStringToFile(csr, csrFile.getAbsolutePath());

    return sb.toString();
  }

  /**
   * Generates a KeyPair and corresponding CSR, storing it in the specified KeyStore
   * 
   * @param suffix
   *          String suffix for alias and key files
   * @param csrFile
   *          File to save the CSR to
   * @param ks
   *          KeyStore to save keys into
   * @param type
   *          SignatureType of the keys to generate
   * @throws KeyGenerationException
   * @throws TVSSignatureException
   */
  protected void generateKey(String suffix, File csrFile, KeyStore ks, SignatureType type) throws KeyGenerationException,
      TVSSignatureException {
    try {
      KeyPair kp = GenerateSignatureKeyAndCSR.generateKeyPair(type);

      // Prepare the distinguished name for the certificate from any available parameters
      String dn = this.generateCSR(kp, suffix, csrFile, type);

      ContentSigner sigGen = new JcaContentSignerBuilder(TVSSignature.getSignatureTypeString(type))
          .setProvider(CryptoUtils.BC_PROV).build(kp.getPrivate());
      Date startDate = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
      Date endDate = new Date(System.currentTimeMillis() + 365 * 24 * 60 * 60 * 1000);

      X509v1CertificateBuilder v1CertGen = new JcaX509v1CertificateBuilder(new X500Principal(dn), BigInteger.ONE, startDate,
          endDate, new X500Principal(dn), kp.getPublic());

      X509CertificateHolder certHolder = v1CertGen.build(sigGen);
      JcaX509CertificateConverter conv = new JcaX509CertificateConverter().setProvider(CryptoUtils.BC_PROV);
      ks.setKeyEntry(this.id + suffix, kp.getPrivate(), "".toCharArray(), new Certificate[] { conv.getCertificate(certHolder) });
    }
    catch (IOException e) {
      throw new KeyGenerationException("Exception whilst generating key pair", e);
    }
    catch (CertificateRequestGenException e) {
      throw new KeyGenerationException("Exception whilst generating key pair", e);
    }
    catch (OperatorCreationException e) {
      throw new KeyGenerationException("Exception whilst generating key pair", e);
    }
    catch (KeyStoreException e) {
      throw new KeyGenerationException("Exception whilst generating key pair", e);
    }
    catch (CertificateException e) {
      throw new KeyGenerationException("Exception whilst generating key pair", e);
    }
  }

  /**
   * Creates both the key pair and the CSR. Saving the CSR to the csrFolder with the filename id+CSR_EXT. The keys are stored in a
   * copy of the base key store at the location specified in keyStorePath. Will generate the appropriate keys for the SignatureType
   * argument
   * 
   * @param csrFolder
   *          String with path to the folder to save the CSR into
   * @param keyStorePath
   *          String with filepath to store the new key store to
   * @param type
   *          SignatureType defining the type of keys to generate
   * @throws CryptoIOException
   * @throws KeyGenerationException
   * @throws TVSSignatureException
   */
  public void generateKeyAndCSR(String csrFolder, String keyStorePath, SignatureType type) throws CryptoIOException,
      KeyGenerationException, TVSSignatureException {
    logger.info("generateKeyAndCSR called for type:{}", type);

    // Load the base keystore
    KeyStore ks = CryptoUtils.loadKeyStore(KeyGeneration.BASE_KEYSTORE_PATH);

    // Create the required output folder for the CSR
    File csrOutput = new File(csrFolder);
    try {
      IOUtils.checkAndMakeDirs(csrOutput);
    }
    catch (IOException e) {
      throw new CryptoIOException("Could not create output directory", e);
    }

    // Generate the Keys and CSR
    this.generateKey("", new File(csrOutput, this.id + CSR_EXT), ks, type);

    // Write out the keystore to a file, making the parent directories if necessary
    File keyStoreFile = new File(keyStorePath);
    try {
      IOUtils.checkAndMakeDirs(keyStoreFile.getParentFile());
    }
    catch (IOException e) {
      throw new CryptoIOException("Could not make keystore output directory");
    }

    logger.info("Writing out keystore to {}", keyStorePath);
    CryptoUtils.storeKeyStore(ks, keyStorePath);
  }

  /**
   * Imports certificates from the specified directory.
   * 
   * @param keyStorePath
   *          String with filepath to store the new key store to
   * @param directory
   *          File path to directory containing certificates.
   * @param parentCert
   *          Parent certificate within the directory.
   * 
   * @throws CertificateStoringException
   */
  public void importRelevantCertsFromDir(String keyStorePath, File directory, String parentCert) throws CertificateStoringException {
    FileInputStream in = null;

    try {
      KeyStore ks = CryptoUtils.loadKeyStore(keyStorePath);

      CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE);

      in = new FileInputStream(new File(directory, parentCert));
      Certificate peerCA = certFactory.generateCertificate(in);

      char[] password = new char[0];

      for (File file : directory.listFiles()) {
        if (file.getName().startsWith(this.id)) {
          String alias = file.getName().substring(0, file.getName().indexOf("."));
          Key key = ks.getKey(file.getName().substring(0, file.getName().indexOf(".")), password);

          FileInputStream certificateIn = null;

          try {
            Certificate certificate = certFactory.generateCertificate(new FileInputStream(file));

            ks.setKeyEntry(alias, key, password, new Certificate[] { certificate, peerCA });
          }
          finally {
            if (certificateIn != null) {
              certificateIn.close();
            }
          }
        }
      }

      CryptoUtils.storeKeyStore(ks, keyStorePath);
    }
    catch (IOException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (CertificateException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (UnrecoverableKeyException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (KeyStoreException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (CryptoIOException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    finally {
      if (in != null) {
        try {
          in.close();
        }
        catch (IOException e) {
          // Not much we can do here.
        }
      }
    }
  }

  /**
   * Imports the certificates into the key store specified in keyStorePath
   * 
   * WARNING: This method currently includes a slash at the beginning of the entity name. This has no impact on SSL connections, but
   * would when looking up individual entities by name. As such, it has no impact on the client, but using it for other purposes may
   * create a problem. This will be fixed in 2015, but the codebase for the client is now locked and since this is not creating a
   * problem on the client there is no justification for risking changing it. We have checked to ensure this is not called
   * elsewhere.
   * 
   * This method only adds trusted certificates, it does not add a certificate an existing private key
   * 
   * @param keyStorePath
   *          String path to KeyStore
   * @param filenames
   *          String array of paths to certificates to import
   * @throws CertificateStoringException
   */
  public static void importCertificateFiles(String keyStorePath, String[] filenames) throws CertificateStoringException {
    try {
      KeyStore ks = CryptoUtils.loadKeyStore(keyStorePath);
      CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE);

      for (String file : filenames) {
        FileInputStream in = null;

        try {
          in = new FileInputStream(file);
          ks.setCertificateEntry(file.substring(file.lastIndexOf("/"), file.lastIndexOf(".")), certFactory.generateCertificate(in));
        }
        finally {
          if (in != null) {
            in.close();
          }
        }
      }

      CryptoUtils.storeKeyStore(ks, keyStorePath);
    }
    catch (IOException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (CryptoIOException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (KeyStoreException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (CertificateException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
  }

  /**
   * Import a certificate for an existing private key. This will overwrite any existing certificate (i.e. a self-signed certificate
   * generated during key generation).
   * 
   * @param filepath
   *          String path to certificate file
   * @param alias
   *          String alias of the private key in the key store
   * @param keyStorePath
   *          String path to keystore
   * @throws CertificateStoringException
   */
  public static void importCertificates(String filepath, String alias, String keyStorePath) throws CertificateStoringException {
    FileInputStream in = null;

    try {
      KeyStore ks = CryptoUtils.loadKeyStore(keyStorePath);
      CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE);

      in = new FileInputStream(filepath);
      ks.setCertificateEntry(alias, certFactory.generateCertificate(in));

      CryptoUtils.storeKeyStore(ks, keyStorePath);
    }
    catch (IOException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (CryptoIOException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (CertificateException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (KeyStoreException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    finally {
      try {
        if (in != null) {
          in.close();
        }
      }
      catch (IOException e) {
        // Not much we can do here.
      }
    }
  }

  /**
   * Imports the specified certificate file, alongside the CA certificate file, in a hierarchy. This is for importing a certificate
   * for an existing private key entry. It will overwrite any existing certificate associated with the key entry.
   * 
   * Importing the hierarchy is important for importing SSL certificates where the connection decision may be based on the parent
   * certificate and as such needs to be stored in the keystore in the correct hierarchy.
   * 
   * @param certFile
   *          String path to the certificate file
   * @param caFile
   *          String path to the parent CA certificate file
   * @param alias
   *          String alias
   * @param keyStorePath
   *          String path to key store
   * @param pwd
   *          String password for key store
   * @throws CertificateStoringException
   */
  public static void importCertificates(String certFile, String caFile, String alias, String keyStorePath, String pwd)
      throws CertificateStoringException {
    FileOutputStream out = null;
    FileInputStream certIn = null;
    FileInputStream caIn = null;

    try {
      KeyStore ks = CryptoUtils.loadKeyStore(keyStorePath);
      CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE);
      Certificate[] certs = new Certificate[2];

      certIn = new FileInputStream(certFile);
      caIn = new FileInputStream(caFile);

      certs[0] = certFactory.generateCertificate(certIn);
      certs[1] = certFactory.generateCertificate(caIn);

      ks.setKeyEntry(alias, ks.getKey(alias, pwd.toCharArray()), pwd.toCharArray(), certs);

      out = new FileOutputStream(keyStorePath);
      ks.store(out, "".toCharArray());
    }
    catch (IOException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (CryptoIOException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (CertificateException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (UnrecoverableKeyException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (KeyStoreException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new CertificateStoringException("Exception whilst importing certificates", e);
    }
    finally {
      if (out != null) {
        try {
          out.close();
        }
        catch (IOException e) {
          // Not much we can do here.
        }
      }

      if (certIn != null) {
        try {
          certIn.close();
        }
        catch (IOException e) {
          // Not much we can do here.
        }
      }

      if (caIn != null) {
        try {
          caIn.close();
        }
        catch (IOException e) {
          // Not much we can do here.
        }
      }
    }
  }
}
