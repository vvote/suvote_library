/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import java.security.KeyStoreException;

/**
 * Exception created when an error occurs reading or writing to a TVSKeyStore
 * 
 * @author Chris Culnane
 * 
 */
public class TVSKeyStoreException extends KeyStoreException {

  /**
   * Required for inherited serialisation.
   */
  private static final long serialVersionUID = 1542572472455952068L;

  /**
   * Constructs a new exception with {@code null} as its detail message.
   */
  public TVSKeyStoreException() {

  }

  /**
   * Constructs a new exception with the specified detail message.
   * 
   * @param msg
   *          the detail message.
   */
  public TVSKeyStoreException(String msg) {
    super(msg);

  }

  /**
   * Constructs a new exception with the specified cause and a detail message.
   * 
   * @param cause
   *          the cause. A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.
   */
  public TVSKeyStoreException(Throwable cause) {
    super(cause);

  }

  /**
   * Constructs a new exception with the specified detail message and cause.
   * <p>
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically incorporated in this exception's detail
   * message.
   * 
   * @param message
   *          the detail message.
   * @param cause
   *          the cause. A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or unknown.
   */
  public TVSKeyStoreException(String message, Throwable cause) {
    super(message, cause);

  }

}
