/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.verify;

/**
 * Enum that lists possible results of Preference verification.
 * 
 * EMPTY - No preferences were found <br>
 * SINGLE_PREF - A single 1 preference was found, all others were empty <br>
 * VALID_PREF - A valid set of numerical preferences. Note, this could be a single 1 preference, since that is strictly a valid
 * preference list<br>
 * INVALID - The preferences are not valid, could be missing numbers (1,4,3,5), could be duplicates (1,3,4,1) or could be incorrect
 * values (2,A,3,1)<br>
 * ALL_VALID - Signifies all races were valid. Note: currently if both ATL and BTL are EMPTY it is considered INVALID
 * 
 * @author Chris Culnane
 * 
 */
public enum PrefsVerification {
  EMPTY, SINGLE_PREF, INVALID, VALID_PREF, ALL_VALID
}
