/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.security.DigestInputStream;
import java.security.MessageDigest;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Utility class for IO Utilities, provides methods for reading and writing files and JSON files.
 * 
 * @author Chris Culnane
 * 
 */
public class IOUtils {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(IOUtils.class);

  /**
   * Adds the contents of a file into a MessageDigest. Reads the files bytes and adds them to the digest, the actual contents of the
   * file is not stored or returned.
   * 
   * @param file
   *          to read
   * @param md
   *          MessageDigest to add bytes to
   * @return int number of bytes read
   * @throws IOException
   */
  public static int addFileIntoDigest(File file, MessageDigest md) throws IOException {
    int filesize = 0;
    int read = 0;
    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
    DigestInputStream dis = new DigestInputStream(bis, md);
    byte[] bytesIn = new byte[1024];

    try {
      while ((read = dis.read(bytesIn)) != -1) {
        filesize = filesize + read;
      }
    }
    finally {
      dis.close();
    }

    return filesize;
  }

  /**
   * Utility method for decoding a Hex or Base64 encoded string into a byte array
   * 
   * @param encType
   *          EncodingType of the string (Base64 or Hex)
   * @param data
   *          String of encoded data
   * @return decoded byte array or null if there is an exception
   */
  public static byte[] decodeData(EncodingType encType, String data) {
    try {
      switch (encType) {
        case BASE64:
          return Base64.decodeBase64(data);
        case HEX:
          return Hex.decodeHex(data.toCharArray());
        default:
          return data.getBytes();
      }
    }
    catch (DecoderException e) {
      logger.warn("Exception decoding Hex String", e);
      return null;
    }
  }

  /**
   * Utility method for encoding byte arrays into strings. Currently provides Hex and Base64 encoding.
   * 
   * @param encType
   *          EncodingType to use (Base64, Hex)
   * @param data
   *          byte array of data to be encoded
   * @return String of the encoded data
   */
  public static String encodeData(EncodingType encType, byte[] data) {
    switch (encType) {
      case BASE64:
        return Base64.encodeBase64String(data);
      case HEX:
        return Hex.encodeHexString(data);
      default:
        return new String(data);
    }
  }

  /**
   * Converts a serial number into a filename - this will replace any file system incompatible characters in the serial number
   * string with "_"
   * 
   * @param serialNo
   *          String of serial number
   * @return file name String of the serial number
   */
  public static String getFileNameFromSerialNo(String serialNo) {
    return serialNo.replaceAll("\\W", "_");
  }

  /**
   * Utility method to move a file from one location to another. If the destination directories do not exist they will be created.
   * 
   * @param source
   *          String path of source file
   * @param destination
   *          String path of destination
   * @throws IOException
   */
  public static void moveFile(String source, String destination) throws IOException {
    File sourceFile = new File(source);
    File destinationFolder = new File(destination);

    checkAndMakeDirs(destinationFolder);

    File destinationFile = new File(destinationFolder, sourceFile.getName());
    Files.move(sourceFile.toPath(), destinationFile.toPath());
  }

  /**
   * Read fileSize bytes from the InputStream and store the data into the destination file, simultaneously creating a SHA1 hash of
   * the data using the specified DigestInputStream.
   * 
   * This method does not return a hash for this file, instead allowing multiple files to be hashed together, for example, when
   * multiple files are transferred during round 2 of the commit protocol.
   * 
   * 
   * @param fileSize
   *          number of bytes to read from the stream
   * @param destination
   *          target file to write the bytes to
   * @param dis
   *          underlying DataInputStream to read the data from
   * @throws IOException
   */
  public static void readFile(long fileSize, File destination, DigestInputStream dis) throws IOException {
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destination));
    try {
      int b;
      long count = 0;

      if (fileSize > 0) {
        while ((b = dis.read()) != -1) {
          bos.write(b);
          count++;
          if (count == fileSize) {
            break;
          }
        }
      }
      bos.flush();
    }
    finally {
      bos.close();
    }
  }

  /**
   * Reads JSONArray from a file
   * 
   * @param filepath
   *          string path to the file
   * @return JSONArray contained in the file
   * @throws JSONIOException
   */
  public static JSONArray readJSONArrayFromFile(String filepath) throws JSONIOException {
    try {
      return new JSONArray(readStringFromFile(filepath));
    }
    catch (IOException e) {
      logger.error("Exception whilst trying to open:" + filepath, e);
      throw new JSONIOException("Exception whilst trying to open:" + filepath, e);
    }
    catch (JSONException e) {
      logger.error("Exception whilst trying to open:" + filepath, e);
      throw new JSONIOException("Exception whilst trying to open:" + filepath, e);
    }
  }

  /**
   * Reads a JSONObject from the specified file
   * 
   * @param filepath
   *          string path to the file
   * @return JSONObject of the files contents
   * @throws JSONIOException
   */
  public static JSONObject readJSONObjectFromFile(String filepath) throws JSONIOException {
    try {
      return new JSONObject(readStringFromFile(filepath));
    }
    catch (IOException e) {
      logger.error("Exception whilst trying to open:" + filepath, e);
      throw new JSONIOException("Exception whilst trying to open:" + filepath, e);
    }
    catch (JSONException e) {
      logger.error("Exception whilst trying to open:" + filepath, e);
      throw new JSONIOException("Exception whilst trying to open:" + filepath, e);
    }
  }

  /**
   * Reads a JSONObject from the specified file
   * 
   * @param filepath
   *          string path to the file
   * @param schemaPath
   *          string path to the schema to validate against
   * @return JSONObject of the files contents
   * @throws JSONIOException
   */
  public static JSONObject readJSONObjectFromFileAndValidate(String filepath, String schemaPath) throws JSONIOException {
    try {
      String fileContents = readStringFromFile(filepath);
      String schemaContents = JSONUtils.loadSchema(schemaPath);
      if (JSONUtils.validateSchema(schemaContents, fileContents)) {
        return new JSONObject(readStringFromFile(filepath));
      }
      else {
        throw new JSONIOException("File failed schema validation");
      }
    }
    catch (IOException e) {
      logger.error("Exception whilst trying to open:" + filepath, e);
      throw new JSONIOException("Exception whilst trying to open:" + filepath, e);
    }
    catch (JSONException e) {
      logger.error("Exception whilst trying to open:" + filepath, e);
      throw new JSONIOException("Exception whilst trying to open:" + filepath, e);
    }
  }

  /**
   * Reads a file into a string variable. NOTE: this will strip any newline characters
   * 
   * @param filepath
   *          string path to the file
   * @return String of the files contents
   * @throws IOException
   */
  public static String readStringFromFile(String filepath) throws IOException {
    BufferedReader br = null;
    StringBuffer sb = null;
    try {
      br = new BufferedReader(new FileReader(filepath));

      sb = new StringBuffer();
      String line;
      while ((line = br.readLine()) != null) {
        sb.append(line);
      }
    }
    finally {
      if (br != null) {
        br.close();
      }
    }

    return sb.toString();
  }

  /**
   * Writes a JSONArray to a file
   * 
   * @param obj
   *          JSONArray to write to the file
   * @param filepath
   *          String path to the output file
   * @throws JSONIOException
   */
  public static void writeJSONToFile(JSONArray obj, String filepath) throws JSONIOException {
    try {
      writeStringToFile(obj.toString(), filepath);
    }
    catch (IOException e) {
      logger.error("Exception whilst trying to write:" + filepath, e);
      throw new JSONIOException("Exception whilst trying to write:" + filepath, e);
    }
  }

  /**
   * Writes the JSONObject to a file
   * 
   * @param obj
   *          JSONObject to write to the file
   * @param filepath
   *          String path to the output file
   * @throws JSONIOException
   */
  public static void writeJSONToFile(JSONObject obj, String filepath) throws JSONIOException {
    try {
      writeStringToFile(obj.toString(), filepath);
    }
    catch (IOException e) {
      logger.error("Exception whilst trying to write:" + filepath, e);
      throw new JSONIOException("Exception whilst trying to write:" + filepath, e);
    }
  }

  /**
   * Writes a string to a file
   * 
   * @param data
   *          String to write to the file
   * @param filepath
   *          String path to the output file
   * @throws IOException
   */
  public static void writeStringToFile(String data, String filepath) throws IOException {
    File filep = new File(filepath);
    checkAndMakeDirs(filep.getAbsoluteFile().getParentFile());
    BufferedWriter bw = null;
    try {
      bw = new BufferedWriter(new FileWriter(filepath));
      bw.write(data);
    }
    finally {
      if (bw != null) {
        bw.close();
      }
    }
  }

  /**
   * A utility method that checks if the result of a mkdirs call was successful, logging it as info if it was and raising an
   * IOException if it wasn't. This should only be called where a failure to create a directory warrants an exception.
   * 
   * @param fileOperationSuccessful
   *          boolean return value of mkdir or mkdirs
   * @param file
   *          File object that mkdir or mkdirs was called on
   * @throws IOException
   */
  public static void checkDirSuccess(boolean fileOperationSuccessful, File file) throws IOException {
    if (fileOperationSuccessful) {
      logger.info("Successfully created {}", file);
    }
    else {
      throw new IOException("mkdirs on " + file.getAbsolutePath() + " returned false");
    }

  }

  /**
   * Utility method to check if directory exists, if not it will attempt to make it and any necessary parent directories. Failure in
   * the mkdirs call will result in an IOException, success will result in an info log entry stating the directory that was created.
   * 
   * @param file
   *          File object to check if it exists and if not to create
   * @throws IOException
   */
  public static void checkAndMakeDirs(File file) throws IOException {
    if (!file.exists()) {
      if (!file.mkdirs()) {
        throw new IOException("mkdirs returned false on:" + file.getAbsolutePath());
      }
      else {
        logger.info("Created directory at: {}", file);
      }
    }
  }

  /**
   * Checks if a file exists and if it does deletes it. If the deletion fails it throws an IOException. If the file does not exist
   * the fact delete was called is logged, but no exception is thrown.
   * 
   * @param file File object that should be deleted
   * @throws IOException
   */
  public static void checkAndDeleteFile(File file) throws IOException {
    if (file.exists()) {
      if (!file.delete()) {
        throw new IOException("Delete returned false on:" + file.getAbsolutePath());
      }
      else {
        logger.info("Deleted file at: {}", file);
      }
    }
    else {
      logger.info("Delete called on: {} but it doesn't exist - will ignore", file);
    }
  }
}
