/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import it.unisa.dia.gas.jpbc.Pairing;
import it.unisa.dia.gas.plaf.jpbc.pairing.DefaultCurveParameters;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;
import it.unisa.dia.gas.plaf.jpbc.pairing.d.TypeDPairing;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A singleton for providing access to the system wide parameters of the BLS signatures used in the library. Note this is hard-coded
 * to the the parameters specified in the file d62003-159-158.param contained in the source tree. In future we will look at moving
 * this to be configurable, however, for the moment we will fix the parameters to ensure compatibility across the entire system.
 * 
 * The main purpose of this class is to provide access to the pairing object and the curve parameters. We create this as a singleton
 * to save having to reload the data each time an object needs access to the system parameters for BLS.
 * 
 * TODO check the underlying DefaultCurveParameters is thread safe
 * @author Chris Culnane
 * 
 */
public class CurveParams {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(CurveParams.class);
  /**
   * Static constant containing the filename of the parameter name
   */
  private static final String      PARAMS_FILE = "d62003-159-158.param";

  /**
   * Single instance of CurveParams
   */
  private static final CurveParams _instance   = new CurveParams();

  /**
   * Contains the loaded curve parameters contained in the PARAMS_FILE
   */
  private DefaultCurveParameters   curveParams;

  /**
   * Holds a reference to the pairing associated with the curve specified in the PARAMS_FILE
   */
  private Pairing                  pairing;

  /**
   * Private constructor used during initialisation of the singleton.
   */
  private CurveParams() {
    logger.info("Creating CurveParams instance from {}",PARAMS_FILE);
    InputStream is = null;
    try {
      is = getClass().getResourceAsStream(PARAMS_FILE);
      curveParams = new DefaultCurveParameters().load(is);
    }
    finally {
      if (is != null) {
        try {
          is.close();
        }
        catch (IOException e) {
          logger.error("Exception whilst closing curve params file",e);
        }
      }
    }
    pairing = PairingFactory.getPairing(curveParams);
    
    logger.info("Finished creating curveParams and pairing");
  }

  public void saveTwist(){
    ((TypeDPairing)pairing).saveTwist();
  }
  /**
   * Gets the Pairing from the previously initialised curve parameters
   * @return Pairing associated with the default curve parameters
   */
  public Pairing getPairing() {
    return this.pairing;
  }

  /**
   * Gets the DefaultCurveParameters loaded from the default file
   * @return DefaultCurveParameters loaded from the file specified in the constant PARAMS_FILE
   */
  public DefaultCurveParameters getCurveParams() {
    return this.curveParams;
  }

  /**
   * Gets the singleton instance of the CurveParams singleton that then provides access to the curve parameters and pairing objects.
   * @return singleton instance of CurveParams
   */
  public static final CurveParams getInstance() {
    return _instance;
  }
}
