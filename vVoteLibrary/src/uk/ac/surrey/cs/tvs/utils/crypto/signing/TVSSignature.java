/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import it.unisa.dia.gas.jpbc.Element;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSCombiner;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPublicKey;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Provides a wrapper for signature operations to provide any easy way of switching between DSA, ECDSA and BLS signatures
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class TVSSignature {

  /**
   * Logger
   */
  private static final Logger logger         = LoggerFactory.getLogger(TVSSignature.class);

  /**
   * Constant for field in JSON response when checking a set of signatures, holds the combined signature if created
   */
  public static final String  COMBINED       = "combined";
  /**
   * Constant for the JSON Response - field for whether the signature is valid
   */
  public static final String  IS_VALID       = "valid";

  /**
   * Constant for JSON Response - field for signature array
   */
  public static final String  SIGNATURE_DATA = "sigData";

  /**
   * Constant for field in JSON response when checking a set of signatures
   */
  public static final String  VALID_COUNT    = "validCount";

  /**
   * Returns the relevant instance string for a SignatureType. Utilises the DEFAULT_MESSAGE_DIGEST defined in SystemConstants to
   * define the message digest algorithm to use
   * 
   * @param type
   *          SignatureType to get instance string for
   * @return instance String (SHA1WithDSA, SHA1WithECDSA)
   * @throws TVSSignatureException
   */
  public static String getSignatureTypeString(SignatureType type) throws TVSSignatureException {
    switch (type) {
      case DSA:
      case DEFAULT:
        return SystemConstants.DEFAULT_MESSAGE_DIGEST + "WithDSA";
      case ECDSA:
        return SystemConstants.DEFAULT_MESSAGE_DIGEST + "WithECDSA";
      case BLS:
        return SystemConstants.DEFAULT_MESSAGE_DIGEST;
      case RSA:
        return SystemConstants.DEFAULT_MESSAGE_DIGEST + "WithRSA";
      default:
        throw new TVSSignatureException("Not currently implemented");
    }
  }

  /**
   * Underlying BLS Signature object - only used for BLS and Threshold BLS
   */
  private BLSSignature          blsSignature;

  /**
   * Determines whether data is passed straight to the underlying signature object or cached first
   */
  private boolean               cacheData = false;
  /**
   * Underlying KeyStore
   */
  private TVSKeyStore           certKS    = null;

  /**
   * Determines whether the underlying signature is standard Java signature object or BLS Signatures
   */
  private boolean               isBLS     = false;

  /**
   * Determines whether we are checking against the joint public key or the parial public key in threshold BLS
   */
  private boolean               partial   = false;

  /**
   * Stores the generated signature - if there is one
   */
  private byte[]                sig       = null;

  /**
   * ByteArrayOutputStream to cache signature data
   */
  private ByteArrayOutputStream sigdata;

  /**
   * Underlying signature object - only used for DSA and ECDSA
   */
  private Signature             signature;

  /**
   * Stores the SignatureType of this instance
   */
  private SignatureType         type;

  /**
   * Constructs a BLS TVSSignature that will be used for signing data. The inclusion of the BLSPrivateKey implies BLS Signatures and
   * that this is going to be used for create a signature, not verify one. Data will not be cached, but will be passed directly to
   * the underlying signature object.
   * 
   * @param key
   *          PrivateKey to use for signing
   * @throws TVSSignatureException
   */
  public TVSSignature(BLSPrivateKey key) throws TVSSignatureException {
    super();
    this.type = SignatureType.BLS;
    this.createSignatureObject(type);
    this.blsSignature.initSign(key);

  }

  /**
   * Constructs a BLS TVSSignature that will be used for signing data. The inclusion of the BLSPrivateKey implies BLS Signatures and
   * that this is going to be used for create a signature, not verify one. Data will cached because there is no underlying signature
   * object to use.
   * 
   * @param key
   *          PrivateKey to use for signing
   * @param cacheData
   *          boolean value that determines whether the data should be cached outside of the signature object to allow it to be
   *          re-used
   * @throws TVSSignatureException
   */
  public TVSSignature(BLSPrivateKey key, boolean cacheData) throws TVSSignatureException {
    this(key);
    this.cacheData = cacheData;
  }

  /**
   * Constructs a TVSSignature that will be used for verifying data. The inclusion of a BLSPublicKey indicates that we will be
   * verifying BLS signatures. If cacheData is true the data will be cached and will only be passed to the underlying signature
   * object at verification. SignatureType is auto set to BLS
   * 
   * @param pubKey
   *          PublicKey to use during verification
   * @param cacheData
   *          boolean if True data is cached, if false data is sent directly to underlying signature
   * @throws TVSSignatureException
   */
  public TVSSignature(BLSPublicKey pubKey, boolean cacheData) throws TVSSignatureException {
    super();

    this.type = SignatureType.BLS;
    this.cacheData = cacheData;
    this.createSignatureObject(type);
    this.blsSignature.initVerify(pubKey);

  }

  /**
   * Constructs a TVSSignature that will be used for generating a signature. The inclusion of the BLSPrivateKey indicates that it
   * will be for generating a signature and also implies that the type can only be BLS. If a different type to BLS is set an
   * exception will be thrown.
   * 
   * The reason for not explicitly setting the type is to aid the integration with the existing Signatures types, which would call
   * this method with the same signature, but a different key type.
   * 
   * @param type
   *          SignatureType - only BLS is suitable for a BLSPrivateKey
   * @param key
   *          BLSPrivateKey containing the BLS Private key to be used
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, BLSPrivateKey key) throws TVSSignatureException {
    super();
    if (type != SignatureType.BLS) {
      throw new TVSSignatureException("Invalid type for BLS Key");
    }
    this.type = type;
    this.createSignatureObject(type);
    this.blsSignature.initSign(key);

  }

  /**
   * Constructs a TVSSignature that will be used for generating a signature. The inclusion of the BLSPrivateKey indicates that it
   * will be for generating a signature and also implies that the type can only be BLS. If a different type to BLS is set an
   * exception will be thrown. If cacheData is true the data will be cached and will only be passed to the underlying signature
   * object at verification.
   * 
   * The reason for not explicitly setting the type is to aid the integration with the existing Signatures types, which would call
   * this method with the same signature, but a different key type.
   * 
   * @param type
   *          SignatureType - only BLS is suitable for a BLSPrivateKey
   * @param key
   *          BLSPrivateKey containing the BLS Private key to be used
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, BLSPrivateKey key, boolean cacheData) throws TVSSignatureException {
    this(type, key);
    this.cacheData = cacheData;
  }

  /**
   * Constructs a TVSSignature that will be used for verification. The inclusion of a Certificate indicates this is going to be a
   * verification signature. This just calls the constructor @link
   * {@link TVSSignature#TVSSignature(SignatureType, Certificate, boolean)} with the cache parameter set to false;
   * 
   * @param type
   *          SignatureType of this signature
   * @param cert
   *          Public key certificate
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, Certificate cert) throws TVSSignatureException {
    this(type, cert, false);
  }

  /**
   * Constructs a TVSSignature that will be used for verifying data. The inclusion of a KeyStore (certificates) indicates that we
   * will be verifying signatures. If cacheData is true the data will be cached and will only be passed to the underlying signature
   * object at verification. Since BLS public key cannot be included in Certificate objects an exception will be thrown if this is
   * called with type set to BLS
   * 
   * @param type
   *          SignatureType of this signature
   * @param cert
   *          Public key certificate
   * @param cacheData
   *          boolean if True data is cached, if false data is sent directly to underlying signature
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, Certificate cert, boolean cacheData) throws TVSSignatureException {
    super();

    // Check we have consistent type and certificate
    if (cert == null) {
      throw new TVSSignatureException("Certificate cannot be null");
    }
    else if (type == SignatureType.BLS) {
      throw new TVSSignatureException("Invalid Certificate type for BLS");
    }

    this.type = type;
    this.cacheData = cacheData;
    this.createSignatureObject(type);
    try {
      // Construct a signature of the appropriate type
      this.signature.initVerify(cert);

    }
    catch (InvalidKeyException e) {
      throw new TVSSignatureException("Exception whilst initialising Signature object with key", e);
    }

  }

  /**
   * Constructs a TVSSignature that will be used for verifying data. The inclusion of a KeyStore (certificates) indicates that we
   * will be verifying signatures. By default the data will be cached. The inclusion of a standard Java Keystore indicates this
   * signature cannot be of BLS type because BLS keys cannot be stored in a standard KeyStore. As such, the type is checked and if
   * set to BLS and exception will be thrown.
   * 
   * @param type
   *          SignatureType of this signature
   * @param certificateKeyStore
   *          KeyStore containing public key certificates
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, KeyStore certificateKeyStore) throws TVSSignatureException {
    super();
    if (type == SignatureType.BLS) {
      throw new TVSSignatureException("Invalid keyStore type for BLS signatures");
    }
    this.type = type;
    this.certKS = new TVSKeyStore(certificateKeyStore);
    this.cacheData = true;
    this.createSignatureObject(type);
  }

  /**
   * Constructs a TVSSignature that will be used for signing data. The inclusion of the PrivateKey implies this is going to be used
   * for create a signature, not verify one. Data will not be cached, but will be passed directly to the underlying signature
   * object.
   * 
   * @param type
   *          SignatureType of this signature
   * @param key
   *          PrivateKey to use for signing
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, PrivateKey key) throws TVSSignatureException {
    super();
    if (type == SignatureType.BLS) {
      throw new TVSSignatureException("Invalid key for BLS Signature");
    }
    this.type = type;
    this.createSignatureObject(type);

    try {
      // Since we have a private key initialise it for signing
      this.signature.initSign(key);
    }
    catch (InvalidKeyException e) {
      throw new TVSSignatureException("Exception whilst initialising Signature object with key", e);
    }
  }

  /**
   * Constructs a TVSSignature that will be used for signing data. The inclusion of the PrivateKey implies this is going to be used
   * to create a signature, not verify one. If cacheData is true the data will be cached and will only be passed to the underlying
   * signature object at signing
   * 
   * @param type
   *          SignatureType of this signature
   * @param key
   *          PrivateKey to use for signing
   * @param cacheData
   *          boolean if True data is cached, if false data is sent directly to underlying signature
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, PrivateKey key, boolean cacheData) throws TVSSignatureException {
    this(type, key);
    this.cacheData = cacheData;
  }

  /**
   * Constructs a TVSSignature that will be used for verifying data. The inclusion of a PublicKey indicates that we will be
   * verifying signatures. If cacheData is true the data will be cached and will only be passed to the underlying signature object
   * at verification
   * 
   * @param type
   *          SignatureType of this signature
   * @param pubKey
   *          PublicKey to use during verification
   * @param cacheData
   *          boolean if True data is cached, if false data is sent directly to underlying signature
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, PublicKey pubKey, boolean cacheData) throws TVSSignatureException {
    super();
    if (type == SignatureType.BLS) {
      throw new TVSSignatureException("Invalid key for BLS Signature");
    }
    this.type = type;
    this.cacheData = cacheData;
    this.createSignatureObject(type);
    try {
      this.signature.initVerify(pubKey);
    }
    catch (InvalidKeyException e) {
      throw new TVSSignatureException("Exception whilst initialising Signature object with key", e);
    }
  }

  /**
   * Constructs a TVSSignature that will be used for verifying data. The inclusion of a Certificate indicates that we will be
   * verifying signatures. The data will be cached.
   * 
   * @param type
   *          SignatureType of this signature
   * @param cert
   *          Certificate to use during verification
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, TVSCertificate cert) throws TVSSignatureException {
    this(type, cert, false);
  }

  /**
   * Constructs a TVSSignature that will be used for verifying data. The inclusion of a KeyStore (certificates) indicates that we
   * will be verifying signatures. If cacheData is true the data will be cached and will only be passed to the underlying signature
   * object at verification
   * 
   * @param type
   *          SignatureType of this signature
   * @param cert
   *          KeyStore containing public key certificates
   * @param cacheData
   *          boolean if True data is cached, if false data is sent directly to underlying signature
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, TVSCertificate cert, boolean cacheData) throws TVSSignatureException {
    super();

    if (cert == null) {
      throw new TVSSignatureException("Certificate cannot be null");
    }

    this.type = type;
    this.cacheData = cacheData;
    this.createSignatureObject(type);
    try {
      // Construct a signature of the appropriate type
      if (isBLS) {
        this.blsSignature.initVerify(cert.getBLSPublicKey());
      }
      else {
        this.signature.initVerify(cert.getCertificate());
      }
    }
    catch (InvalidKeyException e) {
      throw new TVSSignatureException("Exception whilst initialising Signature object with key", e);
    }

  }

  /**
   * Constructs a TVSSignature that will be used for verifying data. The inclusion of a KeyStore (certificates) indicates that we
   * will be verifying signatures. By default the data will be cached
   * 
   * @param type
   *          SignatureType of this signature
   * @param certificateKeyStore
   *          KeyStore containing public key certificates
   * @throws TVSSignatureException
   */
  public TVSSignature(SignatureType type, TVSKeyStore certificateKeyStore) throws TVSSignatureException {
    super();

    this.type = type;
    this.certKS = certificateKeyStore;
    this.cacheData = true;
    this.createSignatureObject(type);
  }

  /**
   * Internal method to construct the underlying signature object. Note that we currently have a DEFAULT object, which most calls to
   * this method should use. Therefore the default underlying signature scheme for the whole system can be changed my changing where
   * the DEFAULT type is.
   * 
   * @param type
   *          SignatureType to create
   * @throws TVSSignatureException
   */
  private void createSignatureObject(SignatureType type) throws TVSSignatureException {
    this.sigdata = new ByteArrayOutputStream();
    switch (type) {
      case DSA:
      case DEFAULT:
      case ECDSA:
        try {
          this.signature = Signature.getInstance(TVSSignature.getSignatureTypeString(type));
        }
        catch (NoSuchAlgorithmException e) {
          throw new TVSSignatureException("Exception whilst creating signature object", e);
        }
        break;
      case BLS:
        try {
          this.blsSignature = BLSSignature.getInstance(TVSSignature.getSignatureTypeString(type));
          isBLS = true;
        }
        catch (NoSuchAlgorithmException e) {
          throw new TVSSignatureException("Exception whilst creating signature object", e);
        }
        break;
      case RSA:
        throw new TVSSignatureException("Not implemented for signatures, only SSL");
      default:
        throw new TVSSignatureException("Unknown signature type");
    }
  }

  /**
   * Internal method that actually does the signing, updating the underlying signature with any cached data first.
   * 
   * @throws TVSSignatureException
   */
  private void doSigning() throws TVSSignatureException {
    switch (this.type) {
      case DSA:
      case ECDSA:
      case DEFAULT:
        try {
          if (this.cacheData) {
            // If we have cached data get it from the message digest and pass onto the signature
            this.signature.update(this.sigdata.toByteArray());
            this.sigdata.reset();
          }
          this.sig = this.signature.sign();
        }
        catch (SignatureException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        break;
      case BLS:

        if (this.cacheData) {
          // If we have cached data get it from the message digest and pass onto the signature
          this.blsSignature.update(this.sigdata.toByteArray());
          this.sigdata.reset();
        }
        this.sig = this.blsSignature.sign().toBytes();
        break;
      case RSA:
        throw new TVSSignatureException("Not implemented for signatures, only SSL");
      default:
        throw new TVSSignatureException("Unknown signature type");
    }
  }

  /**
   * Only valid for Threshold BLS Signatures. If set to true will verify the signature against the partial public key, instead of
   * the joint public key. Default value is false. Set to True to check partial signatures
   * 
   * @param partial
   *          boolean true to check with the partial public key, false to check with joint public key
   */
  public void setPartial(boolean partial) {
    this.partial = partial;
  }

  /**
   * Returns the signature as a byte array, if the sign method has already been called on this signature object it will return the
   * existing signature, otherwise it calls the relevant sign method to generate the actual signature.
   * 
   * @return byte array of the signature
   * @throws TVSSignatureException
   */
  public byte[] sign() throws TVSSignatureException {
    if (this.sig == null) {
      this.doSigning();
    }
    return this.sig;
  }

  /**
   * Utility method for generating the signature and encoding into a string.
   * 
   * @param encType
   *          EncodingType for output
   * @return String of encoded signature
   * @throws TVSSignatureException
   */
  public String signAndEncode(EncodingType encType) throws TVSSignatureException {
    return IOUtils.encodeData(encType, this.sign());
  }

  /**
   * Updates the cache or underlying signature with additional data
   * 
   * @param data
   *          byte array of data to add to the signature
   * @throws TVSSignatureException
   */
  public void update(byte[] data) throws TVSSignatureException {
    if (this.sig != null) {
      throw new TVSSignatureException("Trying to update an already signed signature object");
    }

    switch (this.type) {
      case DSA:
      case ECDSA:
      case DEFAULT:
        try {
          if (this.cacheData) {
            // if cached we write to the stream
            this.sigdata.write(data);
          }
          else {
            // otherwise pass straight to signature
            this.signature.update(data);
          }
        }
        catch (IOException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        catch (SignatureException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        break;
      case BLS:
        try {
          if (this.cacheData) {
            // if cached we write to the stream
            this.sigdata.write(data);
          }
          else {
            // otherwise pass straight to signature
            this.blsSignature.update(data);
          }
        }
        catch (IOException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        break;
      case RSA:
        throw new TVSSignatureException("Not implemented for signatures, only SSL");
      default:
        throw new TVSSignatureException("Unknown signature type");
    }
  }

  /**
   * Utility method for updating with string data, calls the standard update method with getBytes() called on the string.
   * 
   * @param data
   *          String to add to the signature
   * @throws TVSSignatureException
   */
  public void update(String data) throws TVSSignatureException {
    this.update(data.getBytes());
  }

  /**
   * Verify the data that has been submitted against the signature passed in
   * 
   * @param sigToCheck
   *          Byte Array of signature to check
   * @return boolean, true if valid, false if invalid
   * @throws TVSSignatureException
   */
  public boolean verify(byte[] sigToCheck) throws TVSSignatureException {
    byte[] dataToVerify;
    switch (this.type) {
      case DSA:
      case ECDSA:
      case DEFAULT:
        try {
          if (this.cacheData) {
            // Submit the cacheData if we have any
            dataToVerify = this.sigdata.toByteArray();
            this.sigdata.reset();
            this.signature.update(dataToVerify);
          }
          return this.signature.verify(sigToCheck);
        }
        catch (SignatureException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }

      case BLS:
        if (this.cacheData) {
          // Submit the cacheData if we have any
          dataToVerify = this.sigdata.toByteArray();
          this.sigdata.reset();
          this.blsSignature.update(dataToVerify);
        }
        return this.blsSignature.verify(sigToCheck, partial);

      case RSA:
        throw new TVSSignatureException("Not implemented for signatures, only SSL");
      default:
        throw new TVSSignatureException("Unknown signature type");
    }
  }

  /**
   * Utility method for verifying a JSONArray of signatures - this is a common requirement when dealing with a peered setup and each
   * one returns a signature or a signature share. All need checking and possible combining, depending on underlying scheme. This
   * type of check is only allowed when the data has been cached, since the same data needs to be passed to multiple underlying
   * signature objects. An exception will be thrown if it is called on a non-cached Signature object.
   * 
   * @param sigsArray
   *          JSONArray of signatures
   * @param idSuffix
   *          String of the suffix to attach to the ID when getting the certificate from the keystore
   * @return JSONObject that contains a validCount field and the JSONArray showing the signatures and their validity
   * @throws TVSSignatureException
   */
  public JSONObject verify(JSONArray sigsArray, String idSuffix) throws TVSSignatureException {
    int validCount = 0;

    // Create return object
    JSONObject response = new JSONObject();
    byte[] dataToVerify = null;

    switch (this.type) {
      case DSA:
      case ECDSA:
      case DEFAULT:
        try {
          // we need to check that the signatures are from different peers
          Map<String, Boolean> signerIdChecked = new HashMap<String, Boolean>();
          // Check the signature is cached and backed by a keystore
          if (this.cacheData && this.certKS != null) {
            // Get the data to be checked
            dataToVerify = this.sigdata.toByteArray();
            this.sigdata.reset();

            // Loop through all the signature objects
            for (int i = 0; i < sigsArray.length(); i++) {
              JSONObject sigData = sigsArray.getJSONObject(i);

              // Because we are doing this multiple times we create a new signature object each time
              Certificate cert = this.certKS.getKeyStore().getCertificate(
                  sigData.getString(JSONConstants.Signature.SIGNER_ID) + idSuffix);

              this.signature.initVerify(cert);
              this.signature.update(dataToVerify);

              if (this.signature.verify(IOUtils.decodeData(EncodingType.BASE64,
                  sigData.getString(JSONConstants.Signature.SIGNATURE)))) {
                if (!signerIdChecked.containsKey(sigData.getString(JSONConstants.Signature.SIGNER_ID) + idSuffix)) {
                  // we only increment if this is from a different peer
                  // If valid, increment counter and mark signature as valid
                  validCount++;
                  signerIdChecked.put(sigData.getString(JSONConstants.Signature.SIGNER_ID) + idSuffix, true);
                }
                sigData.put(TVSSignature.IS_VALID, true);
              }
              else {
                // If invalid, mark signature as invalid
                sigData.put(TVSSignature.IS_VALID, false);
              }

              // Reset the signature object.
              this.createSignatureObject(this.type);
            }

            // Put the response data into the JSONObject
            response.put(TVSSignature.VALID_COUNT, validCount);
            response.put(TVSSignature.SIGNATURE_DATA, sigsArray);
          }
          else {
            throw new TVSSignatureException(
                "Attempting to perform multiple signature verification on either non-cached data or without a KeyStore backing the signature");
          }
        }
        catch (SignatureException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        catch (JSONException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        catch (InvalidKeyException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        catch (KeyStoreException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        break;
      case BLS:
        try {
          // Check the signature is cached and backed by a keystore
          if (this.cacheData && this.certKS != null) {
            // Get the data to be checked
            dataToVerify = this.sigdata.toByteArray();
            this.sigdata.reset();

            // Loop through all the signature objects
            for (int i = 0; i < sigsArray.length(); i++) {
              JSONObject sigData = sigsArray.getJSONObject(i);

              // Because we are doing this multiple times we create a new signature object each time
              TVSCertificate cert = this.certKS.getBLSCertificate(sigData.getString(JSONConstants.Signature.SIGNER_ID) + idSuffix);

              this.blsSignature.initVerify(cert);
              this.blsSignature.update(dataToVerify);

              if (this.blsSignature.verify(
                  IOUtils.decodeData(EncodingType.BASE64, sigData.getString(JSONConstants.Signature.SIGNATURE)), true)) {
                // If valid, increment counter and mark signature as valid
                validCount++;
                sigData.put(TVSSignature.IS_VALID, true);
              }
              else {
                // If invalid, mark signature as invalid
                sigData.put(TVSSignature.IS_VALID, false);
              }

              // Reset the signature object.
              this.createSignatureObject(this.type);
            }

            // Put the response data into the JSONObject
            response.put(TVSSignature.VALID_COUNT, validCount);
            response.put(TVSSignature.SIGNATURE_DATA, sigsArray);
          }
          else {
            throw new TVSSignatureException(
                "Attempting to perform multiple signature verification on either non-cached data or without a KeyStore backing the signature");
          }
        }

        catch (JSONException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        catch (KeyStoreException e) {
          throw new TVSSignatureException("Exception whilst adding data to signature", e);
        }
        break;
      case RSA:
        throw new TVSSignatureException("Not implemented for signatures, only SSL");
      default:
        throw new TVSSignatureException("Unknown signature type");
    }

    // Return the response
    return response;
  }

  /**
   * Utility method for checking a signature when the signature itself is an encoded string. The string will be decoded and the
   * standard verify method called with it.
   * 
   * @param sigToCheckString
   *          String of the encoded signature
   * @param encodingType
   *          EncodingType of the string
   * @return boolean, true if valid, false if invalid
   * @throws TVSSignatureException
   */
  public boolean verify(String sigToCheckString, EncodingType encodingType) throws TVSSignatureException {
    byte[] sigToCheck = IOUtils.decodeData(encodingType, sigToCheckString);

    return this.verify(sigToCheck);
  }

  /**
   * Utility method for verifying and combining a JSONArray of signatures - this is a common requirement when dealing with a peered
   * setup and each one returns a signature or a signature share. A threshold of valid shares needs to be found and then combined.
   * This type of check is only allowed when the data has been cached, since the same data needs to be passed to multiple underlying
   * signature objects. An exception will be thrown if it is called on a non-cached Signature object.
   * 
   * @param sigsArray
   *          JSONArray of signatures
   * @param idSuffix
   *          String of the suffix to attach to the ID when getting the certificate from the keystore
   * @param threshold
   *          int threshold of the underlying signature scheme
   * @param peers
   *          int total number of peers/shares
   * @param inlineVerify
   *          boolean if true performs verification of the individual shares before combining, if false assumes they are valid
   *          (pre-checked)
   * @param verifyCombined
   *          boolean if true a further signature check is performed to check that the combined signature is valid. This is just a
   *          sanity check but a sensible one to perform on the client
   * @return JSONObject that contains a validCount field and the JSONArray showing the signatures and their validity
   * @throws TVSSignatureException
   */
  public JSONObject verifyAndCombine(JSONArray sigsArray, String idSuffix, int threshold, int peers, boolean inlineVerify,
      boolean verifyCombined) throws TVSSignatureException {
    if (this.type != SignatureType.BLS) {
      throw new TVSSignatureException("Only Threshold BLS Signature can be combined");
    }
    logger.info("Preparing to combine signature inlineVerify:{}, verifyCombined:{}", inlineVerify, verifyCombined);
    // Counter for valid count to stop when we hit a threshold
    int validCount = 0;

    // BLSCombiner to combine the signature shares
    BLSCombiner combiner = new BLSCombiner(peers, threshold);
    // Create return object
    JSONObject response = new JSONObject();
    byte[] dataToVerify = null;

    try {
      // Check the signature is cached and backed by a keystore
      if (this.cacheData && this.certKS != null) {
        // Get the data to be checked
        dataToVerify = this.sigdata.toByteArray();
        this.sigdata.reset();

        // Loop through all the signature objects
        for (int i = 0; i < sigsArray.length(); i++) {
          JSONObject sigData = sigsArray.getJSONObject(i);

          Element signatureElem = BLSSignature.getSignatureElement(IOUtils.decodeData(EncodingType.BASE64,
              sigData.getString(JSONConstants.Signature.SIGNATURE)));
          boolean isValid = true;
          // Because we are doing this multiple times we create a new signature object each time
          TVSCertificate cert = this.certKS.getBLSCertificate(sigData.getString(JSONConstants.Signature.SIGNER_ID) + idSuffix);

          if (inlineVerify) {
            // Check the individual signature, otherwise assume it is valid
            this.blsSignature.initVerify(cert);
            this.blsSignature.update(dataToVerify);
            isValid = this.blsSignature.verify(signatureElem, true);
          }
          if (isValid) {
            // Add a valid share to the combiner
            combiner.addShare(signatureElem, cert.getBLSPublicKey().getSequenceNo());
            // If valid, increment counter and mark signature as valid
            validCount++;
            sigData.put(TVSSignature.IS_VALID, true);
            logger.info("Signature from {} is valid", sigData.getString(JSONConstants.Signature.SIGNER_ID));
          }
          else {
            // If invalid, mark signature as invalid
            sigData.put(TVSSignature.IS_VALID, false);
            logger.warn("Signature from {} is invalid", sigData.getString(JSONConstants.Signature.SIGNER_ID));
          }
          // Check if we have found a threshold of signatures
          if (validCount >= threshold) {
            logger.info("Threshold of signatures met, preparing to combine");
            // if we have combine them and return
            Element combinedElem = combiner.combined();
            boolean combinedValid = true;
            if (verifyCombined) {
              // If verifyCombined we construct another signature object and check the combined signature
              cert = this.certKS.getBLSCertificate(SystemConstants.JOINT_KEY);
              this.blsSignature.initVerify(cert);
              this.blsSignature.update(dataToVerify);
              combinedValid = this.blsSignature.verify(combinedElem, false);
              logger.info("Verified combined, isValid:{}", combinedValid);
            }

            if (combinedValid) {
              // If the combined value is valid add it - the presence of the Combined fields determines if a valid signature was
              // found
              response.put(TVSSignature.COMBINED, IOUtils.encodeData(EncodingType.BASE64, combinedElem.toBytes()));
            }
            response.put(TVSSignature.VALID_COUNT, validCount);
            response.put(TVSSignature.SIGNATURE_DATA, sigsArray);
            return response;
          }
          // Reset the signature object.
          this.createSignatureObject(this.type);
        }

        // Put the response data into the JSONObject
        response.put(TVSSignature.VALID_COUNT, validCount);
        response.put(TVSSignature.SIGNATURE_DATA, sigsArray);
      }
      else {
        throw new TVSSignatureException(
            "Attempting to perform multiple signature verification on either non-cached data or without a KeyStore backing the signature");
      }
    }

    catch (JSONException e) {
      throw new TVSSignatureException("Exception whilst adding data to signature", e);
    }
    catch (KeyStoreException e) {
      throw new TVSSignatureException("Exception whilst adding data to signature", e);
    }

    // Return the response
    return response;
  }
}
