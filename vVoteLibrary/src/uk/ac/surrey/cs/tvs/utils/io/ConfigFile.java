/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Provides a generic config file object for reading JSON based config files. Has some utility methods for accessing Strings and
 * integers. Currently it does not provide direct access to the underlying JSON. If direct access is required this class should be
 * extended with the additional functionality added.
 * 
 * @author Chris Culnane
 * 
 */
public class ConfigFile {

  /**
   * Protected JSONObject for holding the underlying JSON file - it is protected so subclasses can get direct access to it
   */
  protected JSONObject conf;

  /**
   * path to the config file, default is client.conf
   */
  private String       confFile = "config.conf";

  /**
   * Constructor that uses default path for config file
   * 
   * @throws JSONIOException
   */
  public ConfigFile() throws JSONIOException {
    super();

    this.conf = IOUtils.readJSONObjectFromFile(this.confFile);
  }

  /**
   * Constructor that loads the JSON file located at configFile
   * 
   * @param configFile
   *          path to config file
   * @throws JSONIOException
   */
  public ConfigFile(String configFile) throws JSONIOException {
    this.confFile = configFile;

    this.conf = IOUtils.readJSONObjectFromFile(this.confFile);
  }

  /**
   * Constructor that loads the JSON file located at configFile
   * 
   * @param configFile
   *          path to config file
   * @param schemaFile
   *          path to schema to check config file against
   * @throws JSONIOException
   */
  public ConfigFile(String configFile, String schemaFile) throws JSONIOException {
    this.confFile = configFile;

    this.conf = IOUtils.readJSONObjectFromFileAndValidate(this.confFile, schemaFile);
  }

  /**
   * Attempts to retrieve the boolean value for a given parameter name.
   * 
   * @param name
   *          string containing parameter name
   * @return the boolean value of the parameter or false if it does not exist or is not a string
   */
  public boolean getBooleanParameter(String name) {
    if (this.conf.has(name)) {
      try {
        return this.conf.getBoolean(name);
      }
      catch (JSONException e) {
        return false;
      }
    }
    else {
      return false;
    }
  }

  /**
   * Attempts to retrieve the int value for a given parameter name.
   * 
   * @param name
   *          string containing parameter name
   * @return the int value of the parameter or -1 if it does not exist or is not an int
   */
  public int getIntParameter(String name) {
    if (this.conf.has(name)) {
      try {
        return this.conf.getInt(name);
      }
      catch (JSONException e) {
        return -1;
      }
    }
    else {
      return -1;
    }
  }

  /**
   * Attempts to retrieve the String value for a given parameter name.
   * 
   * @param name
   *          string containing parameter name
   * @return the String value of the parameter or null if it does not exist or is not a string
   */
  public String getStringParameter(String name) {
    if (this.conf.has(name)) {
      try {
        return this.conf.getString(name);
      }
      catch (JSONException e) {

        return null;
      }
    }
    else {
      return null;
    }
  }

  /**
   * Checks if the parameter exits in the underlying JSON Object
   * 
   * @param name
   *          string containing parameter name
   * @return boolean true if it does exist, false if not
   */
  public boolean hasParam(String name) {
    return this.conf.has(name);
  }

  /**
   * Sets an int parameter and immediately writes the config JSONObject to file. Whilst slightly inefficient it does guarantee the
   * change has been permanently saved
   * 
   * @param name
   *          parameter name
   * @param value
   *          parameter value as an int
   * @throws JSONIOException
   * @throws JSONException
   */

  public void setIntParameter(String name, int value) throws JSONIOException, JSONException {
    this.conf.put(name, value);
    IOUtils.writeJSONToFile(this.conf, this.confFile);
  }

  /**
   * Sets a string parameter and immediately writes the config JSONObject to file. Whilst slightly inefficient it does guarantee the
   * change has been permanently saved
   * 
   * @param name
   *          parameter name
   * @param value
   *          parameter value as a string
   * @throws JSONIOException
   * @throws JSONException
   */
  public void setStringParameter(String name, String value) throws JSONIOException, JSONException {
    this.conf.put(name, value);
    IOUtils.writeJSONToFile(this.conf, this.confFile);
  }
}
