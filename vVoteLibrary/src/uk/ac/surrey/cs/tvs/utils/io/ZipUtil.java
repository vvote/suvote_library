/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException;

/**
 * ZipUtils provides a safe way of opening and reading zip files.
 * 
 * ZipFiles present a range of possible attack vectors, all of which need to be protected against. Of particular note is the ZipBomb
 * <a href="http://en.wikipedia.org/wiki/Zip_bomb">Zip Bomb - Wikipedia</a>
 * 
 * We therefore are careful in both how we extract filenames and the sizes of zip entries we will accept. There is no explicit limit
 * on the size of the overall zip file, but that could be enforced elsewhere.
 * 
 * @author Chris Culnane
 * 
 */
public class ZipUtil {

  /**
   * Buffer for reading data
   */
  private static final int BUFFER      = 512;

  /**
   * Maximum size of zip entry - initially set to 1MB
   */
  private long             maxZipEntry = 1048576;

  /**
   * Constructs the ZipUtil class
   * 
   * @param maxZipEntry
   *          The maximum number of MB an ZIP entry can contain.
   * 
   */
  public ZipUtil(int maxZipEntry) {
    super();

    this.maxZipEntry = (long)maxZipEntry * 1048576L;
  }

  /**
   * Unzip the zip file from the passed in filename to the target directory
   * 
   * @param filepath
   *          file path for the zip file
   * @param targetDirectory
   *          the target directory to unzip to
   * @throws java.io.IOException
   * @throws UploadedZipFileEntryTooBigException
   * @throws UploadedZipFileNameException
   */
  public final void unzip(String filepath, File targetDirectory) throws java.io.IOException, UploadedZipFileEntryTooBigException,
      UploadedZipFileNameException {
    FileInputStream fis = new FileInputStream(filepath);
    ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
    ZipEntry entry;

    try {
      // Loop through each zip entry
      while ((entry = zis.getNextEntry()) != null) {
        int count;
        byte data[] = new byte[BUFFER];
        int total = 0;

        // Validate filename
        String name = this.validateFilename(entry.getName(), targetDirectory.getAbsolutePath());
        FileOutputStream fos = new FileOutputStream(name);
        BufferedOutputStream dest = null;
        try {
          dest = new BufferedOutputStream(fos, BUFFER);

          // Read data until max zip entry. If it exceeds stop and throw an exception
          while (total <= this.maxZipEntry && (count = zis.read(data, 0, BUFFER)) != -1) {
            dest.write(data, 0, count);
            total += count;
          }
          dest.flush();
        }
        finally {
          if (dest != null) {
            dest.close();
          }
        }
        zis.closeEntry();

        if (total > this.maxZipEntry) {
          throw new UploadedZipFileEntryTooBigException("An entry in " + filepath + " is larger than the maximum allowed size");
        }
      }
    }
    finally {
      zis.close();
    }
  }

  /**
   * Validates that the filename in the zip entry does not contain any path operators that could see it being saved elsewhere. All
   * zip contents are unzipped to a unique folder for that zip file, so there is no danger of inter-zip name collisions.
   * 
   * If the filename is malicious an exception is thrown
   * 
   * @param filename
   *          the filename in the zip entry
   * @param intendedDir
   *          the directory where we want to unzip the file to
   * @return the path to unzip
   * @throws IOException
   * @throws UploadedZipFileNameException
   */
  private String validateFilename(String filename, String intendedDir) throws IOException, UploadedZipFileNameException {
    // Create file object with intendedDir and filename
    File file = new File(intendedDir, filename);

    // Get the canonical path of that value
    String canonicalPath = file.getCanonicalPath();

    // Get file for intended dir
    File iD = new File(intendedDir);

    // Get canonical path of intended dir
    String canonicalID = iD.getCanonicalPath();

    // Check that the canonical paths match
    if (canonicalPath.startsWith(canonicalID)) {
      return canonicalPath;
    }
    else {
      throw new UploadedZipFileNameException("An entry in the zip file appears to be trying to save to a different location");
    }
  }

  /**
   * Utility method that adds a file to a ZipOutputStream whilst simultaneously adding the contents of the file to a message digest.
   * We use this during the commit calculation to construct a combined database transmission with signature of all data. The
   * filename is used as the zip entry name.
   * 
   * @param file
   *          the file to add
   * @param md
   *          the message digest to add the file to
   * @param zos
   *          the ZipOutputStream to write to
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static void addFileToZip(File file, MessageDigest md, ZipOutputStream zos) throws FileNotFoundException, IOException {
    addFileToZip(file, file.getName(), md, zos);
  }

  /**
   * Utility method that adds a file to a ZipOutputStream whilst simultaneously adding the contents of the file to a message digest.
   * We use this during the commit calculation to construct a combined database transmission with signature of all data. The
   * specified name is used for the zip entry name
   * 
   * @param file
   *          the file to add
   * @param entryName
   *          String to use as the zip entry name
   * @param md
   *          the message digest to add the file to
   * @param zos
   *          the ZipOutputStream to write to
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static void addFileToZip(File file, String entryName, MessageDigest md, ZipOutputStream zos) throws FileNotFoundException,
      IOException {
    zos.putNextEntry(new ZipEntry(entryName));

    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
    DigestInputStream dis = new DigestInputStream(bis, md);

    try {
      byte[] bytesIn = new byte[1024];
      int read = 0;

      while ((read = dis.read(bytesIn)) != -1) {
        zos.write(bytesIn, 0, read);
      }
      zos.closeEntry();
    }
    finally {
      dis.close();
    }
  }
}
