/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils;

import java.io.File;
import java.io.FileFilter;

/**
 * Default FileFilter for file backed timeouts. This file filter will return those files that end in ".timeout" which is the default
 * extension for timeout files.
 * 
 * @author Chris Culnane
 * 
 */
public class DefaultTimeoutFileFilter implements FileFilter {

  /**
   * Constant to hold the default extension
   */
  public static final String DEFAULT_TIMEOUT_FILE_EXT = ".timeout";

  /**
   * Default constructor for DefaultTimeoutFileFilter that will return files that end in ".timeout"
   */
  public DefaultTimeoutFileFilter() {
  }

  /**
   * Checks if a file ends with ".timeout" and return true if it does and false if not
   * 
   * @param pathname
   *          File to check
   * @return boolean true if it ends in .timeout false if not
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.io.FileFilter#accept(java.io.File)
   */
  @Override
  public boolean accept(File pathname) {
    if (pathname.getName().endsWith(DEFAULT_TIMEOUT_FILE_EXT)) {
      return true;
    }
    else {
      return false;
    }
  }

}
