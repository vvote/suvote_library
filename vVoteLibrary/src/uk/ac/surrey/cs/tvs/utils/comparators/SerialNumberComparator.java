/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.comparators;

import java.util.Comparator;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;

/**
 * Simple comparator to compare two serial numbers to allow them to be naturally sorted. This is required because the serial number
 * is a combination of a prefix and number. As such, built in sorting will not sort the numbers correctly.
 * 
 * @author Chris Culnane
 * 
 */
public class SerialNumberComparator implements Comparator<String> {

  /**
   * Compares two serial numbers by first comparing the prefix and then the number. If the prefix is different (pre ':') the natural
   * ordering of the prefix string is returned. If the prefix is the same the numerical order of the number will be compared.
   * 
   * @param lhs
   *          the first object to be compared.
   * @param rhs
   *          the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the
   *         second.
   */
  @Override
  public int compare(String lhs, String rhs) {
    int comp = lhs.substring(0, lhs.indexOf(MessageFields.SERIAL_NO_SEPARATOR)).compareTo(
        rhs.substring(0, rhs.indexOf(MessageFields.SERIAL_NO_SEPARATOR)));

    if (comp == 0) {
      int one = Integer.parseInt(lhs.substring(lhs.indexOf(MessageFields.SERIAL_NO_SEPARATOR) + 1));
      int two = Integer.parseInt(rhs.substring(rhs.indexOf(MessageFields.SERIAL_NO_SEPARATOR) + 1));

      return one - two;
    }
    else {
      return comp;
    }
  }
}
