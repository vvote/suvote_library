/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils;

import java.io.File;
import java.io.IOException;

import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;

/**
 * Abstract class for creating file backed timeouts. These timeouts will create a file in a specified directory, which will remain
 * even if the system crashes. On restarting a file-backed TimeoutManager the folder will be checked and any timeout events that
 * should have fired will be fired. This is achieved through the default String constructor that calls the performRecovery method
 * and should do the same processing as would have been performed when the timeout normally fired. Note: this makes no guarantees
 * that the event will be fired, it relies on the implementer correctly implementing the performRecovery method and acting correctly
 * during a recovery.
 * 
 * Of particular note is the behaviour of what should happen if the timeout should not have fired. Ideally the perform recovery
 * should place a new timeout event back into the in-memory Timeout manager, but again, that is up to the implementer to actually
 * do.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public abstract class FileBackedTimeout implements Runnable {

  /**
   * Creates the file back up. The data written out to the file will be read in and passed back via the performRecovery method. The
   * file name should match the FileFilter used during the initialisation of the FileBackedTimeoutManager. If no FileFilter is
   * specified the default FileFilter will be used which looks for an extension that ends with ".timeout". The extension can be
   * obtained via the static variable DEFAULT_TIMEOUT_FILE_EXT in the DefaultTimeoutFileFilter class.
   * 
   * 
   * @param timeout
   *          int timeout value - this should be part of the value written to the file and used to check whether to fire the event
   *          or not
   */
  public abstract void createFileBackup(int timeout) throws IOException;

  /**
   * Called when iterating over backup files in the backupDir during startup. This method should implement all the necessary actions
   * to recover from a shutdown and re-establish the timeouts. This method should check that the timeout is still valid. There is a
   * small window when a timeout could fire in-memory but the file backup not yet be removed, hence an erroneous firing could
   * happen. However, all timeouts should fire at least once. <b>IMPORTANT: This method will be called on a default constructed
   * instance of the FileBackedTimeout, therefore the implementation of this method should not rely on instance variables within the
   * class, unless the implementer guarantees they are correct via the object passed into the constructor for the FileBackedTimeout
   * manager. If in doubt treat this method like a static method</b>
   * 
   * @param file
   *          File that points to a backup file
   * @param timeoutManager
   *          the FileBackedTimeoutManager that should be used for creating new in-memory timeout
   * 
   */
  public abstract void performRecovery(File file, FileBackedTimeoutManager timeoutManager) throws IOException, MaxTimeoutExceeded;

  /**
   * Sets the underlying backup directory. This may or may not be required depending on implementation. If the backup directory is
   * stored within the disk based timeout it may not be required
   * 
   * @param backupDir File that points to the backup directory location
   */
  public abstract void setBackUpDir(File backupDir);

}
