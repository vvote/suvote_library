/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import java.io.IOException;
import java.io.StringWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.DSAParameterSpec;
import java.util.Vector;

import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.crypto.generators.DSAParametersGenerator;
import org.bouncycastle.crypto.params.DSAParameters;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;

/**
 * Utility class for generating an ECDSA KeyPair and for generating an accompanying Certificate Signing Request
 * 
 * @author Chris Culnane
 * 
 */
public class GenerateSignatureKeyAndCSR {

  /**
   * Logger
   */
  private static final Logger logger       = LoggerFactory.getLogger(GenerateSignatureKeyAndCSR.class);

  /**
   * Size of the DSA key.
   */
  private static final int    DSA_KEY_SIZE = 2048;

  /**
   * Generates a CertificateSigningRequest and returns it as a string.
   * 
   * @param dn
   *          String of the distinguished name, in standard format
   * @param pair
   *          ECDSA KeyPair we want the CSR to cover
   * @param days
   *          the number of days we are requesting the certificate for
   * @param signatureType
   *          SignatureType of underlying keys - we will sign our own request using this signature type
   * @param challengePassword
   *          Challenge Password for revocation - generally left blank because revocation is centrally managed
   * @return String containing encoded CSR
   * @throws CertificateRequestGenException
   */
  public static String generateCertificateSigningRequest(String dn, KeyPair pair, int days, SignatureType signatureType,
      String challengePassword) throws CertificateRequestGenException {
    try {
      CryptoUtils.initProvider();
      logger.info("Generating CSR for {}", dn);
      // Create the subject
      X500Name subject = new X500Name(dn);

      // Get the public key
      SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfo.getInstance(pair.getPublic().getEncoded());

      // Create a builder object
      PKCS10CertificationRequestBuilder certificationRequestBuilder = new PKCS10CertificationRequestBuilder(subject, publicKeyInfo);

      // Set how we want to use it
      certificationRequestBuilder.addAttribute(Extension.keyUsage, new KeyUsage(KeyUsage.digitalSignature
          | KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.keyAgreement));

      // Set the extended usage
      Vector<KeyPurposeId> ekUsages = new Vector<KeyPurposeId>();
      // Client and Server auth usage
      ekUsages.add(KeyPurposeId.id_kp_clientAuth);
      ekUsages.add(KeyPurposeId.id_kp_serverAuth);
      certificationRequestBuilder.addAttribute(Extension.extendedKeyUsage,
          new ExtendedKeyUsage(ekUsages.toArray(new KeyPurposeId[0])));

      // We need to sign our request
      JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder(TVSSignature.getSignatureTypeString(signatureType));
      // Set the provider as BouncyCastle
      contentSignerBuilder.setProvider(CryptoUtils.BC_PROV);

      // Create a content signer, this will do the actual signing
      ContentSigner contentSigner = contentSignerBuilder.build(pair.getPrivate());

      // We currently use a blank password
      DERPrintableString password = new DERPrintableString(challengePassword);
      certificationRequestBuilder.addAttribute(PKCSObjectIdentifiers.pkcs_9_at_challengePassword, password);

      // Build the certificate request and encode it with PEM to a string
      PKCS10CertificationRequest certificationRequest = certificationRequestBuilder.build(contentSigner);
      StringWriter sw = new StringWriter();
      PEMWriter pemWrt = new PEMWriter(sw);
      pemWrt.writeObject(certificationRequest);
      pemWrt.close();

      logger.info("Finished generating certificate request");

      return sw.toString();
    }
    catch (IOException e) {
      logger.error("Exception whilst generating CSR", e);
      throw new CertificateRequestGenException("Exception whilst generating CSR", e);
    }
    catch (OperatorCreationException e) {
      logger.error("Exception whilst generating CSR", e);
      throw new CertificateRequestGenException("Exception whilst generating CSR", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception whilst creating ContentSigner", e);
      throw new CertificateRequestGenException("Exception whilst creating ContentSigner", e);
    }
  }

  /**
   * Generates a suitable KeyPair and returns the KeyPair object.
   * 
   * @param signatureType
   *          SignatureType to generate keys for
   * @return KeyPair object
   * @throws KeyGenerationException
   */
  public static KeyPair generateKeyPair(SignatureType signatureType) throws KeyGenerationException {
    try {
      // Initialise the provider
      CryptoUtils.initProvider();
      KeyPairGenerator gen = null;

      switch (signatureType) {
        case DSA:
        case DEFAULT:
          gen = KeyPairGenerator.getInstance("DSA", CryptoUtils.BC_PROV);

          // Bouncy Castle require a convoluted way of creating with key sizes > 1024.
          DSAParametersGenerator pGen = new DSAParametersGenerator();
          pGen.init(DSA_KEY_SIZE, 80, new SecureRandom());
          DSAParameters dsaParams = pGen.generateParameters();
          DSAParameterSpec dps = new DSAParameterSpec(dsaParams.getP(), dsaParams.getQ(), dsaParams.getG());
          gen.initialize(dps);
          break;

        case ECDSA:
          // Create a ParameterSpec for the Curve
          ECUtils ecUtils = new ECUtils();
          ECParameterSpec ecSpec = ecUtils.getParams();
          gen = KeyPairGenerator.getInstance("ECDSA", CryptoUtils.BC_PROV);
          gen.initialize(ecSpec, new SecureRandom());
          break;

        case BLS:
          throw new KeyGenerationException("Not currently implemented");
        case RSA:
          gen = KeyPairGenerator.getInstance("RSA", CryptoUtils.BC_PROV);
          gen.initialize(2048);
          break;
      }

      if (gen != null) {
        KeyPair pair = gen.generateKeyPair();

        return pair;
      }
      else {
        throw new KeyGenerationException("Exception whilst creating generator");
      }
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst generating KeyPair", e);
      throw new KeyGenerationException("Exception whilst generating KeyPair", e);
    }
    catch (InvalidParameterException e) {
      logger.error("Exception whilst generating KeyPair", e);
      throw new KeyGenerationException("Exception whilst generating KeyPair", e);
    }
    catch (InvalidAlgorithmParameterException e) {
      logger.error("Exception whilst generating KeyPair", e);
      throw new KeyGenerationException("Exception whilst generating KeyPair", e);
    }
  }
}
