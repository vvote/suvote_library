/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

/**
 * Configuration file constants.
 * 
 * @author Chris Culnane
 * 
 */
public class ConfigFiles {

  /**
   * Ballot generation configuration constants.
   */
  public class BallotGenConfig {

    public static final String BALLOT_LIST                = "ballotList";
    public static final String BALLOT_DB                  = "ballotDB";
    public static final String NUMBER_BALLOTS_TO_AUDIT    = "ballotsToAudit";
    public static final String RACES                      = "races";
    public static final String RACE_NUM_CANDIDATES        = "candidates";
    public static final String NUMBER_BALLOTS_TO_GENERATE = "ballotToGenerate";

  }

  /**
   * Client configuration constants.
   */
  public class ClientConfig {

    public static final String MBB_CONF_FILE               = "MBBConfFile";

    public static final String SIGNING_KEY_ALIAS           = "clientSigningKey";
    public static final String THRESHOLD                   = "threshold";
    public static final String RANDOMNESS_THRESHOLD        = "randomnessThreshold";
    public static final String PUBLIC_KEY_SUFFIX           = "PublicKey.json";
    public static final String ELECTION_PK                 = "ElectionPublicKey";
    public static final String RANDOMNESS_FOLDER           = "RandomnessFolder";
    public static final String BASE_ENCRYPTED_IDS          = "BaseEncryptedIds";
    public static final String CIPHERS_FILE                = "CiphersFile";
    public static final String BALLOT_OUTPUT_FOLDER        = "BallotOutputFolder";
    public static final String BALLOT_GEN_CONF             = "BallotGenConf";
    public static final String ID                          = "id";
    public static final String PURPOSE                     = "purpose";
    public static final String ORGANISATION                = "organisation";
    public static final String ORGANISATION_UNIT           = "organisationUnit";
    public static final String LOCATION                    = "location";
    public static final String STATE                       = "state";
    public static final String COUNTRY                     = "country";
    public static final String CRYPTO_KEY_SIZE             = "CryptoKeySize";
    public static final String CIPHER_SUBMISSION_FILE      = "CipherSubmissionFile";
    public static final String CIPHER_SUBMISSION_RESPONSE  = "CipherSubmissionResponseFile";
    public static final String BALLOTS_TO_AUDIT_FILE       = "BallotsToAudit";
    public static final String BALLOT_GEN_AUDIT_OUTPUT     = "BallotGenAuditOutput";
    public static final String BALLOT_GEN_AUDIT_RESPONSE   = "AuditOutputFile";
    public static final String BALLOT_GEN_AUDIT_SUBMISSION = "AuditSubmissionFile";
    public static final String CERTIFICATE_LIFE            = "CertificateLife";
    public static final String CRYPTO_KP_FILENAME           = "_CryptoKeyPair.json";
    public static final String CRYPTO_PK_FILENAME           = "_Crypto" + PUBLIC_KEY_SUFFIX;

    public static final String BALLOTS_GENERATED           = "BallotsGenerated";
    public static final String BALLOTS_AUDITED             = "BallotsAudited";
    public static final String ENCRYPTION_KEY_STORE        = "EncryptionKeyStore";
    public static final String SIGNING_KEY_FILENAME        = "SigningKey.json";
    public static final String CSR_FILENAME                = "CertReq.csr";
    public static final String SSL_KEYSTORE_FILENAME       = "SSLKeyStore";
    public static final String SSL_CSR                     = "SSLCertReq.csr";
    public static final String SSL_SUFFIX                  = "SSL";

    public static final String CSR_LOCATION                = "CSRLocation";
    public static final String CERTIFICATE_LOCATION        = "CertificateLocation";
    public static final String SIGNING_KEY_STORE           = "SigningKeyStore";
    public static final String UI_PORT                     = "UIPort";
    public static final String SSL_CSR_LOCATION            = "SSLCSRLocation";
    public static final String SSL_KEYSTORE_LOCATION       = "SSLKeyStore";
    public static final String CA_CERT_LOCATION            = "CACertLocation";
    public static final String PEER_CA_CERT_LOCATION            = "PeerCACertLocation";
    public static final String CERTS_LOCATION              = "CertsLocation";
    public static final String CIPHER_SUITE                = "cipherSuite";
    public static final String SYSTEM_CERTIFICATE_STORE    = "CertKeystore";
    public static final String BALLOT_TIMEOUT              = "ballotTimeout";
    public static final String BALLOT_TIMEOUT_SEGMENT      = "ballotTimeoutSegment";
    public static final String AES_ALGORITHM               = "aesAlgorithm";
    public static final String TIMEOUT_BACKUP_DIR          = "timeoutBackupDir";
    public static final String CANCEL_AUTH_ADDRESS         = "CancelAuthAddress";
    public static final String EVENT_ID                    = "EventID";
    public static final String VVA_CA_CERT                    = "VVACA";
    public static final String VVA_CA_PW                    = "VVAKSPassword";
    

  }

  /**
   * District configuration constants.
   */
  public class DistrictConfig {

    public static final String DISTRICT_RACE   = "la";
    public static final String REGION_RACE_ATL = "lc_atl";
    public static final String REGION_RACE_BTL = "lc_btl";

  }

  /**
   * Key store configuration constants.
   */
  public class KeyStoreConfig {

    public static final String SIGNATURE_ENTITY_PWD = "signatureKeyEntityPwd";
    public static final String SIGNATURE_KEY_PATH   = "signatureKeyPath";
    public static final String SIGNATURE_KEY_PWD    = "signatureKeyPwd";
    public static final String SIGNATURE_ENTITY     = "signatureKeyEntity";
    public static final String KEY_STORE_TYPE       = "JKS";

  }

  /**
   * Private WBB configuration constants.
   */
  public class MBBConfig {

    public static final String PEERS        = "peers";
    public static final String PEER_ID      = "id";
    public static final String PEER_ADDRESS = "address";
    public static final String PEER_PORT    = "port";
    public static final String MBB_TIMEOUT  = "MBBTimeOut";
    public static final String MBB_FILE_TIMEOUT  = "MBBFileUploadTimeOut";
  }

  /**
   * Randomness configuration constants.
   */
  public class RandomnessConfig {

    public static final String COMMIT_DATA_FILE       = "commitData";
    public static final String AES_KEY_FILE           = "aesKey";
    public static final String RANDOMNESS_DATA_FILE   = "randomData";
    public static final String PEER_ID                = "peerID";
    public static final String RANDOMNESS_CONFIG_FILE = "conf.json";
    public static final String WBBSIG                 = "wbbSig";
    public static final String WBBMSG                 = "wbbMsg";
  }

  /**
   * Randomness server configuration constants.
   */
  public class RandomnessServerConfig extends KeyStoreConfig {

    public static final String UI_PORT                  = "uiPort";
    public static final String UI_FOLDER                = "uiFolder";
    public static final String KEYS_TO_PROCESS          = "keyToProcess";
    public static final String PROCESSED_KEYS           = "processedKeys";
    public static final String BASE_OUTPUT_FOLDER       = "baseOutputFolder";
    public static final String ROWS_TO_GEN              = "rows";
    public static final String SERIAL_NO_FILENAME       = "serialNumberFilename";
    public static final String RANDOM_DATA_FILENAME     = "randomDataFilename";
    public static final String COMMIT_DATA_FILENAME     = "commitDataFilename";
    public static final String WBB_SUB_FILENAME         = "wbbSubmissionFilename";

    public static final String AES_KEY_FILENAME         = "AESFilename";
    public static final String MBB_CONF_FILE            = "MBBConfFile";
    public static final String COLS_TO_GEN              = "columns";
    public static final String BIT_SIZE                 = "bitSize";

    public static final String PEER_ID                  = "peerID";
    public static final String CSR_FOLDER               = "csrFolder";
    public static final String CA_CERT_LOCATION         = "caCertLocation";
    public static final String CIPHER_SUITE             = "cipherSuite";
    public static final String SYSTEM_CERTIFICATE_STORE = "certKeystore";

    public static final String THRESHOLD                = "threshold";
    public static final String ELGAMAL_ALGORITHM        = "elGamalAlg";

  }
}
