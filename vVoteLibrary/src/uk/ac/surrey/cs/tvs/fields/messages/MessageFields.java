/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.DistrictConfig;

/**
 * Constants for all of the different message fields.
 * 
 * @author Chris Culnane
 * 
 */
public class MessageFields {

  /**
   * Audit message fields.
   */
  public static class AuditMessage extends ExternalMessage {

    public static final String INTERNAL_REDUCED_PERMUTATION = "_reducedPerms";
    public static final String PERMUTATION                  = "permutation";
    public static final String COMMIT_WITNESS               = "commitWitness";
  }

  /**
   * Ballot message fields.
   */
  public static class Ballot {

    public static final String SERIAL_NO             = "serialNo";
    public static final String PERMUTATIONS          = "permutations";
    public static final String COMBINED_RANDOMNESS   = "combinedRandomness";
    public static final String COMMIT_WITNESS        = "commitWitness";
    public static final String BALLOT_FILE_EXTENSION = ".json";
  }

  /**
   * Ballot file message fields.
   */
  public static class BallotFileMessage extends FileMessage {

  }

  public static class BallotAuditCommitMessage extends BallotFileMessage {

    public static final String AUDIT_DATA_ZIP_ENTRY          = "AuditDataFile.json";
    public static final String SUBMISSION_RESPONSE_ZIP_ENTRY = "BallotSubmitResponseFile.json";
  }

  /**
   * Cancel message fields.
   */
  public static class CancelMessage {

    public static final String ID         = "serialNo";
    public static final String SENDER_SIG = "boothSig";
    public static final String SENDER_ID  = "boothID";
    public static final String AUTH_SIG   = "cancelAuthSig";
    public static final String AUTH_ID    = "cancelAuthID";
  }

  /**
   * Cancel Auth Request.
   */
  public static class CancelAuthMessage {

    public static final String EVENT_ID = "eventId";

  }

  /**
   * Cipher message fields.
   */
  public static class Cipher {

    public static final String ID      = "serialNo";
    public static final String CIPHERS = "ciphers";
  }

  /**
   * Commit round 1 message fields.
   */
  public static class Commit {

    public static final String COMMIT_ID      = "commitID";
    public static final String SIGNATURE      = "signature";
    public static final String HASH           = "hash";
    public static final String DATETIME       = "dateTime";
    public static final String CURRENT_COMMIT = "currentCommit";
    public static final String COMMITS        = "commits";
    public static final String COMMIT_TIME    = "commitTime";
    public static final String PEER_ID        = "peerID";
    public static final String COMMIT_DESC    = "commitDesc";
  }

  /**
   * Commit round 2 message fields.
   */
  public static class CommitR2 {

    public static final String FROM_COMMIT         = "fromCommit";
    public static final String MSG_TYPE            = "type";
    public static final String TYPE_STRING         = "peerfile";
    public static final String COMMIT_ID           = "commitID";
    public static final String COMMIT_TIME         = "commitTime";
    public static final String FILESIZE            = "fileSize";
    public static final String FILENAME            = "fileName";
    public static final String ATTACHMENT_SIZE     = "attachmentSize";
    public static final String ATTACHMENT_FILENAME = "attachmentFileName";
    public static final String DIGEST              = "digest";
    public static final String PEER_ID             = "peerID";
    public static final String PEER_SIGNATURE      = "peerSig";
    public static final String INTERNAL_FILENAME   = "_fileName";
    public static final String INTERNAL_DIGEST     = "_digest";
  }

  /**
   * Commit round 2 signature message fields.
   */
  public static class CommitSK2 {

    public static final String HASH            = "hash";
    public static final String COMMIT_TIME     = "commitTime";
    public static final String COMMIT_DESC     = "commitDesc";
    public static final String PEER_ID         = "WBBID";
    public static final String PEER_SIG        = "WBBSig";
    public static final String FILE_SIZE       = "fileSize";
    public static final String ATTACHMENT_SIZE = "fileSizeAttachment";

  }

  /**
   * Generic error message fields.
   */
  public static class ErrorMessage {

    public static final String PEER_ID    = "WBBID";
    public static final String PEER_SIG   = "WBBSig";
    public static final String MESSAGE    = "msg";
    public static final String ID         = "serialNo";
    public static final String TYPE_ERROR = "ERROR";

  }

  /**
   * Generic external message fields.
   */
  public static class ExternalMessage extends JSONWBBMessage {

  }

  /**
   * Generic file message fields.
   */
  public static class FileMessage extends ExternalMessage {

    public static final String ID                = "submissionID";
    public static final String FILESIZE          = "fileSize";
    public static final String SENDER_SIG        = "boothSig";
    public static final String SENDER_ID         = "boothID";
    public static final String INTERNAL_FILENAME = "_fileName";
    public static final String INTERNAL_DIGEST   = "_digest";
    public static final String SAFE_UPLOAD_DIR   = "safeUploadDir";
    public static final String DIGEST            = "digest";
    public static final String DESCRIPTION       = "desc";
  }

  /**
   * Generic message fields.
   */
  public static class JSONWBBMessage {

    public static final String ID          = "serialNo";
    public static final String TYPE        = "type";
    public static final String PEER_ID     = "WBBID";
    public static final String PEER_SIG    = "WBBSig";
    public static final String COMMIT_TIME = "commitTime";
    public static final String DISTRICT    = "district";
    public static final String SENDER_SIG  = "boothSig";
    public static final String SENDER_ID   = "boothID";
    public static final String SERIAL_SIG  = "serialSig";

  }

  /**
   * Mixnet random commit message fields.
   */
  public static class MixRandomCommit extends BallotFileMessage {

    public static final String PRINTER_ID = "printerID";
  }

  /**
   * Peer cancel message fields.
   */
  public static class PeerCancelMessage extends PeerMessage {

  }

  /**
   * Peer file message fields.
   */
  public static class PeerFile {

    public static final String ATTACHMENT_FILENAME = "attachmentFileName";
    public static final String FILENAME            = "fileName";
    public static final String ATTACHMENT_SIZE     = "attachmentSize";
    public static final String SAFE_UPLOAD_DIR     = "safeUploadDir";
  }

  /**
   * Peer message fields.
   */
  public static class PeerMessage {

    public static final String PEER_SIG = "signature";
    public static final String ID       = "serialNo";
    public static final String TYPE     = "type";
    public static final String PEER_ID  = "peerID";
  }

  /**
   * Peer POD message fields.
   */
  public static class PeerPODMessage extends PeerMessage {

  }

  /**
   * Peer response message fields.
   */
  public static class PeerResponse {

    public static final String PEER_ID     = "WBBID";
    public static final String PEER_SIG    = "WBBSig";
    public static final String COMMIT_TIME = "commitTime";
  }

  /**
   * POD message fields.
   */
  public static class PODMessage extends ExternalMessage {

    public static final String   BALLOT_REDUCTIONS                   = "ballotReductions";
    public static final String   BALLOT_REDUCTIONS_PERMUTATION_INDEX = "index";
    public static final String   BALLOT_REDUCTIONS_CANDIDATE_INDEX   = "candidateIndex";
    public static final String   BALLOT_REDUCTIONS_RANDOMNESS        = "randomness";
    public static final String[] RACE_ORDER                          = { DistrictConfig.DISTRICT_RACE,
                                                                         DistrictConfig.REGION_RACE_ATL,
                                                                         DistrictConfig.REGION_RACE_BTL };
  }

  /**
   * Randomness file message fields.
   */
  public static class RandomnessFile {

    public static final String RANDOMNESS          = "randomness";
    public static final String RANDOM_VALUE        = "r";
    public static final String RANDOM_VALUE_COMMIT = "rComm";
    public static final String SERIAL_NO           = "serialNo";
    public static final String PEER_ID             = "peerID";
  }

  /**
   * Start EVM message fields.
   */
  public static class StartEVMMessage extends ExternalMessage {

  }

  /**
   * Vote message fields.
   */
  public static class VoteMessage extends ExternalMessage {

    public static final String RACES          = "races";
    public static final String LC_ATL         = "LC_ATL";
    public static final String LC_BTL         = "LC_BTL";
    public static final String LA             = "LA";
    public static final String PREFERENCES    = "preferences";
    public static final String RACE_ID        = "id";
    public static final String QUOTE_CHAR     = MessageFields.QUOTE_CHAR;
    public static final String INTERNAL_PREFS = "_vPrefs";
    public static final String START_EVM_SIG  = "startEVMSig";
  }

  /**
   * Private WBB cipher message fields.
   */
  public static class WBBCipher {

    public static final String SERIAL_NO   = "serialNo";
    public static final String PERMUTATION = "permutation";
    public static final String CIPHERS     = "ciphers";
  }

  public static final String UUID                 = "_uuid";
  public static final String FROM_PEER            = "_fromPeer";
  public static final String TYPE                 = "type";
  public static final String RACE_SEPARATOR       = ":";
  public static final String PREFERENCE_SEPARATOR = ",";
  public static final char   SERIAL_NO_SEPARATOR  = ':';
  public static final String QUOTE_CHAR           = "\"";

}
