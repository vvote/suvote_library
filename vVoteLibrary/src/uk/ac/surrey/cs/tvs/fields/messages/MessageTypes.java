/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

/**
 * Message type constants.
 * 
 * @author Chris Culnane
 * 
 */
public class MessageTypes {

  public static final String BALLOT_GEN_COMMIT   = "ballotgencommit";
  public static final String BALLOT_AUDIT_COMMIT = "ballotauditcommit";
  public static final String AUDIT               = "audit";
  public static final String CANCEL              = "cancel";
  public static final String MIX_RANDOM_COMMIT   = "mixrandomcommit";
}
