/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

/**
 * System-wide constants.
 * 
 * @author Chris Culnane
 * 
 */
public class SystemConstants {

  public static final String DEFAULT_MESSAGE_DIGEST = "SHA1";       // TODO set this to SHA256
  public static final String SIGNING_KEY_SK2_SUFFIX = "_SigningSK2";
  public static final String SIGNING_KEY_SK1_SUFFIX = "_SigningSK1";
  public static final String JOINT_KEY = "WBB";
}
