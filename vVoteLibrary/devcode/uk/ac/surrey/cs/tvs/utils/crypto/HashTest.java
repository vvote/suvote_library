/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base64;

import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CommitException;

/**
 * @author Chris Culnane
 * 
 */
public class HashTest {

  /**
   * 
   */
  public HashTest() {

  }

  /**
   * @param args
   * @throws CommitException
   * @throws NoSuchAlgorithmException
   */
  public static void main(String[] args) throws CommitException, NoSuchAlgorithmException {
    SecureRandom sr = new SecureRandom();
    byte[] rand = new byte[256 / 8];
    sr.nextBytes(rand);
    // rand = Base64.decodeBase64("OWoLlIDZNuUvcBytRDT3QKJiCv2tXOeHYWsg3xk9SJE=");
    System.out.println(rand.length);
    CryptoUtils.initProvider();
    MessageDigest md = MessageDigest.getInstance("SHA256");
    System.out.println(md.digest("hello".getBytes()).length);
    byte[] commData = CryptoUtils.generateCommitment(
        md.digest("hesddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddllo".getBytes()), rand);
    // System.out.println(Base64.encodeBase64String(commit.getSecret()));
    System.out.println(Base64.encodeBase64String(rand));
    // System.out.println(Base64.encodeBase64String(commData[1]));
    System.out.println(CryptoUtils.checkCommitment(commData, rand,
        md.digest("hesddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddllo".getBytes())));
  }

}
