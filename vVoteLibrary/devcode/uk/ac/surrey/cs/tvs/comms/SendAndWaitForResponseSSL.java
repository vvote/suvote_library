/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a utility class that handles sending a message to a remote peer and waiting for a response. It implements the Callable
 * interface, returning a String containing either the JSON response or an error string. When invoking this a timeout should be used
 * to ensure it does not wait forever. This class itself does not impose such a timeout.
 * 
 * @author Chris Culnane
 * 
 */
public class SendAndWaitForResponseSSL implements Callable<String> {

  /**
   * Logger
   */
  private static final Logger logger              = LoggerFactory.getLogger(SendAndWaitForResponseSSL.class);

  /**
   * Default time to wait (in ms) between retries
   */
  private static final int    DEFAULT_RETRY_DELAY = 2000;

  /**
   * The IP address to send the message to
   */
  private String              address;

  /**
   * Port number to connect to
   */
  private int                 port;

  /**
   * Message to send, could be several messages or a composite message
   */
  private String              message;

  /**
   * Holds the retry delay between send attempts
   */
  private int                 retryDelay;

  private boolean             useSSL              = false;
  
  private String[] cipherSuites=null;
  
  private ArrayList<ResponseListener> responseListeners = new ArrayList<ResponseListener>();
  /**
   * Constructs and SendAndWaitForResponse and sets the initial values. Uses the default retryDelay of 2 seconds
   * 
   * @param address
   *          string address (IP address) of remote peer
   * @param port
   *          int port number to connect to
   * @param message
   *          string message to send
   */
  public SendAndWaitForResponseSSL(String address, int port, String message) {
    this(address, port, message, SendAndWaitForResponseSSL.DEFAULT_RETRY_DELAY);
  }

  public void setSSLSocketFactory(SSLSocketFactory factory) {
    this.factory = factory;
    this.useSSL = true;
  }

  public void addResponseListener(ResponseListener listener){
    this.responseListeners.add(listener);
  }
  public void removeResponseListener(ResponseListener listener){
    if(this.responseListeners.contains(listener)){
      this.responseListeners.remove(listener);
    }
  }
  private void fireResponse(String response){
    for(ResponseListener listener:this.responseListeners){
      listener.responseFromWait(response);
    }
  }
  private SSLSocketFactory factory;

  public void setCipherSuite(String suite){
    logger.info("Setting cipher suite to only accept {}",suite);
    this.cipherSuites = new String[]{suite};
  }
  public void setCipherSuites(String[] suites){
    logger.info("Setting cipher suite to accept any of {}",Arrays.toString(suites));
    this.cipherSuites=suites;
  }
  /**
   * Constructs and SendAndWaitForResponse and sets the initial values
   * 
   * @param address
   *          string address (IP address) of remote peer
   * @param port
   *          int port number to connect to
   * @param message
   *          string message to send
   * @param retryDelay
   *          int delay to between send attempts
   */
  public SendAndWaitForResponseSSL(String address, int port, String message, int retryDelay) {
    super();

    this.address = address;
    this.port = port;
    this.message = message;
    this.retryDelay = retryDelay;

  }

  /**
   * Call method is where the actual work takes places, called via an ExecutorService. Connects to remote host, sends message and
   * waits for response. The response is returned via Futures.
   * 
   * @see java.util.concurrent.Callable#call()
   */
  @Override
  public String call() throws Exception {
    logger.info("Created SendAndWaitForResponse object for {}:{}", this.address, this.port);

    // Create socket address and socket
    InetSocketAddress isa = new InetSocketAddress(this.address, this.port);

    Socket sock = null;

    // We will keep trying until successful or interrupted
    while (true) {
      try {
        if (useSSL) {
          // Create new SSLSocket
          sock = (SSLSocket) this.factory.createSocket();
          if(this.cipherSuites!=null){
            logger.info("Using custom cipher suite list");
            ((SSLSocket)sock).setEnabledCipherSuites(this.cipherSuites);
          }
          // We want to use ClientAuthentication (mutual authentication)
          ((SSLSocket)sock).setUseClientMode(true);
        }else{
          // Create a new socket - the last one may have closed
          sock = new Socket();
        }
        
        // Connect to socket address
        sock.connect(isa);
        
       
        // A single message is just the JSON string, no newline. However, a file message will contain a newline and the path to the
        // file
        StringTokenizer st = new StringTokenizer(this.message, MBB.MESSAGE_SEPARATOR);

        // Loop through the tokens - could be just the message
        while (st.hasMoreTokens()) {
          String m = st.nextToken();
          // If this is a file message the token will be FILE: otherwise we are dealing with an actual message
          if (m.startsWith(MBB.FILE_SUB_TOKEN)) {
            logger.info("About to send File message {}", m);

            // Open InputStream to read the file - path is token without "FILE:"
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(m.substring(MBB.FILE_SUB_TOKEN.length())));

            // Prepare the output stream
            BufferedOutputStream bos = new BufferedOutputStream(sock.getOutputStream());

            // read the file from the local machine and send it to the output stream
            byte[] buf = new byte[1024];
            int read = 0;
            while ((read = bis.read(buf)) != -1) {
              bos.write(buf, 0, read);
            }
            bos.flush();
            bis.close();

            logger.info("Finished sending File message {}", m);
          }
          else {
            // This is a normal message, just write it to the stream
            logger.info("Sending message");

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
            bw.write(m);
            bw.newLine();
            bw.flush();
          }
        }

        // We will wait for a response - all responses end with a newline, so create a reader and call readLine
        BufferedReader br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        String response = br.readLine();
        logger.info("Received response:{}", response);

        // We have a response, close the socket and return it
        sock.close();
        this.fireResponse(response);
        return response;
      }
      catch (IOException e) {
        logger.warn("An error has occured when trying to connect/send to {}. The server could be offline. Will retry in 2 seconds",
            isa, e);
        try {
          // Sleep for a set amount of time
          Thread.sleep(this.retryDelay);
        }
        catch (InterruptedException ie) {
          logger.warn("Whilst waiting to retry a connection to {}, we have been interrupted. Message has not been sent.", isa);
          // Check if the socket is still open
          if (sock != null) {
            try {
              sock.close();
            }
            catch (IOException e2) {
              logger.warn("After an interruption tried closing the socket and received an exception", e2);
            }
          }

          // Get out the loop
          return null;
        }

      }
      finally {
        // Tidy up socket just in case it is still open
        if (sock != null) {
          try {
            sock.close();
          }
          catch (IOException e) {
            logger.warn("Exception closing socket in finally block.", e);
          }
        }
      }
    }
  }
}
