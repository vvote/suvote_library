/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * The class <code>ClientConstantsTest</code> contains tests for the class <code>{@link ClientConstants}</code>.
 */
public class ClientConstantsTest {

  /**
   * Run the ClientConstants() constructor test.
   */
  @Test
  public void testClientConstants_1() throws Exception {
    ClientConstants result = new ClientConstants();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.AESKeyFile() constructor test.
   */
  @Test
  public void testClientConstantsAESKeyFile_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.AESKeyFile result = fixture.new AESKeyFile();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.AuditFile() constructor test.
   */
  @Test
  public void testClientConstantsAuditFile_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.AuditFile result = fixture.new AuditFile();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.BallotAuditCommitResponse() constructor test.
   */
  @Test
  public void testClientConstantsBallotAuditCommitResponse_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.BallotAuditCommitResponse result = fixture.new BallotAuditCommitResponse();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.BallotDB() constructor test.
   */
  @Test
  public void testClientConstantsBallotDB_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.BallotDB result = fixture.new BallotDB();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.BallotGenCommitResponse() constructor test.
   */
  @Test
  public void testClientConstantsBallotGenCommitResponse_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.BallotGenCommitResponse result = fixture.new BallotGenCommitResponse();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UI() constructor test.
   */
  @Test
  public void testClientConstantsUI_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UI result = fixture.new UI();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UIAUDITResponse() constructor test.
   */
  @Test
  public void testClientConstantsUIAUDITResponse_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UIAUDITResponse result = fixture.new UIAUDITResponse();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UIERROR() constructor test.
   */
  @Test
  public void testClientConstantsUIERROR_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UIERROR result = fixture.new UIERROR();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UIMessage() constructor test.
   */
  @Test
  public void testClientConstantsUIMessage_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UIMessage result = fixture.new UIMessage();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UIPODMessage() constructor test.
   */
  @Test
  public void testClientConstantsUIPODMessage_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UIPODMessage result = fixture.new UIPODMessage();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UIPODResponse() constructor test.
   */
  @Test
  public void testClientConstantsUIPODResponse_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UIPODResponse result = fixture.new UIPODResponse();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UIResponse() constructor test.
   */
  @Test
  public void testClientConstantsUIResponse_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UIResponse result = fixture.new UIResponse();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UISTARTEVMMessage() constructor test.
   */
  @Test
  public void testClientConstantsUISTARTEVMMessage_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UISTARTEVMMessage result = fixture.new UISTARTEVMMessage();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UISTARTEVMResponse() constructor test.
   */
  @Test
  public void testClientConstantsUISTARTEVMResponse_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UISTARTEVMResponse result = fixture.new UISTARTEVMResponse();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UIVOTEMessage() constructor test.
   */
  @Test
  public void testClientConstantsUIVOTEMessage_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UIVOTEMessage result = fixture.new UIVOTEMessage();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.UIVOTEResponse() constructor test.
   */
  @Test
  public void testClientConstantsUIVOTEResponse_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.UIVOTEResponse result = fixture.new UIVOTEResponse();
    assertNotNull(result);
  }

  /**
   * Run the ClientConstants.X509() constructor test.
   */
  @Test
  public void testClientConstantsX509_1() throws Exception {
    ClientConstants fixture = new ClientConstants();

    ClientConstants.X509 result = fixture.new X509();
    assertNotNull(result);
  }
}