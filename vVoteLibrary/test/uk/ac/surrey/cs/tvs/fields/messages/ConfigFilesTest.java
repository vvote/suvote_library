/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * The class <code>ConfigFilesTest</code> contains tests for the class <code>{@link ConfigFiles}</code>.
 */
public class ConfigFilesTest {

  /**
   * Run the ConfigFiles() constructor test.
   */
  @Test
  public void testConfigFiles_1() throws Exception {
    ConfigFiles result = new ConfigFiles();
    assertNotNull(result);
  }

  /**
   * Run the ConfigFiles.BallotGenConfig() constructor test.
   */
  @Test
  public void testConfigFilesBallotGenConfig_1() throws Exception {
    ConfigFiles fixture = new ConfigFiles();

    ConfigFiles.BallotGenConfig result = fixture.new BallotGenConfig();
    assertNotNull(result);
  }

  /**
   * Run the ConfigFiles.ClientConfig() constructor test.
   */
  @Test
  public void testConfigFilesClientConfig_1() throws Exception {
    ConfigFiles fixture = new ConfigFiles();

    ConfigFiles.ClientConfig result = fixture.new ClientConfig();
    assertNotNull(result);
  }

  /**
   * Run the ConfigFiles.DistrictConfig() constructor test.
   */
  @Test
  public void testConfigFilesDistrictConfig_1() throws Exception {
    ConfigFiles fixture = new ConfigFiles();

    ConfigFiles.DistrictConfig result = fixture.new DistrictConfig();
    assertNotNull(result);
  }

  /**
   * Run the ConfigFiles.KeyStoreConfig() constructor test.
   */
  @Test
  public void testConfigFilesKeyStoreConfig_1() throws Exception {
    ConfigFiles fixture = new ConfigFiles();

    ConfigFiles.KeyStoreConfig result = fixture.new KeyStoreConfig();
    assertNotNull(result);
  }

  /**
   * Run the ConfigFiles.MBBConfig() constructor test.
   */
  @Test
  public void testConfigFilesMBBConfig_1() throws Exception {
    ConfigFiles fixture = new ConfigFiles();

    ConfigFiles.MBBConfig result = fixture.new MBBConfig();
    assertNotNull(result);
  }

  /**
   * Run the ConfigFiles.RandomnessConfig() constructor test.
   */
  @Test
  public void testConfigFilesRandomnessConfig_1() throws Exception {
    ConfigFiles fixture = new ConfigFiles();

    ConfigFiles.RandomnessConfig result = fixture.new RandomnessConfig();
    assertNotNull(result);
  }

  /**
   * Run the ConfigFiles.RandomnessServerConfig() constructor test.
   */
  @Test
  public void testConfigFilesRandomnessServerConfig_1() throws Exception {
    ConfigFiles fixture = new ConfigFiles();

    ConfigFiles.RandomnessServerConfig result = fixture.new RandomnessServerConfig();
    assertNotNull(result);
  }

}