/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.fields.messages;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * The class <code>JSONConstantsTest</code> contains tests for the class <code>{@link JSONConstants}</code>.
 */
public class JSONConstantsTest {

  /**
   * Run the JSONConstants() constructor test.
   */
  @Test
  public void testJSONConstants_1() throws Exception {
    JSONConstants result = new JSONConstants();
    assertNotNull(result);
  }

  /**
   * Run the JSONConstants.ProgressMessage() constructor test.
   */
  @Test
  public void testJSONConstantsProgressMessage_1() throws Exception {
    JSONConstants fixture = new JSONConstants();

    JSONConstants.ProgressMessage result = fixture.new ProgressMessage();
    assertNotNull(result);
  }

  /**
   * Run the JSONConstants.SchemaProcessing() constructor test.
   */
  @Test
  public void testJSONConstantsSchemaProcessing_1() throws Exception {
    JSONConstants fixture = new JSONConstants();

    JSONConstants.SchemaProcessing result = fixture.new SchemaProcessing();
    assertNotNull(result);
  }

  /**
   * Run the JSONConstants.Signature() constructor test.
   */
  @Test
  public void testJSONConstantsSignature_1() throws Exception {
    JSONConstants fixture = new JSONConstants();

    JSONConstants.Signature result = fixture.new Signature();
    assertNotNull(result);
  }

  /**
   * Run the JSONConstants.Vote() constructor test.
   */
  @Test
  public void testJSONConstantsVote_1() throws Exception {
    JSONConstants fixture = new JSONConstants();

    JSONConstants.Vote result = fixture.new Vote();
    assertNotNull(result);
  }

}