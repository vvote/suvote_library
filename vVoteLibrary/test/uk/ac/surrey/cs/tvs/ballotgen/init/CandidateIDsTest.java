/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.init;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * The class <code>CandidateIDsTest</code> contains tests for the class <code>{@link CandidateIDs}</code>.
 */
public class CandidateIDsTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the output files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the output files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Run the CandidateIDs() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidateIDs_1() throws Exception {
    CandidateIDs result = new CandidateIDs();
    assertNotNull(result);
  }

  /**
   * Run the void create(int,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreate_1() throws Exception {
    // Generate the ids.
    CandidateIDs.create(LibraryTestParameters.CANDIDATES, LibraryTestParameters.PUBLIC_KEY_FILE,
        LibraryTestParameters.OUTPUT_PLAIN_TEXT_FILE, LibraryTestParameters.OUTPUT_ENCRYPTED_FILE);

    // Test that the files have been created with the correct content.
    this.testFiles();
  }

  /**
   * Tests that the correct files have been created and that their content matches (plain text vs. encrypted).
   * 
   * @throws IOException
   * @throws JSONException
   * @throws JSONIOException
   */
  private void testFiles() throws IOException, JSONException, JSONIOException {
    // Test that each file has the correct number of content.
    ECUtils group = new ECUtils();
    int plainCount = 0;
    int encryptedCount = 0;

    BufferedReader plainReader = null;
    BufferedReader encryptedReader = null;

    try {
      plainReader = new BufferedReader(new FileReader(LibraryTestParameters.OUTPUT_PLAIN_TEXT_FILE));
      encryptedReader = new BufferedReader(new FileReader(LibraryTestParameters.OUTPUT_ENCRYPTED_FILE));

      String plainLine = plainReader.readLine();
      String encryptedLine = encryptedReader.readLine();

      while ((plainLine != null) && (encryptedLine != null)) {
        plainCount++;
        encryptedCount++;

        // Convert the lines into JSON arrays.
        JSONArray plainArray = new JSONArray(plainLine);
        JSONArray encryptedArray = new JSONArray(encryptedLine);

        assertEquals(LibraryTestParameters.CANDIDATES, plainArray.length());
        assertEquals(LibraryTestParameters.CANDIDATES, encryptedArray.length());

        // Check the content. We cannot check if the encrypted content really is an encrypted form of the plain text because of the
        // randomness used in the encryption.
        for (int i = 0; i < plainArray.length(); i++) {
          // Get the plain text point.
          ECPoint point = group.pointFromJSON(plainArray.getJSONObject(i));
          assertNotNull(point);

          // Get the encrypted point.
          ECPoint[] points = group.jsonToCipher(encryptedArray.getJSONObject(i));
          assertNotNull(points);
          assertEquals(2, points.length);
        }

        plainLine = plainReader.readLine();
        encryptedLine = encryptedReader.readLine();
      }
    }
    // Pass all exceptions up.
    finally {
      if (plainReader != null) {
        plainReader.close();
      }
      if (encryptedReader != null) {
        encryptedReader.close();
      }
    }

    // Check the number of objects: only 1 each.
    assertEquals(1, plainCount);
    assertEquals(1, encryptedCount);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    // Test missing arguments.
    CandidateIDs.main(new String[0]);

    // No files should have been created.
    assertFalse(new File(LibraryTestParameters.OUTPUT_PLAIN_TEXT_FILE).exists());
    assertFalse(new File(LibraryTestParameters.OUTPUT_ENCRYPTED_FILE).exists());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    // Test invalid arguments.
    CandidateIDs.main(new String[] { "rubbish", "rubbish", "rubbish", "rubbish" });

    // No files should have been created.
    assertFalse(new File(LibraryTestParameters.OUTPUT_PLAIN_TEXT_FILE).exists());
    assertFalse(new File(LibraryTestParameters.OUTPUT_ENCRYPTED_FILE).exists());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_3() throws Exception {
    // Test valid arguments.
    CandidateIDs.main(new String[] { Integer.toString(LibraryTestParameters.CANDIDATES), LibraryTestParameters.PUBLIC_KEY_FILE,
        LibraryTestParameters.OUTPUT_PLAIN_TEXT_FILE, LibraryTestParameters.OUTPUT_ENCRYPTED_FILE });

    // Test that the files have been created with the correct content.
    this.testFiles();
  }
}