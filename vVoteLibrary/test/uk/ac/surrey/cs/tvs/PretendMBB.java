/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.ErrorMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;

/**
 * Class which pretends to be an MBB by listening on a port.
 */
public class PretendMBB extends Thread {

  /**
   * Thread to process a message and a file.
   */
  private class DataThread extends Thread {

    /** The client socket. */
    private Socket     socket   = null;

    /** The response to send. */
    private String     response = null;

    /** The message that has been read. */
    private JSONObject message  = null;

    /** The file data that has been read. */
    private byte[]     data     = null;

    /** Are we expecting a file? */
    private boolean    file     = true;

    /**
     * @param socket
     *          The client socket.
     * @param response
     *          The response to send.
     * @param file
     *          Do we expect a file as well?
     */
    public DataThread(Socket socket, String response, boolean file) {
      super();

      this.socket = socket;
      this.response = response;
      this.file = file;
    }

    /**
     * @return the data.
     */
    private byte[] getData() {
      return this.data;
    }

    /**
     * @return the message.
     */
    private JSONObject getMessage() {
      return this.message;
    }

    /**
     * Processes the data for a pretend MBB.
     * 
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      BufferedInputStream in = null;
      BufferedWriter out = null;

      try {
        // Suck all the data and save it away.
        in = new BufferedInputStream(this.socket.getInputStream());
        boolean readMessage = false;
        boolean finished = false;
        int readLength = 0;
        int count = 0;
        int b = in.read();

        while ((b != -1) && !finished) {
          count++;

          // We expect a message.
          if (!readMessage && (b == '\n')) {
            this.message = new JSONObject(buffer.toString());

            // See if we expect a file.
            if (this.file) {
              count = 0;
              readLength = (Integer) this.message.get("fileSize");
            }

            readMessage = true;
            buffer.reset();
          }

          // And maybe a file.
          if (readMessage && count >= readLength) {
            finished = true;
          }

          buffer.write(b);

          // Only read in the required amount of data.
          if (!finished) {
            b = in.read();
          }
        }

        this.data = buffer.toByteArray();

        // Send the response.
        out = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
        out.write(this.response);
        out.newLine();
        out.flush();
      }
      catch (IOException e){
        e.printStackTrace();
      }catch(JSONException e) {
        e.printStackTrace();
      }
      finally {
        if (out != null) {
          try {
            out.close();
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }
        if (in != null) {
          try {
            in.close();
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }

        try {
          this.socket.close();
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /** The port to listen on. */
  private int              port         = 0;

  /** The response to send. */
  private String           response     = null;

  /** The listening server socket. */
  private SSLServerSocket  serverSocket = null;

  /** The data threads. */
  private List<DataThread> threads      = new ArrayList<DataThread>();

  /** Are we running? */
  private boolean          running      = true;

  /** Are we expecting a file? */
  private boolean          file         = true;

  /**
   * Constructor for pretend MBB. Private so that the MBB can only be started and stopped via the static methods.
   * 
   * @param port
   *          The port to listen on.
   * @param response
   *          The response to send.
   */
  private PretendMBB(int port, String response) {
    super();

    this.port = port;
    this.response = response;
  }

  /**
   * @return All of the sub-thread data.
   */
  public List<byte[]> getData() {
    List<byte[]> data = new ArrayList<byte[]>();

    for (DataThread thread : this.threads) {
      data.add(thread.getData());
    }

    return data;
  }

  /**
   * @return All of the sub-thread messages.
   */
  public List<JSONObject> getMessages() {
    List<JSONObject> messages = new ArrayList<JSONObject>();

    for (DataThread thread : this.threads) {
      messages.add(thread.getMessage());
    }

    return messages;
  }

  /**
   * Initialises and returns the SSL socket factory needed to create server SSL connections.
   * 
   * @return The SSL socket factory.
   * @throws CryptoIOException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws UnrecoverableKeyException
   * @throws KeyManagementException
   */
  public SSLServerSocketFactory getServerSSLSocketFactory() throws CryptoIOException, NoSuchAlgorithmException,
      UnrecoverableKeyException, KeyStoreException, KeyManagementException {
    // Initialise the trust store for the server key.
    TrustManagerFactory tmf = null;
    KeyManagerFactory kmf = null;

    KeyStore keyStore = CryptoUtils.loadKeyStore(LibraryTestParameters.CLIENT_KEY_STORE,
        LibraryTestParameters.CLIENT_KEY_STORE_PASSWORD.toCharArray());

    tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    tmf.init(keyStore);

    kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    kmf.init(keyStore, LibraryTestParameters.CLIENT_KEY_STORE_PASSWORD.toCharArray());

    // Create the server socket factory using the trust store.
    SSLContext context = SSLContext.getInstance("TLS");

    context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
    return context.getServerSocketFactory();
  }

  /**
   * @return the whether the MBB expects a file as well as a message.
   */
  public boolean isFile() {
    return this.file;
  }

  /**
   * Pretends to be the MBB by listening.
   * 
   * @see java.lang.Thread#run()
   */
  @Override
  public void run() {
    // Listen on the specified port and wait for a connection.
    try {
      this.serverSocket = (SSLServerSocket) this.getServerSSLSocketFactory().createServerSocket();
      this.serverSocket.setEnabledCipherSuites(new String[] { LibraryTestParameters.CIPHERS });
      this.serverSocket.setNeedClientAuth(true);
      this.serverSocket.setUseClientMode(false);
      this.serverSocket.setReuseAddress(true);

      InetSocketAddress address = new InetSocketAddress(this.port);
      this.serverSocket.bind(address);

      while (this.running) {
        DataThread thread = new DataThread(this.serverSocket.accept(), this.response, this.file);
        thread.start();
        this.threads.add(thread);
      }
    }
    catch (IOException e) {
      // Ignore - we are expecting a socket error on close.
    }
    catch (UnrecoverableKeyException e) {
      e.printStackTrace();
    }
    catch (KeyManagementException e) {
      e.printStackTrace();
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    catch (KeyStoreException e) {
      e.printStackTrace();
    }
    catch (CryptoIOException e) {
      e.printStackTrace();
    }
    finally {
      if (this.serverSocket != null) {
        try {
          this.serverSocket.close();
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * @param file
   *          set whether the MBB expects a file as well as a message.
   */
  public void setFile(boolean file) {
    this.file = file;
  }

  /**
   * Shuts down listening.
   */
  public void shutdown() {
    this.running = false;
    try {
      this.serverSocket.close();
    }
    catch (IOException e1) {
      e1.printStackTrace();
    }

    // Wait for the sub-threads.
    for (Thread thread : this.threads) {
      try {
        thread.join();
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Constructs a response.
   * 
   * @param error
   *          Do we respond with an error?
   * @return The response.
   * @throws JSONException
   */
  public static JSONObject getResponse(boolean error) throws JSONException {
    JSONObject response = new JSONObject();

    if (error) {
      response.put(MessageFields.TYPE, ErrorMessage.TYPE_ERROR);
    }
    else {
      response.put(MessageFields.TYPE, MessageFields.TYPE);
    }

    response.put(MessageFields.PeerResponse.PEER_ID, LibraryTestParameters.PEER_ID);
    response.put(MessageFields.PeerResponse.PEER_SIG, "signature");

    return response;
  }

  /**
   * Starts up a pretend MBB as a series of peers on separate threads.
   * 
   * @return The pretend MBB threads.
   * @throws JSONException
   */
  public static List<PretendMBB> startPrentendMBB() throws JSONException {
    return startPrentendMBB(true, false);
  }

  /**
   * Starts up a pretend MBB as a series of peers on separate threads with optional file parameter.
   * 
   * @param file
   *          Do we expect a file as well?
   * @param error
   *          Do we respond with an error?
   * @return The pretend MBB threads.
   * @throws JSONException
   */
  public static List<PretendMBB> startPrentendMBB(boolean file, boolean error) throws JSONException {
    // Run some pretend MBBs to receive the data.
    int[] ports = new int[] { 9081, 9082, 9083 };
    List<PretendMBB> pretendMBBs = new ArrayList<PretendMBB>();

    for (int i = 0; i < ports.length; i++) {
      JSONObject response = getResponse(error);
      PretendMBB pretendMBB = new PretendMBB(ports[i], response.toString());
      pretendMBB.setFile(file);
      pretendMBB.start();
      pretendMBBs.add(pretendMBB);
    }

    return pretendMBBs;
  }

  /**
   * Shuts down and waits for the pretend MBB.
   * 
   * @param pretendMBBs
   *          The list of peer threads.
   * @throws InterruptedException
   */
  public static void stopPretendMBB(List<PretendMBB> pretendMBBs) throws InterruptedException {
    for (PretendMBB pretendMBB : pretendMBBs) {
      pretendMBB.shutdown();
      pretendMBB.join();
    }
  }
}
