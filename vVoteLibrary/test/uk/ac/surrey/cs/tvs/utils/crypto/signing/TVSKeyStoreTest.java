/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Arrays;
import java.util.Enumeration;

import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPublicKey;

/**
 * @author james
 * 
 */
public class TVSKeyStoreTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		LibraryTestParameters.tidyFiles();
		new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		LibraryTestParameters.tidyFiles();
	}

	@Test
	public void testExtractBLSKeysFromPKCS12() throws Exception {
		// hmm, this fails because of some oddity with the BouncyCastle
		// versioning
		TVSKeyStore.extractBLSKeysFromPKCS12("./data/bls_node1.p12", "Hello",
				"Peer1_SigningSK2", "./convertedKeys/Peer3_public_test.bks",
				"./convertedKeys/Peer3_private_test.bks");
	}

	@Test
	public void testGetInstanceString() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS");
		assertNotNull(store);
		assertEquals(store.getKeyStore().getType(), "JKS");
	}

	@Test
	public void testGetInstanceStringProvider() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS",
				Security.getProvider("SUN"));
		assertNotNull(store);
		assertEquals(store.getKeyStore().getType(), "JKS");
		assertEquals(store.getKeyStore().getProvider().getName(), "SUN");
	}

	@Test
	public void testGetInstanceStringString() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		assertNotNull(store);
		assertEquals(store.getKeyStore().getType(), "JKS");
		assertEquals(store.getKeyStore().getProvider().getName(), "SUN");
	}

	@Test
	public void testTVSKeyStoreKeyStore() throws Exception {
		KeyStore kstore = KeyStore.getInstance("JKS");
		TVSKeyStore store = new TVSKeyStore(kstore);
		assertNotNull(store);
		assertSame(kstore, store.getKeyStore());
	}

	private void checkKeyEquals(BLSPrivateKey privKey1, BLSPrivateKey privKey2)
			throws JSONException {
		// it would be nice to do these tests with an equals() test on the keys,
		// but
		// there is no equals() implementation on the keys or on the JSON
		assertEquals(privKey1.toJSON().toString(), privKey2.toJSON().toString());
	}

	private void checkKeyEquals(BLSPublicKey pubKey1, BLSPublicKey pubKey2)
			throws JSONException {
		assertEquals(pubKey1.toJSON().toString(), pubKey2.toJSON().toString());
	}

	@Test
	public void testAddBLSKeyPairBLSKeyPairString() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();
		store.addBLSKeyPair(keyPair, "testKeyPair");
		checkKeyEquals(keyPair.getPrivateKey(),
				store.getBLSPrivateKey("testKeyPair"));
		checkKeyEquals(keyPair.getPublicKey(),
				store.getBLSPublicKey("testKeyPair"));
	}

	@Test
	public void testAddBLSKeyPairBLSPublicKeyBLSPrivateKeyString()
			throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();
		BLSPublicKey pubKey = keyPair.getPublicKey();
		BLSPrivateKey privKey = keyPair.getPrivateKey();
		store.addBLSKeyPair(pubKey, privKey, "testKeyPair");
		checkKeyEquals(privKey, store.getBLSPrivateKey("testKeyPair"));
		checkKeyEquals(pubKey, store.getBLSPublicKey("testKeyPair"));
	}

	@Test
	public void testAddBLSKeyStoreEntries() throws Exception {
		TVSKeyStore oldStore = TVSKeyStore.getInstance("JKS", "SUN");

		BLSKeyPair keys[] = new BLSKeyPair[5];

		// add some key pairs to it
		for (int i = 0; i < 3; i++) {
			keys[i] = BLSKeyPair.generateKeyPair();
			oldStore.addBLSKeyPair(keys[i], "test pair " + i);
		}

		TVSKeyStore newStore = TVSKeyStore.getInstance("JKS", "SUN");

		// add some key pairs into this one too
		for (int i = 3; i < 5; i++) {
			keys[i] = BLSKeyPair.generateKeyPair();
			newStore.addBLSKeyPair(keys[i], "test pair " + i);
		}

		// now combine them
		newStore.addBLSKeyStoreEntries(oldStore);

		// check everything's tickety-boo
		for (int i = 0; i < 5; i++) {
			checkKeyEquals(newStore.getBLSPublicKey("test pair " + i),
					keys[i].getPublicKey());
			checkKeyEquals(newStore.getBLSPrivateKey("test pair " + i),
					keys[i].getPrivateKey());
		}
	}

	@Test
	public void testAddBLSPrivateKeyAndAddBLSPublicKey() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");

		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();
		store.addBLSPrivateKey(keyPair.getPrivateKey(), "testPrivKey");
		store.addBLSPublicKey(keyPair.getPublicKey(), "testPubKey");
		checkKeyEquals(keyPair.getPrivateKey(),
				store.getBLSPrivateKey("testPrivKey"));
		checkKeyEquals(keyPair.getPublicKey(),
				store.getBLSPublicKey("testPubKey"));
	}

	@Test
	public void testAddCertificate() throws Exception {
		// try this out with a non-BLS cert

		// read in test cert
		FileInputStream fis = new FileInputStream("testdata/SurreyPAV.pem");
		BufferedInputStream bis = new BufferedInputStream(fis);

		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		Certificate cert = cf.generateCertificate(bis);

		// create new TVSCertificate using the test cert
		TVSCertificate tcert = new TVSCertificate(cert);

		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");

		store.initKeyStore();
		store.addCertificate(tcert, "nonBLSCert");
		TVSCertificate tcert2 = store.getCertificate("nonBLSCert");
		assertNotNull(tcert2);
		assertNull(tcert2.getBLSPublicKey());
		assertEquals(tcert.getCertificate(), store.getCertificate("nonBLSCert")
				.getCertificate());
	}

	@Test
	public void testAddCertificate_BLS() throws Exception {
		// try this out with a BLS cert

		// create cert
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();

		// create new TVSCertificate using the key
		TVSCertificate tcert = new TVSCertificate(keyPair.getPublicKey());

		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");

		store.addCertificate(tcert, "BLSCert");
		TVSCertificate tcert2 = store.getCertificate("BLSCert");
		assertNotNull(tcert2);
		assertNull(tcert2.getCertificate());
		checkKeyEquals(tcert2.getBLSPublicKey(), keyPair.getPublicKey());
	}

	@Test
	public void testGetBLSCertificate() throws Exception {
		// create cert
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();

		// create new TVSCertificate using the key
		TVSCertificate tcert = new TVSCertificate(keyPair.getPublicKey());

		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");

		store.addCertificate(tcert, "BLSCert");
		TVSCertificate tcert2 = store.getBLSCertificate("BLSCert");
		assertNotNull(tcert2);
		assertNull(tcert2.getCertificate());
		checkKeyEquals(tcert2.getBLSPublicKey(), keyPair.getPublicKey());
	}

	@Test
	public void testGetBLSEntries() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");

		// strictly speaking, if there are no entries, we ought to get an empty
		// array
		assertTrue(store.getBLSEntries().length == 0);

		String aliases[] = new String[5];

		for (int i = 0; i < 5; i++) {
			BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();
			aliases[i] = "test pair " + i;
			store.addBLSKeyPair(keyPair, aliases[i]);
		}

		String aliases2[] = store.getBLSEntries();
		Arrays.sort(aliases2);
		assertArrayEquals(aliases, aliases2);
	}

	@Test
	public void testLoadInputStreamCharArray() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");

		FileInputStream fis = new FileInputStream(
				"testdata/import/keysBeforeImport.jks");
		store.load(fis, "".toCharArray());
		fis.close();
		Enumeration<String> aliases = store.getKeyStore().aliases();
		// check there's something there
		assertTrue(aliases.hasMoreElements());
	}

	@Test
	public void testInitKeyStore() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		try {
			// this throws an exception if it hasn't been initialised
			store.keyStore.aliases();
			fail("Should have thrown an exception because the KeyStore wasn't initialised");
		} catch (KeyStoreException e) {
		}
		store.initKeyStore();
		// now it should work
		store.keyStore.aliases();
	}

	@Test
	public void testLoadStringCharArray() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		store.load("testdata/sampleBLSKeys/Peer1_private.bks", null);
		assertNotNull(store.getCertificate("Peer1_SigningSK2"));
	}

	@Test
	public void testStoreString() throws Exception {
		String jksTestPath = LibraryTestParameters.OUTPUT_FOLDER + "/test.jks";
		String bksTestPath = LibraryTestParameters.OUTPUT_FOLDER + "/test.bks";

		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		store.load("testdata/sampleBLSKeys/Peer1_private.bks", null);

		// change path to Java key store to put it somewhere for testing
		store.getBLSKeyStore().put(TVSKeyStore.JKSPATH, jksTestPath);
		store.store(bksTestPath);
		assertTrue(new File(bksTestPath).exists());
		assertTrue(new File(jksTestPath).exists());

		TVSKeyStore store2 = TVSKeyStore.getInstance("JKS", "SUN");
		store2.load(bksTestPath, null);
		assertTrue(store.getBLSEntries().length == store2.getBLSEntries().length);
		assertTrue(store.getKeyStore().size() == store2.getKeyStore().size());
	}

	@Test
	public void testStoreStringCharArray() throws Exception {
		String jksTestPath = LibraryTestParameters.OUTPUT_FOLDER + "/test2.jks";
		String bksTestPath = LibraryTestParameters.OUTPUT_FOLDER + "/test2.bks";

		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		store.load("testdata/sampleBLSKeys/Peer1_private.bks", null);

		// change path to Java key store to put it somewhere for testing
		store.getBLSKeyStore().put(TVSKeyStore.JKSPATH, jksTestPath);

		// this time, store the jks with a password
		store.store(bksTestPath, "somepassword".toCharArray());
		assertTrue(new File(bksTestPath).exists());
		assertTrue(new File(jksTestPath).exists());

		// create a new keystore and load it in
		TVSKeyStore store2 = TVSKeyStore.getInstance("JKS", "SUN");

		try {
			// use the wrong password to load it in, so it should fail
			store2.load(bksTestPath, "wrongpassword".toCharArray());
			fail("wrong password for keystore should have generated exception");
		} catch (TVSKeyStoreException e) {
			// this is what we were expecting
		}
		
		//on the other hand, the password is only for integrity checking
		//so we can open it with a null password and still get the public key
		store2.load(bksTestPath, null);
		assertTrue(store.getBLSEntries().length == store2.getBLSEntries().length);
		assertTrue(store.getKeyStore().size() == store2.getKeyStore().size());
		TVSCertificate cert = store2.getCertificate("surreypav");
		assertNotNull(cert);
	}

	@Test
	public void testStoreStringStringCharArray() throws Exception {
		String jksTestPath = LibraryTestParameters.OUTPUT_FOLDER + "/test3.jks";
		String bksTestPath = LibraryTestParameters.OUTPUT_FOLDER + "/test3.bks";

		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		store.load("testdata/sampleBLSKeys/Peer1_private.bks", null);

		// this time, store the jks without a password
		store.store(bksTestPath, jksTestPath, "".toCharArray());
		assertTrue(new File(bksTestPath).exists());
		assertTrue(new File(jksTestPath).exists());

		// create a new keystore and load it in
		TVSKeyStore store2 = TVSKeyStore.getInstance("JKS", "SUN");

		// open what we saved
		store2.load(bksTestPath, null);
		assertTrue(store.getBLSEntries().length == store2.getBLSEntries().length);
		assertTrue(store.getKeyStore().size() == store2.getKeyStore().size());
		TVSCertificate cert = store2.getCertificate("surreypav");
		assertNotNull(cert);
	}

	@Test
	public void testExportPublicKeyAsPublicStore() throws Exception {
		String testOutputPath = LibraryTestParameters.OUTPUT_FOLDER
				+ "/testStore.bks";

		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		store.addBLSKeyPair(keyPair, "testKey");
		store.exportPublicKeyAsPublicStore("testKey", testOutputPath);

		TVSKeyStore store2 = TVSKeyStore.getInstance("JKS", "SUN");
		store2.load(testOutputPath, null);
		checkKeyEquals(store2.getBLSPublicKey("testKey"),
				store.getBLSPublicKey("testKey"));
	}

	@Test
	public void testGetJavaKeystorePath() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		assertNull(store.getJavaKeystorePath());
		store.load("testdata/sampleBLSKeys/Peer1_private.bks", null);
		assertEquals(store.getJavaKeystorePath(),
				"./testdata/sampleBLSKeys/Peer1keys.jks");
	}

}
