/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>CryptoIOExceptionTest</code> contains tests for the class <code>{@link CryptoIOException}</code>.
 */
public class CryptoIOExceptionTest {

  /**
   * Run the CryptoIOException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCryptoIOException_1() throws Exception {
    CryptoIOException result = new CryptoIOException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the CryptoIOException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCryptoIOException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    CryptoIOException result = new CryptoIOException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CryptoIOException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCryptoIOException_3() throws Exception {
    Throwable cause = new Throwable();

    CryptoIOException result = new CryptoIOException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the CryptoIOException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCryptoIOException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    CryptoIOException result = new CryptoIOException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CryptoIOException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCryptoIOException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    CryptoIOException result = new CryptoIOException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}