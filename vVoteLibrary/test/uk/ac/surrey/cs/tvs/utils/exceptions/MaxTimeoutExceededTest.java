/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>MaxTimeoutExceededTest</code> contains tests for the class <code>{@link MaxTimeoutExceeded}</code>.
 */
public class MaxTimeoutExceededTest {

  /**
   * Run the MaxTimeoutExceeded() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMaxTimeoutExceeded_1() throws Exception {
    MaxTimeoutExceeded result = new MaxTimeoutExceeded();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the MaxTimeoutExceeded(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMaxTimeoutExceeded_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    MaxTimeoutExceeded result = new MaxTimeoutExceeded(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the MaxTimeoutExceeded(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMaxTimeoutExceeded_3() throws Exception {
    Throwable cause = new Throwable();

    MaxTimeoutExceeded result = new MaxTimeoutExceeded(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the MaxTimeoutExceeded(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMaxTimeoutExceeded_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    MaxTimeoutExceeded result = new MaxTimeoutExceeded(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the MaxTimeoutExceeded(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMaxTimeoutExceeded_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    MaxTimeoutExceeded result = new MaxTimeoutExceeded(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}