/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>CertificateStoringExceptionTest</code> contains tests for the class
 * <code>{@link CertificateStoringException}</code>.
 */
public class CertificateStoringExceptionTest {

  /**
   * Run the CertificateStoringException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateStoringException_1() throws Exception {
    CertificateStoringException result = new CertificateStoringException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the CertificateStoringException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateStoringException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    CertificateStoringException result = new CertificateStoringException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CertificateStoringException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateStoringException_3() throws Exception {
    Throwable cause = new Throwable();

    CertificateStoringException result = new CertificateStoringException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the CertificateStoringException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateStoringException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    CertificateStoringException result = new CertificateStoringException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the CertificateStoringException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCertificateStoringException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    CertificateStoringException result = new CertificateStoringException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException: "
        + LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}