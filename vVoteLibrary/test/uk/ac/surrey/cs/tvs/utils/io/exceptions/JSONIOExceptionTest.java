/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>JSONIOExceptionTest</code> contains tests for the class <code>{@link JSONIOException}</code>.
 */
public class JSONIOExceptionTest {

  /**
   * Run the JSONIOException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONIOException_1() throws Exception {
    JSONIOException result = new JSONIOException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the JSONIOException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONIOException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    JSONIOException result = new JSONIOException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the JSONIOException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONIOException_3() throws Exception {
    Throwable cause = new Throwable();

    JSONIOException result = new JSONIOException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the JSONIOException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONIOException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    JSONIOException result = new JSONIOException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the JSONIOException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testJSONIOException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    JSONIOException result = new JSONIOException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}