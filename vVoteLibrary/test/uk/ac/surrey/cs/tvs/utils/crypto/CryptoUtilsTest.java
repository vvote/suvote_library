/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.crypto.prng.FixedSecureRandom;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;

/**
 * The class <code>CryptoUtilsTest</code> contains tests for the class <code>{@link CryptoUtils}</code>.
 */
public class CryptoUtilsTest {

  /**
   * Run the CryptoUtils() constructor test.
   */
  @Test
  public void testCryptoUtils_1() throws Exception {
    CryptoUtils result = new CryptoUtils();
    assertNotNull(result);
  }

  /**
   * Run the byte[] elGamalDecrypt(byte[],PrivateKey) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testElGamalDecrypt_1() throws Exception {
    // Encrypt some data using an ElGamal key.
    KeyPair pair = CryptoUtils.generatorElGamalKeyPair(LibraryTestParameters.ELGAMAL_KEY_SIZE);

    Cipher cipher = Cipher.getInstance("ElGamal/None/NoPadding", "BC");
    cipher.init(Cipher.ENCRYPT_MODE, pair.getPublic());
    byte[] cipherText = cipher.doFinal(LibraryTestParameters.DATA_SHORT_1);

    // Decrypt and test that it is correct.
    byte[] result = CryptoUtils.elGamalDecrypt(cipherText, pair.getPrivate());
    assertNotNull(result);
    assertEquals(LibraryTestParameters.DATA_SHORT_1.length, result.length);

    for (int i = 0; i < result.length; i++) {
      assertEquals(LibraryTestParameters.DATA_SHORT_1[i], result[i]);
    }
  }

  /**
   * Run the byte[] elGamalDecrypt(byte[],PrivateKey) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testElGamalDecrypt_2() throws Exception {
    CryptoUtils.initProvider();

    // Encrypt some data using an ElGamal key.
    KeyPair pair = CryptoUtils.generatorElGamalKeyPair(LibraryTestParameters.ELGAMAL_KEY_SIZE);

    Cipher cipher = Cipher.getInstance("ElGamal/None/NoPadding", "BC");
    cipher.init(Cipher.ENCRYPT_MODE, pair.getPublic());
    byte[] cipherText = cipher.doFinal(LibraryTestParameters.DATA_SHORT_1);

    // Decrypt and test that it is correct.
    byte[] result = CryptoUtils.elGamalDecrypt(cipherText, pair.getPrivate());
    assertNotNull(result);
    assertEquals(LibraryTestParameters.DATA_SHORT_1.length, result.length);

    for (int i = 0; i < result.length; i++) {
      assertEquals(LibraryTestParameters.DATA_SHORT_1[i], result[i]);
    }
  }

  /**
   * Run the byte[][] generateCommitment(byte[],SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCommitment_1() throws Exception {
    CryptoUtils.initProvider();

    // Generate a commitment for some data > 32 bytes.
    byte[] commitment = CryptoUtils.generateCommitment(LibraryTestParameters.DATA_LONG_1, LibraryTestParameters.RANDOM_DATA);
    assertNotNull(commitment);

    // Test that the commitment can be verified.
    boolean result = CryptoUtils.checkCommitment(commitment, LibraryTestParameters.RANDOM_DATA, LibraryTestParameters.DATA_LONG_1);
    assertTrue(result);
  }

  /**
   * Run the byte[][] generateCommitment(byte[],SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCommitment_2() throws Exception {
    CryptoUtils.initProvider();

    // Generate a commitment for some data > 32 bytes.
    byte[] commitment = CryptoUtils.generateCommitment(LibraryTestParameters.DATA_LONG_1, LibraryTestParameters.RANDOM_DATA);
    assertNotNull(commitment);

    // Test that the commitment cannot be verified with different data.
    boolean result = CryptoUtils.checkCommitment(commitment, LibraryTestParameters.RANDOM_DATA, LibraryTestParameters.DATA_LONG_2);
    assertFalse(result);
  }

  /**
   * Run the byte[][] generateCommitment(byte[],SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCommitment_3() throws Exception {
    CryptoUtils.initProvider();

    // Generate a commitment for some data = 32 bytes.
    byte[] commitment = CryptoUtils.generateCommitment(LibraryTestParameters.DATA_SHORT_1, LibraryTestParameters.RANDOM_DATA);
    assertNotNull(commitment);

    // Test that the commitment can be verified.
    boolean result = CryptoUtils.checkCommitment(commitment, LibraryTestParameters.RANDOM_DATA, LibraryTestParameters.DATA_SHORT_1);
    assertTrue(result);
  }

  /**
   * Run the byte[][] generateCommitment(byte[],SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCommitment_4() throws Exception {
    CryptoUtils.initProvider();

    // Generate a commitment for some data = 32 bytes.
    byte[] commitment = CryptoUtils.generateCommitment(LibraryTestParameters.DATA_SHORT_1, LibraryTestParameters.RANDOM_DATA);
    assertNotNull(commitment);

    // Test that the commitment cannot be verified with different data.
    boolean result = CryptoUtils.checkCommitment(commitment, LibraryTestParameters.RANDOM_DATA, LibraryTestParameters.DATA_SHORT_2);
    assertFalse(result);
  }

  /**
   * Run the byte[][] generateCommitment(byte[],SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCommitment_5() throws Exception {
    CryptoUtils.initProvider();

    // Generate a commitment for some data > 32 bytes.
    byte[][] commitment = CryptoUtils.generateCommitment(LibraryTestParameters.DATA_LONG_1, new FixedSecureRandom(
        LibraryTestParameters.RANDOM_DATA));
    assertNotNull(commitment);
    assertNotNull(commitment[0]);
    assertNotNull(commitment[1]);

    // Test that the commitment can be verified.
    boolean result = CryptoUtils.checkCommitment(commitment[0], commitment[1], LibraryTestParameters.DATA_LONG_1);
    assertTrue(result);
  }

  /**
   * Run the byte[][] generateCommitment(byte[],SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCommitment_6() throws Exception {
    CryptoUtils.initProvider();

    // Generate a commitment for some data > 32 bytes.
    byte[][] commitment = CryptoUtils.generateCommitment(LibraryTestParameters.DATA_LONG_1, new FixedSecureRandom(
        LibraryTestParameters.RANDOM_DATA));
    assertNotNull(commitment);
    assertNotNull(commitment[0]);
    assertNotNull(commitment[1]);

    // Test that the commitment cannot be verified with different data.
    boolean result = CryptoUtils.checkCommitment(commitment[0], commitment[1], LibraryTestParameters.DATA_LONG_2);
    assertFalse(result);
  }

  /**
   * Run the byte[][] generateCommitment(byte[],SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCommitment_7() throws Exception {
    CryptoUtils.initProvider();

    // Generate a commitment for some data = 32 bytes.
    byte[][] commitment = CryptoUtils.generateCommitment(LibraryTestParameters.DATA_SHORT_1, new FixedSecureRandom(
        LibraryTestParameters.RANDOM_DATA));
    assertNotNull(commitment);
    assertNotNull(commitment[0]);
    assertNotNull(commitment[1]);

    // Test that the commitment can be verified.
    boolean result = CryptoUtils.checkCommitment(commitment[0], commitment[1], LibraryTestParameters.DATA_SHORT_1);
    assertTrue(result);
  }

  /**
   * Run the byte[][] generateCommitment(byte[],SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCommitment_8() throws Exception {
    CryptoUtils.initProvider();

    // Generate a commitment for some data = 32 bytes.
    byte[][] commitment = CryptoUtils.generateCommitment(LibraryTestParameters.DATA_SHORT_1, new FixedSecureRandom(
        LibraryTestParameters.RANDOM_DATA));
    assertNotNull(commitment);
    assertNotNull(commitment[0]);
    assertNotNull(commitment[1]);

    // Test that the commitment cannot be verified with different data.
    boolean result = CryptoUtils.checkCommitment(commitment[0], commitment[1], LibraryTestParameters.DATA_SHORT_2);
    assertFalse(result);
  }

  /**
   * Run the KeyPair generatorElGamalKeyPair(int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGeneratorElGamalKeyPair_1() throws Exception {
    KeyPair keyPair = CryptoUtils.generatorElGamalKeyPair(LibraryTestParameters.ELGAMAL_KEY_SIZE);
    assertNotNull(keyPair);
    assertEquals("ElGamal", keyPair.getPrivate().getAlgorithm());
    assertEquals("ElGamal", keyPair.getPublic().getAlgorithm());

    keyPair = CryptoUtils.generatorElGamalKeyPair(LibraryTestParameters.ELGAMAL_KEY_SIZE);
    assertNotNull(keyPair);
    assertEquals("ElGamal", keyPair.getPrivate().getAlgorithm());
    assertEquals("ElGamal", keyPair.getPublic().getAlgorithm());
  }

  /**
   * Run the Cipher getAESDecryptionCipherFromEncKey(JSONObject,JSONObject) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetAESDecryptionCipherFromEncKey_1() throws Exception {
    // Generate an AES key and encrypt some data with it.
    KeyGenerator generator = KeyGenerator.getInstance("AES");
    generator.init(LibraryTestParameters.AES_KEY_SIZE);
    SecretKey aesKey = generator.generateKey();
    byte[] aesKeyData = aesKey.getEncoded();

    Cipher aesCipher = Cipher.getInstance("AES/ECB/NoPadding");
    aesCipher.init(Cipher.ENCRYPT_MODE, aesKey);

    byte[] aesEncrypted = aesCipher.doFinal(LibraryTestParameters.DATA_SHORT_2);

    // Encrypt it using an ElGamal key.
    KeyPair pair = CryptoUtils.generatorElGamalKeyPair(LibraryTestParameters.ELGAMAL_KEY_SIZE);

    Cipher elgamalCipher = Cipher.getInstance("ElGamal/None/NoPadding", "BC");
    elgamalCipher.init(Cipher.ENCRYPT_MODE, pair.getPublic());
    byte[] elgamalCipherText = elgamalCipher.doFinal(aesKeyData);

    JSONObject encryptedAESKey = new JSONObject();
    encryptedAESKey.put(ClientConstants.AESKeyFile.AES_KEY, Hex.encodeHexString(elgamalCipherText));

    JSONObject elGamalKeyPair = CryptoUtils.keyPairToJSON(pair);

    // Test obtaining the decryption from the AES key.
    Cipher result = CryptoUtils.getAESDecryptionCipherFromEncKey(encryptedAESKey, elGamalKeyPair);
    assertNotNull(result);

    // Make sure the key is correct by decrypting.
    byte[] aesPlain = result.doFinal(aesEncrypted);
    assertEquals(LibraryTestParameters.DATA_SHORT_2.length, aesPlain.length);

    for (int i = 0; i < aesPlain.length; i++) {
      assertEquals(LibraryTestParameters.DATA_SHORT_2[i], aesPlain[i]);
    }
  }


  /**
   * Run the PrivateKey getSignatureKeyFromKeyStore(String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetSignatureKeyFromKeyStore_1() throws Exception {
    PrivateKey result = CryptoUtils.getSignatureKeyFromKeyStore(LibraryTestParameters.SIGNING_KEY_STORE,
        LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD, LibraryTestParameters.SIGNING_KEY_ENTITY,
        LibraryTestParameters.SIGNING_KEY_ENTITY_PASSWORD);
    assertNotNull(result);
  }

  /**
   * Run the void initProvider() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testInitProvider_1() throws Exception {
    // Once.
    CryptoUtils.initProvider();

    // Twice.
    CryptoUtils.initProvider();
  }

  /**
   * Run the KeyPair jsonToKeyPair(JSONObject) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testJsonToKeyPair_1() throws Exception {
    KeyPair pair = CryptoUtils.generatorElGamalKeyPair(LibraryTestParameters.ELGAMAL_KEY_SIZE);
    JSONObject elGamalKeyPair = CryptoUtils.keyPairToJSON(pair);
    assertTrue(elGamalKeyPair.has("private"));
    assertTrue(elGamalKeyPair.has("public"));

    KeyPair result = CryptoUtils.jsonToKeyPair(elGamalKeyPair);
    assertNotNull(result);

    assertEquals(pair.getPrivate(), result.getPrivate());
    assertEquals(pair.getPublic(), result.getPublic());
  }

  /**
   * Run the KeyPair jsonToSigningKeyPair(JSONObject,SignatureType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testJsonToSigningKeyPair_1() throws Exception {
    CryptoUtils.initProvider();

    KeyPairGenerator generator = KeyPairGenerator.getInstance(SignatureType.DSA.toString(), "BC");
    SecureRandom random = new SecureRandom();
    generator.initialize(1024, random);

    KeyPair pair = generator.generateKeyPair();
    JSONObject jsonKeyPair = CryptoUtils.keyPairToJSON(pair);

    KeyPair result = CryptoUtils.jsonToSigningKeyPair(jsonKeyPair, SignatureType.DSA);
    assertNotNull(result);

    assertEquals(pair.getPrivate(), result.getPrivate());
    assertEquals(pair.getPublic(), result.getPublic());
  }

  /**
   * Run the PrivateKey jsonToSigningPrivateKey(JSONObject,SignatureType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testJsonToSigningPrivateKey_1() throws Exception {
    CryptoUtils.initProvider();

    KeyPairGenerator generator = KeyPairGenerator.getInstance(SignatureType.DSA.toString(), "BC");
    SecureRandom random = new SecureRandom();
    generator.initialize(1024, random);

    KeyPair pair = generator.generateKeyPair();
    JSONObject jsonKeyPair = CryptoUtils.keyPairToJSON(pair);

    PrivateKey result = CryptoUtils.jsonToSigningPrivateKey(jsonKeyPair, SignatureType.DSA);
    assertNotNull(result);
  }

  /**
   * Run the PublicKey jsonToSigningPublicKey(JSONObject,SignatureType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testJsonToSigningPublicKey_1() throws Exception {
    CryptoUtils.initProvider();

    KeyPairGenerator generator = KeyPairGenerator.getInstance(SignatureType.DSA.toString(), "BC");
    SecureRandom random = new SecureRandom();
    generator.initialize(1024, random);

    KeyPair pair = generator.generateKeyPair();
    JSONObject jsonKeyPair = CryptoUtils.keyPairToJSON(pair);

    PublicKey result = CryptoUtils.jsonToSigningPublicKey(jsonKeyPair, SignatureType.DSA);
    assertNotNull(result);
  }

  /**
   * Run the KeyStore loadKeyStore(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testLoadKeyStore_1() throws Exception {
    // Create a key store from a base store.
    File file = File.createTempFile(LibraryTestParameters.CERTIFICATE_FILE, null);
    file.deleteOnExit();

    KeyStore keyStore = CryptoUtils.loadKeyStore(KeyGeneration.BASE_KEYSTORE_PATH);
    CryptoUtils.storeKeyStore(keyStore, file.getAbsolutePath());
    assertTrue(file.exists());
    assertTrue(file.length() > 0);

    keyStore = CryptoUtils.loadKeyStore(KeyGeneration.BASE_KEYSTORE_PATH, "");
    CryptoUtils.storeKeyStore(keyStore, file.getAbsolutePath(), "");
    assertTrue(file.exists());
    assertTrue(file.length() > 0);

    keyStore = CryptoUtils.loadKeyStore(KeyGeneration.BASE_KEYSTORE_PATH, "".toCharArray());
    CryptoUtils.storeKeyStore(keyStore, file.getAbsolutePath(), "".toCharArray());
    assertTrue(file.exists());
    assertTrue(file.length() > 0);
  }

  /**
   * Run the JSONObject publicKeyToJSON(PublicKey) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPublicKeyToJSON_1() throws Exception {
    KeyPair pair = CryptoUtils.generatorElGamalKeyPair(LibraryTestParameters.ELGAMAL_KEY_SIZE);

    JSONObject result = CryptoUtils.publicKeyToJSON(pair.getPublic());
    assertNotNull(result);
    assertTrue(result.has("public"));
    byte[] content = Hex.decodeHex(result.getString("public").toCharArray());

    byte[] encoded = pair.getPublic().getEncoded();
    assertEquals(encoded.length, content.length);

    for (int i = 0; i < encoded.length; i++) {
      assertEquals(encoded[i], content[i]);
    }
  }
}