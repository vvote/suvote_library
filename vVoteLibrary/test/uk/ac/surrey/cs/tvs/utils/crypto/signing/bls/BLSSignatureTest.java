package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.unisa.dia.gas.jpbc.Element;

import java.io.BufferedReader;
import java.io.FileReader;
import java.security.NoSuchAlgorithmException;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSCertificate;

public class BLSSignatureTest {

	private BLSPrivateKey privKey;
	private BLSPublicKey pubKey;
    private BLSKeyPair kp = BLSKeyPair.generateKeyPair();
	private final byte[] testBytes = { 1,45,92,-120,0,0,33 }; 
	private final byte[] otherBytes = { 1,45,92,-120,0,0 };
	

	@Before
	public void setUp() throws Exception {
		//read in Peer 1's private key
		BufferedReader br = new BufferedReader(new FileReader("testdata/convertedKeys/Peer1_private.bks"));
		String json = "";
		while (br.ready())
			json += br.readLine();
		br.close();
		JSONObject jsonKey = new JSONObject(json);

		//the key itself is wrapped in a couple of extra layers
		JSONObject innerKey = jsonKey.getJSONObject("Peer1").getJSONObject("privateKey");

		privKey = new BLSPrivateKey(innerKey);		

		//read in Peer 1's public key
		br = new BufferedReader(new FileReader("testdata/convertedKeys/Peer1_public.bks"));
		json = "";
		while (br.ready())
			json += br.readLine();
		br.close();
		jsonKey = new JSONObject(json);

		//the key itself is wrapped in a couple of extra layers
		innerKey = jsonKey.getJSONObject("Peer1").getJSONObject("pubKeyEntry");

		pubKey = new BLSPublicKey(innerKey);		
	
	}

	@Test
	public void testGetInstance() throws Exception {
		BLSSignature sig = BLSSignature.getInstance("SHA-1");
		assertNotNull(sig);
	}

	@Test(expected=NoSuchAlgorithmException.class)
	public void testGetInstanceWrong() throws Exception {
		BLSSignature.getInstance("blither");
	}

	@Test
	public void testInitSign() throws Exception {
		//can't do much here except check this doesn't throw an exception
		BLSSignature sig = BLSSignature.getInstance("SHA-1");		
		sig.initSign(privKey);
	}

	@Test
	public void testInitVerifyBLSPublicKey() throws Exception {
		//can't do much here except check this doesn't throw an exception
		BLSSignature sig = BLSSignature.getInstance("SHA-1");		
		sig.initVerify(pubKey);
	}

	@Test
	public void testInitVerifyTVSCertificate() throws Exception {
		//can't do much here except check this doesn't throw an exception
		BLSSignature sig = BLSSignature.getInstance("SHA-1");		
		sig.initVerify(new TVSCertificate(pubKey));
	}

	@Test
	public void testUpdateString() throws Exception {
		BLSSignature sig = BLSSignature.getInstance("SHA-1");		
		sig.initSign(privKey);

		BLSSignature sigCopy = BLSSignature.getInstance("SHA-1");		
		sigCopy.initSign(privKey);

		sig.update("some first nonsense");
		sigCopy.update("some first nonsense");
		Element sig1 = sig.sign();
		Element sig1Copy = sigCopy.sign();
		//deterministic sig of same data so should be same result
		assertEquals(sig1.toString(),sig1Copy.toString());
		
		sig.update("some old nonsense");
		sigCopy.update("some more old nonsense");
		Element sig2 = sig.sign();
		Element sig2Copy = sigCopy.sign();
		//if the update method really did update, the sigs should now be different
		assertNotEquals(sig2.toString(),sig2Copy.toString());
	}

	@Test
	public void testUpdateByteArray() throws Exception {
		BLSSignature sig = BLSSignature.getInstance("SHA-1");		
		sig.initSign(privKey);

		BLSSignature sigCopy = BLSSignature.getInstance("SHA-1");		
		sigCopy.initSign(privKey);

		sig.update("some first nonsense".getBytes());
		sigCopy.update("some first nonsense".getBytes());
		Element sig1 = sig.sign();
		Element sig1Copy = sigCopy.sign();
		//deterministic sig of same data so should be same result
		assertEquals(sig1.toString(),sig1Copy.toString());
		
		sig.update("some old nonsense".getBytes());
		sigCopy.update("some more old nonsense".getBytes());
		Element sig2 = sig.sign();
		Element sig2Copy = sigCopy.sign();
		//if the update method really did update, the sigs should now be different
		assertNotEquals(sig2.toString(),sig2Copy.toString());
	}

	@Test
	public void testSignAndVerifyByteArray() throws Exception {	
		//we'll check a full (not partial) signature by using the new key
		BLSSignature sig = BLSSignature.getInstance("SHA-1");		
		sig.initSign(kp.getPrivateKey());
	    sig.update(testBytes);
		Element testSig = sig.sign();
		
		BLSSignature verif = BLSSignature.getInstance("SHA-1");
		verif.initVerify(kp.getPublicKey());
		verif.update(testBytes);
		assertTrue(verif.verify(testSig.toBytes()));
	}

	@Test
	public void testSignAndVerifyByteArrayBoolean() throws Exception {
		//checked full sig above so here we'll check a partial sig
		BLSSignature sig = BLSSignature.getInstance("SHA-1");		
		sig.initSign(privKey);
		sig.update(testBytes);
		Element testSig = sig.sign();
		
		BLSSignature verif = BLSSignature.getInstance("SHA-1");
		verif.initVerify(pubKey);
		verif.update(testBytes);
		//this succeeds: checking sig on right thing
		//checking a partial signature here
		assertTrue(verif.verify(testSig.toBytes(),true));
	}

	@Test
	public void testGetSignatureElement() throws Exception {
		//convert an Element to bytes, then back to an Element, and check they're the same
		Element testElt = CurveParams.getInstance().getPairing().getG1().newElement();
		testElt.setToRandom();
		Element testElt2 = BLSSignature.getSignatureElement(testElt.toBytes());
		assertEquals(testElt.toString(),testElt2.toString());
	}

	@Test
	public void testSignAndVerifyElementBoolean() throws Exception {
		
		BLSSignature sig = BLSSignature.getInstance("SHA-1");		
		sig.initSign(privKey);
		sig.update(testBytes);
		Element testSig = sig.sign();
		
		BLSSignature verif = BLSSignature.getInstance("SHA-1");
		verif.initVerify(pubKey);
		
		verif.update(otherBytes);
		//this fails: checking sig on wrong thing
		assertTrue(!verif.verify(testSig,false));

		verif.update(testBytes);
		//this succeeds: checking sig on right thing
		//checking a partial signature here
		assertTrue(verif.verify(testSig,true));
		
		//we'll check a full (not partial) signature by using the new key
		sig.initSign(kp.getPrivateKey());
	    sig.update(testBytes);
		testSig = sig.sign();
		
		verif.initVerify(kp.getPublicKey());
		verif.update(testBytes);
		assertTrue(verif.verify(testSig,false));
	}

}
