/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>AllTests</code> builds a suite that can be used to run all of the tests within its package as well as within any
 * subpackages of its package.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ BLSKeyPairTest.class, BLSPrivateKeyTest.class, BLSPublicKeyTest.class, CurveParamsTest.class, BLSSignatureTest.class, BLSCombinerTest.class, })
public class AllTests {
}
