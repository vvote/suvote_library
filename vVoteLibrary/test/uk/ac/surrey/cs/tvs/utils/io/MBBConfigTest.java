/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * The class <code>MBBConfigTest</code> contains tests for the class <code>{@link MBBConfig}</code>.
 */
public class MBBConfigTest {

  /**
   * Run the MBBConfig() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBConfig_1() throws Exception {
    MBBConfig result = new MBBConfig();
    assertNotNull(result);

    // Test a value that is only in the default configuration file.
    assertNotNull(result.conf);
    assertTrue(result.conf.has(LibraryTestParameters.CONFIG_ONLY_IN_DEFAULT));
    assertTrue(result.hasParam(LibraryTestParameters.CONFIG_ONLY_IN_DEFAULT));
  }

  /**
   * Run the MBBConfig() constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testMBBConfig_2() throws Exception {
    // Test missing configuration file.
    MBBConfig result = new MBBConfig("");
    assertNull(result);
  }

  /**
   * Run the MBBConfig() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBConfig_3() throws Exception {
    MBBConfig result = new MBBConfig(LibraryTestParameters.PEER_CONFIGURATION);
    assertNotNull(result);

    // Test a value that is not in the custom configuration file.
    assertNotNull(result.conf);
    assertFalse(result.conf.has(ConfigFiles.MBBConfig.MBB_TIMEOUT));
    assertFalse(result.hasParam(LibraryTestParameters.CONFIG_ONLY_IN_DEFAULT));

    // Test the peer content.
    Map<String, JSONObject> peers = result.getPeers();
    assertNotNull(peers);
    assertEquals(3, peers.size());

    JSONObject peer = result.getHostDetails("POD_Peer1");
    assertNotNull(peer);

    peer = result.getHostDetails("PODPeer2");
    assertNotNull(peer);

    peer = result.getHostDetails("PODPeer3");
    assertNotNull(peer);
  }
}