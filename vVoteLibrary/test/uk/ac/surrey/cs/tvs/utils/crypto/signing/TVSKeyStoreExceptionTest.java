/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     James Heather
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>TVSKeyStoreExceptionTest</code> contains tests for the class
 * <code>{@link TVSKeyStoreException}</code>.
 */
public class TVSKeyStoreExceptionTest {

	@Test
	public void testTVSKeyStoreException() throws Exception {
		
		TVSKeyStoreException result = new TVSKeyStoreException();
		
		assertNotNull(result);
	    assertEquals(null, result.getCause());
	    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException", result.toString());
	    assertEquals(null, result.getMessage());
	    assertEquals(null, result.getLocalizedMessage());
	}

	@Test
	public void testTVSKeyStoreExceptionString() throws Exception {
	    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

		TVSKeyStoreException result = new TVSKeyStoreException(message);
		
		assertNotNull(result);
	    assertEquals(null, result.getCause());
	    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException: "+LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
	    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
	    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
	}

	@Test
	public void testTVSKeyStoreExceptionThrowable() throws Exception {
	    Throwable cause = new Throwable();

		TVSKeyStoreException result = new TVSKeyStoreException(cause);
		
		assertNotNull(result);
	    assertEquals(cause, result.getCause());
	    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException: java.lang.Throwable", result.toString());
	    assertEquals("java.lang.Throwable", result.getMessage());
	    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
	}

	@Test
	public void testTVSKeyStoreExceptionStringThrowable() throws Exception {
	    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
	    Throwable cause = new Throwable();

		TVSKeyStoreException result = new TVSKeyStoreException(message,cause);
		
		assertNotNull(result);
	    assertEquals(cause, result.getCause());
	    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStoreException: "+LibraryTestParameters.EXCEPTION_MESSAGE, result.toString());
	    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
	    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
	}

}
