package uk.ac.surrey.cs.tvs.utils.exceptions;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>AllTests</code> builds a suite that can be used to run all of the tests within its package as well as within any
 * subpackages of its package.
 * 
 * @generatedBy CodePro at 06/11/13 08:54
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ MaxTimeoutExceededTest.class, })
public class AllTests {
}
