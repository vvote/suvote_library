/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import it.unisa.dia.gas.jpbc.Element;

import java.io.BufferedReader;
import java.io.FileReader;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

/**
 * @author james
 *
 */
public class BLSPublicKeyTest {

	private JSONObject jsonKey;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		//read in Peer 1's public key
		BufferedReader br = new BufferedReader(new FileReader("testdata/convertedKeys/Peer1_public.bks"));
		String json = "";
		while (br.ready())
			json += br.readLine();
		br.close();
		jsonKey = new JSONObject(json);		
	}

	@Test
	public void testBLSPublicKeyJSONObject() throws Exception {

		//the key itself is wrapped in a couple of extra layers
		JSONObject innerKey = jsonKey.getJSONObject("Peer1").getJSONObject("pubKeyEntry");

		BLSPublicKey pubKey = new BLSPublicKey(innerKey);
		
		assertNotNull(pubKey);
		assertNotNull(pubKey.getG());
		assertNotNull(pubKey.getPartialPublicKey());
		assertNotNull(pubKey.getPublicKey());
	}

	@Test
	public void testBLSPublicKeyElement() throws Exception {
		Element publicKey = CurveParams.getInstance().getPairing().getZr().newElement();
		
		BLSPublicKey pubKey = new BLSPublicKey(publicKey);
		
		assertNotNull(pubKey);
		assertSame(publicKey,pubKey.getPublicKey());
		assertNull(pubKey.getG());
		assertNull(pubKey.getPartialPublicKey());
		assertTrue(pubKey.getSequenceNo()==-1);
	}

	@Test
	public void testBLSPublicKeyElementElement() throws Exception {
		Element publicKey = CurveParams.getInstance().getPairing().getZr().newElement();
		Element g = CurveParams.getInstance().getPairing().getZr().newElement();
		
		BLSPublicKey pubKey = new BLSPublicKey(publicKey,g);
		
		assertNotNull(pubKey);
		assertSame(publicKey,pubKey.getPublicKey());
		assertSame(g,pubKey.getG());
		assertNull(pubKey.getPartialPublicKey());
		assertTrue(pubKey.getSequenceNo()==-1);
	}

	@Test
	public void testBLSPublicKeyElementElementElement() throws Exception {
		Element publicKey = CurveParams.getInstance().getPairing().getZr().newElement();
		Element partialPublicKey = CurveParams.getInstance().getPairing().getZr().newElement();
		Element g = CurveParams.getInstance().getPairing().getZr().newElement();
		
		BLSPublicKey pubKey = new BLSPublicKey(publicKey,g,partialPublicKey);
		
		assertNotNull(pubKey);
		assertSame(publicKey,pubKey.getPublicKey());
		assertSame(g,pubKey.getG());
		assertSame(partialPublicKey,pubKey.getPartialPublicKey());
		assertTrue(pubKey.getSequenceNo()==-1);
	}

	@Test
	public void testBLSPublicKeyElementElementElementInt() throws Exception {
		Element publicKey = CurveParams.getInstance().getPairing().getZr().newElement();
		Element partialPublicKey = CurveParams.getInstance().getPairing().getZr().newElement();
		Element g = CurveParams.getInstance().getPairing().getZr().newElement();
		int seqNum = 3;
		
		BLSPublicKey pubKey = new BLSPublicKey(publicKey,g,partialPublicKey,seqNum);
		
		assertNotNull(pubKey);
		assertSame(publicKey,pubKey.getPublicKey());
		assertSame(g,pubKey.getG());
		assertSame(partialPublicKey,pubKey.getPartialPublicKey());
		assertTrue(pubKey.getSequenceNo()==seqNum);
	}

	@Test
	public void testToJSON() throws Exception {
		//the key itself is wrapped in a couple of extra layers
		JSONObject innerKey = jsonKey.getJSONObject("Peer1").getJSONObject("pubKeyEntry");

		BLSPublicKey pubKey = new BLSPublicKey(innerKey);

		//Note that it would be reasonable for the class to emit the JSON in a different order, but it doesn't.
		//Ideally there would be a proper equals() method on the JSONObject.
		assertEquals(pubKey.toJSON().toString(),innerKey.toString());
	}

}
