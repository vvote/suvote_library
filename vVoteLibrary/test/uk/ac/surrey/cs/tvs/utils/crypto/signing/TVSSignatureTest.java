/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>TVSSignatureTest</code> contains tests for the class
 * <code>{@link TVSSignature}</code>.
 */
public class TVSSignatureTest {

	/**
	 * Dummy X509 certificate used to force exception.
	 */
	private class DummyCertificate extends X509Certificate {

		public DummyCertificate() {
			super();
		}

		@Override
		public void checkValidity() throws CertificateExpiredException,
				CertificateNotYetValidException {
		}

		@Override
		public void checkValidity(Date date)
				throws CertificateExpiredException,
				CertificateNotYetValidException {
		}

		@Override
		public int getBasicConstraints() {
			return 0;
		}

		@Override
		public Set<String> getCriticalExtensionOIDs() {
			return null;
		}

		@Override
		public byte[] getEncoded() throws CertificateEncodingException {
			return null;
		}

		@Override
		public byte[] getExtensionValue(String oid) {
			return null;
		}

		@Override
		public Principal getIssuerDN() {
			return null;
		}

		@Override
		public boolean[] getIssuerUniqueID() {
			return null;
		}

		@Override
		public boolean[] getKeyUsage() {
			return null; // To force an initialisation exception.
		}

		@Override
		public Set<String> getNonCriticalExtensionOIDs() {
			return null;
		}

		@Override
		public Date getNotAfter() {
			return null;
		}

		@Override
		public Date getNotBefore() {
			return null;
		}

		@Override
		public PublicKey getPublicKey() {
			return null;
		}

		@Override
		public BigInteger getSerialNumber() {
			return null;
		}

		@Override
		public String getSigAlgName() {
			return null;
		}

		@Override
		public String getSigAlgOID() {
			return null;
		}

		@Override
		public byte[] getSigAlgParams() {
			return null;
		}

		@Override
		public byte[] getSignature() {
			return null;
		}

		@Override
		public Principal getSubjectDN() {
			return null;
		}

		@Override
		public boolean[] getSubjectUniqueID() {
			return null;
		}

		@Override
		public byte[] getTBSCertificate() throws CertificateEncodingException {
			return null;
		}

		@Override
		public int getVersion() {
			return 0;
		}

		@Override
		public boolean hasUnsupportedCriticalExtension() {
			return false;
		}

		@Override
		public String toString() {
			return null;
		}

		@Override
		public void verify(PublicKey key) throws CertificateException,
				NoSuchAlgorithmException, InvalidKeyException,
				NoSuchProviderException, SignatureException {
		}

		@Override
		public void verify(PublicKey key, String sigProvider)
				throws CertificateException, NoSuchAlgorithmException,
				InvalidKeyException, NoSuchProviderException,
				SignatureException {
		}
	}

	/**
	 * Run the String getSignatureTypeString(SignatureType) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetSignatureTypeString_1() throws Exception {
		// Loop through every signature type.
		for (SignatureType type : SignatureType.values()) {
			try {
				String typeString = type.toString();

				if (type.equals(SignatureType.DEFAULT)) {
					typeString = SignatureType.DSA.toString();
				}

				String result = TVSSignature.getSignatureTypeString(type);

				assertNotNull(result);
				if (type.equals(SignatureType.BLS)) {
					// BLS just specifies message digest
					assertEquals(SystemConstants.DEFAULT_MESSAGE_DIGEST, result);
				} else {
					assertEquals(SystemConstants.DEFAULT_MESSAGE_DIGEST
							+ "With" + typeString, result);
				}
			}

			catch (TVSSignatureException e) {
				fail("exception");
			}

		}
	}

	/**
	 * Run the byte[] sign() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSign_1() throws Exception {
		// Generate a private/public key pair.
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();

		// Test with BLS
		TVSSignature fixture = new TVSSignature(SignatureType.BLS,
				keyPair.getPrivateKey(), false);

		// should be able to sign nothing in particular
		byte[] result = fixture.sign();
		assertNotNull(result);
	}

	/**
	 * Run the byte[] sign() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSign_2() throws Exception {
		// Generate a private/public key pair.
		CryptoUtils.initProvider();
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "BC");
		keyGen.initialize(1024, new SecureRandom());
		KeyPair pair = keyGen.generateKeyPair();

		// Test with no cache.
		TVSSignature signing = new TVSSignature(SignatureType.DSA,
				pair.getPrivate(), false);

		signing.update(LibraryTestParameters.LINE);
		signing.update(LibraryTestParameters.LINE);
		byte[] result = signing.sign();
		assertNotNull(result);

		// Verify the signature.
		TVSSignature verifying = new TVSSignature(SignatureType.DSA,
				pair.getPublic(), false);

		verifying.update(LibraryTestParameters.LINE);
		verifying.update(LibraryTestParameters.LINE);
		assertTrue(verifying.verify(result));

		// Test that the same signed data is still available.
		result = signing.sign();
		assertNotNull(result);

		verifying = new TVSSignature(SignatureType.DSA, pair.getPublic(), false);

		verifying.update(LibraryTestParameters.LINE);
		verifying.update(LibraryTestParameters.LINE);
		assertTrue(verifying.verify(result));
	}

	/**
	 * Run the byte[] sign() method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSign_3() throws Exception {
		// Generate a private/public key pair.
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();

		// Test with cache
		TVSSignature signing = new TVSSignature(SignatureType.BLS,
				keyPair.getPrivateKey(), true);

		signing.update(LibraryTestParameters.LINE);
		signing.update(LibraryTestParameters.LINE);
		byte[] result = signing.sign();
		assertNotNull(result);

		// Verify the signature.
		TVSSignature verifying = new TVSSignature(keyPair.getPublicKey(), true);

		verifying.update(LibraryTestParameters.LINE);
		verifying.update(LibraryTestParameters.LINE);
		assertTrue(verifying.verify(result));

		// Test that the same signed data is still available.
		result = signing.sign();
		assertNotNull(result);

		verifying = new TVSSignature(keyPair.getPublicKey(), true);

		verifying.update(LibraryTestParameters.LINE);
		verifying.update(LibraryTestParameters.LINE);
		assertTrue(verifying.verify(result));
	}

	/**
	 * Run the byte[] sign() method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testSign_4() throws Exception {
		// Generate a private/public key pair.
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();

		// Test with cache
		TVSSignature signing = new TVSSignature(SignatureType.BLS,
				keyPair.getPrivateKey(), true);

		signing.update(LibraryTestParameters.LINE);
		signing.update(LibraryTestParameters.LINE);
		byte[] result = signing.sign();
		assertNotNull(result);

		signing.update(LibraryTestParameters.LINE);
	}

	/**
	 * Run the String signAndEncode(EncodingType) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSignAndEncode_1() throws Exception {
		// Generate a private/public key pair.
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();

		// Test with all encoding types.
		for (EncodingType type : EncodingType.values()) {
			TVSSignature signing = new TVSSignature(keyPair.getPrivateKey(), false);

			signing.update(LibraryTestParameters.LINE);
			signing.update(LibraryTestParameters.LINE);
			String data = signing.signAndEncode(type);
			assertNotNull(data);

			// Decode the data.
			byte[] result = IOUtils.decodeData(type, data);

			// Verify the signature.
			TVSSignature verifying = new TVSSignature(keyPair.getPublicKey(), false);

			verifying.update(LibraryTestParameters.LINE);
			verifying.update(LibraryTestParameters.LINE);
			assertTrue(verifying.verify(result));

			// Do the same with the provided decode method.
			verifying = new TVSSignature(keyPair.getPublicKey(),
					false);

			verifying.update(LibraryTestParameters.LINE);
			verifying.update(LibraryTestParameters.LINE);
			assertTrue(verifying.verify(data, type));
		}
	}

	/**
	 * Run the TVSSignature(SignatureType,Certificate) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testTVSSignature_1() throws Exception {
		// Test with an invalid certificate.
		TVSSignature result = new TVSSignature(SignatureType.DEFAULT,
				new DummyCertificate());
		assertNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,PublicKey,boolean) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testTVSSignature_10() throws Exception {
		// Test with an invalid public key.
		TVSSignature result = new TVSSignature(SignatureType.DEFAULT,
				(PublicKey) null, false);
		assertNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,PublicKey,boolean) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTVSSignature_11() throws Exception {
		// Generate a public key.
		CryptoUtils.initProvider();
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "BC");
		keyGen.initialize(1024, new SecureRandom());
		KeyPair pair = keyGen.generateKeyPair();

		// Test with a valid public key.
		TVSSignature result = new TVSSignature(SignatureType.DSA,
				pair.getPublic(), false);
		assertNotNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,Certificate) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testTVSSignature_12() throws Exception {
		// Test with an invalid certificate.
		TVSSignature result = new TVSSignature(SignatureType.DEFAULT,
				(Certificate) null, true);
		assertNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,Certificate) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTVSSignature_2() throws Exception {
		// Load the signing certificate.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());

		// Test with a valid certificate.
		TVSSignature result = new TVSSignature(
				SignatureType.DEFAULT,
				keyStore.getCertificate(LibraryTestParameters.SIGNING_KEY_ENTITY));
		assertNotNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,Certificate,boolean) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testTVSSignature_3() throws Exception {
		// Test with an invalid certificate.
		TVSSignature result = new TVSSignature(SignatureType.DEFAULT,
				new DummyCertificate(), false);
		assertNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,Certificate,boolean) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTVSSignature_4() throws Exception {
		// Load the signing certificate.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());

		// Test with a valid certificate.
		TVSSignature result = new TVSSignature(
				SignatureType.DEFAULT,
				keyStore.getCertificate(LibraryTestParameters.SIGNING_KEY_ENTITY),
				false);
		assertNotNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,KeyStore) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTVSSignature_5() throws Exception {
		// Load the signing certificate.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());

		TVSSignature result = new TVSSignature(SignatureType.DEFAULT, keyStore);
		assertNotNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,PrivateKey) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testTVSSignature_6() throws Exception {
		// Test with an invalid private key.
		TVSSignature result = new TVSSignature(SignatureType.DEFAULT,
				(PrivateKey) null);
		assertNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,PrivateKey) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTVSSignature_7() throws Exception {
		// Load the private key.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());
		PrivateKey key = (PrivateKey) keyStore
				.getKey(LibraryTestParameters.SIGNING_KEY_ENTITY,
						LibraryTestParameters.SIGNING_KEY_ENTITY_PASSWORD
								.toCharArray());

		// Test with a valid private key.
		TVSSignature result = new TVSSignature(SignatureType.DEFAULT, key);
		assertNotNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,PrivateKey,boolean) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testTVSSignature_8() throws Exception {
		// Test with an invalid private key.
		TVSSignature result = new TVSSignature(SignatureType.DEFAULT,
				(PrivateKey) null, false);
		assertNull(result);
	}

	/**
	 * Run the TVSSignature(SignatureType,PrivateKey,boolean) constructor test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testTVSSignature_9() throws Exception {
		// Load the private key.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());
		PrivateKey key = (PrivateKey) keyStore
				.getKey(LibraryTestParameters.SIGNING_KEY_ENTITY,
						LibraryTestParameters.SIGNING_KEY_ENTITY_PASSWORD
								.toCharArray());

		// Test with a valid private key.
		TVSSignature result = new TVSSignature(SignatureType.DEFAULT, key,
				false);
		assertNotNull(result);
	}

	/**
	 * Run the JSONObject verify(JSONArray) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testVerify_1() throws Exception {
		// Generate a private/public key pair.
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();

		TVSSignature fixture = new TVSSignature(keyPair.getPublicKey(), false);

		// Test without a cache or key store.
		JSONArray sigsArray = new JSONArray();
		JSONObject result = fixture.verify(sigsArray, "");
		assertNull(result);
	}

	/**
	 * Run the JSONObject verify(JSONArray) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testVerify_2() throws Exception {
		// Load the signing certificate.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());

		TVSSignature fixture = new TVSSignature(SignatureType.DSA, keyStore);

		// Test with no signatures.
		JSONArray sigsArray = new JSONArray();
		JSONObject result = fixture.verify(sigsArray, "");
		assertNotNull(result);

		assertTrue(result.has(TVSSignature.VALID_COUNT));
		assertTrue(result.has(TVSSignature.SIGNATURE_DATA));

		assertEquals(0, result.get(TVSSignature.VALID_COUNT));
		assertEquals(sigsArray,
				result.getJSONArray(TVSSignature.SIGNATURE_DATA));
	}

	/**
	 * Run the JSONObject verify(JSONArray) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testVerify_3() throws Exception {
		// Load the signing certificate.
		// This test should failed because the signer id is the same
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());
		PrivateKey key = (PrivateKey) keyStore
				.getKey(LibraryTestParameters.SIGNING_KEY_ENTITY,
						LibraryTestParameters.SIGNING_KEY_ENTITY_PASSWORD
								.toCharArray());

		TVSSignature signing = new TVSSignature(SignatureType.DSA, key);

		signing.update(LibraryTestParameters.LINE);
		String signed = signing.signAndEncode(EncodingType.BASE64);

		JSONArray sigsArray = new JSONArray();
		JSONObject signature = new JSONObject();
		signature.put(JSONConstants.Signature.SIGNER_ID,
				LibraryTestParameters.SIGNING_KEY_ENTITY);
		signature.put(JSONConstants.Signature.SIGNATURE, signed);
		sigsArray.put(signature); // x1
		sigsArray.put(signature); // x2

		TVSSignature verifying = new TVSSignature(SignatureType.DSA, keyStore);

		// Test with valid signatures.
		verifying.update(LibraryTestParameters.LINE);
		JSONObject result = verifying.verify(sigsArray, "");
		assertNotNull(result);

		assertTrue(result.has(TVSSignature.VALID_COUNT));
		assertTrue(result.has(TVSSignature.SIGNATURE_DATA));

		assertEquals(sigsArray.length() - 1,
				result.get(TVSSignature.VALID_COUNT));
		assertEquals(sigsArray,
				result.getJSONArray(TVSSignature.SIGNATURE_DATA));

		JSONArray responses = result.getJSONArray(TVSSignature.SIGNATURE_DATA);
		assertNotNull(responses);

		assertEquals(sigsArray.length(), responses.length());

		for (int i = 0; i < sigsArray.length(); i++) {
			JSONObject response = responses.getJSONObject(i);

			assertEquals(
					signature.getString(JSONConstants.Signature.SIGNER_ID),
					response.getString(JSONConstants.Signature.SIGNER_ID));
			assertEquals(
					signature.getString(JSONConstants.Signature.SIGNATURE),
					response.getString(JSONConstants.Signature.SIGNATURE));
			assertEquals(true, response.get(TVSSignature.IS_VALID));
		}
	}

	/**
	 * Run the JSONObject verify(JSONArray) method test.
	 * 
	 * @throws Exception
	 */
	@Test(expected = TVSSignatureException.class)
	public void testVerify_4() throws Exception {
		// Load the signing certificate.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());
		PrivateKey key = (PrivateKey) keyStore
				.getKey(LibraryTestParameters.SIGNING_KEY_ENTITY,
						LibraryTestParameters.SIGNING_KEY_ENTITY_PASSWORD
								.toCharArray());

		TVSSignature signing = new TVSSignature(SignatureType.DSA, key);

		signing.update(LibraryTestParameters.LINE);
		String signed = signing.signAndEncode(EncodingType.BASE64);

		JSONArray sigsArray = new JSONArray();
		JSONObject signature = new JSONObject();
		signature.put(JSONConstants.Signature.SIGNATURE, signed);
		sigsArray.put(signature); // x1
		sigsArray.put(signature); // x2

		TVSSignature verifying = new TVSSignature(SignatureType.DSA, keyStore);

		// Test with missing field.
		verifying.update(LibraryTestParameters.LINE);
		JSONObject result = verifying.verify(sigsArray, "");
		assertNotNull(result);

		assertTrue(result.has(TVSSignature.VALID_COUNT));
		assertTrue(result.has(TVSSignature.SIGNATURE_DATA));

		assertEquals(sigsArray.length(), result.get(TVSSignature.VALID_COUNT));
		assertEquals(sigsArray,
				result.getJSONArray(TVSSignature.SIGNATURE_DATA));

		JSONArray responses = result.getJSONArray(TVSSignature.SIGNATURE_DATA);
		assertNotNull(responses);

		assertEquals(sigsArray.length(), responses.length());

		for (int i = 0; i < sigsArray.length(); i++) {
			JSONObject response = responses.getJSONObject(i);

			assertEquals(
					signature.getString(JSONConstants.Signature.SIGNER_ID),
					response.getString(JSONConstants.Signature.SIGNER_ID));
			assertEquals(
					signature.getString(JSONConstants.Signature.SIGNATURE),
					response.getString(JSONConstants.Signature.SIGNATURE));
			assertEquals(true, response.get(TVSSignature.IS_VALID));
		}
	}

	/**
	 * Run the JSONObject verify(JSONArray) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testVerify_5() throws Exception {
		// Load the signing certificate.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());
		PrivateKey key = (PrivateKey) keyStore
				.getKey(LibraryTestParameters.SIGNING_KEY_ENTITY,
						LibraryTestParameters.SIGNING_KEY_ENTITY_PASSWORD
								.toCharArray());

		TVSSignature signing = new TVSSignature(SignatureType.DSA, key);

		signing.update(LibraryTestParameters.LINE);
		String signed = signing.signAndEncode(EncodingType.BASE64);

		JSONArray sigsArray = new JSONArray();
		JSONObject signature1 = new JSONObject();
		signature1.put(JSONConstants.Signature.SIGNER_ID,
				LibraryTestParameters.SIGNING_KEY_ENTITY);
		signature1.put(JSONConstants.Signature.SIGNATURE, signed);
		sigsArray.put(signature1);

		JSONObject signature2 = new JSONObject();
		signature2.put(JSONConstants.Signature.SIGNER_ID,
				LibraryTestParameters.SIGNING_KEY_ENTITY);
		signature2.put(JSONConstants.Signature.SIGNATURE,
				LibraryTestParameters.DUMMY_SIGNATURE);
		sigsArray.put(signature2);

		TVSSignature verifying = new TVSSignature(SignatureType.DSA, keyStore);

		// Test with a valid and an invalid signature.
		verifying.update(LibraryTestParameters.LINE);
		JSONObject result = verifying.verify(sigsArray, "");
		assertNotNull(result);

		assertTrue(result.has(TVSSignature.VALID_COUNT));
		assertTrue(result.has(TVSSignature.SIGNATURE_DATA));

		assertEquals(1, result.get(TVSSignature.VALID_COUNT));
		assertEquals(sigsArray,
				result.getJSONArray(TVSSignature.SIGNATURE_DATA));

		JSONArray responses = result.getJSONArray(TVSSignature.SIGNATURE_DATA);
		assertNotNull(responses);

		assertEquals(sigsArray.length(), responses.length());

		assertEquals(
				signature1.getString(JSONConstants.Signature.SIGNER_ID),
				responses.getJSONObject(0).getString(
						JSONConstants.Signature.SIGNER_ID));
		assertEquals(
				signature1.getString(JSONConstants.Signature.SIGNATURE),
				responses.getJSONObject(0).getString(
						JSONConstants.Signature.SIGNATURE));
		assertEquals(true, responses.getJSONObject(0)
				.get(TVSSignature.IS_VALID));

		assertEquals(
				signature2.getString(JSONConstants.Signature.SIGNER_ID),
				responses.getJSONObject(1).getString(
						JSONConstants.Signature.SIGNER_ID));
		assertEquals(
				signature2.getString(JSONConstants.Signature.SIGNATURE),
				responses.getJSONObject(1).getString(
						JSONConstants.Signature.SIGNATURE));
		assertEquals(false,
				responses.getJSONObject(1).get(TVSSignature.IS_VALID));
	}

	/**
	 * Run the JSONObject verify(JSONArray) method test.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testVerify_6() throws Exception {
		// Load the signing certificate.
		KeyStore keyStore = KeyStore
				.getInstance(ConfigFiles.KeyStoreConfig.KEY_STORE_TYPE);
		keyStore.load(new FileInputStream(
				LibraryTestParameters.SIGNING_KEY_STORE),
				LibraryTestParameters.SIGNING_KEY_STORE_PASSWORD.toCharArray());
		PrivateKey key = (PrivateKey) keyStore
				.getKey(LibraryTestParameters.SIGNING_KEY_ENTITY,
						LibraryTestParameters.SIGNING_KEY_ENTITY_PASSWORD
								.toCharArray());

		TVSSignature signing = new TVSSignature(SignatureType.DSA, key);

		signing.update(LibraryTestParameters.LINE);
		String signed = signing.signAndEncode(EncodingType.BASE64);

		PrivateKey key2 = (PrivateKey) keyStore
				.getKey(LibraryTestParameters.SIGNING_KEY_ENTITY_2,
						LibraryTestParameters.SIGNING_KEY_ENTITY_PASSWORD
								.toCharArray());

		TVSSignature signing2 = new TVSSignature(SignatureType.DSA, key2);
		signing2.update(LibraryTestParameters.LINE);
		String signed2 = signing2.signAndEncode(EncodingType.BASE64);

		JSONArray sigsArray = new JSONArray();
		JSONObject signature = new JSONObject();
		signature.put(JSONConstants.Signature.SIGNER_ID,
				LibraryTestParameters.SIGNING_KEY_ENTITY);
		signature.put(JSONConstants.Signature.SIGNATURE, signed);
		sigsArray.put(signature); // x1
		JSONObject signature2 = new JSONObject();
		signature2.put(JSONConstants.Signature.SIGNER_ID,
				LibraryTestParameters.SIGNING_KEY_ENTITY_2);
		signature2.put(JSONConstants.Signature.SIGNATURE, signed2);
		sigsArray.put(signature2); // x1

		TVSSignature verifying = new TVSSignature(SignatureType.DSA, keyStore);

		// Test with valid signatures.
		verifying.update(LibraryTestParameters.LINE);
		JSONObject result = verifying.verify(sigsArray, "");
		assertNotNull(result);

		assertTrue(result.has(TVSSignature.VALID_COUNT));
		assertTrue(result.has(TVSSignature.SIGNATURE_DATA));

		assertEquals(sigsArray.length(), result.get(TVSSignature.VALID_COUNT));
		assertEquals(sigsArray,
				result.getJSONArray(TVSSignature.SIGNATURE_DATA));

		JSONArray responses = result.getJSONArray(TVSSignature.SIGNATURE_DATA);
		assertNotNull(responses);

		assertEquals(sigsArray.length(), responses.length());

		// for (int i = 0; i < sigsArray.length(); i++) {
		JSONObject response = responses.getJSONObject(0);

		assertEquals(signature.getString(JSONConstants.Signature.SIGNER_ID),
				response.getString(JSONConstants.Signature.SIGNER_ID));
		assertEquals(signature.getString(JSONConstants.Signature.SIGNATURE),
				response.getString(JSONConstants.Signature.SIGNATURE));
		assertEquals(true, response.get(TVSSignature.IS_VALID));

		response = responses.getJSONObject(1);

		assertEquals(signature2.getString(JSONConstants.Signature.SIGNER_ID),
				response.getString(JSONConstants.Signature.SIGNER_ID));
		assertEquals(signature2.getString(JSONConstants.Signature.SIGNATURE),
				response.getString(JSONConstants.Signature.SIGNATURE));
		assertEquals(true, response.get(TVSSignature.IS_VALID));
		// }
	}

}