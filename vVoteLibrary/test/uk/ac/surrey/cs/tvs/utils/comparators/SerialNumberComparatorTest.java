/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.comparators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * The class <code>SerialNumberComparatorTest</code> contains tests for the class <code>{@link SerialNumberComparator}</code>.
 */
public class SerialNumberComparatorTest {

  /**
   * Run the int compare(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCompare_1() throws Exception {
    SerialNumberComparator fixture = new SerialNumberComparator();

    assertEquals(0, fixture.compare("a:1", "a:1"));

    assertTrue(fixture.compare("a:1", "b:1") < 0);
    assertTrue(fixture.compare("a:1", "a:2") < 0);

    assertTrue(fixture.compare("b:1", "a:1") > 0);
    assertTrue(fixture.compare("a:2", "a:1") > 0);
  }
}