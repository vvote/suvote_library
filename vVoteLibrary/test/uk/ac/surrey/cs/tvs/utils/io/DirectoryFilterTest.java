/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>DirectoryFilterTest</code> contains tests for the class <code>{@link DirectoryFilter}</code>.
 */
public class DirectoryFilterTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Run the boolean accept(File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAccept_1() throws Exception {
    // Create a set of files and directories.
    File outputDir = new File(LibraryTestParameters.OUTPUT_FOLDER);

    outputDir.mkdirs();

    // Some files.
    File.createTempFile("test", "txt", outputDir);
    File.createTempFile("test", "txt", outputDir);
    File.createTempFile("test", "txt", outputDir);
    File.createTempFile("test", "txt", outputDir);

    // A known number of directories.
    for (int i = 0; i < LibraryTestParameters.NUM_DIRECTORIES; i++) {
      new File(outputDir, Integer.toString(i)).mkdir();
    }

    // Test the filter.
    DirectoryFilter fixture = new DirectoryFilter();

    File[] directories = outputDir.listFiles(fixture);
    assertEquals(LibraryTestParameters.NUM_DIRECTORIES, directories.length);
  }
}