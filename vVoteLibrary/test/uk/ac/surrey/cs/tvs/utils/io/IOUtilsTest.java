/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * The class <code>IOUtilsTest</code> contains tests for the class <code>{@link IOUtils}</code>.
 */
public class IOUtilsTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Run the int addFileIntoDigest(File,MessageDigest) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddFileIntoDigest_1() throws Exception {
    // Create a test file.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    File file = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_DATA_FILE);

    LibraryTestParameters.createTestFile(file);

    // Test that the digest matches the expected content.
    MessageDigest fileDigest = MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST);
    int result = IOUtils.addFileIntoDigest(file, fileDigest);
    assertEquals(LibraryTestParameters.LINE.length(), result);

    MessageDigest expectedDigest = MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST);
    expectedDigest.update(LibraryTestParameters.LINE.getBytes());

    assertEquals(IOUtils.encodeData(EncodingType.BASE64, expectedDigest.digest()),
        IOUtils.encodeData(EncodingType.BASE64, fileDigest.digest()));
  }

  /**
   * Run the String encodeData(EncodingType,byte[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testEncodeData_1() throws Exception {
    for (EncodingType type : EncodingType.values()) {
      // Create the test data.
      byte[] data = new byte[LibraryTestParameters.NUM_DATA];

      for (byte i = 0; i < LibraryTestParameters.NUM_DATA; i++) {
        data[i] = i;
      }

      // Test encode.
      String encoded = IOUtils.encodeData(type, data);
      assertNotNull(encoded);

      // Test decode.
      byte[] decoded = IOUtils.decodeData(type, encoded);
      assertNotNull(decoded);

      for (byte i = 0; i < LibraryTestParameters.NUM_DATA; i++) {
        assertEquals(data[i], decoded[i]);
      }
    }
  }

  /**
   * Run the String getFileNameFromSerialNo(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetFileNameFromSerialNo_1() throws Exception {
    String result = IOUtils.getFileNameFromSerialNo(" " + LibraryTestParameters.SERIAL_NO);
    assertNotNull(result);
    assertFalse(result.contains(" "));
    assertFalse(result.contains(":"));
    assertEquals("_" + LibraryTestParameters.SERIAL_NO.replace(":", "_"), result);
  }

  /**
   * Run the IOUtils() constructor test.
   */
  @Test
  public void testIOUtils_1() throws Exception {
    IOUtils result = new IOUtils();
    assertNotNull(result);
  }

  /**
   * Run the void moveFile(String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = IOException.class)
  public void testMoveFile_1() throws Exception {
    // Test no source file.
    IOUtils.moveFile("rubbish", LibraryTestParameters.OUTPUT_FOLDER);
  }

  /**
   * Run the void moveFile(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMoveFile_2() throws Exception {
    // Create a test file to move.
    File file = File.createTempFile(LibraryTestParameters.OUTPUT_DATA_FILE, null);
    LibraryTestParameters.createTestFile(file);

    // Test destination folder creation.
    IOUtils.moveFile(file.getAbsolutePath(), LibraryTestParameters.OUTPUT_FOLDER);
  }

  /**
   * Run the void moveFile(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMoveFile_3() throws Exception {
    // Create a test file to move.
    File file = File.createTempFile(LibraryTestParameters.OUTPUT_DATA_FILE, null);
    LibraryTestParameters.createTestFile(file);

    // Test destination folder exists.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    IOUtils.moveFile(file.getAbsolutePath(), LibraryTestParameters.OUTPUT_FOLDER);
  }

  /**
   * Run the void readFile(long,File,DigestInputStream) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadFile_1() throws Exception {
    // Create the output folder.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    File file = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_DATA_FILE);

    InputStream in = new ByteArrayInputStream(LibraryTestParameters.LINE.getBytes());
    DigestInputStream digestIn = new DigestInputStream(in, MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST));

    // Test that the file is written.
    IOUtils.readFile(LibraryTestParameters.LINE.length(), file, digestIn);
    assertTrue(LibraryTestParameters.compareFile(file, new String[] { LibraryTestParameters.LINE }));
  }

  /**
   * Run the void readFile(long,File,DigestInputStream) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadFile_2() throws Exception {
    // Create the output folder.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    File file = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_DATA_FILE);

    InputStream in = new ByteArrayInputStream(LibraryTestParameters.LINE.getBytes());
    DigestInputStream digestIn = new DigestInputStream(in, MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST));

    // Test that the file is written but that we are expecting more data.
    IOUtils.readFile(LibraryTestParameters.LINE.length() * 2, file, digestIn);
    assertTrue(LibraryTestParameters.compareFile(file, new String[] { LibraryTestParameters.LINE }));
  }

  /**
   * Run the void readFile(long,File,DigestInputStream) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadFile_3() throws Exception {
    // Create the output folder.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    File file = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_DATA_FILE);

    InputStream in = new ByteArrayInputStream(LibraryTestParameters.LINE.getBytes());
    DigestInputStream digestIn = new DigestInputStream(in, MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST));

    // Test that the file is not written if we put the filesize as 0.
    IOUtils.readFile(0, file, digestIn);
    assertFalse(LibraryTestParameters.compareFile(file, new String[] { LibraryTestParameters.LINE }));
  }

  /**
   * Run the JSONArray readJSONArrayFromFile(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testReadJSONArrayFromFile_1() throws Exception {
    // Test reading with no file.
    JSONArray result = IOUtils.readJSONArrayFromFile("rubbish");
    assertNull(result);
  }

  /**
   * Run the JSONArray readJSONArrayFromFile(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadJSONArrayFromFile_2() throws Exception {
    // Create the output folder.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    File file = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_DATA_FILE);

    // Create the test array.
    JSONArray array = new JSONArray();
    JSONObject object = new JSONObject(LibraryTestParameters.MESSAGE);
    array.put(object); // x1
    array.put(object); // x2

    // Test writing the object.
    IOUtils.writeJSONToFile(array, file.getAbsolutePath());

    // Test reading the content.
    JSONArray result = IOUtils.readJSONArrayFromFile(file.getAbsolutePath());
    assertNotNull(result);

    // Compare the content.
    for (int i = 0; i < array.length(); i++) {
      assertEquals(new JSONObject(array.getJSONObject(i).toString()).toString(), result.getJSONObject(i).toString());
    }
  }

  /**
   * Run the JSONObject readJSONObjectFromFile(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testReadJSONObjectFromFile_1() throws Exception {
    // Test reading with no file.
    JSONObject result = IOUtils.readJSONObjectFromFile("rubbish");
    assertNull(result);
  }

  /**
   * Run the JSONObject readJSONObjectFromFile(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadJSONObjectFromFile_2() throws Exception {
    // Create the output folder.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    File file = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_DATA_FILE);

    // Create the test object.
    JSONObject object = new JSONObject(LibraryTestParameters.MESSAGE);

    // Test writing content.
    IOUtils.writeJSONToFile(object, file.getAbsolutePath());

    // Test reading content.
    JSONObject result = IOUtils.readJSONObjectFromFile(file.getAbsolutePath());
    assertNotNull(result);

    // Compare content.
    assertEquals(new JSONObject(object.toString()).toString(), result.toString());
  }

  /**
   * Run the void readJSONObjectFromFileAndValidate(String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testReadJSONObjectFromFileAndValidate_1() throws Exception {
    // Test reading with no file.
    JSONObject result = IOUtils.readJSONObjectFromFileAndValidate("rubbish", LibraryTestParameters.PEER_CONFIGURATION_SCHEMA);
    assertNull(result);
  }

  /**
   * Run the void readJSONObjectFromFileAndValidate(String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testReadJSONObjectFromFileAndValidate_2() throws Exception {
    // Test reading with no schema.
    JSONObject result = IOUtils.readJSONObjectFromFileAndValidate(LibraryTestParameters.PEER_CONFIGURATION, "rubbish");
    assertNull(result);
  }

  /**
   * Run the void readJSONObjectFromFileAndValidate(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReadJSONObjectFromFileAndValidate_3() throws Exception {
    // Create the output folder.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    File file = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_DATA_FILE);

    // Create the test object.
    JSONObject object = IOUtils.readJSONObjectFromFile(LibraryTestParameters.PEER_CONFIGURATION);

    // Test writing content.
    IOUtils.writeJSONToFile(object, file.getAbsolutePath());

    // Test reading content.
    JSONObject result = IOUtils.readJSONObjectFromFileAndValidate(file.getAbsolutePath(),
        LibraryTestParameters.PEER_CONFIGURATION_SCHEMA);
    assertNotNull(result);

    // Compare content.
    assertEquals(new JSONObject(object.toString()).toString(), result.toString());
  }
}