/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import org.junit.Test;

/**
 * @author james
 *
 */
public class BLSKeyPairTest {

	@Test
	public void testBLSKeyPair() throws Exception {
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();
		BLSPrivateKey privKey = keyPair.getPrivateKey();
		BLSPublicKey pubKey = keyPair.getPublicKey();
		BLSKeyPair keyPair2 = new BLSKeyPair(privKey,pubKey);
		assertNotNull(keyPair2);
		assertSame(privKey,keyPair2.getPrivateKey());
		assertSame(pubKey,keyPair2.getPublicKey());
		
	}

	@Test
	public void testGenerateKeyPair() throws Exception {

		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();
		
		assertNotNull(keyPair);
		assertNotNull(keyPair.getPrivateKey());
		assertNotNull(keyPair.getPublicKey());
		
	}

}
