package uk.ac.surrey.cs.tvs.utils.verify;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>AllTests</code> builds a suite that can be used to run all of the tests within its package as well as within any
 * subpackages of its package.
 * 
 * @generatedBy CodePro at 06/11/13 10:26
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ VerifyPreferencesTest.class, })
public class AllTests {
}
