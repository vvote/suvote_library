/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.util.zip.ZipOutputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException;

/**
 * The class <code>ZipUtilTest</code> contains tests for the class <code>{@link ZipUtil}</code>.
 */
public class ZipUtilTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Run the void addFileToZip(File,MessageDigest,ZipOutputStream) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddFileToZip_1() throws Exception {
    // Create a test file.
    new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
    File dataFile = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_DATA_FILE);

    LibraryTestParameters.createTestFile(dataFile);

    // Create an output ZIP file.
    File zipFile = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.OUTPUT_ZIP_FILE);
    ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile));

    // Test that the digest matches the expected content.
    MessageDigest fileDigest = MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST);
    ZipUtil.addFileToZip(dataFile, fileDigest, zos);

    MessageDigest expectedDigest = MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST);
    expectedDigest.update(LibraryTestParameters.LINE.getBytes());

    assertEquals(IOUtils.encodeData(EncodingType.BASE64, expectedDigest.digest()),
        IOUtils.encodeData(EncodingType.BASE64, fileDigest.digest()));

    zos.close();
  }

  /**
   * Run the void unzip(String,File) method test.
   * 
   * @throws Exception
   */
  @Test(expected = UploadedZipFileEntryTooBigException.class)
  public void testUnzip_1() throws Exception {
    // Create the output directory.
    File target = new File(LibraryTestParameters.OUTPUT_FOLDER);
    target.mkdirs();

    ZipUtil fixture = new ZipUtil(1);

    // Test that a ZIP file with an entry greater than the maximum allowed size causes an exception.
    fixture.unzip(LibraryTestParameters.LARGE_ZIP_FILE, target);
  }

  /**
   * Run the void unzip(String,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testUnzip_2() throws Exception {
    // Create a test file.
    File target = new File(LibraryTestParameters.OUTPUT_FOLDER);
    target.mkdirs();

    File dataFile = new File(target, LibraryTestParameters.OUTPUT_DATA_FILE);

    LibraryTestParameters.createTestFile(dataFile);

    // Create a valid ZIP file.
    File zipFile = new File(target, LibraryTestParameters.OUTPUT_ZIP_FILE);
    ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile));
    MessageDigest fileDigest = MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST);
    ZipUtil.addFileToZip(dataFile, fileDigest, zos);
    zos.close();

    // Create the unzip folder.
    File unzipDirectory = new File(target, LibraryTestParameters.ZIP_FOLDER);
    unzipDirectory.mkdirs();

    ZipUtil fixture = new ZipUtil(1);

    // Test that the ZIP file is correctly unzipped.
    fixture.unzip(zipFile.getAbsolutePath(), unzipDirectory);
    assertTrue(LibraryTestParameters.compareFile(new File(unzipDirectory, LibraryTestParameters.OUTPUT_DATA_FILE),
        new String[] { LibraryTestParameters.LINE }));
  }

  /**
   * Run the ZipUtil(int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testZipUtil_1() throws Exception {
    ZipUtil result = new ZipUtil(1);
    assertNotNull(result);
  }
}