/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.GenerateSignatureKeyAndCSR;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;

/**
 * The class <code>KeyGenerationTest</code> contains tests for the class <code>{@link KeyGeneration}</code>.
 */
public class KeyGenerationTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Run the String generateCSR(KeyPair,String,File,SignatureType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCSR_1() throws Exception {
    KeyGeneration fixture = new KeyGeneration(LibraryTestParameters.ID);

    // Loop through every signature type.
    for (SignatureType type : SignatureType.values()) {
      File file = File.createTempFile(LibraryTestParameters.CERTIFICATE_FILE, null);
      file.deleteOnExit();

      // We don't create keys for BLS.
      if (type != SignatureType.BLS) {
        KeyPair pair = GenerateSignatureKeyAndCSR.generateKeyPair(type);

        // Test the result.
        String result = fixture.generateCSR(pair, LibraryTestParameters.SUFFIX, file, type);
        assertNotNull(result);

        assertTrue(file.exists());
        assertTrue(file.length() > 0);

        StringBuffer buffer = new StringBuffer();
        buffer.append(ClientConstants.X509.COMMON_NAME);
        buffer.append("=");
        buffer.append(LibraryTestParameters.ID);
        buffer.append(LibraryTestParameters.SUFFIX);

        fixture.addToDistinguishedName(buffer, ClientConstants.X509.ORGANISATION, KeyGeneration.ORGANISATION);
        //fixture.addToDistinguishedName(buffer, ClientConstants.X509.ORGANISATION, KeyGeneration.ORGANISATION);
        fixture.addToDistinguishedName(buffer, ClientConstants.X509.ORGANISATION_UNIT, KeyGeneration.ORGANISATION_UNIT);
        fixture.addToDistinguishedName(buffer, ClientConstants.X509.COUNTRY, KeyGeneration.COUNTRY);
        fixture.addToDistinguishedName(buffer, ClientConstants.X509.LOCATION, KeyGeneration.LOCATION);
        fixture.addToDistinguishedName(buffer, ClientConstants.X509.STATE, KeyGeneration.STATE);

        assertEquals(buffer.toString(), result.toString());
      }
    }
  }

  /**
   * Run the void generateKey(String,File,KeyStore,SignatureType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateKey_1() throws Exception {
    KeyGeneration fixture = new KeyGeneration(LibraryTestParameters.ID);

    // Loop through every signature type.
    for (SignatureType type : SignatureType.values()) {
      File file = File.createTempFile(LibraryTestParameters.CERTIFICATE_FILE, null);
      file.deleteOnExit();

      // We don't create keys for BLS.
      if (type != SignatureType.BLS) {
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(null);

        // Test that the key store has the key.
        fixture.generateKey(LibraryTestParameters.SUFFIX, file, keyStore, type);

        assertTrue(file.exists());
        assertTrue(file.length() > 0);
        assertTrue(keyStore.containsAlias(LibraryTestParameters.ID + LibraryTestParameters.SUFFIX));
      }
    }
  }

  /**
   * Run the void generateKeyAndCSR(String,String,SignatureType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateKeyAndCSR_1() throws Exception {
    KeyGeneration fixture = new KeyGeneration(LibraryTestParameters.ID);

    // Loop through every signature type.
    for (SignatureType type : SignatureType.values()) {
      File file = File.createTempFile(LibraryTestParameters.CERTIFICATE_FILE, null);
      file.deleteOnExit();

      // We don't create keys for BLS.
      if (type != SignatureType.BLS) {
        fixture.generateKeyAndCSR(LibraryTestParameters.OUTPUT_FOLDER, file.getAbsolutePath(), type);

        File csr = new File(LibraryTestParameters.OUTPUT_FOLDER, LibraryTestParameters.ID + KeyGeneration.CSR_EXT);
        assertTrue(csr.exists());
        assertTrue(csr.length() > 0);

        // Test that the key store has the key.
        KeyStore keyStore = CryptoUtils.loadKeyStore(file.getAbsolutePath());
        assertTrue(keyStore.containsAlias(LibraryTestParameters.ID));
      }
    }
  }

  /**
   * Run the void importCertificateFiles(String,String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testImportCertificateFiles_1() throws Exception {
    // Create a key store.
    File file = File.createTempFile(LibraryTestParameters.CERTIFICATE_FILE, null);
    file.deleteOnExit();

    KeyStore keyStore = CryptoUtils.loadKeyStore(KeyGeneration.BASE_KEYSTORE_PATH);
    CryptoUtils.storeKeyStore(keyStore, file.getAbsolutePath());

    // Test importing the certificate.
    KeyGeneration.importCertificateFiles(file.getAbsolutePath(), new String[] { LibraryTestParameters.IMPORT_CERTIFICATE_FILE });

    // Test it has been stored.
    keyStore = CryptoUtils.loadKeyStore(file.getAbsolutePath());
    assertTrue(keyStore.containsAlias(LibraryTestParameters.IMPORT_CERTIFICATE));
  }

  /**
   * Run the void importCertificates(String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testImportCertificates_1() throws Exception {
    // Create a key store.
    File file = File.createTempFile(LibraryTestParameters.CERTIFICATE_FILE, null);
    file.deleteOnExit();

    KeyStore keyStore = CryptoUtils.loadKeyStore(KeyGeneration.BASE_KEYSTORE_PATH);
    CryptoUtils.storeKeyStore(keyStore, file.getAbsolutePath());

    // Test importing the certificate.
    KeyGeneration.importCertificates(LibraryTestParameters.IMPORT_CERTIFICATE_FILE, LibraryTestParameters.IMPORT_CERTIFICATE,
        file.getAbsolutePath());

    // Test it has been stored.
    keyStore = CryptoUtils.loadKeyStore(file.getAbsolutePath());
    assertTrue(keyStore.containsAlias(LibraryTestParameters.IMPORT_CERTIFICATE));
  }

  /**
   * Run the void importCertificates(String,String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testImportCertificates_2() throws Exception {
    // Create a key store.
    File file = File.createTempFile(LibraryTestParameters.CERTIFICATE_FILE, null);
    file.deleteOnExit();

    KeyStore keyStore = CryptoUtils.loadKeyStore(KeyGeneration.BASE_KEYSTORE_PATH);

    // Generate a key pair and store the private key in the key store.
    KeyPair keyPair = GenerateSignatureKeyAndCSR.generateKeyPair(SignatureType.DEFAULT);
    CertificateFactory certFactory = CertificateFactory.getInstance(KeyGeneration.CERTIFICATE_TYPE);
    FileInputStream certIn = new FileInputStream(LibraryTestParameters.IMPORT_CERTIFICATE_FILE);
    keyStore.setKeyEntry(LibraryTestParameters.ID, keyPair.getPrivate(), "".toCharArray(),
        new Certificate[] { certFactory.generateCertificate(certIn) });
    certIn.close();

    CryptoUtils.storeKeyStore(keyStore, file.getAbsolutePath());

    // Test importing the certificate.
    KeyGeneration.importCertificates(LibraryTestParameters.IMPORT_CERTIFICATE_FILE, LibraryTestParameters.IMPORT_CERTIFICATE_FILE,
        LibraryTestParameters.ID, file.getAbsolutePath(), "");

    // Test it has been stored.
    keyStore = CryptoUtils.loadKeyStore(file.getAbsolutePath());
    assertTrue(keyStore.containsAlias(LibraryTestParameters.IMPORT_CERTIFICATE));
    assertTrue(keyStore.containsAlias(LibraryTestParameters.ID));
  }

  /**
   * Run the void importRelevantCertsFromDir(String,File,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testImportRelevantCertsFromDir_1() throws Exception {
    KeyGeneration fixture = new KeyGeneration(LibraryTestParameters.SIGNED_IMPORT_ID);

    // Create a key store.
    File file = File.createTempFile(LibraryTestParameters.CERTIFICATE_FILE, null);
    file.deleteOnExit();

    // Load a key store with the correct private keys, but not the signed certificates and save it off to the output folder.
    KeyStore keyStore = CryptoUtils.loadKeyStore(LibraryTestParameters.SIGNED_IMPORT_BASE_KEY_STORE);
    CryptoUtils.storeKeyStore(keyStore, file.getAbsolutePath());

    // Test importing certificates from a directory.
    fixture.importRelevantCertsFromDir(file.getAbsolutePath(), new File(LibraryTestParameters.SIGNED_IMPORT_DIR),
        LibraryTestParameters.SIGNED_IMPORT_CA);

    // Test it has been stored.
    keyStore = CryptoUtils.loadKeyStore(file.getAbsolutePath());
    assertTrue(keyStore.containsAlias(LibraryTestParameters.SIGNED_IMPORT_ID));
  }

  /**
   * Run the KeyGeneration(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testKeyGeneration_1() throws Exception {
    KeyGeneration result = new KeyGeneration(LibraryTestParameters.ID);
    assertNotNull(result);
    assertEquals(LibraryTestParameters.ID, result.id);
  }
}