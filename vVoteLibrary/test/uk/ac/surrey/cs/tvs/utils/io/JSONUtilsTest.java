/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * The class <code>JSONUtilsTest</code> contains tests for the class <code>{@link JSONUtils}</code>.
 */
public class JSONUtilsTest {

  /**
   * Run the JSONUtils() constructor test.
   */
  @Test
  public void testJSONUtils_1() throws Exception {
    JSONUtils result = new JSONUtils();
    assertNotNull(result);
  }

  /**
   * Run the String loadSchema(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testLoadSchema_1() throws Exception {
    String result = JSONUtils.loadSchema(LibraryTestParameters.SCHEMA);
    assertNotNull(result);
  }

  /**
   * Run the String loadSchema(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testLoadSchema_2() throws Exception {
    String result = JSONUtils.loadSchema("");
    assertNotNull(result);
  }

  /**
   * Run the String permutationsToString(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPermutationsToString_1() throws Exception {
    JSONArray perms = new JSONArray();

    // Test no permutations.
    String result = JSONUtils.permutationsToString(perms);
    assertNotNull(result);
    assertEquals("", result);
  }

  /**
   * Run the String permutationsToString(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPermutationsToString_2() throws Exception {
    JSONArray perms = new JSONArray(LibraryTestParameters.PERMUTATIONS_OBJECT);

    // Test permutations.
    String result = JSONUtils.permutationsToString(perms);
    assertNotNull(result);

    assertEquals(LibraryTestParameters.PERMUTATIONS, result);
  }

  /**
   * Run the String preferencesToString(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPreferencesToString_1() throws Exception {
    JSONArray races = new JSONArray(LibraryTestParameters.RACES);

    String result = JSONUtils.preferencesToString(races);
    assertNotNull(result);

    assertEquals(LibraryTestParameters.PERMUTATIONS, result);
  }

  /**
   * Run the boolean validateSchema(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateSchema_1() throws Exception {
    String schema = JSONUtils.loadSchema(LibraryTestParameters.SCHEMA);

    // Attempt to validate a vote message against its schema.
    JSONObject object = new JSONObject(LibraryTestParameters.VOTE_MESSAGE);
    // Missing type.
    object.put(MessageFields.JSONWBBMessage.DISTRICT, LibraryTestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, LibraryTestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, LibraryTestParameters.DUMMY_SIGNATURE);
    object.put(LibraryTestParameters.SERIAL_SIG_FIELD, LibraryTestParameters.DUMMY_SIGNATURE);
    object.put(LibraryTestParameters.SERIAL_SIGS_FIELD, LibraryTestParameters.SERIAL_SIGNATURES);
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, LibraryTestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, LibraryTestParameters.COMMIT_TIME);

    boolean result = JSONUtils.validateSchema(schema, object.toString());
    assertEquals(false, result);

    // Do it again against the pre-compiled schema.
    result = JSONUtils.validateSchema(schema, object.toString());
    assertEquals(false, result);
  }

  /**
   * Run the boolean validateSchema(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateSchema_2() throws Exception {
    String schema = JSONUtils.loadSchema(LibraryTestParameters.SCHEMA);

    // Attempt to validate a vote message against its schema.
    JSONObject object = new JSONObject(LibraryTestParameters.VOTE_MESSAGE);
    object.put(MessageFields.JSONWBBMessage.TYPE, LibraryTestParameters.MESSAGE_TYPE_VOTE_MESSAGE_STRING);
    object.put(MessageFields.JSONWBBMessage.DISTRICT, LibraryTestParameters.DISTRICT);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, LibraryTestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, LibraryTestParameters.DUMMY_SIGNATURE);
    object.put(LibraryTestParameters.SERIAL_SIG_FIELD, LibraryTestParameters.DUMMY_SIGNATURE);
    object.put(LibraryTestParameters.SERIAL_SIGS_FIELD, LibraryTestParameters.SERIAL_SIGNATURES);
    object.put(MessageFields.VoteMessage.INTERNAL_PREFS, LibraryTestParameters.PERMUTATION);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, LibraryTestParameters.COMMIT_TIME);

    boolean result = JSONUtils.validateSchema(schema, object.toString());
    assertEquals(true, result);

    // Do it again against the pre-compiled schema.
    result = JSONUtils.validateSchema(schema, object.toString());
    assertEquals(true, result);
  }
}