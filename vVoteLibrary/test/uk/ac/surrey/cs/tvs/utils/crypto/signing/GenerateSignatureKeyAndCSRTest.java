/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.signing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringReader;
import java.security.KeyPair;

import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.GenerateSignatureKeyAndCSR;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;

/**
 * The class <code>GenerateSignatureKeyAndCSRTest</code> contains tests for the class
 * <code>{@link GenerateSignatureKeyAndCSR}</code>.
 */
public class GenerateSignatureKeyAndCSRTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
  }

  /**
   * Run the String generateCertificateSigningRequest(String,KeyPair,int,SignatureType,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCertificateSigningRequest_1() throws Exception {
    // Loop through every signature type.
    for (SignatureType type : SignatureType.values()) {
      try {
        KeyPair pair = GenerateSignatureKeyAndCSR.generateKeyPair(type);

        String result = GenerateSignatureKeyAndCSR.generateCertificateSigningRequest(LibraryTestParameters.DN, pair,
            LibraryTestParameters.DAYS, type, LibraryTestParameters.CHALLENGE_PASSWORD);
        assertNotNull(result);

        StringReader sr = new StringReader(result);
        PEMParser parser = new PEMParser(sr);
        PKCS10CertificationRequest certificationRequest = (PKCS10CertificationRequest) parser.readObject();

        assertEquals(LibraryTestParameters.DN, certificationRequest.getSubject().toString());

        parser.close();
      }
      catch (KeyGenerationException e) {
        if (!type.equals(SignatureType.BLS)) {
          fail("exception");
        }
      }
    }
  }

  /**
   * Run the KeyPair generateKeyPair(SignatureType) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateKeyPair_1() throws Exception {
    // Loop through every signature type.
    for (SignatureType type : SignatureType.values()) {
      String typeString = type.toString();

      if (type.equals(SignatureType.DEFAULT)) {
        typeString = SignatureType.DSA.toString();
      }

      try {
        KeyPair result = GenerateSignatureKeyAndCSR.generateKeyPair(type);
        assertNotNull(result);

        // BLS throws an exception.
        if (type.equals(SignatureType.BLS)) {
          fail("no exception");
        }

        assertEquals(typeString, result.getPrivate().getAlgorithm());
        assertEquals(typeString, result.getPublic().getAlgorithm());

        // Test that the key pair are valid for signing.
        TVSSignature sign = new TVSSignature(type, result.getPrivate());

        // RSA throws an exception.
        if (type.equals(SignatureType.RSA)) {
          fail("no exception");
        }

        sign.update(LibraryTestParameters.DATA_SHORT_1);
        byte[] signature = sign.sign();

        TVSSignature verify = new TVSSignature(type, result.getPublic(), false);
        verify.update(LibraryTestParameters.DATA_SHORT_1);
        assertTrue(verify.verify(signature));
      }
      catch (TVSSignatureException e) {
        if (!type.equals(SignatureType.RSA)) {
          fail("exception");
        }
      }
      catch (KeyGenerationException e) {
        if (!type.equals(SignatureType.BLS)) {
          fail("exception");
        }
      }
    }
  }

  /**
   * Run the GenerateSignatureKeyAndCSR() constructor test.
   */
  @Test
  public void testGenerateSignatureKeyAndCSR_1() throws Exception {
    GenerateSignatureKeyAndCSR result = new GenerateSignatureKeyAndCSR();
    assertNotNull(result);
  }
}