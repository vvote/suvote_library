/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import it.unisa.dia.gas.jpbc.Element;

import java.io.BufferedReader;
import java.io.FileReader;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

/**
 * @author james
 *
 */
public class BLSPrivateKeyTest {

	private JSONObject jsonKey;

	@Before
	public void setUp() throws Exception {
		//read in Peer 1's private key
		BufferedReader br = new BufferedReader(new FileReader("testdata/convertedKeys/Peer1_private.bks"));
		String json = "";
		while (br.ready())
			json += br.readLine();
		br.close();
		jsonKey = new JSONObject(json);		
	}
	
	@Test
	public void testBLSPrivateKeyJSONObject() throws Exception {

		//the key itself is wrapped in a couple of extra layers
		JSONObject innerKey = jsonKey.getJSONObject("Peer1").getJSONObject("privateKey");

		BLSPrivateKey privKey = new BLSPrivateKey(innerKey);
		
		assertNotNull(privKey);
		assertNotNull(privKey.getKey());
	}

	@Test
	public void testBLSPrivateKeyElement() throws Exception {
		Element privateKey = CurveParams.getInstance().getPairing().getZr().newElement();
		
		BLSPrivateKey privKey = new BLSPrivateKey(privateKey);
		
		assertNotNull(privKey);
		assertSame(privateKey,privKey.getKey());
	}

	@Test
	public void testToJSON() throws Exception {
		//the key itself is wrapped in a couple of extra layers
		JSONObject innerKey = jsonKey.getJSONObject("Peer1").getJSONObject("privateKey");

		BLSPrivateKey privKey = new BLSPrivateKey(innerKey);
		
		//it would be nice to do this without converting to Strings, but
		//JSONObjects don't seem to have a nice equals() method
		assertEquals(innerKey.toString(),privKey.toJSON().toString());
	}

}
