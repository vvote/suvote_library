/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>StreamBoundExceededExceptionTest</code> contains tests for the class
 * <code>{@link StreamBoundExceededException}</code>.
 */
public class StreamBoundExceededExceptionTest {

  /**
   * Run the StreamBoundExceededException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testStreamBoundExceededException_1() throws Exception {
    StreamBoundExceededException result = new StreamBoundExceededException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the StreamBoundExceededException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testStreamBoundExceededException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    StreamBoundExceededException result = new StreamBoundExceededException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals(
        "uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the StreamBoundExceededException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testStreamBoundExceededException_3() throws Exception {
    Throwable cause = new Throwable();

    StreamBoundExceededException result = new StreamBoundExceededException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the StreamBoundExceededException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testStreamBoundExceededException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    StreamBoundExceededException result = new StreamBoundExceededException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals(
        "uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the StreamBoundExceededException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testStreamBoundExceededException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    StreamBoundExceededException result = new StreamBoundExceededException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals(
        "uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}