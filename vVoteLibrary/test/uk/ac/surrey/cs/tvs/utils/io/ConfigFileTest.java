/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * The class <code>ConfigFileTest</code> contains tests for the class <code>{@link ConfigFile}</code>.
 */
public class ConfigFileTest {

  /**
   * Run the ConfigFile() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testConfigFile_1() throws Exception {
    ConfigFile result = new ConfigFile();
    assertNotNull(result);

    // Test a value that is only in the default configuration file.
    assertNotNull(result.conf);
    assertTrue(result.conf.has(LibraryTestParameters.CONFIG_ONLY_IN_DEFAULT));
    assertTrue(result.hasParam(LibraryTestParameters.CONFIG_ONLY_IN_DEFAULT));
  }

  /**
   * Run the ConfigFile() constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testConfigFile_2() throws Exception {
    // Test missing configuration file.
    ConfigFile result = new ConfigFile("");
    assertNull(result);
  }

  /**
   * Run the ConfigFile() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testConfigFile_3() throws Exception {
    ConfigFile result = new ConfigFile(LibraryTestParameters.PEER_CONFIGURATION);
    assertNotNull(result);

    // Test a value that is not in the custom configuration file.
    assertNotNull(result.conf);
    assertFalse(result.conf.has(ConfigFiles.MBBConfig.MBB_TIMEOUT));
    assertFalse(result.hasParam(LibraryTestParameters.CONFIG_ONLY_IN_DEFAULT));
  }

  /**
   * Run the ConfigFile() constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testConfigFile_4() throws Exception {
    // Test missing schema.
    ConfigFile result = new ConfigFile(LibraryTestParameters.PEER_CONFIGURATION, "");
    assertNotNull(result);
  }

  /**
   * Run the ConfigFile() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testConfigFile_5() throws Exception {
    ConfigFile result = new ConfigFile(LibraryTestParameters.PEER_CONFIGURATION, LibraryTestParameters.PEER_CONFIGURATION_SCHEMA);
    assertNotNull(result);

    // Test a value that is not in the custom configuration file.
    assertNotNull(result.conf);
    assertFalse(result.conf.has(ConfigFiles.MBBConfig.MBB_TIMEOUT));
    assertFalse(result.hasParam(LibraryTestParameters.CONFIG_ONLY_IN_DEFAULT));
  }

  /**
   * Run the boolean getBooleanParameter(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetBooleanParameter_1() throws Exception {
    ConfigFile fixture = new ConfigFile();

    // Test parameter exists.
    boolean result = fixture.getBooleanParameter(LibraryTestParameters.CONFIG_BOOLEAN);
    assertTrue(result);
  }

  /**
   * Run the boolean getBooleanParameter(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetBooleanParameter_2() throws Exception {
    ConfigFile fixture = new ConfigFile(LibraryTestParameters.PEER_CONFIGURATION);

    // Test parameter does not exist.
    boolean result = fixture.getBooleanParameter(LibraryTestParameters.CONFIG_BOOLEAN);
    assertFalse(result);
  }

  /**
   * Run the int getIntParameter(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetIntParameter_1() throws Exception {
    ConfigFile fixture = new ConfigFile();

    // Test parameter exists.
    int result = fixture.getIntParameter(LibraryTestParameters.CONFIG_INT);
    assertEquals(1, result);
  }

  /**
   * Run the int getIntParameter(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetIntParameter_2() throws Exception {
    ConfigFile fixture = new ConfigFile(LibraryTestParameters.PEER_CONFIGURATION);

    // Test parameter does not exist.
    int result = fixture.getIntParameter(LibraryTestParameters.CONFIG_INT);
    assertEquals(-1, result);
  }

  /**
   * Run the String getStringParameter(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetStringParameter_1() throws Exception {
    ConfigFile fixture = new ConfigFile();

    // Test parameter exists.
    String result = fixture.getStringParameter(LibraryTestParameters.CONFIG_STRING);
    assertNotNull(result);
    assertEquals(LibraryTestParameters.CONFIG_STRING_VALUE, result);
  }

  /**
   * Run the String getStringParameter(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetStringParameter_2() throws Exception {
    ConfigFile fixture = new ConfigFile(LibraryTestParameters.PEER_CONFIGURATION);

    // Test parameter does not exist.
    String result = fixture.getStringParameter(LibraryTestParameters.CONFIG_STRING);
    assertNull(result);
  }

  /**
   * Run the void setIntParameter(String,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetIntParameter_1() throws Exception {
    ConfigFile fixture = new ConfigFile();

    // Test changing the integer value.
    fixture.setIntParameter(LibraryTestParameters.CONFIG_INT, 999);

    assertEquals(999, fixture.conf.get(LibraryTestParameters.CONFIG_INT));

    ConfigFile another = new ConfigFile();

    assertEquals(999, another.conf.get(LibraryTestParameters.CONFIG_INT));

    // Put it back to what it should be.
    fixture.setIntParameter(LibraryTestParameters.CONFIG_INT, LibraryTestParameters.CONFIG_INT_VALUE);
  }

  /**
   * Run the void setStringParameter(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetStringParameter_1() throws Exception {
    ConfigFile fixture = new ConfigFile();

    // Test changing the integer value.
    fixture.setStringParameter(LibraryTestParameters.CONFIG_STRING, "sausages");

    assertEquals("sausages", fixture.conf.get(LibraryTestParameters.CONFIG_STRING));

    ConfigFile another = new ConfigFile();

    assertEquals("sausages", another.conf.get(LibraryTestParameters.CONFIG_STRING));

    // Put it back to what it should be.
    fixture.setStringParameter(LibraryTestParameters.CONFIG_STRING, LibraryTestParameters.CONFIG_STRING_VALUE);
  }
}