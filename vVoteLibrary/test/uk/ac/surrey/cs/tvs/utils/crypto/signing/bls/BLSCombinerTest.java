/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;

import static org.junit.Assert.assertNotNull;
import it.unisa.dia.gas.jpbc.Element;

import java.io.BufferedReader;
import java.io.FileReader;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;

/**
 * @author james
 * 
 */
public class BLSCombinerTest {

	BLSPrivateKey keys[] = new BLSPrivateKey[4];
	final String signMe = "something to sign";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		for (int i = 0; i < 4; i++) {
			// read in private key
			BufferedReader br = new BufferedReader(new FileReader(
					String.format("testdata/convertedKeys/Peer%d_private.bks",
							i + 1)));
			String json = "";
			while (br.ready())
				json += br.readLine();
			br.close();
			JSONObject jsonKey = new JSONObject(json);
			JSONObject innerKey = jsonKey.getJSONObject(
					String.format("Peer%d", i + 1)).getJSONObject("privateKey");

			keys[i] = new BLSPrivateKey(innerKey);
		}
	}

	@Test
	public void testBLSCombiner() throws Exception {
		BLSCombiner comb = new BLSCombiner(5, 4);
		assertNotNull(comb);

		// perhaps this is OK for the constructor to succeed
		comb = new BLSCombiner(0, 0);
		assertNotNull(comb);
	}

	@Test(expected = TVSSignatureException.class)
	public void testBLSCombiner2() throws Exception {
		// this makes no sense, so we get an exception
		new BLSCombiner(4, 5);
	}

	@Test(expected = TVSSignatureException.class)
	public void testAddShareElementInt() throws Exception {
		// only one share added, but added repeatedly
		// this ought to cause problems
		BLSCombiner comb = new BLSCombiner(4, 3);
		BLSSignature sig = BLSSignature.getInstance("SHA1");
		sig.initSign(keys[0]);
		sig.update(signMe);
		Element signElt = sig.sign();
		for (int i = 0; i < 4; i++) {
			comb.addShare(signElt, 0);
		}
		assertNotNull(comb.combined());
	}

	@Test
	public void testAddShareByteArrayInt() throws Exception {
		// do it properly for this test
		BLSCombiner comb = new BLSCombiner(4, 3);
		for (int i = 0; i < 4; i++) {
			BLSSignature sig = BLSSignature.getInstance("SHA1");
			sig.initSign(keys[i]);
			sig.update(signMe.getBytes());
			Element signElt = sig.sign();
			comb.addShare(signElt, i);
		}
		assertNotNull(comb.combined());
	}

	@Test(expected = TVSSignatureException.class)
	public void testCombined1() throws Exception {
		// need 4 shares to sign
		BLSCombiner comb = new BLSCombiner(5, 4);

		// only added three
		for (int i = 0; i < 2; i++) {
			BLSSignature sig = BLSSignature.getInstance("SHA1");
			sig.initSign(keys[i]);
			sig.update(signMe);
			comb.addShare(sig.sign(), i);
		}

		// so this should complain
		Element combined = comb.combined();
		assertNotNull(combined);
	}

	@Test
	public void testCombined2() throws Exception {
		BLSCombiner comb = new BLSCombiner(4, 3);

		for (int i = 0; i < 3; i++) {
			BLSSignature sig = BLSSignature.getInstance("SHA1");
			sig.initSign(keys[i]);
			sig.update(signMe);
			comb.addShare(sig.sign(), i);
		}

		Element combined = comb.combined();
		assertNotNull(combined);
	}
}
