/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.io.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>UploadedZipFileNameExceptionTest</code> contains tests for the class
 * <code>{@link UploadedZipFileNameException}</code>.
 */
public class UploadedZipFileNameExceptionTest {

  /**
   * Run the UploadedZipFileNameException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileNameException_1() throws Exception {
    UploadedZipFileNameException result = new UploadedZipFileNameException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the UploadedZipFileNameException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileNameException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    UploadedZipFileNameException result = new UploadedZipFileNameException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals(
        "uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the UploadedZipFileNameException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileNameException_3() throws Exception {
    Throwable cause = new Throwable();

    UploadedZipFileNameException result = new UploadedZipFileNameException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the UploadedZipFileNameException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileNameException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    UploadedZipFileNameException result = new UploadedZipFileNameException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals(
        "uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the UploadedZipFileNameException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testUploadedZipFileNameException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    UploadedZipFileNameException result = new UploadedZipFileNameException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals(
        "uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}