/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONObject;
import org.junit.Test;

/**
 * The class <code>ECUtilsTest</code> contains tests for the class <code>{@link ECUtils}</code>.
 */
public class ECUtilsTest {

  /**
   * Run the JSONObject cipherToJSON(ECPoint[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCipherToJSON_1() throws Exception {
    ECUtils fixture = new ECUtils();

    // Generate a cipher.
    ECPoint plainText = fixture.getRandomValue();
    BigInteger privateKey = fixture.getRandomInteger(fixture.getOrderUpperBound(), new SecureRandom());
    ECPoint publicKey = fixture.getG().multiply(privateKey);

    ECPoint[] cipher = fixture.encrypt(plainText, publicKey);

    // Test conversion to JSON.
    JSONObject result = fixture.cipherToJSON(cipher);
    assertNotNull(result);

    // Test content.
    assertTrue(result.has(ECUtils.GR));
    assertTrue(result.has(ECUtils.MYR));

    JSONObject gr = result.getJSONObject(ECUtils.GR);
    assertTrue(gr.has(ECUtils.X_POINT));
    assertTrue(gr.has(ECUtils.Y_POINT));

    JSONObject myr = result.getJSONObject(ECUtils.MYR);
    assertTrue(myr.has(ECUtils.X_POINT));
    assertTrue(myr.has(ECUtils.Y_POINT));

    // Test conversion back to a cipher.
    ECPoint[] converted = fixture.jsonToCipher(result);

    assertEquals(cipher[0], converted[0]);
    assertEquals(cipher[1], converted[1]);
  }

  /**
   * Run the ECUtils() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testECUtils_1() throws Exception {
    ECUtils result = new ECUtils();
    assertNotNull(result);

    assertNotNull(result.getG());
    assertNotNull(result.getOrderUpperBound());
    assertNotNull(result.getParams());
  }

  /**
   * Run the ECUtils() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testECUtils_2() throws Exception {
    ECUtils result = new ECUtils("P-256");
    assertNotNull(result);

    assertNotNull(result.getG());
    assertNotNull(result.getOrderUpperBound());
    assertNotNull(result.getParams());
  }

  /**
   * Run the ECPoint[] encrypt(ECPoint,ECPoint) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testEncrypt_1() throws Exception {
    ECUtils fixture = new ECUtils();

    ECPoint plainText = fixture.getRandomValue();
    BigInteger privateKey = fixture.getRandomInteger(fixture.getOrderUpperBound(), new SecureRandom());
    ECPoint publicKey = fixture.getG().multiply(privateKey);

    ECPoint[] result = fixture.encrypt(plainText, publicKey);

    ECPoint decrypt = result[1].add(result[0].multiply(privateKey).negate());

    assertEquals(plainText, decrypt);
    assertNotNull(result);
  }

  /**
   * Run the BigInteger getRandomInteger(BigInteger,SecureRandom) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetRandomInteger_1() throws Exception {
    ECUtils fixture = new ECUtils();

    BigInteger result = fixture.getRandomInteger(fixture.getOrderUpperBound(), new SecureRandom());
    assertNotNull(result);
  }

  /**
   * Run the ECPoint getRandomValue() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetRandomValue_1() throws Exception {
    ECUtils fixture = new ECUtils();

    ECPoint result = fixture.getRandomValue();
    assertNotNull(result);
  }

  /**
   * Run the ECPoint getRandomValue() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetRandomValue_2() throws Exception {
    ECUtils fixture = new ECUtils();

    ECPoint result = fixture.getRandomValue(new SecureRandom());
    assertNotNull(result);
  }
}