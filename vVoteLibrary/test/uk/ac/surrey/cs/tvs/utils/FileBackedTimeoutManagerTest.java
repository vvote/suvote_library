/**
 * 
 */
package uk.ac.surrey.cs.tvs.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;

/**
 * @author james
 * 
 */
public class FileBackedTimeoutManagerTest {

	/**
	 * Dummy timeout runnable.
	 */
	private class DummyTimeout extends FileBackedTimeout {

		/** Has the timeout been called? */
		private boolean timeout = false;
		private File backupDir;
		private int someVal;

		public DummyTimeout(int someVal) {
			this.someVal = someVal;
		}

		/**
		 * @return the timeout.
		 */
		private boolean isTimeout() {
			return this.timeout;
		}

		private int getSomeVal() {
			return someVal;
		}

		private void setSomeVal(int someVal) {
			this.someVal = someVal;
		}

		/**
		 * Executed when the timeout occurs.
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			this.timeout = true;
			// write a file to say that we're done
			try {
				BufferedWriter out = new BufferedWriter(new FileWriter(
						LibraryTestParameters.OUTPUT_FOLDER + File.separator
								+ "resultfile.txt"));
				out.write(Integer.toString(someVal));
				out.newLine();
				out.close();
			} catch (IOException e) {
				fail("couldn't write output file");
			}
		}

		@Override
		public void createFileBackup(int timeout) throws IOException {
			BufferedWriter out = new BufferedWriter(new FileWriter(backupDir
					+ File.separator + "somefile"
					+ DefaultTimeoutFileFilter.DEFAULT_TIMEOUT_FILE_EXT));
			out.write(Integer.toString(someVal));
			out.newLine();
			out.write(Long.toString(System.currentTimeMillis()
					+ TimeUnit.SECONDS.toMillis(timeout)));
			out.newLine();
			out.close();
		}

		@Override
		public void performRecovery(File file,
				FileBackedTimeoutManager timeoutManager) throws IOException,
				MaxTimeoutExceeded {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			line = br.readLine();
			int someVal = Integer.parseInt(line);
			line = br.readLine();
			br.close();
			Long timeoutVal = Long.parseLong(line);
			Long curTime = System.currentTimeMillis();
			if (timeoutVal < curTime)
				throw new MaxTimeoutExceeded(String.format(
						"tried to recover after timeout had expired by %dms",
						curTime - timeoutVal));
			DummyTimeout repl = new DummyTimeout(someVal);
			repl.setBackUpDir(timeoutManager.getBackupDir());
			// following the construction in DeleteBallotWorker: don't add the
			// supertype
			timeoutManager
					.addTimeout(
							(Runnable) repl,
							(int) TimeUnit.MILLISECONDS.toSeconds(timeoutVal
									- curTime));
		}

		@Override
		public void setBackUpDir(File backupDir) {
			this.backupDir = backupDir;
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		LibraryTestParameters.tidyFiles();
		new File(LibraryTestParameters.OUTPUT_FOLDER).mkdirs();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		LibraryTestParameters.tidyFiles();
	}

	@Test
	public void testAddDefaultTimeout() throws Exception {
		// timeout will be 3 secs
		FileBackedTimeoutManager manager = new FileBackedTimeoutManager(1000,
				6, 3, LibraryTestParameters.OUTPUT_FOLDER, new DummyTimeout(0));

		// create a task, give it a number to store
		DummyTimeout task = new DummyTimeout(98765);
		manager.addDefaultTimeout(task);
		Thread manThread = new Thread(manager);
		manThread.start();

		// after 2.5 sec, shouldn't have timed out
		Thread.sleep(2500);
		assertFalse(task.isTimeout());
		// but now it should have done
		Thread.sleep(1000);
		assertTrue(task.isTimeout());
	}

	@Test
	public void testAddTimeout() throws Exception {
		// default timeout will be 3 secs
		FileBackedTimeoutManager manager = new FileBackedTimeoutManager(1000,
				6, 3, LibraryTestParameters.OUTPUT_FOLDER, new DummyTimeout(0));

		// create a task, give it a number to store
		DummyTimeout task = new DummyTimeout(76543);

		// our timeout will be 5 sec
		manager.addTimeout(task, 5);
		Thread manThread = new Thread(manager);
		manThread.start();

		// after 3.5 sec, shouldn't have timed out even though default timeout
		// has passed
		Thread.sleep(3500);
		assertFalse(task.isTimeout());
		// but now it should have done
		Thread.sleep(2000);
		assertTrue(task.isTimeout());
	}

	@Test
	public void testCrashNotTimedOut() throws Exception {

		int testVal = 54321;

		// timeout will be 5 secs
		FileBackedTimeoutManager manager = new FileBackedTimeoutManager(1000,
				6, 5, LibraryTestParameters.OUTPUT_FOLDER, new DummyTimeout(0));

		// create a task, give it a number to store
		DummyTimeout task = new DummyTimeout(testVal);
		manager.addDefaultTimeout(task);
		Thread manThread = new Thread(manager);
		manThread.start();

		// after 1 sec, shouldn't have timed out
		Thread.sleep(1000);
		assertFalse(task.isTimeout());

		// shut down the manager
		manager.shutdown();

		// recreate it all
		FileBackedTimeoutManager phoenix = new FileBackedTimeoutManager(1000,
				6, 5, LibraryTestParameters.OUTPUT_FOLDER, new DummyTimeout(0));

		Thread phoenixThread = new Thread(phoenix);
		phoenixThread.start();

		// but now it should have been recreated and finished
		Thread.sleep(6000);
		// check that the original task really didn't finish
		assertFalse(task.isTimeout());

		// check the existence of the result file
		BufferedReader br = new BufferedReader(new FileReader(
				LibraryTestParameters.OUTPUT_FOLDER + File.separator
						+ "resultfile.txt"));
		String line;
		line = br.readLine();
		br.close();
		int someVal = Integer.parseInt(line);
		assertTrue(someVal == testVal);
	}

	@Test
	public void testCrashTimedOut() throws Exception {

		int testVal = 54321;

		// timeout will be 5 secs
		FileBackedTimeoutManager manager = new FileBackedTimeoutManager(1000,
				6, 5, LibraryTestParameters.OUTPUT_FOLDER, new DummyTimeout(0));

		// create a task, give it a number to store
		DummyTimeout task = new DummyTimeout(testVal);
		manager.addDefaultTimeout(task);
		Thread manThread = new Thread(manager);
		manThread.start();

		// after 1 sec, shouldn't have timed out
		Thread.sleep(1000);
		assertFalse(task.isTimeout());

		// shut down the manager
		manager.shutdown();

		// wait to time out
		Thread.sleep(6000);

		// recreate it all
		try {
			FileBackedTimeoutManager phoenix = new FileBackedTimeoutManager(
					1000, 6, 5, LibraryTestParameters.OUTPUT_FOLDER,
					new DummyTimeout(0));
			fail("should have timed out but didn't");
		} catch (MaxTimeoutExceeded e) {
			// should reach here
		}

		// check that it really didn't write any output
		assertFalse(new File(LibraryTestParameters.OUTPUT_FOLDER
				+ File.separator + "resultfile.txt").exists());
	}

}
