/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.utils.crypto.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>TVSSignatureExceptionTest</code> contains tests for the class <code>{@link TVSSignatureException}</code>.
 */
public class TVSSignatureExceptionTest {

  /**
   * Run the TVSSignatureException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTVSSignatureException_1() throws Exception {
    TVSSignatureException result = new TVSSignatureException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the TVSSignatureException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTVSSignatureException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    TVSSignatureException result = new TVSSignatureException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the TVSSignatureException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTVSSignatureException_3() throws Exception {
    Throwable cause = new Throwable();

    TVSSignatureException result = new TVSSignatureException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the TVSSignatureException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTVSSignatureException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    TVSSignatureException result = new TVSSignatureException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the TVSSignatureException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTVSSignatureException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    TVSSignatureException result = new TVSSignatureException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}