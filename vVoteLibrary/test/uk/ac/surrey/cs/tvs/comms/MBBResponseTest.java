package uk.ac.surrey.cs.tvs.comms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.json.JSONArray;
import org.junit.Test;

public class MBBResponseTest {
	
	final String testArray = "[ \"x\", \"y\", \"z\" ]";
	final String dataItem = "data";

	@Test
	public void testMBBResponseJSONArrayBoolean() throws Exception {
		final JSONArray json = new JSONArray(testArray);
		
		MBBResponse resp = new MBBResponse(json,true);
		assertNotNull(resp);
		assertSame(json,resp.getResponseArray());
		assertTrue(resp.isThresholdChecked());
		assertNull(resp.getDataItem());

		//check that it also does the right thing when boolean parameter set to false
		MBBResponse respF = new MBBResponse(json,false);
		assertNotNull(respF);
		assertSame(json,respF.getResponseArray());
		assertFalse(respF.isThresholdChecked());
		assertNull(respF.getDataItem());	
	}

	@Test
	public void testMBBResponseJSONArrayBooleanString() throws Exception {
		final JSONArray json = new JSONArray(testArray);
		
		MBBResponse resp = new MBBResponse(json,true,dataItem);
		assertNotNull(resp);
		assertSame(json,resp.getResponseArray());
		assertTrue(resp.isThresholdChecked());
		assertEquals(dataItem,resp.getDataItem());
	}

	@Test
	public void testMBBResponseJSONArray() throws Exception {
		final JSONArray json = new JSONArray(testArray);
		
		MBBResponse resp = new MBBResponse(json);
		assertNotNull(resp);
		assertSame(json,resp.getResponseArray());
		//should be defaulting to false here
		assertFalse(resp.isThresholdChecked());
		assertNull(resp.getDataItem());
	}

}
