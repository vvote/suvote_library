/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshakeBuilder;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>VECWebSocketTest</code> contains tests for the class <code>{@link VECWebSocket}</code>.
 */
public class VECWebSocketTest {

  /**
   * Dummy listener.
   */
  private class DummyVECWebSocketListener implements VECWebSocketListener {

    private JSONObject message   = null;

    private WebSocket  webSocket = null;

    public JSONObject getMessage() {
      return this.message;
    }

    public WebSocket getWebSocket() {
      return this.webSocket;
    }

    @Override
    public void processMessage(JSONObject message, WebSocket webSocket) {
      this.message = message;
      this.webSocket = webSocket;
    }
  };

  /**
   * Dummy web socket class. Does nothing.
   */
  private class DummyWebSocket extends WebSocket {

    @Override
    public void close(int arg0) {
    }

    @Override
    public void close(int arg0, String arg1) {
    }

    @Override
    protected void close(InvalidDataException arg0) {
    }

    @Override
    public Draft getDraft() {
      return null;
    }

    @Override
    public InetSocketAddress getLocalSocketAddress() {
      return null;
    }

    @Override
    public int getReadyState() {
      return 0;
    }

    @Override
    public InetSocketAddress getRemoteSocketAddress() {
      return null;
    }

    @Override
    public boolean hasBufferedData() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return false;
    }

    @Override
    public boolean isClosing() {
      return false;
    }

    @Override
    public boolean isConnecting() {
      return false;
    }

    @Override
    public boolean isOpen() {
      return false;
    }

    @Override
    public void send(byte[] arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(ByteBuffer arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(String arg0) throws NotYetConnectedException {
    }

    @Override
    public void sendFrame(Framedata arg0) {
    }

    @Override
    public void startHandshake(ClientHandshakeBuilder arg0) throws InvalidHandshakeException {
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the output files.
    LibraryTestParameters.tidyFiles();

    // Create the output folder.
    File outputFolder = new File(LibraryTestParameters.OUTPUT_FOLDER);
    outputFolder.mkdirs();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the output files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Run the void onClose(WebSocket,int,String,boolean) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testOnClose_1() throws Exception {
    VECWebSocket fixture = new VECWebSocket(new InetSocketAddress(LibraryTestParameters.WEB_SOCKET_PORT));
    fixture.onClose(null, 123, "onClose", true);
  }

  /**
   * Run the void onError(WebSocket,Exception) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testOnError_1() throws Exception {
    VECWebSocket fixture = new VECWebSocket(new InetSocketAddress(LibraryTestParameters.WEB_SOCKET_PORT));
    fixture.onError(null, new Exception());
  }

  /**
   * Run the void onMessage(WebSocket,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testOnMessage_1() throws Exception {
    VECWebSocket fixture = new VECWebSocket(new InetSocketAddress(LibraryTestParameters.WEB_SOCKET_PORT));
    WebSocket ws = new DummyWebSocket();

    // Test with an invalid JSONObject.
    fixture.onMessage(ws, "rubbish");
  }

  /**
   * Run the void onMessage(WebSocket,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testOnMessage_2() throws Exception {
    VECWebSocket fixture = new VECWebSocket(new InetSocketAddress(LibraryTestParameters.WEB_SOCKET_PORT));
    WebSocket ws = new DummyWebSocket();

    // Test with a valid JSONObject but no listeners.
    fixture.onMessage(ws, "{}");
  }

  /**
   * Run the void onMessage(WebSocket,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testOnMessage_3() throws Exception {
    VECWebSocket fixture = new VECWebSocket(new InetSocketAddress(LibraryTestParameters.WEB_SOCKET_PORT));
    WebSocket webSocket = new DummyWebSocket();
    JSONObject request = new JSONObject();
    request.put("type", "genRandomness");

    // Create a listener.
    DummyVECWebSocketListener listener = new DummyVECWebSocketListener();
    fixture.addWebSocketListener(listener);

    // Start the web socket and execute the message.
    fixture.start();
    fixture.onMessage(webSocket, request.toString());

    // Test that the message was processed.
    assertEquals(request.toString(), listener.getMessage().toString());
    assertEquals(webSocket, listener.getWebSocket());

    // Remove the listener.
    fixture.removeWebSocketListener(listener);

    // Test that a new message is not processed.
    fixture.onMessage(webSocket, "{}");
    assertEquals(request.toString(), listener.getMessage().toString());

    fixture.stop();
  }

  /**
   * Run the void onOpen(WebSocket,ClientHandshake) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testOnOpen_1() throws Exception {
    VECWebSocket fixture = new VECWebSocket(new InetSocketAddress(LibraryTestParameters.WEB_SOCKET_PORT));
    WebSocket ws = new DummyWebSocket();

    fixture.onOpen(ws, null);
  }

  /**
   * Run the VECWebSocket(InetSocketAddress) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVECWebSocket_1() throws Exception {
    InetSocketAddress address = new InetSocketAddress(LibraryTestParameters.WEB_SOCKET_PORT);
    VECWebSocket result = new VECWebSocket(address);

    assertNotNull(result);
    assertEquals(LibraryTestParameters.WEB_SOCKET_PORT, result.getPort());
  }

  /**
   * Run the void waitForStartAndUpdatePort(File) method test.
   * 
   * @throws Exception
   */
  @Test(expected = VECWebSocketException.class)
  public void testWaitForStartAndUpdatePort_1() throws Exception {
    InetSocketAddress address = new InetSocketAddress(0);
    VECWebSocket fixture = new VECWebSocket(address);

    // Attempt to wait for the port assignment even though we have not started the web socket: timeout.
    fixture.waitForStartAndUpdatePort(LibraryTestParameters.OUTPUT_PORT_FILE);
  }

  /**
   * Run the void waitForStartAndUpdatePort(File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testWaitForStartAndUpdatePort_2() throws Exception {
    InetSocketAddress address = new InetSocketAddress(0);
    VECWebSocket fixture = new VECWebSocket(address);

    // Start and obtain the port.
    fixture.start();
    fixture.waitForStartAndUpdatePort(LibraryTestParameters.OUTPUT_PORT_FILE);

    // Read the file to get the port.
    JSONObject portObject = IOUtils.readJSONObjectFromFile(LibraryTestParameters.OUTPUT_PORT_FILE);

    assertTrue(portObject.has(LibraryTestParameters.PORT_FIELD));
    assertTrue(fixture.getPort() > 0);
    assertEquals(fixture.getPort(), portObject.get(LibraryTestParameters.PORT_FIELD));

    fixture.stop();
  }

  /**
   * Run the void waitForStartAndUpdatePort(File) method test.
   * 
   * @throws Exception
   */
  @Test(expected = VECWebSocketException.class)
  public void testWaitForStartAndUpdatePort_3() throws Exception {
    InetSocketAddress address = new InetSocketAddress(0);
    VECWebSocket fixture = new VECWebSocket(address);

    // Attempt to wait for the port assignment even though we have not started the web socket: timeout.
    fixture.waitForStartAndUpdatePort(new File(LibraryTestParameters.OUTPUT_PORT_FILE));
  }

  /**
   * Run the void waitForStartAndUpdatePort(File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testWaitForStartAndUpdatePort_4() throws Exception {
    InetSocketAddress address = new InetSocketAddress(0);
    VECWebSocket fixture = new VECWebSocket(address);

    // Start and obtain the port.
    fixture.start();
    fixture.waitForStartAndUpdatePort(new File(LibraryTestParameters.OUTPUT_PORT_FILE));

    // Read the file to get the port.
    JSONObject portObject = IOUtils.readJSONObjectFromFile(LibraryTestParameters.OUTPUT_PORT_FILE);

    assertTrue(portObject.has(LibraryTestParameters.PORT_FIELD));
    assertTrue(fixture.getPort() > 0);
    assertEquals(fixture.getPort(), portObject.get(LibraryTestParameters.PORT_FIELD));

    fixture.stop();
  }
}