/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.PretendMBB;
import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.ErrorMessage;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;

/**
 * The class <code>MBBTest</code> contains tests for the class <code>{@link MBB}</code>.
 */
public class MBBTest {

  /**
   * Run the boolean checkThresholdNonErrors(JSONArray,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThresholdNonErrors_1() throws Exception {
    JSONArray array = new JSONArray();

    JSONObject message = new JSONObject();
    message.put(MessageFields.TYPE, ErrorMessage.TYPE_ERROR);
    array.put(message);

    message = new JSONObject();
    message.put(MessageFields.TYPE, "rubbish");
    array.put(message);

    message = new JSONObject();
    message.put(MessageFields.TYPE, "rubbish");
    array.put(message);

    assertFalse(MBB.checkThresholdNonErrors(array, array.length()));
    assertTrue(MBB.checkThresholdNonErrors(array, array.length() - 1));
  }

  /**
   * Run the boolean checkThresholdOfResponsesReceived(JSONArray,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCheckThresholdOfResponsesReceived_1() throws Exception {
    JSONArray array = new JSONArray();

    JSONObject message = new JSONObject();
    message.put(MessageFields.TYPE, ErrorMessage.TYPE_ERROR);
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    array.put(message);

    message = new JSONObject();
    message.put(MessageFields.TYPE, "rubbish");
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    array.put(message);

    message = new JSONObject();
    message.put(MessageFields.TYPE, "rubbish");
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    array.put(message);

    assertFalse(MBB.checkThresholdOfResponsesReceived(array, array.length()));
    assertTrue(MBB.checkThresholdOfResponsesReceived(array, array.length() - 1));
  }

  /**
   * Run the JSONArray getJustSignatures(JSONArray) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetJustSignatures_1() throws Exception {
    JSONArray array = new JSONArray();

    JSONObject message = new JSONObject();
    message.put(TVSSignature.IS_VALID, true);
    message.put(MessageFields.PeerResponse.PEER_ID, LibraryTestParameters.ID);
    message.put(MessageFields.PeerResponse.PEER_SIG, LibraryTestParameters.ID);
    array.put(message);

    message = new JSONObject();
    message.put(TVSSignature.IS_VALID, false);
    message.put(MessageFields.PeerResponse.PEER_ID, "rubbish");
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    array.put(message);

    JSONArray result = MBB.getJustSignatures(array);

    assertEquals(1, result.length());
    assertEquals(LibraryTestParameters.ID, result.getJSONObject(0).getString(MessageFields.PeerResponse.PEER_ID));
    assertEquals(LibraryTestParameters.ID, result.getJSONObject(0).getString(MessageFields.PeerResponse.PEER_SIG));
  }

  /**
   * Run the String getThresholdFieldValue(JSONArray,String,int) method test.
   * 
   * @throws Exception
   */
  @Test(expected = ConsensusException.class)
  public void testGetThresholdFieldValue_1() throws Exception {
    JSONArray array = new JSONArray();

    JSONObject message = new JSONObject();
    message.put(MessageFields.TYPE, ErrorMessage.TYPE_ERROR);
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    message.put(LibraryTestParameters.ID, Integer.toString(LibraryTestParameters.CANDIDATES));
    array.put(message);

    message = new JSONObject();
    message.put(MessageFields.TYPE, "rubbish");
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    message.put(LibraryTestParameters.ID, Integer.toString(LibraryTestParameters.CANDIDATES));
    array.put(message);

    message = new JSONObject();
    message.put(MessageFields.TYPE, "rubbish");
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    message.put(LibraryTestParameters.ID, Integer.toString(LibraryTestParameters.CANDIDATES));
    array.put(message);

    String result = MBB.getThresholdFieldValue(array, LibraryTestParameters.ID, array.length());
    assertNull(result);
  }

  /**
   * Run the String getThresholdFieldValue(JSONArray,String,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetThresholdFieldValue_2() throws Exception {
    JSONArray array = new JSONArray();

    JSONObject message = new JSONObject();
    message.put(MessageFields.TYPE, ErrorMessage.TYPE_ERROR);
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    message.put(LibraryTestParameters.ID, Integer.toString(LibraryTestParameters.CANDIDATES));
    array.put(message);

    message = new JSONObject();
    message.put(MessageFields.TYPE, "rubbish");
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    message.put(LibraryTestParameters.ID, Integer.toString(LibraryTestParameters.CANDIDATES));
    array.put(message);

    message = new JSONObject();
    message.put(MessageFields.TYPE, "rubbish");
    message.put(MessageFields.PeerResponse.PEER_SIG, "rubbish");
    message.put(LibraryTestParameters.ID, Integer.toString(LibraryTestParameters.CANDIDATES));
    array.put(message);

    String result = MBB.getThresholdFieldValue(array, LibraryTestParameters.ID, array.length() - 1);
    assertNotNull(result);
    assertEquals(Integer.toString(LibraryTestParameters.CANDIDATES), result);
  }

  /**
   * Run the JSONArray sendMessage(MBBConfig,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendMessage_1() throws Exception {
    MBBConfig mbbConf = new MBBConfig(LibraryTestParameters.PEER_CONFIGURATION);
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    // Try sending the message with no timeout defined.
    JSONArray result = MBB.sendMessage(mbbConf, message.toString(), LibraryTestParameters.getClientSSLSocketFactory(),
        new String[] { LibraryTestParameters.CIPHERS });
    assertNotNull(result);
    assertEquals(0, result.length());
  }

  /**
   * Run the JSONArray sendMessage(MBBConfig,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendMessage_2() throws Exception {
    MBBConfig mbbConf = new MBBConfig();
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    // Try sending the message with no peers available.
    JSONArray result = MBB.sendMessage(mbbConf, message.toString(), LibraryTestParameters.getClientSSLSocketFactory(),
        new String[] { LibraryTestParameters.CIPHERS });
    assertNotNull(result);
    assertEquals(0, result.length());
  }

  /**
   * Run the JSONArray sendMessage(MBBConfig,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendMessage_3() throws Exception {
    MBBConfig mbbConf = new MBBConfig();
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(false, false);

    // Try sending the message with peers available.
    JSONArray result = MBB.sendMessage(mbbConf, message.toString(), LibraryTestParameters.getClientSSLSocketFactory(),
        new String[] { LibraryTestParameters.CIPHERS });
    assertNotNull(result);

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    assertEquals(pretendMBBs.size(), result.length());

    for (int i = 0; i < result.length(); i++) {
      assertEquals(PretendMBB.getResponse(false).toString(), result.getJSONObject(i).toString());
    }

    for (PretendMBB pretendMBB : pretendMBBs) {
      List<JSONObject> messages = pretendMBB.getMessages();

      assertEquals(1, messages.size());
      assertEquals(message.toString(), new JSONObject(messages.get(0).toString()).toString());
    }
  }

  /**
   * Run the JSONArray sendMessageWaitForSigs(MBBConfig,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendMessageWaitForSigs_1() throws Exception {
    MBBConfig mbbConf = new MBBConfig();
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(false, false);

    // Try sending the message with peers available.
    JSONArray result = MBB.sendMessageWaitForSigs(mbbConf, message.toString(), LibraryTestParameters.getClientSSLSocketFactory(),
        new String[] { LibraryTestParameters.CIPHERS });
    assertNotNull(result);

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    assertEquals(pretendMBBs.size(), result.length());

    for (int i = 0; i < result.length(); i++) {
      assertEquals(PretendMBB.getResponse(false).getString(MessageFields.PeerResponse.PEER_ID),
          result.getJSONObject(i).getString(MessageFields.PeerResponse.PEER_ID));
      assertEquals(PretendMBB.getResponse(false).getString(MessageFields.PeerResponse.PEER_SIG),
          result.getJSONObject(i).getString(MessageFields.PeerResponse.PEER_SIG));
    }

    for (PretendMBB pretendMBB : pretendMBBs) {
      List<JSONObject> messages = pretendMBB.getMessages();

      assertEquals(1, messages.size());
      assertEquals(message.toString(), new JSONObject(messages.get(0).toString()).toString());
    }
  }

  /**
   * Run the JSONArray sendMessageWaitForSigs(MBBConfig,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendMessageWaitForSigs_2() throws Exception {
    MBBConfig mbbConf = new MBBConfig();
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(false, true);

    // Try sending the message with peers available but get an error back.
    JSONArray result = MBB.sendMessageWaitForSigs(mbbConf, message.toString(), LibraryTestParameters.getClientSSLSocketFactory(),
        new String[] { LibraryTestParameters.CIPHERS });
    assertNotNull(result);

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    assertEquals(0, result.length());

    for (PretendMBB pretendMBB : pretendMBBs) {
      List<JSONObject> messages = pretendMBB.getMessages();

      assertEquals(1, messages.size());
      assertEquals(message.toString(), new JSONObject(messages.get(0).toString()).toString());
    }
  }
}