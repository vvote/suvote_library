/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>PeerSSLInitExceptionTest</code> contains tests for the class <code>{@link PeerSSLInitException}</code>.
 */
public class PeerSSLInitExceptionTest {

  /**
   * Run the PeerSSLInitException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerSSLInitException_1() throws Exception {

    PeerSSLInitException result = new PeerSSLInitException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the PeerSSLInitException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerSSLInitException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    PeerSSLInitException result = new PeerSSLInitException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the PeerSSLInitException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerSSLInitException_3() throws Exception {
    Throwable cause = new Throwable();

    PeerSSLInitException result = new PeerSSLInitException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the PeerSSLInitException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerSSLInitException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    PeerSSLInitException result = new PeerSSLInitException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the PeerSSLInitException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPeerSSLInitException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    PeerSSLInitException result = new PeerSSLInitException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}