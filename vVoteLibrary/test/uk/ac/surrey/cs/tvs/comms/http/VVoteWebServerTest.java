/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import fi.iki.elonen.NanoHTTPD.CookieHandler;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.NanoHTTPD.ResponseException;

/**
 * The class <code>VVoteWebServerTest</code> contains tests for the class <code>{@link VVoteWebServer}</code>.
 */
public class VVoteWebServerTest {

  /**
   * Dummy servlet.
   */
  private class DummyServlet implements NanoServlet {

    public static final String SERVLET = "dummy";

    @Override
    public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
        Map<String, String> files, File homeDir) {
      return new Response(SERVLET);
    }
  }

  /**
   * Dummy HTTP session class.
   */
  private class DummySession implements IHTTPSession {

    public Method method = null;
    public String uri    = null;

    @Override
    public void execute() throws IOException {
    }

    @Override
    public CookieHandler getCookies() {
      return null;
    }

    @Override
    public Map<String, String> getHeaders() {
      return new HashMap<String, String>();
    }

    @Override
    public InputStream getInputStream() {
      return null;
    }

    @Override
    public Method getMethod() {
      return this.method;
    }

    @Override
    public Map<String, String> getParms() {
      return null;
    }

    @Override
    public String getUri() {
      return this.uri;
    }

    @Override
    public void parseBody(Map<String, String> files) throws IOException, ResponseException {
    }

    @Override
    public String getQueryParameterString() {
      // TODO Auto-generated method stub
      return null;
    }
  }

  /**
   * Run the fi.iki.elonen.NanoHTTPD.Response serve(IHTTPSession) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testServe_1() throws Exception {
    File folder = new File(LibraryTestParameters.OUTPUT_FOLDER);
    folder.mkdirs();
    VVoteWebServer fixture = new VVoteWebServer(LibraryTestParameters.WEB_HOST, LibraryTestParameters.WEB_PORT, folder,
        new HashMap<String, NanoServlet>());

    DummySession session = new DummySession();
    session.method = Method.GET;
    session.uri = "";

    // Test a non-servlet request.
    Response result = fixture.serve(session);
    assertNotNull(result);

    BufferedReader reader = new BufferedReader(new InputStreamReader(result.getData()));
    StringBuffer data = new StringBuffer();
    String line = null;

    while ((line = reader.readLine()) != null) {
      data.append(line);
    }

    reader.close();

    assertNotNull(data);
    assertFalse(data.length() <= 0);
  }

  /**
   * Run the fi.iki.elonen.NanoHTTPD.Response serve(IHTTPSession) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testServe_2() throws Exception {
    File folder = new File(LibraryTestParameters.OUTPUT_FOLDER);
    folder.mkdirs();
    VVoteWebServer fixture = new VVoteWebServer(LibraryTestParameters.WEB_HOST, LibraryTestParameters.WEB_PORT, folder,
        new HashMap<String, NanoServlet>());

    DummySession session = new DummySession();
    session.method = Method.PUT;
    session.uri = "";

    // Test a non-servlet request with headers.
    Response result = fixture.serve(session);
    assertNotNull(result);

    BufferedReader reader = new BufferedReader(new InputStreamReader(result.getData()));
    StringBuffer data = new StringBuffer();
    String line = null;

    while ((line = reader.readLine()) != null) {
      data.append(line);
    }

    reader.close();

    assertNotNull(data);
    assertFalse(data.length() <= 0);
  }

  /**
   * Run the fi.iki.elonen.NanoHTTPD.Response serve(IHTTPSession) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testServe_3() throws Exception {
    File folder = new File(LibraryTestParameters.OUTPUT_FOLDER);
    folder.mkdirs();
    VVoteWebServer fixture = new VVoteWebServer(LibraryTestParameters.WEB_HOST, LibraryTestParameters.WEB_PORT, folder,
        new HashMap<String, NanoServlet>());

    DummySession session = new DummySession();
    session.method = Method.PUT;
    session.uri = "/servlet/";

    // Test a servlet request with no content.
    Response result = fixture.serve(session);
    assertNotNull(result);

    BufferedReader reader = new BufferedReader(new InputStreamReader(result.getData()));
    StringBuffer data = new StringBuffer();
    String line = null;

    while ((line = reader.readLine()) != null) {
      data.append(line);
    }

    reader.close();

    assertNotNull(data);
    assertFalse(data.length() <= 0);
  }

  /**
   * Run the fi.iki.elonen.NanoHTTPD.Response serve(IHTTPSession) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testServe_4() throws Exception {
    File folder = new File(LibraryTestParameters.OUTPUT_FOLDER);
    folder.mkdirs();

    Map<String, NanoServlet> servlets = new HashMap<String, NanoServlet>();
    VVoteWebServer fixture = new VVoteWebServer(LibraryTestParameters.WEB_HOST, LibraryTestParameters.WEB_PORT, folder, servlets);

    DummySession session = new DummySession();
    session.method = Method.PUT;
    session.uri = "/servlet/" + DummyServlet.SERVLET;
    servlets.put(DummyServlet.SERVLET, new DummyServlet());

    // Test a servlet request with content.
    Response result = fixture.serve(session);
    assertNotNull(result);

    BufferedReader reader = new BufferedReader(new InputStreamReader(result.getData()));
    StringBuffer data = new StringBuffer();
    String line = null;

    while ((line = reader.readLine()) != null) {
      data.append(line);
    }

    reader.close();

    assertNotNull(data);
    assertFalse(data.length() <= 0);

    assertEquals(DummyServlet.SERVLET, data.toString());
  }

  /**
   * Run the fi.iki.elonen.NanoHTTPD.Response serve(IHTTPSession) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testServe_5() throws Exception {
    File folder = new File(LibraryTestParameters.OUTPUT_FOLDER);
    folder.mkdirs();

    Map<String, NanoServlet> servlets = new HashMap<String, NanoServlet>();
    VVoteWebServer fixture = new VVoteWebServer(LibraryTestParameters.WEB_HOST, LibraryTestParameters.WEB_PORT, folder, servlets);

    DummySession session = new DummySession();
    session.method = Method.PUT;
    session.uri = "/servlet/" + DummyServlet.SERVLET + "?ignored";
    servlets.put(DummyServlet.SERVLET, new DummyServlet());

    // Test a servlet request with content.
    Response result = fixture.serve(session);
    assertNotNull(result);

    BufferedReader reader = new BufferedReader(new InputStreamReader(result.getData()));
    StringBuffer data = new StringBuffer();
    String line = null;

    while ((line = reader.readLine()) != null) {
      data.append(line);
    }

    reader.close();

    assertNotNull(data);
    assertFalse(data.length() <= 0);

    assertEquals(DummyServlet.SERVLET, data.toString());
  }

  /**
   * Run the VVoteWebServer(String,int,File,Map<String,NanoServlet>) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVVoteWebServer_1() throws Exception {
    File folder = new File(LibraryTestParameters.OUTPUT_FOLDER);
    folder.mkdirs();
    VVoteWebServer result = new VVoteWebServer(LibraryTestParameters.WEB_HOST, LibraryTestParameters.WEB_PORT, folder,
        new HashMap<String, NanoServlet>());
    assertNotNull(result);

    assertEquals(false, result.isAlive());
    assertEquals(false, result.wasStarted());
  }
}