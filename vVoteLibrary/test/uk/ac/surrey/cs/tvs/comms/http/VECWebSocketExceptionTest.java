/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>VECWebSocketExceptionTest</code> contains tests for the class <code>{@link VECWebSocketException}</code>.
 */
public class VECWebSocketExceptionTest {

  /**
   * Run the VECWebSocketException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVECWebSocketException_1() throws Exception {

    VECWebSocketException result = new VECWebSocketException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.http.VECWebSocketException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the VECWebSocketException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVECWebSocketException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    VECWebSocketException result = new VECWebSocketException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.http.VECWebSocketException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the VECWebSocketException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVECWebSocketException_3() throws Exception {
    Throwable cause = new Throwable();

    VECWebSocketException result = new VECWebSocketException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.http.VECWebSocketException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the VECWebSocketException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVECWebSocketException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    VECWebSocketException result = new VECWebSocketException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.http.VECWebSocketException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the VECWebSocketException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testVECWebSocketException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    VECWebSocketException result = new VECWebSocketException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.http.VECWebSocketException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}