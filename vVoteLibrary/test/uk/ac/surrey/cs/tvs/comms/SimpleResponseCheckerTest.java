package uk.ac.surrey.cs.tvs.comms;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;

public class SimpleResponseCheckerTest {

	private static final String testData = "somethingToSign";
	
	@Test
	public void testSimpleResponseChecker() throws Exception {
		BLSKeyPair keyPair = BLSKeyPair.generateKeyPair();
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		store.addBLSPublicKey(keyPair.getPublicKey(), "Peer1_pubkey");
		
		SimpleResponseChecker checker = new SimpleResponseChecker(store,"_pubkey",testData);
		assertNotNull(checker);
	}

	@Test
	public void testCheckEarlyResponse() throws Exception {
		TVSKeyStore store = TVSKeyStore.getInstance("JKS", "SUN");
		store.load("keyoutput/Peer1_public.bks",null);
		TVSKeyStore privStore = TVSKeyStore.getInstance("JKS", "SUN");
		privStore.load("keyoutput/Peer1_private.bks",null);
		
		SimpleResponseChecker checker = new SimpleResponseChecker(store,"_SigningSK2",testData);
		
		//this response is rubbish: the signature is incorrect
		JSONObject response = new JSONObject("{ \"WBBID\": \"Peer1\", \"WBBSig\": \"blah\" }");
		//so the check comes back as false
		assertFalse(checker.checkEarlyResponse(response).isValid());

		//this response is rubbish: the JSON is junk
		response = new JSONObject("{ \"x\": \"hypotenuse\" }");
		//so the check comes back as false
		assertFalse(checker.checkEarlyResponse(response).isValid());

		TVSSignature sign = new TVSSignature(privStore.getBLSPrivateKey("Peer1_SigningSK2"));
		sign.setPartial(true);
		sign.update(testData);
		//properly encoded partial sig
		String base64sig = sign.signAndEncode(EncodingType.BASE64);

		response = new JSONObject(String.format("{ \"WBBID\": \"Peer1\", \"WBBSig\": \"%s\" }",base64sig));
		assertTrue(checker.checkEarlyResponse(response).isValid());	
	}

}
