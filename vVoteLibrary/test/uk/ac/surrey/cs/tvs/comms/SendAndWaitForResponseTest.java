/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.PretendMBB;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;

/**
 * The class <code>SendAndWaitForResponseTest</code> contains tests for the class <code>{@link SendAndWaitForResponse}</code>.
 */
public class SendAndWaitForResponseTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the output files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the output files.
    LibraryTestParameters.tidyFiles();
  }

  /**
   * Run the String call() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCall_1() throws Exception {
    MBBConfig mbbConf = new MBBConfig(LibraryTestParameters.PEER_CONFIGURATION);
    Collection<JSONObject> peers = mbbConf.getPeers().values();
    JSONObject peer = peers.iterator().next();

    String address = peer.getString(ConfigFiles.MBBConfig.PEER_ADDRESS);
    int port = peer.getInt(ConfigFiles.MBBConfig.PEER_PORT);
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    final SendAndWaitForResponse fixture = new SendAndWaitForResponse(address, port, message.toString());

    // Test sending a message with no recipient. The call has to be from within a thread so we can interrupt it.
    Thread thread = new Thread(new Runnable() {

      @Override
      public void run() {
        String result;
        try {
          result = fixture.call();
          assertNull(result);
        }
        catch (Exception e) {
          fail("exception" + e.toString());
        }
      }
    });

    thread.start();

    // Wait for a bit to allow timeout.
    LibraryTestParameters.wait(10);

    // Stop the thread.
    thread.interrupt();
    thread.join();
  }

  /**
   * Run the String call() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCall_2() throws Exception {
    MBBConfig mbbConf = new MBBConfig(LibraryTestParameters.PEER_CONFIGURATION);
    Collection<JSONObject> peers = mbbConf.getPeers().values();
    JSONObject peer = peers.iterator().next();

    String address = peer.getString(ConfigFiles.MBBConfig.PEER_ADDRESS);
    int port = peer.getInt(ConfigFiles.MBBConfig.PEER_PORT);
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    String messages = message.toString() + "\n";

    SendAndWaitForResponse fixture = new SendAndWaitForResponse(address, port, messages);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(false, false);

    // Create a listener.
    ResponseListener listener = new ResponseListener() {

      @Override
      public void responseFromWait(String response) {
        assertNotNull(response);
        try {
          assertEquals(PretendMBB.getResponse(false).toString(), response);
        }
        catch (JSONException e) {
          fail("exception");
        }
      }
    };

    // Test sending a message over SSL.
    fixture.setCipherSuite(LibraryTestParameters.CIPHERS);
    fixture.setSSLSocketFactory(LibraryTestParameters.getClientSSLSocketFactory());
    fixture.addResponseListener(listener);

    String result = fixture.call();
    assertNotNull(result);
    assertEquals(PretendMBB.getResponse(false).toString(), result);

    fixture.removeResponseListener(listener);

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Test the message was received.
    List<JSONObject> received = pretendMBBs.get(1).getMessages();

    assertEquals(1, received.size());
    assertEquals(message.toString(), new JSONObject(received.get(0).toString()).toString());
  }

  /**
   * Run the String call() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCall_3() throws Exception {
    MBBConfig mbbConf = new MBBConfig(LibraryTestParameters.PEER_CONFIGURATION);
    Collection<JSONObject> peers = mbbConf.getPeers().values();
    JSONObject peer = peers.iterator().next();

    String address = peer.getString(ConfigFiles.MBBConfig.PEER_ADDRESS);
    int port = peer.getInt(ConfigFiles.MBBConfig.PEER_PORT);
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    File file = new File(LibraryTestParameters.OUTPUT_DATA_FILE);
    LibraryTestParameters.createTestFile(file);
    String messages = message.toString() + "\n" + MBB.FILE_SUB_TOKEN + file.getAbsolutePath();

    SendAndWaitForResponse fixture = new SendAndWaitForResponse(address, port, messages);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(true, false);

    // Create a listener.
    ResponseListener listener = new ResponseListener() {

      @Override
      public void responseFromWait(String response) {
        assertNotNull(response);
        try {
          assertEquals(PretendMBB.getResponse(false).toString(), response);
        }
        catch (JSONException e) {
          fail("exception");
        }
      }
    };

    // Test sending a message with a file over SSL.
    fixture.setCipherSuite(LibraryTestParameters.CIPHERS);
    fixture.setSSLSocketFactory(LibraryTestParameters.getClientSSLSocketFactory());
    fixture.addResponseListener(listener);

    String result = fixture.call();
    assertNotNull(result);
    assertEquals(PretendMBB.getResponse(false).toString(), result);

    fixture.removeResponseListener(listener);

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Test the message was received.
    List<JSONObject> received = pretendMBBs.get(1).getMessages();

    assertEquals(1, received.size());
    assertEquals(message.toString(), new JSONObject(received.get(0).toString()).toString());

    // Test the file was received.
    List<byte[]> data = pretendMBBs.get(1).getData();

    assertEquals(1, data.size());
    assertEquals("\n" + LibraryTestParameters.LINE, new String(data.get(0)));
  }

  /**
   * Run the String call() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCall_4() throws Exception {
    MBBConfig mbbConf = new MBBConfig(LibraryTestParameters.PEER_CONFIGURATION);
    Collection<JSONObject> peers = mbbConf.getPeers().values();
    JSONObject peer = peers.iterator().next();

    String address = peer.getString(ConfigFiles.MBBConfig.PEER_ADDRESS);
    int port = peer.getInt(ConfigFiles.MBBConfig.PEER_PORT);
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    File file = new File(LibraryTestParameters.OUTPUT_DATA_FILE);
    LibraryTestParameters.createTestFile(file);
    String messages = message.toString() + "\n" + MBB.FILE_SUB_TOKEN + file.getAbsolutePath();

    SendAndWaitForResponse fixture = new SendAndWaitForResponse(address, port, messages);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(true, false);

    // Create a listener.
    ResponseListener listener = new ResponseListener() {

      @Override
      public void responseFromWait(String response) {
        assertNotNull(response);
        try {
          assertEquals(PretendMBB.getResponse(false).toString(), response);
        }
        catch (JSONException e) {
          fail("exception");
        }
      }
    };

    // Test sending a message with a file over SSL.
    fixture.setCipherSuite(LibraryTestParameters.CIPHERS);
    fixture.setSSLSocketFactory(LibraryTestParameters.getClientSSLSocketFactory());
    fixture.addResponseListener(listener);

    String result = fixture.call();
    assertNotNull(result);
    assertEquals(PretendMBB.getResponse(false).toString(), result);

    fixture.removeResponseListener(listener);

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Test the message was received.
    List<JSONObject> received = pretendMBBs.get(1).getMessages();

    assertEquals(1, received.size());
    assertEquals(message.toString(), new JSONObject(received.get(0).toString()).toString());

    // Test the file was received.
    List<byte[]> data = pretendMBBs.get(1).getData();

    assertEquals(1, data.size());
    assertEquals("\n" + LibraryTestParameters.LINE, new String(data.get(0)));
  }

  /**
   * Run the SendAndWaitForResponse() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendAndWaitForResponse_1() throws Exception {
    MBBConfig mbbConf = new MBBConfig(LibraryTestParameters.PEER_CONFIGURATION);
    Collection<JSONObject> peers = mbbConf.getPeers().values();
    JSONObject peer = peers.iterator().next();

    String address = peer.getString(ConfigFiles.MBBConfig.PEER_ADDRESS);
    int port = peer.getInt(ConfigFiles.MBBConfig.PEER_PORT);
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    // Test construction.
    SendAndWaitForResponse result = new SendAndWaitForResponse(address, port, message.toString());
    assertNotNull(result);
  }

  /**
   * Run the SendAndWaitForResponse() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendAndWaitForResponse_2() throws Exception {
    MBBConfig mbbConf = new MBBConfig(LibraryTestParameters.PEER_CONFIGURATION);
    Collection<JSONObject> peers = mbbConf.getPeers().values();
    JSONObject peer = peers.iterator().next();

    String address = peer.getString(ConfigFiles.MBBConfig.PEER_ADDRESS);
    int port = peer.getInt(ConfigFiles.MBBConfig.PEER_PORT);
    JSONObject message = new JSONObject(LibraryTestParameters.MESSAGE);

    // Test construction.
    SendAndWaitForResponse result = new SendAndWaitForResponse(address, port, message.toString(), 3000);
    assertNotNull(result);
  }
}