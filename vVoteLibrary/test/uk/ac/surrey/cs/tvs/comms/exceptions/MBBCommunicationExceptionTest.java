/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>MBBCommunicationExceptionTest</code> contains tests for the class <code>{@link MBBCommunicationException}</code>.
 */
public class MBBCommunicationExceptionTest {

  /**
   * Run the MBBCommunicationException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBCommunicationException_1() throws Exception {

    MBBCommunicationException result = new MBBCommunicationException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the MBBCommunicationException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBCommunicationException_2() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;

    MBBCommunicationException result = new MBBCommunicationException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the MBBCommunicationException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBCommunicationException_3() throws Exception {
    Throwable cause = new Throwable();

    MBBCommunicationException result = new MBBCommunicationException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the MBBCommunicationException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBCommunicationException_4() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    MBBCommunicationException result = new MBBCommunicationException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the MBBCommunicationException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBCommunicationException_5() throws Exception {
    String message = LibraryTestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    MBBCommunicationException result = new MBBCommunicationException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException: " + LibraryTestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(LibraryTestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}