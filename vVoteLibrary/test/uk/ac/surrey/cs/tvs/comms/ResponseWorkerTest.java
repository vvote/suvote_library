/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.comms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;

/**
 * The class <code>ResponseWorkerTest</code> contains tests for the class <code>{@link ResponseWorker}</code>.
 */
public class ResponseWorkerTest {

  /**
   * Run the ResponseWorker(InputStream) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testResponseWorker_1() throws Exception {
    ResponseWorker result = new ResponseWorker(null);
    assertNotNull(result);
    assertFalse(result.gotResponse());
    assertEquals(null, result.getResponse());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    ByteArrayInputStream inputStream = new ByteArrayInputStream(LibraryTestParameters.FILE_LINE.getBytes());

    ResponseWorker fixture = new ResponseWorker(inputStream);

    // Test reading the line.
    fixture.run();

    assertTrue(fixture.gotResponse());
    assertEquals(LibraryTestParameters.LINE, fixture.getResponse());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    ByteArrayInputStream inputStream = new ByteArrayInputStream(new byte[0]);

    ResponseWorker fixture = new ResponseWorker(inputStream);

    // Test reading the line.
    fixture.run();
  }
}