package uk.ac.surrey.cs.tvs.comms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ResponseCheckerResultTest {

	final String testString = "something";

	@Test
	public void testResponseCheckerResultStringBoolean() throws Exception {
		
		ResponseCheckerResult res = new ResponseCheckerResult(testString,true);
		assertNotNull(res);
		assertTrue(res.isValid());
		assertTrue(res.hasDataItem());
		assertEquals(res.getDataItem(),testString);
	}

	@Test
	public void testResponseCheckerResultBoolean() throws Exception {
		ResponseCheckerResult res = new ResponseCheckerResult(true);
		assertTrue(res.isValid());
		assertFalse(res.hasDataItem());
		assertNull(res.getDataItem());

		ResponseCheckerResult resFalse = new ResponseCheckerResult(false);
		assertFalse(resFalse.isValid());
		assertFalse(resFalse.hasDataItem());
		assertNull(resFalse.getDataItem());
	}

}
