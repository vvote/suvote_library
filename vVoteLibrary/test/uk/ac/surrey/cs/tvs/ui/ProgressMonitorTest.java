/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshakeBuilder;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.LibraryTestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;

/**
 * The class <code>ProgressMonitorTest</code> contains tests for the class <code>{@link ProgressMonitor}</code>.
 */
public class ProgressMonitorTest {

  /**
   * Dummy web socket class. Does nothing.
   */
  private class DummyWebSocket extends WebSocket {

    /** The message which has been sent. */
    private String message;

    @Override
    public void close(int arg0) {
    }

    @Override
    public void close(int arg0, String arg1) {
    }

    @Override
    protected void close(InvalidDataException arg0) {
    }

    @Override
    public Draft getDraft() {
      return null;
    }

    @Override
    public InetSocketAddress getLocalSocketAddress() {
      return null;
    }

    /**
     * @return the message.
     */
    private String getMessage() {
      return this.message;
    }

    @Override
    public int getReadyState() {
      return 0;
    }

    @Override
    public InetSocketAddress getRemoteSocketAddress() {
      return null;
    }

    @Override
    public boolean hasBufferedData() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return false;
    }

    @Override
    public boolean isClosing() {
      return false;
    }

    @Override
    public boolean isConnecting() {
      return false;
    }

    @Override
    public boolean isOpen() {
      return false;
    }

    @Override
    public void send(byte[] arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(ByteBuffer arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(String message) throws NotYetConnectedException {
      this.message = message;
    }

    @Override
    public void sendFrame(Framedata arg0) {
    }

    @Override
    public void startHandshake(ClientHandshakeBuilder arg0) throws InvalidHandshakeException {
    }
  }

  /**
   * Run the ProgressMonitor(String,WebSocket) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProgressMonitor_1() throws Exception {
    WebSocket ws = new DummyWebSocket();
    ProgressMonitor result = new ProgressMonitor(LibraryTestParameters.NAME, ws);
    assertNotNull(result);
  }

  /**
   * Run the void updateProgress(int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testUpdateProgress_1() throws Exception {
    DummyWebSocket ws = new DummyWebSocket();
    ProgressMonitor fixture = new ProgressMonitor(LibraryTestParameters.NAME, ws);

    int progress = (int) LibraryTestParameters.PROGRESS;

    fixture.updateProgress(progress);

    // Test that the correct message was sent.
    JSONObject message = new JSONObject(ws.getMessage());

    assertTrue(message.has(JSONConstants.ProgressMessage.TYPE));
    assertTrue(message.has(JSONConstants.ProgressMessage.PROGRESS_ID));
    assertTrue(message.has(JSONConstants.ProgressMessage.VALUE));

    assertEquals(JSONConstants.ProgressMessage.TYPE_PROGRESS_UPDATE, message.get(JSONConstants.ProgressMessage.TYPE));
    assertEquals(LibraryTestParameters.NAME, message.get(JSONConstants.ProgressMessage.PROGRESS_ID));
    assertEquals(progress, message.get(JSONConstants.ProgressMessage.VALUE));
  }
}