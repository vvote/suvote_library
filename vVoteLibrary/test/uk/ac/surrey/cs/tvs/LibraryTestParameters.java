/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;

/**
 * This class defines common test parameters and static initialisers within a singleton.
 */
public class LibraryTestParameters {

  /** The number of candidate ids to be created. */
  public static final int              CANDIDATES                       = 80;

  /** Input file for the public key. */
  public static final String           PUBLIC_KEY_FILE                  = "./testdata/publickey.json";

  /** Test peer configuration file. */
  public static final String           PEER_CONFIGURATION               = "./testdata/mbbConfig.conf";

  /** Test peer configuration schema file. */
  public static final String           PEER_CONFIGURATION_SCHEMA        = "./testdata/mbbConfigSchema.json";

  /** The peer id matching the server_conf.json. */
  public static final String           PEER_ID                          = "SampleEBM";

  /** The signing key store. */
  public static final String           SIGNING_KEY_STORE                = "./testdata/signing.jks";

  /** The signing key store password. */
  public static final String           SIGNING_KEY_STORE_PASSWORD       = "";

  /** The signing key entity. */
  public static final String           SIGNING_KEY_ENTITY               = "SampleEBM";
  public static final String           SIGNING_KEY_ENTITY_2               = "dummykey";

  /** The signing key entity password. */
  public static final String           SIGNING_KEY_ENTITY_PASSWORD      = "";

  /** Test JSON schema. */
  public static final String           SCHEMA                           = "./testdata/schema.json";

  /** Test serial number. */
  public static final String           SERIAL_NO                        = "TestDeviceOne:101";

  /** Permutations as a JSONObject. */
  public static final String           PERMUTATIONS_OBJECT              = "[[1,2,3],[4,5,6],[7,8,9]]";

  /** Permutations as a string. */
  public static final String           PERMUTATIONS                     = "1,2,3:4,5,6:7,8,9:";

  /** Empty preferences. */
  public static final String           EMPTY_PREFERENCES                = "[\" \",\" \",\" \"]";

  /** Invalid preferences. */
  public static final String           INVALID_PREFERENCES              = "[\"1\",\"2\",\"4\"]";

  /** Multiple duplicate preferences. */
  public static final String           DUPLICATE_PREFERENCES            = "[\"1\",\" \",\"1\"]";

  /** Out-of-order preferences. */
  public static final String           OUT_OF_ORDER_PREFERENCES         = "[\"3\",\" \",\"2\"]";

  /** Multiple preferences. */
  public static final String           MULTIPLE_PREFERENCES             = "[\"3\",\"1\",\"2\"]";

  /** Single preferences. */
  public static final String           SINGLE_PREFERENCES               = "[\"1\",\" \",\" \"]";

  /** LA race preferences. */
  public static final String           LA_PREFERENCES                   = "{\"" + MessageFields.VoteMessage.RACE_ID + "\":\""
                                                                            + MessageFields.VoteMessage.LA
                                                                            + "\",\"preferences\":[\"1\",\"2\",\"3\"]}";

  /** LC ATL race preferences. */
  public static final String           LC_ATL_PREFERENCES               = "{\"" + MessageFields.VoteMessage.RACE_ID + "\":\""
                                                                            + MessageFields.VoteMessage.LC_ATL
                                                                            + "\",\"preferences\":[\"1\",\" \",\" \"]}";

  /** LC ATL empty race preferences. */
  public static final String           LC_ATL_EMPTY_PREFERENCES         = "{\"" + MessageFields.VoteMessage.RACE_ID + "\":\""
                                                                            + MessageFields.VoteMessage.LC_ATL
                                                                            + "\",\"preferences\":[\" \",\" \",\" \"]}";
  /** LC BTL race preferences. */
  public static final String           LC_BTL_PREFERENCES               = "{\"" + MessageFields.VoteMessage.RACE_ID + "\":\""
                                                                            + MessageFields.VoteMessage.LC_BTL
                                                                            + "\",\"preferences\":[\"1\",\"2\",\"3\"]}";

  /** LC BTL empty race preferences. */
  public static final String           LC_BTL_EMPTY_PREFERENCES         = "{\"" + MessageFields.VoteMessage.RACE_ID + "\":\""
                                                                            + MessageFields.VoteMessage.LC_BTL
                                                                            + "\",\"preferences\":[\" \",\" \",\" \"]}";

  /** LA only race preferences. */
  public static final String           LA_RACE_PREFERENCES              = "[" + LA_PREFERENCES + "]";

  /** LA and LC ATL only race preferences. */
  public static final String           LA_LC_ATL_RACE_PREFERENCES       = "[" + LA_PREFERENCES + "," + LC_ATL_PREFERENCES + ","
                                                                            + LC_BTL_EMPTY_PREFERENCES + "]";

  /** LA and LC BTL only race preferences. */
  public static final String           LA_LC_BTL_RACE_PREFERENCES       = "[" + LA_PREFERENCES + "," + LC_ATL_EMPTY_PREFERENCES
                                                                            + "," + LC_BTL_PREFERENCES + "]";

  /** Races as a string. */
  public static final String           RACES                            = "[{\"id\":\"LA\",\"preferences\":[\"1\",\"2\",\"3\"]},{\"id\":\"LC_ATL\",\"preferences\":[\"4\",\"5\",\"6\"]},{\"id\":\"LC_BTL\",\"preferences\":[\"7\",\"8\",\"9\"]}]";

  /** The test web socket port. */
  public static final int              WEB_SOCKET_PORT                  = 9090;

  /** Progress proportion. */
  public static final float            PROGRESS                         = 100f;

  /** Dummy name used for testing. */
  public static final String           NAME                             = "myName";

  /** Test string data. */
  public static final String           LINE                             = "123456789";

  /** Test string data for file. */
  public static final String           FILE_LINE                        = LINE + "\n";

  /** Test data for crypto: must be at least 32 bytes. */
  public static byte[]                 DATA_LONG_1                      = new byte[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5,
      4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };

  /** Test data for crypto: must be at least 32 bytes. */
  public static byte[]                 DATA_LONG_2                      = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4,
      5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, };

  /** Test data for crypto: must be 32 bytes. */
  public static byte[]                 DATA_SHORT_1                     = new byte[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5,
      4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8                };

  /** Test data for crypto: must be 32 bytes. */
  public static byte[]                 DATA_SHORT_2                     = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4,
      5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1                };

  /** Random data for crypto: must be 32 bytes. */
  public static byte[]                 RANDOM_DATA                      = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4,
      5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1                };

  /** Test ElGamal key size. */
  public static final int              ELGAMAL_KEY_SIZE                 = 272;

  /** Test AES key size. */
  public static final int              AES_KEY_SIZE                     = 256;

  /** Dummy JSON message. */
  public static String                 MESSAGE                          = "{\"hello\":\"world\",\"fileSize\":" + LINE.length()
                                                                            + "}";

  /** Dummy exception message used for testing. */
  public static final String           EXCEPTION_MESSAGE                = "Exception Message";

  /** Certificate challenge password. */
  public static final String           CHALLENGE_PASSWORD               = "";

  /** Certificate days. */
  public static final int              DAYS                             = 30;

  /** Certificate distinguished name. */
  public static final String           DN                               = "DN=MY_NAME";

  /** An invalid, BASE64 encoded signature. */
  public static final String           DUMMY_SIGNATURE                  = "MCwCFG/12/HTRK5ziSyt3Qzq/4dHRaD9AhQgJoCI4aTYNdFvOvdmSS43h6YTAA==";

  /** Test UUID/session ID. */
  public static final UUID             SESSION_ID                       = UUID.randomUUID();

  /** Test message as string. */
  public static final String           VOTE_MESSAGE                     = "{\""
                                                                            + MessageFields.JSONWBBMessage.ID
                                                                            + "\":\""
                                                                            + SERIAL_NO
                                                                            + "\",\""
                                                                            + MessageFields.UUID
                                                                            + "\":\""
                                                                            + SESSION_ID
                                                                            + "\",\""
                                                                            + MessageFields.VoteMessage.RACES
                                                                            + "\":[{\"id\":\"LA\",\"preferences\":[\"1\",\"2\",\"3\"]},{\"id\":\"LC_ATL\",\"preferences\":[\"1\",\"2\",\"3\"]},{\"id\":\"LC_BTL\",\"preferences\":[\"1\",\"2\",\"3\"]}]}";

  /** Vote message type string. */
  public static final String           MESSAGE_TYPE_VOTE_MESSAGE_STRING = "vote";

  /** Test device. */
  public static final String           DEVICE                           = "TestDeviceOne";

  /** Test district. */
  public static final String           DISTRICT                         = "Northcote";

  /** Test certificate. */
  public static final String           SENDER_CERTIFICATE               = "PODPeer1Signing";

  /** Test permutation. */
  public static final String           PERMUTATION                      = "1,2,3:1,2,3:1,2,3:";

  /** Test commit time. */
  public static final String           COMMIT_TIME                      = "18:00";

  /** Test serial signatures. */
  public static final String           SERIAL_SIGNATURES                = "\"WBBID\":\"" + DEVICE + "\"" + ",\"WBBSig\":\""
                                                                            + DUMMY_SIGNATURE + "\"";

  /** Serial signature message field. */
  public static final String           SERIAL_SIG_FIELD                 = "serialSig";

  /** Serial signatures message field. */
  public static final String           SERIAL_SIGS_FIELD                = "serialSigs";

  /** Configuration file test field: only in the default file. */
  public static final String           CONFIG_ONLY_IN_DEFAULT           = "OnlyInDefault";

  /** Configuration file test field: boolean. */
  public static final String           CONFIG_BOOLEAN                   = "BooleanValue";

  /** Configuration file test field: integer. */
  public static final String           CONFIG_INT                       = "IntValue";

  /** Configuration file test field: integer value. */
  public static final int              CONFIG_INT_VALUE                 = 1;

  /** Configuration file test field: String. */
  public static final String           CONFIG_STRING                    = "StringValue";

  /** Configuration file test field: String value. */
  public static final String           CONFIG_STRING_VALUE              = "hello";

  /** Output folder for files. */
  public static final String           OUTPUT_FOLDER                    = "output";

  /** Output folder for unzipped files. */
  public static final String           ZIP_FOLDER                       = "unzipped";

  /** Output file for plain text ids. */
  public static final String           OUTPUT_PLAIN_TEXT_FILE           = "plaintexts_ids.json";

  /** Output file for encrypted ids. */
  public static final String           OUTPUT_ENCRYPTED_FILE            = "base_encrypted_ids.json";

  /** Output file for port information. */
  public static final String           OUTPUT_PORT_FILE                 = "port.json";

  /** Output file for data. */
  public static final String           OUTPUT_DATA_FILE                 = "data.txt";

  /** Output ZIP file. */
  public static final String           OUTPUT_ZIP_FILE                  = "output.zip";

  /** Test ZIP file with an entry which is too large. */
  public static final String           LARGE_ZIP_FILE                   = "./testdata/large.zip";

  /** Number of test directories to create. */
  public static final int              NUM_DIRECTORIES                  = 10;

  /** Number of test data entries. */
  public static final int              NUM_DATA                         = 100;

  /** JSON field name for port value. */
  public static String                 PORT_FIELD                       = "port";

  /** Web server host. */
  public static String                 WEB_HOST                         = "localhost";

  /** Web server port. */
  public static int                    WEB_PORT                         = 8091;

  /** Test sleep interval. */
  public static final int              SLEEP_INTERVAL                   = 1000;

  /** Test message submission timeout in seconds. */
  public static final int              SUBMISSION_TIMEOUT               = 30;

  /** Test message short submission timeout in seconds. */
  public static final int              SHORT_SUBMISSION_TIMEOUT         = 2;

  /** The data bound for a bounded buffered input stream. */
  public static final int              DATA_BOUND                       = 10240;

  /** Test ID. */
  public static final String           ID                               = "ID";

  /** Test certificate. */
  public static final String           IMPORT_CERTIFICATE               = "SurreyPAV";

  /** Test certificate file. */
  public static final String           IMPORT_CERTIFICATE_FILE          = "./testdata/" +IMPORT_CERTIFICATE + ".pem";

  /** Test signed import ID. */
  public static final String           SIGNED_IMPORT_ID                 = "MixServer2";

  /** Test signed import parent certificate. */
  public static final String           SIGNED_IMPORT_CA                 = "MixServerCA.pem";

  /** Test signed import key store prior to import - has private keys. */
  public static final String           SIGNED_IMPORT_BASE_KEY_STORE     = "./testdata/import/keysBeforeImport.jks";

  /** Test signed import folder for certificates. */
  public static final String           SIGNED_IMPORT_DIR                = "./testdata/import/SignedPublicKeyCerts";

  /** Certificate file name. */
  public static final String           CERTIFICATE_FILE                 = "certificate";

  /** Certificate suffix. */
  public static final String           SUFFIX                           = "suffix";

  /** SSL cipher suite. */
  public static final String           CIPHERS                          = "SSL_RSA_WITH_NULL_SHA";

  /** The client key store. */
  public static final String           CLIENT_KEY_STORE                 = "./testdata/ssl.jks";

  /** The client key store password. */
  public static final String           CLIENT_KEY_STORE_PASSWORD        = "";

  /** The singleton instance of these parameters. */
  private static LibraryTestParameters instance                         = null;

  /**
   * Private default constructor to prevent instantiation.
   */
  private LibraryTestParameters() {
    super();

    // Initialise BouncyCastle.
    CryptoUtils.initProvider();
  }

  /**
   * Compares the content of the file with the specified lines.
   * 
   * @param file
   *          The file to check.
   * @param lines
   *          The lines expected in the file in the correct order.
   * @return True if the lines are in the file in the correct order and no more, false otherwise.
   * @throws IOException
   */
  public static boolean compareFile(File file, String[] lines) throws IOException {
    boolean result = true;

    BufferedReader in = null;

    try {
      in = new BufferedReader(new FileReader(file));

      int count = 0;
      String line = in.readLine();

      while ((line != null) && result) {
        if (count >= lines.length) {
          result = false;
        }
        else if (!line.equals(lines[count])) {
          result = false;
        }

        count++;
        line = in.readLine();
      }

      if (count != lines.length) {
        result = false;
      }
    }
    // Throw exceptions up.
    finally {
      if (in != null) {
        in.close();
      }
    }

    return result;
  }

  /**
   * Creates and populates a test file.
   * 
   * @param file
   *          The file to create and populate.
   * @throws IOException
   */
  public static void createTestFile(File file) throws IOException {
    BufferedWriter bw = new BufferedWriter(new FileWriter(file));
    bw.write(LINE);
    bw.close();
  }

  /**
   * Recursive method to delete a folder and its content. If something goes wrong, this method remains silent.
   * 
   * @param file
   *          The file/folder to delete.
   */
  public static void deleteRecursive(File file) {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        deleteRecursive(child);
      }
    }

    file.delete();
  }

  /**
   * Initialises and returns the SSL socket factory needed to create client SSL connections.
   * 
   * @return The SSL socket factory.
   * @throws PeerSSLInitException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   */
  public static SSLSocketFactory getClientSSLSocketFactory() throws PeerSSLInitException, KeyManagementException,
      NoSuchAlgorithmException {
    String pathToKS = CLIENT_KEY_STORE;
    char[] pwd = CLIENT_KEY_STORE_PASSWORD.toCharArray();

    // Initialise the trust store for the client key.
    FileInputStream keyStoreStream = null;
    TrustManagerFactory tmf = null;
    KeyManagerFactory kmf = null;

    try {
      KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
      keyStoreStream = new FileInputStream(pathToKS);
      keyStore.load(keyStoreStream, pwd);

      tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
      tmf.init(keyStore);

      kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
      kmf.init(keyStore, pwd);
    }
    catch (Exception e) {
      throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores", e);
    }
    finally {
      if (keyStoreStream != null) {
        try {
          keyStoreStream.close();
        }
        catch (IOException ie) {
          throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores (Closing File)", ie);
        }
      }
    }

    // Create the client socket factory using the trust store.
    SSLContext context = SSLContext.getInstance("TLS");
    context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

    return context.getSocketFactory();
  }

  /**
   * Creates a buffer from a string with the specified number of lines each of 10 characters long (including newline).
   * 
   * @param lines
   *          The number of lines required.
   * @return The created string.
   */
  public static String getData(int lines) {
    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < lines; i++) {
      buffer.append(FILE_LINE);
    }

    return buffer.toString();
  }

  /**
   * Singleton access method.
   * 
   * @return The test parameters singleton instance.
   */
  public static LibraryTestParameters getInstance() {
    // Lazy creation.
    if (instance == null) {
      instance = new LibraryTestParameters();
    }

    return instance;
  }

  /**
   * Deletes all of the test output files that are generated.
   */
  public static void tidyFiles() {
    deleteRecursive(new File(OUTPUT_FOLDER));
    deleteRecursive(new File(OUTPUT_PLAIN_TEXT_FILE));
    deleteRecursive(new File(OUTPUT_ENCRYPTED_FILE));
    deleteRecursive(new File(OUTPUT_PORT_FILE));
    deleteRecursive(new File(OUTPUT_DATA_FILE));
  }

  /**
   * Wait for the specified number of loops.
   * 
   * @param count
   *          The number of loops.
   * @return
   */
  public static void wait(int count) {
    for (int i = 0; i < count; i++) {
      try {
        // Sleep for the interval.
        Thread.sleep(LibraryTestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }
    }
  }
}
